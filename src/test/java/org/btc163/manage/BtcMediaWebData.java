package org.btc163.manage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.service.model.ExcelData;
import org.btc163.manage.base.service.model.ExcelMediaModel;
import org.btc163.manage.base.service.model.ExportCapitalModel;
import org.btc163.manage.base.service.model.ExportExcel;
import org.btc163.manage.entity.BtcMedia;
import org.btc163.manage.service.BtcMediaService;
import org.btc163.manage.utils.UidUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class BtcMediaWebData {
    private static String URL_WEB = "http://www.bixiaobai.com/index/MediaWeb/getData?pageNo=%s&keyWords=&label=";

    private static final String INFO_URL = "http://www.bixiaobai.com/index/MediaWeb/detail?id=";

    @Autowired
    private BtcMediaService btcMediaService;

    @Test
    public void mediaWebTest() throws Exception {
        List<String> list = mediaDataTest();
        parseWebHtml(list);
    }

    private void parseWebHtml(List<String> plIds) throws Exception {
        List<ExcelMediaModel> excelMediaModels = new ArrayList<>();
        for (String plId : plIds) {
            Document document = Jsoup.connect(INFO_URL + plId).get();
            Elements documents = document.getElementsByClass("XiangQing");
            Element element = documents.get(0);
            //1
            String proLogo = element.getElementsByTag("img").attr("src"); //logo
            File file = new File(proLogo);
            String proName = element.getElementsByTag("p").get(0).text(); //项目名称
            String introduce = element.getElementsByTag("p").get(1).text(); //项目介绍
            //2
            String url = element.getElementsByTag("a").get(0).text();
            ExcelMediaModel excelMediaModel = new ExcelMediaModel();
            excelMediaModel.setMtIntroduce(introduce);
            excelMediaModel.setMtLogo(file.getName());
            excelMediaModel.setMtName(proName);
            excelMediaModel.setMtUrl(url);
            excelMediaModel.setMtType("0");
            excelMediaModels.add(excelMediaModel);
        }

        if (!CollectionUtils.isEmpty(excelMediaModels)) {
            ExcelData<ExcelMediaModel> excelData = new ExcelData<>();
            List<String> titile = Arrays.asList("媒体名称", "媒体logo", "媒体分类", "媒体简介", "网址");
            excelData.setTitles(titile);
            excelData.setName("媒体库WEB");
            excelData.setRows(excelMediaModels);
            ExportExcel.exportExcel(excelData);
        }
    }


    public List<String> mediaDataTest() throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        List<String> plIds = new ArrayList<>();

        for (int i = 1; i < 11; i++) {
            String url = String.format(URL_WEB, i);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Accept", "application/json, text/javascript, */*; q=0.01");
            httpGet.setHeader("Accept-Encoding", "gzip, deflate");
            httpGet.setHeader("Accept-Language", "zh-CN,zh;q=0.9");
            httpGet.setHeader("Connection", "keep-alive");
            httpGet.setHeader("Cookie", "UM_distinctid=165847b6c4117f-0834a01b1d75ee-6114147a-1fa400-165847b6c42904; LiveWSDHT76227276=1535526007111668070056; NDHT76227276fistvisitetime=1535526007133; Hm_lvt_d1f17343b79e7e042f22cb86953b36e7=1535526006,1535527354,1535529101; NDHT76227276IP=%7C124.64.127.79%7C123.118.109.174%7C; PHPSESSID=0t2qvq00fqqe6u87m3m28u1u9j; LiveWSDHT76227276sessionid=1535686900183276169808; NDHT76227276visitecounts=3; CNZZDATA1274136426=898962317-1535522672-null%7C1535691992; NDHT76227276lastvisitetime=1535696906097; NDHT76227276visitepages=65; Hm_lpvt_d1f17343b79e7e042f22cb86953b36e7=1535696906");
            httpGet.setHeader("Host", "www.bixiaobai.com");
            httpGet.setHeader("Referer", "http://www.bixiaobai.com/index/mediaWeb/index");
            httpGet.setHeader("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36");
            httpGet.setHeader("X-Requested-With", "XMLHttpRequest");
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(35000)// 连接主机服务超时时间
                    .setConnectionRequestTimeout(35000)// 请求超时时间
                    .setSocketTimeout(60000)// 数据读取超时时间
                    .build();
            httpGet.setConfig(requestConfig);
            response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);
            JSONObject json = JSON.parseObject(result);
            JSONArray data = json.getJSONArray("data");
            for (Object datum : data) {
                JSONObject jsonObject = (JSONObject) datum;
                String pl_id = jsonObject.getString("web_id");
                plIds.add(pl_id);
            }
        }
        response.close();
        httpClient.close();
        return plIds;
    }

}
