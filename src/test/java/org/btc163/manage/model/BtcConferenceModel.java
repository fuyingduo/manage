package org.btc163.manage.model;

import lombok.Data;
import lombok.ToString;


/**
 * created by fuyd on 2018/9/4
 */
@Data
@ToString
public class BtcConferenceModel {
    private String pmMonth;
    private String pmCityName;
    private String pmDesc;
    private String pmEndTime;
    private String pmName;
    private String pmStartTime;
    private String pmProvinceName;
    // 地点
    private String pmSite;
    // 会议简介 ^
    private String pmBrief;
    // 参会嘉宾 ^
    private String pmHonored;
    // 会议日程 ^
    private String pmSchedule;

}
