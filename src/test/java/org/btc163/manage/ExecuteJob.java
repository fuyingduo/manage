package org.btc163.manage;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.constant.ProStageEnum;
import org.btc163.manage.exception.BusinessException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * created by fuyd on 2018/9/12
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ExecuteJob {

    private static String URL = "http://www.bixiaobai.com/index/projectLibrary/getData?pageNo=%s&keyWords=&stage=&concept=&orderField=&orderBy=";

    private static String JSOUP_URL = "http://www.bixiaobai.com/index/projectLibrary/detail?id=";

    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @Test
    public void job() throws ExecutionException, InterruptedException, IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        List<String> ids = new ArrayList<>();
        for (int i = 1; i < 70; i++) {
            String format = String.format(URL, i);
            OKHttpTests okHttpTests = new OKHttpTests(httpClient, response, format);
            Future submit = taskExecutor.submit(okHttpTests);
            List<String> plids = (List<String>) submit.get();
            ids.addAll(plids);
        }
        Jsoup(ids);
        System.out.println(ids);
    }

    private void Jsoup(List<String> ids) throws IOException {
        if (CollectionUtils.isEmpty(ids)) {
            return;
        }
        for (String id : ids) {
            Document document = Jsoup.connect(JSOUP_URL + id).get();
            Element mainDiv = document.getElementById("main");
            if (mainDiv.childNodeSize() < 1) {
                return;
            }
            Elements mainChildrens = mainDiv.children();
            Element xmDiv = mainChildrens.get(2);
            if (xmDiv.childNodeSize() < 1) {
                return;
            }
            Element countDiv = xmDiv.children().get(0);
            if (countDiv.childNodeSize() < 1) {
                return;
            }
            Element nrDiv = countDiv.children().get(0);
            if (nrDiv.childNodeSize() < 0) {
                return;
            }
            Elements divs = nrDiv.children();
            // 1
            Element div1 = divs.get(0);
            Element a = div1.children().get(0);
            Element pro = a.children().get(1);
            String proName = pro.getElementsByTag("p").text();
            String proBrief = pro.getElementsByClass("jieShao").get(0).text();
            Elements tds = pro.getElementsByTag("td");
            if (tds.size() > 1) {
                StringBuilder stage = new StringBuilder();
                for (int i = 1; i < tds.size(); i++) {
                    String t = tds.get(i).text();
                    try {
                        stage.append(ProStageEnum.messageCodeOf(t) + "^");
                    } catch (BusinessException e) {
                        System.out.println(t);
                    }
                }
                if (stage.length() > 0) {
                    String stageCode = stage.substring(0, stage.length() - 1);
                }
            }
            // 2
            Element div2 = divs.get(1);
            Element table = div2.getElementsByTag("table").get(0);
            Elements trs = table.getElementsByTag("tr");
            if (trs.size() == 6) {
                String tokenNum = trs.get(0).getElementsByTag("td").get(1).text();
                String daibi = trs.get(1).getElementsByTag("td").get(1).text();
                String gainianbankuai = trs.get(2).getElementsByTag("td").get(1).text();
                Element xiangmujieduan = trs.get(3).getElementsByTag("td").get(1);
                if (xiangmujieduan.childNodeSize() > 0) {
                    Element act = xiangmujieduan.getElementsByClass("act").get(0);
                    String text = act.text();
                }
                String gongsijizhi = trs.get(4).getElementsByTag("td").get(1).text();
                String jiaoyisuo = trs.get(5).getElementsByTag("td").get(1).text();
                System.out.println();
            }
            // 3
            Element div3 = divs.get(2);
            Element ul = div3.getElementsByTag("ul").get(0);
            if (ul.childNodeSize() > 0) {
                Elements lis = ul.children();
                for (Element li : lis) {
                    String attr = li.getElementsByTag("img").attr("src");
                    HeadJob headJob = new HeadJob(attr);
                    taskExecutor.execute(headJob);
                    String renWu_mz = li.getElementsByClass("renWu_mz").get(0).text();
                    String renWu_jj = li.getElementsByClass("renWu_jj").get(0).text();
                    System.out.println();
                }
            }
        }

    }
}
