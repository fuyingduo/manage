package org.btc163.manage;

import java.util.concurrent.Callable;

/**
 * created by fuyd on 2018/9/12
 */
public class ThreadDemoTests implements Callable {

    volatile private String call;

    public ThreadDemoTests(String call) {
        this.call = call;
    }

    @Override
    public String call() throws Exception {
        System.out.println(call + "-----------------------------");
        return Math.random() + "";
    }
}
