package org.btc163.manage;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * created by fuyd on 2018/9/12
 */
public class HeadJob implements Runnable {

    volatile private String url;

    public HeadJob(String url) {
        this.url = url;
    }

    @Override
    public void run() {
        saveImage(url);
    }

    private static String saveImage(String url_path) {
        File file = new File(url_path);
        String name = file.getName();
        InputStream inputStream = null;
        HttpURLConnection httpURLConnection = null;
        try {
            java.net.URL url = new URL(url_path);//创建的URL
            if (url != null) {
                httpURLConnection = (HttpURLConnection) url.openConnection();//打开链接
                httpURLConnection.setConnectTimeout(3000);//设置网络链接超时时间，3秒，链接失败后重新链接
                httpURLConnection.setDoInput(true);//打开输入流
                httpURLConnection.setRequestMethod("GET");//表示本次Http请求是GET方式
                int responseCode = httpURLConnection.getResponseCode();//获取返回码
                if (responseCode == 200) {//成功为200
                    //从服务器获得一个输入流
                    inputStream = httpURLConnection.getInputStream();
                    FileOutputStream outStream = new FileOutputStream("/Volumes/fuydWork/image/" + name);
                    byte[] buffer = new byte[1024]; //创建一个Buffer字符串
                    //每次读取的字符串长度，如果为-1，代表全部读取完毕
                    int len = 0;
                    //使用一个输入流从buffer里把数据读取出来
                    while ((len = inputStream.read(buffer)) != -1) {
                        //用输出流往buffer里写入数据，中间参数代表从哪个位置开始读，len代表读取的长度
                        outStream.write(buffer, 0, len);
                    }
                    outStream.close();
                    inputStream.close();
                }
            }
        } catch (MalformedURLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return name;
    }
}
