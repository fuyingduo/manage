package org.btc163.manage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.btc163.manage.entity.BtcCapitalInfo;
import org.btc163.manage.entity.Capital;
import org.btc163.manage.service.BtcCapitalInfoService;
import org.btc163.manage.service.CapitalService;
import org.btc163.manage.utils.UidUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;


@RunWith(SpringRunner.class)
@SpringBootTest
public class ManageApplicationTests {

    @Autowired
    private CapitalService capitalService;

    @Autowired
    private BtcCapitalInfoService btcCapitalInfoService;

    private static String URL = "http://www.bixiaobai.com/index/capitalLibrary/getData?pageNo=%s&keyWords=&label=";

    private static final String INFO_URL = "http://www.bixiaobai.com/index/capitalLibrary/detail?id=";

    @Test
    public void CapitalDataTest() throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        Set<String> cliIds = new HashSet<>();
        for (int i = 7; i < 8; i++) {
            String url = String.format(URL, i);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Accept", "application/json, text/javascript, */*; q=0.01");
            httpGet.setHeader("Accept-Encoding", "gzip, deflate");
            httpGet.setHeader("Accept-Language", "zh-CN,zh;q=0.9");
            httpGet.setHeader("Connection", "keep-alive");
            httpGet.setHeader("Cookie", "UM_distinctid=165855bba1b0-09c0900daa57e-34627908-1fa400-165855bba1d370; LiveWSDHT76227276=1535540714302830521278; NDHT76227276fistvisitetime=1535540714317; NDHT76227276IP=%7C123.118.109.174%7C; PHPSESSID=gndnnqateko749h1quhs7olbfa; CNZZDATA1274136426=1483721189-1535538329-http%253A%252F%252Fwww.bixiaobai.com%252F%7C1535591652; LiveWSDHT76227276sessionid=1535594936573308273668; NDHT76227276visitecounts=2; Hm_lvt_d1f17343b79e7e042f22cb86953b36e7=1535540708,1535594937; Hm_lpvt_d1f17343b79e7e042f22cb86953b36e7=1535594951; NDHT76227276LR_cookie_t0=1; NDHT76227276lastvisitetime=1535594951367; NDHT76227276visitepages=12");
            httpGet.setHeader("Host", "www.bixiaobai.com");
            httpGet.setHeader("Referer", "http://www.bixiaobai.com/index/capitalLibrary/index");
            httpGet.setHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
            httpGet.setHeader("X-Requested-With", "XMLHttpRequest");
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(35000)// 连接主机服务超时时间
                    .setConnectionRequestTimeout(35000)// 请求超时时间
                    .setSocketTimeout(60000)// 数据读取超时时间
                    .build();
            httpGet.setConfig(requestConfig);
            response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);
            System.out.println(result);
            JSONObject json = JSON.parseObject(result);
            JSONArray datas = json.getJSONArray("data");
            for (Object data : datas) {
                JSONObject j = (JSONObject) data;
                String cl_id = j.getString("cl_id");
                cliIds.add(cl_id);
            }

        }
        response.close();
        httpClient.close();
        System.out.println(cliIds);
        for (String cliId : cliIds) {
            Document document = Jsoup.connect("http://www.bixiaobai.com/index/capitalLibrary/detail?id=" + cliId).get();
            Elements nrs = document.getElementsByClass("nr");
            Element nr = nrs.get(0);
            Elements divs = nr.children();
            Element title = divs.get(0);
            Element brief = divs.get(1);
            Element partners = divs.get(2);
            Element investment = divs.get(3);
            String head = title.getElementsByTag("img").attr("src");
            String name = saveImage(head);
            String caName = title.getElementsByTag("h1").text();
            Elements biaoqian = title.getElementsByClass("biaoQian");
            String label = biaoqian.get(0).getElementsByTag("span").text();
            String time = title.getElementsByClass("time").text();
            // 简介
            Elements wenZhangs = brief.getElementsByTag("div");
            String html = wenZhangs.get(0).text();
            if (!StringUtils.isEmpty(html)) {
                html = html.replace("简介", "").trim();
            }
            Elements heHuos = partners.getElementsByTag("li");

            Capital capital = new Capital();
            capital.setCaName(caName);
            capital.setCaEstablish(time);
            capital.setCaLogo(name);
            capital.setCaLab(label);
            final String caId = UidUtil.getId();
            capital.setId(caId);
            capital.setUpdate(new Date());
            capital.setCaStatistics(0L);
            capital.setCaPraise(0);
            capital.setCaBrief(html);
            Element div = investment.getElementsByTag("div").get(0);
            String projects = null;
            if (div.getElementsByTag("span").size() > 0) {
                projects = div.getElementsByTag("span").get(0).text();
            } else if (div.getElementsByTag("a").size() > 0) {
                projects = div.getElementsByTag("a").text();
            } else if (div.getElementsByTag("p").size() > 1) {
                projects = div.getElementsByTag("p").get(1).text();
            }
            capital.setCaProject(projects);
            capitalService.insert(capital);
            for (Element heHuo : heHuos) {
                String head_1 = heHuo.getElementsByTag("img").attr("src");
                String headName = saveImage(head_1);
                Elements ps = heHuo.getElementsByTag("p");
                String irname = ps.get(0).text();
                String introduce = ps.get(1).text();
                BtcCapitalInfo btcCapitalInfo = new BtcCapitalInfo();
                btcCapitalInfo.setId(UidUtil.getId());
                btcCapitalInfo.setCaId(caId);
                btcCapitalInfo.setInHeadImage(headName);
                btcCapitalInfo.setInUserName(irname);
                btcCapitalInfo.setInBrief(introduce);
                btcCapitalInfoService.insert(btcCapitalInfo);
            }
        }
    }

    private static String saveImage(String url_path) {
        File file = new File(url_path);
        String name = file.getName();
//        InputStream inputStream = null;
//        HttpURLConnection httpURLConnection = null;
//        try {
//            java.net.URL url = new URL(url_path);//创建的URL
//            if (url != null) {
//                httpURLConnection = (HttpURLConnection) url.openConnection();//打开链接
//                httpURLConnection.setConnectTimeout(3000);//设置网络链接超时时间，3秒，链接失败后重新链接
//                httpURLConnection.setDoInput(true);//打开输入流
//                httpURLConnection.setRequestMethod("GET");//表示本次Http请求是GET方式
//                int responseCode = httpURLConnection.getResponseCode();//获取返回码
//                if (responseCode == 200) {//成功为200
//                    //从服务器获得一个输入流
//                    inputStream = httpURLConnection.getInputStream();
//                    FileOutputStream outStream = new FileOutputStream("/Volumes/fuydWork/image/" + name);
//                    byte[] buffer = new byte[1024]; //创建一个Buffer字符串
//                    //每次读取的字符串长度，如果为-1，代表全部读取完毕
//                    int len = 0;
//                    //使用一个输入流从buffer里把数据读取出来
//                    while ((len = inputStream.read(buffer)) != -1) {
//                        //用输出流往buffer里写入数据，中间参数代表从哪个位置开始读，len代表读取的长度
//                        outStream.write(buffer, 0, len);
//                    }
//                    outStream.close();
//                    inputStream.close();
//                }
//            }
//        } catch (MalformedURLException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        return name;
    }

}
