package org.btc163.manage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.service.model.ExcelData;
import org.btc163.manage.base.service.model.ExportCapitalModel;
import org.btc163.manage.base.service.model.ExportExcel;
import org.btc163.manage.entity.BtcCapitalInfo;
import org.btc163.manage.entity.Capital;
import org.btc163.manage.service.BtcCapitalInfoService;
import org.btc163.manage.service.CapitalService;
import org.btc163.manage.utils.UidUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
public class CapitalExcelTests {

	@Autowired
	private CapitalService capitalService;

	@Autowired
	private BtcCapitalInfoService btcCapitalInfoService;

    private static String URL = "http://www.bixiaobai.com/index/capitalLibrary/getData?pageNo=%s&keyWords=&label=";

    private static final String INFO_URL = "http://www.bixiaobai.com/index/capitalLibrary/detail?id=";

    @Test
    public void CapitalDataTest() throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        List<String> cliIds = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            String url = String.format(URL, i);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Accept", "application/json, text/javascript, */*; q=0.01");
            httpGet.setHeader("Accept-Encoding", "gzip, deflate");
            httpGet.setHeader("Accept-Language", "zh-CN,zh;q=0.9");
            httpGet.setHeader("Connection", "keep-alive");
            httpGet.setHeader("Cookie", "UM_distinctid=165855bba1b0-09c0900daa57e-34627908-1fa400-165855bba1d370; LiveWSDHT76227276=1535540714302830521278; NDHT76227276fistvisitetime=1535540714317; NDHT76227276IP=%7C123.118.109.174%7C; PHPSESSID=gndnnqateko749h1quhs7olbfa; CNZZDATA1274136426=1483721189-1535538329-http%253A%252F%252Fwww.bixiaobai.com%252F%7C1535591652; LiveWSDHT76227276sessionid=1535594936573308273668; NDHT76227276visitecounts=2; Hm_lvt_d1f17343b79e7e042f22cb86953b36e7=1535540708,1535594937; Hm_lpvt_d1f17343b79e7e042f22cb86953b36e7=1535594951; NDHT76227276LR_cookie_t0=1; NDHT76227276lastvisitetime=1535594951367; NDHT76227276visitepages=12");
            httpGet.setHeader("Host", "www.bixiaobai.com");
            httpGet.setHeader("Referer", "http://www.bixiaobai.com/index/capitalLibrary/index");
            httpGet.setHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
            httpGet.setHeader("X-Requested-With", "XMLHttpRequest");
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(35000)// 连接主机服务超时时间
                    .setConnectionRequestTimeout(35000)// 请求超时时间
                    .setSocketTimeout(60000)// 数据读取超时时间
                    .build();
            httpGet.setConfig(requestConfig);
            response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);
            JSONObject json = JSON.parseObject(result);
            JSONArray datas = json.getJSONArray("data");
            for (Object data : datas) {
                JSONObject j = (JSONObject) data;
                String cl_id = j.getString("cl_id");
                cliIds.add(cl_id);
                System.out.println(cl_id);
            }

        }
        response.close();
        httpClient.close();
        List<ExportCapitalModel> exportCapitalModels = new ArrayList<>();
        for (String cliId : cliIds) {
            Document document = Jsoup.connect("http://www.bixiaobai.com/index/capitalLibrary/detail?id=" + cliId).get();
            Elements nrs = document.getElementsByClass("nr");
            Element nr = nrs.get(0);
            Elements divs = nr.children();
            Element title = divs.get(0);
            Element brief = divs.get(1);
            Element partners = divs.get(2);
            Element investment = divs.get(3);
            String head = title.getElementsByTag("img").attr("src");
            File f = new File(head);
            String caName = title.getElementsByTag("h1").text();
            Elements biaoqian = title.getElementsByClass("biaoQian");
            String label = biaoqian.get(0).getElementsByTag("span").text();
            String time = title.getElementsByClass("time").text();
            // 简介
            Elements wenZhangs = brief.getElementsByTag("div");
            String html = wenZhangs.get(0).text();
            ExportCapitalModel exportCapitalModel = new ExportCapitalModel();
            exportCapitalModel.setCaName(caName);
            exportCapitalModel.setCaLab(label);
            exportCapitalModel.setCaLogo(f.getName());
            exportCapitalModel.setCaEstablish(time);
            exportCapitalModel.setCaBrief(html);
            StringBuilder nameBuilder = new StringBuilder();
            StringBuilder imageBuilder = new StringBuilder();
            StringBuilder briefBuilder = new StringBuilder();

            Elements uls = partners.getElementsByTag("ul");
            Element ul = uls.get(0);
            Elements heHuos = ul.getElementsByTag("li");
            for (Element heHuo : heHuos) {
                String head_1 = heHuo.getElementsByTag("img").attr("src");
                File file = new File(head_1);
                Elements ps = heHuo.getElementsByTag("p");
                String irname = ps.get(0).text();
                String introduce = ps.get(1).text();
                nameBuilder.append(irname + "|");
                imageBuilder.append(file.getName() + "|");
                briefBuilder.append(introduce + "|");
            }
            if (!StringUtils.isEmpty(nameBuilder)) {
                String names = nameBuilder.substring(0, nameBuilder.length() - 1);
                exportCapitalModel.setPartnerNameAndPosition(names);
            }
            if (!StringUtils.isEmpty(imageBuilder)) {
                String image = imageBuilder.substring(0, imageBuilder.length() - 1);
                exportCapitalModel.setPartnerImage(image);
            }
            if (!StringUtils.isEmpty(briefBuilder)) {
                String briefs = briefBuilder.substring(0, briefBuilder.length() - 1);
                exportCapitalModel.setPartnerBrief(briefs);
            }
            Elements spans = investment.children();
            String projects = spans.get(1).text();
            exportCapitalModel.setProjects(projects);
            exportCapitalModels.add(exportCapitalModel);
        }
        if (!CollectionUtils.isEmpty(exportCapitalModels)) {
            ExcelData<ExportCapitalModel> excelData = new ExcelData<>();
            List<String> titile = Arrays.asList("资本名称", "标签", "logo", "成立时间", "简介", "合伙人（姓名-职位）", "合伙人（简介）", "头像","参投项目");
            excelData.setTitles(titile);
            excelData.setName("资本库");
            excelData.setRows(exportCapitalModels);
            ExportExcel.exportExcel(excelData);
        }
    }


}
