package org.btc163.manage;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.entity.BtcConference;
import org.btc163.manage.entity.BtcMedia;
import org.btc163.manage.entity.BtcMtPm;
import org.btc163.manage.service.BtcConferenceService;
import org.btc163.manage.service.BtcMediaService;
import org.btc163.manage.service.BtcMtPmService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * created by fuyd on 2018/9/4
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BtcConferenceInfoTests {

    private static String URL_WEB = "http://www.bixiaobai.com/index/projectMeeting/getData?pageNo=%s&keyWords=&pm_province_id=&pm_city_id=";

    @Autowired
    private BtcConferenceService btcConferenceService;

    @Autowired
    private BtcMediaService btcMediaService;

    @Autowired
    private BtcMtPmService btcMtPmService;

    @Test
    public void ConferenceData() throws IOException {
        BtcConference btcConference = new BtcConference();
        EntityWrapper<BtcConference> entityWrapper = new EntityWrapper<>(btcConference);
        List<BtcConference> btcConferences = btcConferenceService.selectList(entityWrapper);
        if (CollectionUtils.isEmpty(btcConferences)) {
            return;
        }
        for (BtcConference conference : btcConferences) {
            final String mpId = conference.getId();
            Document document = Jsoup.connect("http://www.bixiaobai.com/index/projectMeeting/detail?id=" + mpId).get();
            Elements nrs = document.getElementsByClass("nr");
            Element nr = nrs.get(0);
            // 0 会议名称 1 会议地点 2 会议事件 3 会议简介  4 参会嘉宾 5 会议日程 6 支持媒体
            Elements childrens = nr.children();
            String siteDiv = childrens.get(1).getElementsByTag("div").get(0).text();   ///会议地点

            conference.setPmSite(siteDiv);
            Element briefDiv = childrens.get(3).getElementsByTag("div").get(0);
            Elements imgs = briefDiv.getElementsByTag("img");
            if (imgs.size() > 0) {
                StringBuilder briefBuilder = new StringBuilder();
                for (Element img : imgs) {
                    String png = img.attr("src");
                    if (png.indexOf("?") > 0) {
                        png = png.split("\\?")[0];
                    }
                    String pngName = saveImage(png);
                    briefBuilder.append(pngName + "^");
                }
                String brief = briefBuilder.substring(0, briefBuilder.length() - 1);   /// 简介
                conference.setPmBrief(brief);
            }
            Element honoredDiv = childrens.get(4).getElementsByTag("div").get(0);
            Elements honoredImgs = honoredDiv.getElementsByTag("img");
            if (honoredImgs.size() > 0) {
                StringBuilder honoredBuilder = new StringBuilder();
                for (Element honoredImg : honoredImgs) {
                    String png = honoredImg.attr("src");
                    String pngName = saveImage(png);
                    honoredBuilder.append(pngName + "^");
                }
                String honored = honoredBuilder.substring(0, honoredBuilder.length() - 1);  /// 嘉宾
                conference.setPmHonored(honored);
            }
            Element scheduleDiv = childrens.get(5).getElementsByTag("div").get(0);
            Elements scheduleImgs = scheduleDiv.getElementsByTag("img");
            if (scheduleImgs.size() > 0) {
                StringBuilder scheduleBuilder = new StringBuilder();
                for (Element scheduleImg : scheduleImgs) {
                    String png = scheduleImg.attr("src");
                    String pngName = saveImage(png);
                    scheduleBuilder.append(pngName + "^");
                }
                String schedule = scheduleBuilder.substring(0, scheduleBuilder.length() - 1); // 日程
                conference.setPmSchedule(schedule);
            }
            btcConferenceService.updateById(conference);
            Element mediaDiv = childrens.get(6).getElementsByTag("div").get(0);
            Elements as = mediaDiv.getElementsByTag("a");
            if (as.size() > 0) {
                for (Element a : as) {
                    String mediaImg = a.getElementsByTag("img").get(0).attr("src");
                    String pngName = saveImage(mediaImg);
                    String mediaName = a.getElementsByTag("p").get(0).text();
                    Map<String, Object> param = new HashMap<>();
                    param.put("mt_name", mediaName.trim());
                    List<BtcMedia> mts = btcMediaService.selectByMap(param);
                    if (!CollectionUtils.isEmpty(mts)) {
                        BtcMedia btcMedia = mts.get(0);
                        BtcMtPm btcMtPm = new BtcMtPm();
                        btcMtPm.setMtId(btcMedia.getId());
                        btcMtPm.setPmId(mpId);
                        btcMtPmService.insert(btcMtPm);
                    }
                }
            }
        }
    }

    private static String saveImage(String url_path) {
        File file = new File(url_path);
        String name = file.getName();
//        InputStream inputStream = null;
//        HttpURLConnection httpURLConnection = null;
//        try {
//            java.net.URL url = new URL(url_path);//创建的URL
//            if (url != null) {
//                httpURLConnection = (HttpURLConnection) url.openConnection();//打开链接
//                httpURLConnection.setConnectTimeout(3000);//设置网络链接超时时间，3秒，链接失败后重新链接
//                httpURLConnection.setDoInput(true);//打开输入流
//                httpURLConnection.setRequestMethod("GET");//表示本次Http请求是GET方式
//                int responseCode = httpURLConnection.getResponseCode();//获取返回码
//                if (responseCode == 200) {//成功为200
//                    //从服务器获得一个输入流
//                    inputStream = httpURLConnection.getInputStream();
//                    FileOutputStream outStream = new FileOutputStream("/Volumes/fuydWork/image/" + name);
//                    byte[] buffer = new byte[1024]; //创建一个Buffer字符串
//                    //每次读取的字符串长度，如果为-1，代表全部读取完毕
//                    int len = 0;
//                    //使用一个输入流从buffer里把数据读取出来
//                    while ((len = inputStream.read(buffer)) != -1) {
//                        //用输出流往buffer里写入数据，中间参数代表从哪个位置开始读，len代表读取的长度
//                        outStream.write(buffer, 0, len);
//                    }
//                    outStream.close();
//                    inputStream.close();
//                }
//            }
//        } catch (MalformedURLException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        return name;
    }
}
