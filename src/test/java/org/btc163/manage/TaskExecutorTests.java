package org.btc163.manage;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * created by fuyd on 2018/9/12
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class TaskExecutorTests {

    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @Test
    public void test() throws ExecutionException, InterruptedException {
        ThreadDemoTests job = new ThreadDemoTests("sad");
        Future submit = taskExecutor.submit(job);
        String o = (String) submit.get();
        System.out.println(o + "[]----------------------");
    }
}
