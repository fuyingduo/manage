package org.btc163.manage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.service.model.ExcelData;
import org.btc163.manage.base.service.model.ExportCapitalModel;
import org.btc163.manage.base.service.model.ExportExcel;
import org.btc163.manage.base.service.model.ExportProjectModel;
import org.btc163.manage.constant.ProStageEnum;
import org.btc163.manage.constant.ReLockUpEnum;
import org.btc163.manage.entity.*;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.*;
import org.btc163.manage.utils.UidUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * created by fuyd on 2018/8/30
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class ProjectExcelTests {

    private static String URL_ = "http://www.bixiaobai.com/index/projectLibrary/getData?pageNo=%s&keyWords=&stage=&concept=&orderField=&orderBy=";

    private static final String INFO_URL = "http://www.bixiaobai.com/index/projectLibrary/detail?id=";

    @Autowired
    private TepConceptService tepConceptService;

    @Autowired
    private CapitalService capitalService;

    @Autowired
    private ProTepService proTepService;

    @Autowired
    private BtcProjectInfoService btcProjectInfoService;

    @Autowired
    private ProCaService proCaService;

    @Autowired
    private ProjectPathService projectPathService;

    @Autowired
    private ProjectDynamicService projectDynamicService;

    @Autowired
    private BtcProjectService btcProjectService;

    @Test
    public void projectDataTest() throws Exception {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        List<String> plIds = new ArrayList<>();
        // 63
        for (int i = 1; i < 63; i++) {
            String url = String.format(URL_, i);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Accept", "application/json, text/javascript, */*; q=0.01");
            httpGet.setHeader("Accept-Encoding", "gzip, deflate");
            httpGet.setHeader("Accept-Language", "zh-CN,zh;q=0.9");
            httpGet.setHeader("Connection", "keep-alive");
            httpGet.setHeader("Cookie", "UM_distinctid=165855bba1b0-09c0900daa57e-34627908-1fa400-165855bba1d370; LiveWSDHT76227276=1535540714302830521278; NDHT76227276fistvisitetime=1535540714317; NDHT76227276IP=%7C123.118.109.174%7C; PHPSESSID=gndnnqateko749h1quhs7olbfa; CNZZDATA1274136426=1483721189-1535538329-http%253A%252F%252Fwww.bixiaobai.com%252F%7C1535591652; LiveWSDHT76227276sessionid=1535594936573308273668; NDHT76227276visitecounts=2; Hm_lvt_d1f17343b79e7e042f22cb86953b36e7=1535540708,1535594937; Hm_lpvt_d1f17343b79e7e042f22cb86953b36e7=1535594951; NDHT76227276LR_cookie_t0=1; NDHT76227276lastvisitetime=1535594951367; NDHT76227276visitepages=12");
            httpGet.setHeader("Host", "www.bixiaobai.com");
            httpGet.setHeader("Referer", "http://www.bixiaobai.com/index/capitalLibrary/index");
            httpGet.setHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
            httpGet.setHeader("X-Requested-With", "XMLHttpRequest");
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(35000)// 连接主机服务超时时间
                    .setConnectionRequestTimeout(35000)// 请求超时时间
                    .setSocketTimeout(60000)// 数据读取超时时间
                    .build();
            httpGet.setConfig(requestConfig);
            response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);
            JSONObject json = JSON.parseObject(result);
            JSONArray data = json.getJSONArray("data");
            for (Object datum : data) {
                JSONObject jsonObject = (JSONObject) datum;
                String pl_id = jsonObject.getString("pl_id");
                plIds.add(pl_id);
            }
        }
        response.close();
        httpClient.close();
        List<ExportProjectModel> exportProjectModels = new ArrayList<>();
        for (String plId : plIds) {
            ExportProjectModel exportProjectModel = new ExportProjectModel();
            Document document = Jsoup.connect(INFO_URL + plId).get();
            Elements documents = document.getElementsByClass("nr");
            final String proId = UidUtil.getId();
            Element doc = documents.get(0);
            Elements divs = doc.children();
            Element pro_1 = divs.get(0);
            Element pro_2 = divs.get(1);
            Element pro_3 = divs.get(2);

            // 1.
            String proLogo = pro_1.getElementsByTag("img").attr("src"); //项目logo
            File file = new File(proLogo);
            Element content = pro_1.getElementsByTag("div").get(0);
            String proName = content.getElementsByTag("p").get(0).text(); //项目名称
            Elements tds = content.getElementsByTag("td");
            String proStage = tds.get(1).text(); // 项目阶段
            Elements jieShao = content.getElementsByClass("jieShao");
            String brief = jieShao.get(0).text(); // 项目介绍

            exportProjectModel.setProName(proName);
            exportProjectModel.setProLogo(file.getName());
            if (!StringUtils.isEmpty(proStage)) {
                Integer proStageCode = null;
                try {
                    proStageCode = ProStageEnum.messageCodeOf(proStage);
                } catch (BusinessException e) {
                }
                exportProjectModel.setProStage(proStageCode);
            }
            exportProjectModel.setProIntroduce(brief);
            // 2.
            Element tbody = pro_2.getElementsByTag("tbody").get(0);
            Elements trs = tbody.children();
            Element rt_0 = trs.get(0);
            Element rt_1 = trs.get(1);
            Element rt_2 = trs.get(2);
            Element rt_4 = trs.get(4);
            Element rt_5 = trs.get(5);
            String tokenNum = rt_0.children().get(1).text(); // token总量
            String distributionOfTokens = rt_1.children().get(1).text(); // 代币分配
            Element td = rt_2.children().get(1);
            Elements plates = td.children();
            StringBuilder tepBuilder = new StringBuilder();
            for (Element plate : plates) {
                tepBuilder.append(plate.text() + "|");
            }
            exportProjectModel.setTepId(tepBuilder.toString());
            String consensus = rt_4.children().get(1).text(); // 共识机制
            Elements spans = rt_5.children().get(1).getElementsByTag("span");
            StringBuilder exchanges = new StringBuilder();
            for (Element span : spans) {
                exchanges.append(span.text() + ",");
            }
            String exchange = null;                 // 交易所
            if (!StringUtils.isEmpty(exchanges)) {
                exchange = exchanges.substring(0, exchanges.length() - 1);
            }

            exportProjectModel.setProTokenNum(tokenNum);
            exportProjectModel.setProTokenAllot(distributionOfTokens);
            exportProjectModel.setProConsesusMechanism(consensus);
            exportProjectModel.setExchange(exchange);
            // 3.
            Element uls = pro_3.getElementsByTag("ul").get(0); // 核心成员
            Elements members = uls.children();
            if (members.size() > 0) {
                StringBuilder nameBuilder = new StringBuilder();
                StringBuilder imageBuilder = new StringBuilder();
                StringBuilder briefBuilder = new StringBuilder();
                for (Element member : members) {
                    String head = member.getElementsByTag("img").attr("src"); // 头像
                    File file1 = new File(head);
                    Elements names = member.getElementsByTag("div");
                    String name = names.get(0).text(); // 姓名职位
                    String briefs = names.get(1).text(); // 简介
                    nameBuilder.append(name + "|");
                    imageBuilder.append(file1.getName() + "|");
                    briefBuilder.append(briefs + "|");
                }
                exportProjectModel.setJobName(nameBuilder.toString());
                exportProjectModel.setHeadImage(imageBuilder.toString());
                exportProjectModel.setBriefs(briefBuilder.toString());
            }

            Element huoBan = pro_3.getElementsByClass("huoBan").get(0); // 合作伙伴
            String caNam = huoBan.text();
            exportProjectModel.setCaNames(caNam);

            Element luJing = pro_3.getElementsByClass("luJing").get(0); // 项目路径
            Elements luJingDivs = luJing.children();
            StringBuilder pathBuilder = new StringBuilder();
            for (Element luJingDiv : luJingDivs) {
                String pathName = luJingDiv.text(); // 路径名称
                pathBuilder.append(pathName + "|");
            }
            exportProjectModel.setPathNames(pathBuilder.toString());

            Elements elses = pro_3.getElementsByClass("nr2");
            Element zhaoMu = elses.get(1); // 招募资料
            Element asd = elses.get(1);
            Element dongTai = elses.get(3); // 相关动态
            Element lianXi = elses.get(4); // 联系我们

            // (1
            Elements zmTrs = zhaoMu.getElementsByTag("tr");
            String tokenTotal = zmTrs.get(0).children().get(1).text(); // token募集总量
            String time = zmTrs.get(1).children().get(1).text(); //招募时间
            String conversion = zmTrs.get(2).children().get(1).text(); // 兑换比例
            String placement = zmTrs.get(3).children().get(1).text(); // 募集资金分配
            String hardTop = zmTrs.get(4).children().get(1).text(); // 硬顶
            String locked = zmTrs.get(5).children().get(1).text(); // 锁仓
            String remark = zmTrs.get(6).children().get(1).text(); // 备注

            if (!StringUtils.isEmpty(tokenTotal)) {
                if (!tokenTotal.equals("暂无")) {
                    exportProjectModel.setRaiseTokenSum(tokenTotal);
                }
            }
            exportProjectModel.setReTime(time);
            exportProjectModel.setReRatio(conversion);
            exportProjectModel.setReHardTop(hardTop);
            exportProjectModel.setReCapitalAllot(placement);
            exportProjectModel.setReLockUp(locked);
            exportProjectModel.setReRemake(remark);

            // （2
            Elements dt = dongTai.getElementsByTag("div"); // 项目动态
            Element dongtai = dt.get(2);
            Elements as = dongtai.getElementsByTag("a");
            StringBuilder dyBuilder = new StringBuilder();
            StringBuilder titleBuilder = new StringBuilder();
            for (Element a : as) {
                String url = a.attr("href"); // url
                String contents = a.text(); // 内容
                dyBuilder.append(url + "|");
                titleBuilder.append(contents + "|");
            }
            exportProjectModel.setDyTitle(titleBuilder.toString());
            exportProjectModel.setDyUrl(dyBuilder.toString());

            // (3
            Elements lianxi = lianXi.getElementsByTag("tr");
            Element gw = lianxi.get(0).getElementsByTag("a").get(0);
            Element llq = lianxi.get(1).getElementsByTag("a").get(0);
            Element bps = lianxi.get(2).getElementsByTag("a").get(0);
            Element qb = lianxi.get(3).getElementsByTag("a").get(0);
            Element wx = lianxi.get(4).getElementsByTag("a").get(0);

            exportProjectModel.setCoOfficial(gw.attr("href"));
            exportProjectModel.setCoBrowser(llq.attr("href"));
            exportProjectModel.setCoWhite(bps.attr("href"));
            exportProjectModel.setCoWallet(qb.attr("href"));
            exportProjectModel.setCoWechat(wx.attr("href"));
            exportProjectModels.add(exportProjectModel);
        }

        if (!CollectionUtils.isEmpty(exportProjectModels)) {
            ExcelData<ExportProjectModel> excelData = new ExcelData<>();
            List<String> titile = Arrays.asList("项目名称", "logo", "token总量", "项目阶段", "共识机制", "代币分配", "项目介绍", "招募时间", "兑换比例", "硬顶",
                    "锁仓", "募集资金总数", "募集资金分配", "备注", "官网", "区块链浏览器", "白皮书", "钱包", "微信", "概念模版", "交易所", "姓名-职位", "简介", "头像"
                    , "合作伙伴", "项目路径", "项目动态URL", "项目动态title");
            excelData.setTitles(titile);
            excelData.setName("项目库");
            excelData.setRows(exportProjectModels);
            ExportExcel.exportExcel(excelData);
        }

    }
}
