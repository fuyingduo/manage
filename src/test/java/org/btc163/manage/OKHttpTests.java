package org.btc163.manage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * created by fuyd on 2018/9/12
 */
public class OKHttpTests implements Callable {

    volatile private List<String> plIds = new ArrayList<>();

    volatile private CloseableHttpClient httpClient;
    volatile private CloseableHttpResponse response;
    volatile private String url;

    public OKHttpTests(CloseableHttpClient httpClient, CloseableHttpResponse response, String url) {
        this.httpClient = httpClient;
        this.response = response;
        this.url = url;
    }

    @Override
    public List<String> call() {
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Accept", "application/json, text/javascript, */*; q=0.01");
        httpGet.setHeader("Accept-Encoding", "gzip, deflate");
        httpGet.setHeader("Accept-Language", "zh-CN,zh;q=0.9");
        httpGet.setHeader("Connection", "keep-alive");
        httpGet.setHeader("Cookie", "UM_distinctid=165c271971128a-02f2c470945f56-34627908-1fa400-165c271971268; LiveWSDHT76227276=cae0fb5a75b047d293edae97f3e52ce5; NDHT76227276fistvisitetime=1536565549224; NDHT76227276IP=%7C61.48.144.22%7C; PHPSESSID=af8u6pappsd3sflvtj6d3fhm3q; CNZZDATA1274136426=1454398220-1536561680-null%7C1536731495; Hm_lvt_d1f17343b79e7e042f22cb86953b36e7=1536565549,1536734156; LiveWSDHT76227276sessionid=2bbddce6e8a346ae8424bc4a3308d59f; NDHT76227276visitecounts=2; NDHT76227276lastinvite=1536734176198; NDHT76227276LR_check_data=4%7C1536734176252%7C%7C%7C; Hm_lpvt_d1f17343b79e7e042f22cb86953b36e7=1536734178; NDHT76227276LR_cookie_t0=1; NDHT76227276lastvisitetime=1536734178350; NDHT76227276visitepages=9");
        httpGet.setHeader("Host", "www.bixiaobai.com");
        httpGet.setHeader("Referer", "http://www.bixiaobai.com/index/capitalLibrary/index");
        httpGet.setHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
        httpGet.setHeader("X-Requested-With", "XMLHttpRequest");
        RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(35000)// 连接主机服务超时时间
                .setConnectionRequestTimeout(35000)// 请求超时时间
                .setSocketTimeout(60000)// 数据读取超时时间
                .build();
        httpGet.setConfig(requestConfig);
        try {
            response = httpClient.execute(httpGet);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity entity = response.getEntity();
        String result = null;
        try {
            result = EntityUtils.toString(entity);
        } catch (IOException e) {
            e.printStackTrace();
        }
        JSONObject json = JSON.parseObject(result);
        JSONArray data = json.getJSONArray("data");
        if (data != null && data.size() > 0) {
            for (Object datum : data) {
                JSONObject jsonObject = (JSONObject) datum;
                plIds.add(jsonObject.getString("pl_id"));
            }
        }
        return plIds;
    }

}
