package org.btc163.manage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.btc163.manage.entity.BtcMedia;
import org.btc163.manage.service.BtcMediaJob;
import org.btc163.manage.service.BtcMediaService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.net.URLEncoder;
import java.util.List;

/**
 * created by fuyd on 2018/9/13
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class WBTests {

    @Autowired
    private BtcMediaService btcMediaService;

    @Autowired
    private ThreadPoolTaskExecutor taskExecutor;

    @Test
    public void mediaWbTests() throws IOException {
        BtcMedia btcMedia = new BtcMedia();
        btcMedia.setMtType(1);
        btcMedia.setMtStatus(0);
        EntityWrapper<BtcMedia> entityWrapper = new EntityWrapper<>(btcMedia);
        List<BtcMedia> btcMedias = btcMediaService.selectList(entityWrapper);
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        for (BtcMedia media : btcMedias) {
            if (!StringUtils.isEmpty(media.getMtName())) {
                String encode = URLEncoder.encode(media.getMtName(), "utf-8");
                String URL = "https://m.weibo.cn/api/container/getIndex?containerid=100103type%3D3%26q%3D" + encode + "%26t%3D0&page_type=searchall";
                System.out.println(URL);
                HttpGet httpGet = new HttpGet(URL);
                RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(35000)// 连接主机服务超时时间
                        .setConnectionRequestTimeout(35000)// 请求超时时间
                        .setSocketTimeout(60000)// 数据读取超时时间
                        .build();
                httpGet.setConfig(requestConfig);
                response = httpClient.execute(httpGet);
                HttpEntity entity = response.getEntity();
                String result = EntityUtils.toString(entity, "UTF-8");
                JSONObject json = JSON.parseObject(result);
                JSONObject data = json.getJSONObject("data");
                JSONArray cards = data.getJSONArray("cards");
                if (cards == null || cards.size() == 0) {
                    continue;
                }
                System.out.println(media.getMtName());
                JSONObject jsonObject = cards.getJSONObject(1);
                JSONArray card_group = jsonObject.getJSONArray("card_group");
                JSONObject jsonObject1 = card_group.getJSONObject(0);
                JSONObject user = jsonObject1.getJSONObject("user");
                if (user != null) {
                    System.out.println(user.getString("screen_name"));
                    String id = user.getString("id");
                    System.out.println("https://m.weibo.cn/profile/" + id);
                    media.setMtUrl("https://m.weibo.cn/profile/" + id);
                    BtcMediaJob btcMediaJob = new BtcMediaJob(media, btcMediaService);
                    taskExecutor.execute(btcMediaJob);
                } else {
                    System.out.println("------------------------------------------");
                }

            }
        }
        response.close();
        httpClient.close();
    }

}
