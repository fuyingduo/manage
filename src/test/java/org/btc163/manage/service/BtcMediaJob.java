package org.btc163.manage.service;

import org.btc163.manage.entity.BtcMedia;

/**
 * created by fuyd on 2018/9/13
 */
public class BtcMediaJob implements Runnable {

    volatile private BtcMedia btcMedia;

    private BtcMediaService btcMediaService;

    public BtcMediaJob(BtcMedia btcMedia, BtcMediaService btcMediaService) {
        this.btcMedia = btcMedia;
        this.btcMediaService = btcMediaService;
    }

    @Override
    public void run() {
        if (!btcMediaService.updateById(btcMedia)) {
            System.out.println("-------------------------------" + btcMedia.toString());
        }
    }
}
