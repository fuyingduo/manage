package org.btc163.manage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.constant.ProStageEnum;
import org.btc163.manage.constant.ReLockUpEnum;
import org.btc163.manage.entity.*;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.*;
import org.btc163.manage.utils.UidUtil;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.*;

/**
 * created by fuyd on 2018/8/30
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BtcProjectDataTests {

    private static String URL_ = "http://www.bixiaobai.com/index/projectLibrary/getData?pageNo=%s&keyWords=&stage=&concept=&orderField=&orderBy=";

    private static final String INFO_URL = "http://www.bixiaobai.com/index/projectLibrary/detail?id=";

    @Autowired
    private TepConceptService tepConceptService;

    @Autowired
    private CapitalService capitalService;

    @Autowired
    private ProTepService proTepService;

    @Autowired
    private BtcProjectInfoService btcProjectInfoService;

    @Autowired
    private ProCaService proCaService;

    @Autowired
    private ProjectPathService projectPathService;

    @Autowired
    private ProjectDynamicService projectDynamicService;

    @Autowired
    private BtcProjectService btcProjectService;

    @Test
    public void projectDataTest() throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        Set<String> plIds = new HashSet<>();
        // 63  
        for (int i = 1; i < 11; i++) {
            String url = String.format(URL_, i);
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Accept", "application/json, text/javascript, */*; q=0.01");
            httpGet.setHeader("Accept-Encoding", "gzip, deflate");
            httpGet.setHeader("Accept-Language", "zh-CN,zh;q=0.9");
            httpGet.setHeader("Connection", "keep-alive");
            httpGet.setHeader("Cookie", "UM_distinctid=165855bba1b0-09c0900daa57e-34627908-1fa400-165855bba1d370; LiveWSDHT76227276=1535540714302830521278; NDHT76227276fistvisitetime=1535540714317; NDHT76227276IP=%7C123.118.109.174%7C; PHPSESSID=gndnnqateko749h1quhs7olbfa; CNZZDATA1274136426=1483721189-1535538329-http%253A%252F%252Fwww.bixiaobai.com%252F%7C1535591652; LiveWSDHT76227276sessionid=1535594936573308273668; NDHT76227276visitecounts=2; Hm_lvt_d1f17343b79e7e042f22cb86953b36e7=1535540708,1535594937; Hm_lpvt_d1f17343b79e7e042f22cb86953b36e7=1535594951; NDHT76227276LR_cookie_t0=1; NDHT76227276lastvisitetime=1535594951367; NDHT76227276visitepages=12");
            httpGet.setHeader("Host", "www.bixiaobai.com");
            httpGet.setHeader("Referer", "http://www.bixiaobai.com/index/capitalLibrary/index");
            httpGet.setHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
            httpGet.setHeader("X-Requested-With", "XMLHttpRequest");
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(35000)// 连接主机服务超时时间
                    .setConnectionRequestTimeout(35000)// 请求超时时间
                    .setSocketTimeout(60000)// 数据读取超时时间
                    .build();
            httpGet.setConfig(requestConfig);
            response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);
            JSONObject json = JSON.parseObject(result);
            JSONArray data = json.getJSONArray("data");
            for (Object datum : data) {
                JSONObject jsonObject = (JSONObject) datum;
                String pl_id = jsonObject.getString("pl_id");
                plIds.add(pl_id);
            }
        }
        response.close();
        httpClient.close();
        for (String plId : plIds) {
            Document document = Jsoup.connect(INFO_URL + plId).get();
            Elements documents = document.getElementsByClass("nr");
            final String proId = UidUtil.getId();
            BtcProject btcProject = new BtcProject();
            btcProject.setId(proId);
            btcProject.setUpdateTime(new Date());
            Element doc = documents.get(0);
            Elements divs = doc.children();
            Element pro_1 = divs.get(0);
            Element pro_2 = divs.get(1);
            Element pro_3 = divs.get(2);

            // 1.
            String proLogo = pro_1.getElementsByTag("img").attr("src"); //项目logo
            String logoname = saveImage(proLogo);
            Element content = pro_1.getElementsByTag("div").get(0);
            String proName = content.getElementsByTag("p").get(0).text(); //项目名称
            Elements tds = content.getElementsByTag("td");
            String proStage = tds.get(1).text(); // 项目阶段
            Elements jieShao = content.getElementsByClass("jieShao");
            String brief = jieShao.get(0).text(); // 项目介绍

            btcProject.setProLogo(logoname);
            btcProject.setProName(proName);
            if (!StringUtils.isEmpty(proStage)) {
                Integer proStageCode = null;
                try {
                    proStageCode = ProStageEnum.messageCodeOf(proStage);
                } catch (BusinessException e) {
                }
                btcProject.setProStage(proStageCode);
            }
            btcProject.setProIntroduce(brief);
            // 2.
            Element tbody = pro_2.getElementsByTag("tbody").get(0);
            Elements trs = tbody.children();
            Element rt_0 = trs.get(0);
            Element rt_1 = trs.get(1);
            Element rt_2 = trs.get(2);
            Element rt_4 = trs.get(4);
            Element rt_5 = trs.get(5);
            String tokenNum = rt_0.children().get(1).text(); // token总量
            String distributionOfTokens = rt_1.children().get(1).text(); // 代币分配
            Element td = rt_2.children().get(1);
            Elements plates = td.children();
            for (Element plate : plates) {
                Map<String, Object> param = new HashMap<>();
                if (!StringUtils.isEmpty(plate.text())) {
                    param.put("name", plate.text());
                    try {
                        List<TepConcept> tepConcept = tepConceptService.selectByMap(param);
                        if (!CollectionUtils.isEmpty(tepConcept)) {
                            Long tepId = tepConcept.get(0).getId(); // // 概念板块ID
                            ProTep proTep = new ProTep();
                            proTep.setProId(proId);
                            proTep.setId(UidUtil.getId());
                            proTep.setTepId(tepId);
                            proTepService.insert(proTep);
                        }
                    } catch (Exception e) {

                    }
                }
            }
            String consensus = rt_4.children().get(1).text(); // 共识机制
            Elements spans = rt_5.children().get(1).getElementsByTag("span");
            StringBuilder exchanges = new StringBuilder();
            for (Element span : spans) {
                exchanges.append(span.text() + ",");
            }
            String exchange = null;                 // 交易所
            if (!StringUtils.isEmpty(exchanges)) {
                exchange = exchanges.substring(0, exchanges.length() - 1);
            }

            btcProject.setProTokenNum(tokenNum);
            btcProject.setProTokenAllot(distributionOfTokens);
            btcProject.setProConsesusMechanism(consensus);
            btcProject.setPopExchange(exchange);
            // 3.
            Element uls = pro_3.getElementsByTag("ul").get(0); // 核心成员
            Elements members = uls.children();
            if (members.size() > 0) {
                for (Element member : members) {
                    String head = member.getElementsByTag("img").attr("src"); // 头像
                    String headImage = saveImage(head);
                    Elements names = member.getElementsByTag("div");
                    String name = names.get(0).text(); // 姓名职位
                    String briefs = names.get(1).text(); // 简介
                    BtcProjectInfo btcProjectInfo = new BtcProjectInfo();
                    btcProjectInfo.setId(UidUtil.getId());
                    btcProjectInfo.setUpdateTime(new Date());
                    btcProjectInfo.setInUserName(name);
                    btcProjectInfo.setInHeadImage(headImage);
                    btcProjectInfo.setInBrief(briefs);
                    btcProjectInfo.setProId(proId);
                    btcProjectInfoService.insert(btcProjectInfo);
                }
            }

            Element huoBan = pro_3.getElementsByClass("huoBan").get(0); // 合作伙伴
            String caNam = huoBan.text();
            btcProject.setProLabel(caNam);

            Element luJing = pro_3.getElementsByClass("luJing").get(0); // 项目路径
            Elements luJingDivs = luJing.children();
            for (Element luJingDiv : luJingDivs) {
                String pathName = luJingDiv.text(); // 路径名称
                ProjectPath path = new ProjectPath();
                path.setId(UidUtil.getId());
                path.setUpdateTime(new Date());
                path.setProId(proId);
                path.setPaIncident(pathName);
                projectPathService.insert(path);
            }

            Elements elses = pro_3.getElementsByClass("nr2");
            Element zhaoMu = elses.get(1); // 招募资料
            Element asd = elses.get(1);
            Element dongTai = elses.get(3); // 相关动态
            Element lianXi = elses.get(4); // 联系我们

            // (1
            Elements zmTrs = zhaoMu.getElementsByTag("tr");
            String tokenTotal = zmTrs.get(0).children().get(1).text(); // token募集总量
            String time = zmTrs.get(1).children().get(1).text(); //招募时间
            String conversion = zmTrs.get(2).children().get(1).text(); // 兑换比例
            String placement = zmTrs.get(3).children().get(1).text(); // 募集资金分配
            String hardTop = zmTrs.get(4).children().get(1).text(); // 硬顶
            String locked = zmTrs.get(5).children().get(1).text(); // 锁仓
            String remark = zmTrs.get(6).children().get(1).text(); // 备注

            if (!StringUtils.isEmpty(tokenTotal)) {
                if (!tokenTotal.equals("暂无")) {
                    btcProject.setRaiseTokenSum(tokenTotal);
                }
            }
            btcProject.setReTime(time);
            btcProject.setReRatio(conversion);
            btcProject.setReHardTop(hardTop);
            btcProject.setReCapitalAllot(placement);
            if (!StringUtils.isEmpty(locked)) {
                Integer lockedCode = null;
                try {
                    lockedCode = ReLockUpEnum.messageCodeOf(locked);
                } catch (BusinessException e) {
                    lockedCode = 1;
                }
                btcProject.setReLockUp(lockedCode);
            }
            btcProject.setReRemake(remark);

            // （2
            Elements dt = dongTai.getElementsByTag("div"); // 项目动态
            Element dongtai = dt.get(2);
            Elements as = dongtai.getElementsByTag("a");
            for (Element a : as) {
                String url = a.attr("href"); // url
                String contents = a.text(); // 内容
                ProjectDynamic projectDynamic = new ProjectDynamic();
                projectDynamic.setDyUrl(url);
                projectDynamic.setDyTitle(contents);
                projectDynamic.setProId(proId);
                projectDynamic.setId(UidUtil.getId());
                projectDynamicService.insert(projectDynamic);
            }

            // (3
            Elements lianxi = lianXi.getElementsByTag("tr");
            Element gw = lianxi.get(0).getElementsByTag("a").get(0);
            Element llq = lianxi.get(1).getElementsByTag("a").get(0);
            Element bps = lianxi.get(2).getElementsByTag("a").get(0);
            Element qb = lianxi.get(3).getElementsByTag("a").get(0);
            Element wx = lianxi.get(4).getElementsByTag("a").get(0);

            btcProject.setCoOfficial(gw.attr("href"));
            btcProject.setCoBrowser(llq.attr("href"));
            btcProject.setCoWhite(bps.attr("href"));
            btcProject.setCoWallet(qb.attr("href"));
            btcProject.setCoWechat(wx.attr("href"));
            btcProjectService.insert(btcProject);
        }

    }

    private static String saveImage(String url_path) {
        File file = new File(url_path);
        String name = file.getName();
//        InputStream inputStream = null;
//        HttpURLConnection httpURLConnection = null;
//        try {
//            java.net.URL url = new URL(url_path);//创建的URL
//            if (url != null) {
//                httpURLConnection = (HttpURLConnection) url.openConnection();//打开链接
//                httpURLConnection.setConnectTimeout(3000);//设置网络链接超时时间，3秒，链接失败后重新链接
//                httpURLConnection.setDoInput(true);//打开输入流
//                httpURLConnection.setRequestMethod("GET");//表示本次Http请求是GET方式
//                int responseCode = httpURLConnection.getResponseCode();//获取返回码
//                if (responseCode == 200) {//成功为200
//                    //从服务器获得一个输入流
//                    inputStream = httpURLConnection.getInputStream();
//                    FileOutputStream outStream = new FileOutputStream("/Volumes/fuydWork/image/" + name);
//                    byte[] buffer = new byte[1024]; //创建一个Buffer字符串
//                    //每次读取的字符串长度，如果为-1，代表全部读取完毕
//                    int len = 0;
//                    //使用一个输入流从buffer里把数据读取出来
//                    while ((len = inputStream.read(buffer)) != -1) {
//                        //用输出流往buffer里写入数据，中间参数代表从哪个位置开始读，len代表读取的长度
//                        outStream.write(buffer, 0, len);
//                    }
//                    outStream.close();
//                    inputStream.close();
//                }
//            }
//        } catch (MalformedURLException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        } catch (IOException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//        }
        return name;
    }
}
