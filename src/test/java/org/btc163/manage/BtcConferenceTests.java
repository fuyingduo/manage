package org.btc163.manage;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import org.apache.http.HttpEntity;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.btc163.manage.entity.BtcConference;
import org.btc163.manage.service.BtcConferenceService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.IOException;
import java.util.Date;

/**
 * created by fuyd on 2018/9/4
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class BtcConferenceTests {

    private static String URL_WEB = "http://www.bixiaobai.com/index/projectMeeting/getData?pageNo=%s&keyWords=&pm_province_id=&pm_city_id=";

    @Autowired
    private BtcConferenceService btcConferenceService;

    @Test
    public void ConferenceData() throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = null;
        for (int i = 1; i < 34; i++) {
            String URL = String.format(URL_WEB, i);
            HttpGet httpGet = new HttpGet(URL);
            httpGet.setHeader("Accept", "application/json, text/javascript, */*; q=0.01");
            httpGet.setHeader("Accept-Encoding", "gzip, deflate");
            httpGet.setHeader("Accept-Language", "zh-CN,zh;q=0.9");
            httpGet.setHeader("Connection", "keep-alive");
            httpGet.setHeader("Cookie", "UM_distinctid=165855bba1b0-09c0900daa57e-34627908-1fa400-165855bba1d370; LiveWSDHT76227276=1535540714302830521278; NDHT76227276fistvisitetime=1535540714317; NDHT76227276IP=%7C123.118.109.174%7C; PHPSESSID=gndnnqateko749h1quhs7olbfa; CNZZDATA1274136426=1483721189-1535538329-http%253A%252F%252Fwww.bixiaobai.com%252F%7C1535591652; LiveWSDHT76227276sessionid=1535594936573308273668; NDHT76227276visitecounts=2; Hm_lvt_d1f17343b79e7e042f22cb86953b36e7=1535540708,1535594937; Hm_lpvt_d1f17343b79e7e042f22cb86953b36e7=1535594951; NDHT76227276LR_cookie_t0=1; NDHT76227276lastvisitetime=1535594951367; NDHT76227276visitepages=12");
            httpGet.setHeader("Host", "www.bixiaobai.com");
            httpGet.setHeader("Referer", "http://www.bixiaobai.com/index/capitalLibrary/index");
            httpGet.setHeader("User-Agent", "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36");
            httpGet.setHeader("X-Requested-With", "XMLHttpRequest");
            RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(35000)// 连接主机服务超时时间
                    .setConnectionRequestTimeout(35000)// 请求超时时间
                    .setSocketTimeout(60000)// 数据读取超时时间
                    .build();
            httpGet.setConfig(requestConfig);
            response = httpClient.execute(httpGet);
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity);
            JSONObject json = JSON.parseObject(result);
            if (json.getInteger("status") == 1) {
                JSONArray datas = json.getJSONArray("data");
                for (Object data : datas) {
                    JSONObject obj = (JSONObject) data;
                    JSONArray childs = obj.getJSONArray("child");
                    String month = obj.getString("month");
                    for (Object c : childs) {
                        JSONObject child = (JSONObject) c;
                        String cityName = child.getString("city_name");
                        String pmDesc = child.getString("pm_desc");
                        String pmEndTime = child.getString("pm_end_time");
                        String pmName = child.getString("pm_name");
                        String pmStartTime = child.getString("pm_start_time");
                        String provinceName = child.getString("province_name");
                        String pmId = child.getString("pm_id");
                        BtcConference btcConference = new BtcConference();
                        btcConference.setPmMonth(month);
                        btcConference.setPmCityName(cityName);
                        btcConference.setPmDesc(pmDesc);
                        btcConference.setPmEndTime(pmEndTime);
                        btcConference.setPmName(pmName);
                        btcConference.setPmStartTime(pmStartTime);
                        btcConference.setPmProvinceName(provinceName);
                        btcConference.setUpdateTime(new Date());
                        btcConference.setId(pmId);
                        btcConferenceService.insert(btcConference);
                    }
                }
            }

        }
        response.close();
        httpClient.close();
    }
}
