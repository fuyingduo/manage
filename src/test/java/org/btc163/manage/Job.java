package org.btc163.manage;

import org.btc163.manage.service.TsScheduleTriggersService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * created by fuyd on 2018/9/13
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class Job {

    @Autowired
    private TsScheduleTriggersService tsScheduleTriggersService;

    @Test
    public void test() {
        tsScheduleTriggersService.refreshTrigger();
    }
}
