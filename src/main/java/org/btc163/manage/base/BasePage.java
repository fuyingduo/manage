package org.btc163.manage.base;

import lombok.Data;

/**
 * created by fuyd on 2018/8/21
 */
@Data
public abstract class BasePage {
    int page;
    int limit;
}
