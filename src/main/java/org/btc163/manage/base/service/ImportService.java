package org.btc163.manage.base.service;

import org.btc163.manage.exception.BusinessException;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * created by fuyd on 2018/8/29
 */
public interface ImportService<T> {
    T excelImport(MultipartFile file) throws IOException, BusinessException;
}
