package org.btc163.manage.base.service;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.BaseImport;
import org.btc163.manage.constant.ProStageEnum;
import org.btc163.manage.constant.ReLockUpEnum;
import org.btc163.manage.constant.TepConceptEnum;
import org.btc163.manage.entity.*;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.*;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * created by fuyd on 2018/8/29
 */
@Slf4j
@Service
public class ProjectImportService extends BaseImport implements ImportService<BtcProject> {

    @Autowired
    private TepConceptService tepConceptService;

    @Autowired
    private CapitalService capitalService;

    @Autowired
    private BtcPlatformInfoService btcPlatformInfoService;

    @Autowired
    private BtcProjectService btcProjectService;

    @Autowired
    private ProTepService proTepService;

    @Autowired
    private BtcProjectInfoService btcProjectInfoService;

    @Autowired
    private ProCaService proCaService;

    @Autowired
    private ProPlatformService proPlatformService;

    @Autowired
    private ProjectPathService projectPathService;

    @Autowired
    private ProjectDynamicService projectDynamicService;

    @Transactional(rollbackFor = {BusinessException.class, IOException.class})
    @Override
    public BtcProject excelImport(MultipartFile file) throws IOException, BusinessException {
        XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
        XSSFSheet sheet = workbook.getSheetAt(0);
        List<CellRangeAddress> cras = getCombineCell(sheet);
        int count = sheet.getLastRowNum() + 1;
        // 项目库基本信息
        BtcProject btcProject = null;
        // 概念模版
        List<TepConcept> tepConcepts = new ArrayList<>();
        // 核心成员
        List<BtcProjectInfo> btcProjectInfos = new ArrayList<>();
        // 合作伙伴
        List<Capital> capitals = new ArrayList<>();
        // 已上线交易所
        List<PlatformInfo> platformInfos = new ArrayList<>();
        // 项目路径
        List<ProjectPath> projectPaths = new ArrayList<>();
        // 新闻标题
        List<ProjectDynamic> projectDynamics = new ArrayList<>();

        for (int i = 1; i < count; i++) {
            boolean pd = isMergedRegion(sheet, i, 0);
            XSSFRow row = sheet.getRow(i);
            XSSFCell proName = row.getCell(0);
            XSSFCell proLogo = row.getCell(1);
            XSSFCell proTokenNum = row.getCell(2);
            XSSFCell proStage = row.getCell(3);
            XSSFCell proConsesusMechanism = row.getCell(4);
            XSSFCell proTokenAllot = row.getCell(5);
            XSSFCell proIntroduce = row.getCell(6);
            XSSFCell raiseTokenSum = row.getCell(7);
            XSSFCell reTime = row.getCell(8);
            XSSFCell reTimeEnd = row.getCell(9);
            XSSFCell reRatio = row.getCell(10);
            XSSFCell reHardTop = row.getCell(11);
            XSSFCell reLockUp = row.getCell(12);
            XSSFCell reCapitalAllot = row.getCell(13);
            XSSFCell reRemake = row.getCell(14);
            XSSFCell coOfficial = row.getCell(15);
            XSSFCell coBrowser = row.getCell(16);
            XSSFCell coWhite = row.getCell(17);
            XSSFCell coWallet = row.getCell(18);
            XSSFCell coWechat = row.getCell(19);
            // 概念板块
            XSSFCell tepName = row.getCell(20);
            // 核心成员
            XSSFCell inUserName = row.getCell(21);
            XSSFCell inPosition = row.getCell(22);
            XSSFCell inHeadImage = row.getCell(23);
            XSSFCell inBrief = row.getCell(24);
            // 合作伙伴
            XSSFCell caName = row.getCell(25);
            // 已上线交易所
            XSSFCell platName = row.getCell(26);
            // 项目路径
            XSSFCell paTime = row.getCell(27);
            XSSFCell paIncident = row.getCell(28);
            // 新闻标题
            XSSFCell dyTitle = row.getCell(29);
            XSSFCell dyUrl = row.getCell(30);
            // 项目基础信息
            if (btcProject == null) {
                btcProject = this.entryProject(btcProject, proName, proLogo, proTokenNum, proStage,
                        proConsesusMechanism, proTokenAllot, proIntroduce, raiseTokenSum,
                        reTime, reTimeEnd, reRatio, reHardTop, reLockUp,
                        reCapitalAllot, reRemake, coOfficial, coBrowser,
                        coWhite, coWallet, coWechat);
            }
            // 概念板块
            if (tepName != null) {
                TepConcept tepConcept = this.getTepConcept(tepName);
                if (tepConcept != null) {
                    tepConcepts.add(tepConcept);
                }
            }
            // 核心成员
            if (inUserName != null || inPosition != null || inHeadImage != null || inBrief != null) {
                BtcProjectInfo btcProjectInfo = this.getBtcProjectInfo(btcProject.getId(), inUserName, inPosition, inHeadImage, inBrief);
                if (btcProjectInfo != null) {
                    btcProjectInfos.add(btcProjectInfo);
                }
            }
            // 合作伙伴
            if (caName != null) {
                Capital capital = this.getCapital(caName);
                if (capital != null) {
                    capitals.add(capital);
                }
            }
            // 已上线交易所
            if (platName != null) {
                PlatformInfo platformInfo = this.getPlatformInfo(platName);
                if (platformInfo != null) {
                    platformInfos.add(platformInfo);
                }
            }
            // 项目路径
            if (paTime != null || paIncident != null) {
                ProjectPath path = this.getProjectPath(btcProject.getId(), paTime, paIncident);
                if (path != null) {
                    projectPaths.add(path);
                }
            }
            // 新闻标题
            if (dyTitle != null || dyUrl != null) {
                ProjectDynamic projectDynamic = this.getProjectDynamic(btcProject.getId(), dyTitle, dyUrl);
                if (projectDynamic != null) {
                    projectDynamics.add(projectDynamic);
                }
            }
            if (pd) {
                int lastRow = getRowNum(cras, sheet.getRow(i).getCell(0), sheet);
                if (i == lastRow) {
                    this.saveAndClear(btcProject, tepConcepts, btcProjectInfos, capitals, platformInfos, projectPaths, projectDynamics);
                }
            } else {
                this.saveAndClear(btcProject, tepConcepts, btcProjectInfos, capitals, platformInfos, projectPaths, projectDynamics);
            }
        }
        return null;
    }

    /**
     * 保存数据 清理
     *
     * @param btcProject
     * @param tepConcepts
     * @param btcProjectInfos
     * @param capitals
     * @param platformInfos
     * @param projectPaths
     * @param projectDynamics
     * @throws BusinessException
     */
    private void saveAndClear(BtcProject btcProject, List<TepConcept> tepConcepts, List<BtcProjectInfo> btcProjectInfos,
                              List<Capital> capitals, List<PlatformInfo> platformInfos, List<ProjectPath> projectPaths, List<ProjectDynamic> projectDynamics) throws BusinessException {
        this.saveProjectData(btcProject, tepConcepts, btcProjectInfos, capitals, platformInfos, projectPaths, projectDynamics);
        tepConcepts.clear();
        btcProjectInfos.clear();
        capitals.clear();
        platformInfos.clear();
        projectPaths.clear();
        projectDynamics.clear();
        btcProject = null;
    }

    /**
     * 新闻标题
     *
     * @param proId
     * @param dyTitle
     * @param dyUrl
     * @return
     */
    private ProjectDynamic getProjectDynamic(final String proId, XSSFCell dyTitle, XSSFCell dyUrl) {
        ProjectDynamic projectDynamic = new ProjectDynamic();
        projectDynamic.setId(UidUtil.getId());
        projectDynamic.setProId(proId);
        projectDynamic.setDyTitle(dyTitle == null ? null : dyTitle.getStringCellValue());
        projectDynamic.setDyUrl(dyUrl == null ? null : dyUrl.getStringCellValue());
        return projectDynamic;
    }

    /**
     * 项目路径
     *
     * @param proId
     * @param paTime
     * @param paIncident
     * @return
     */
    private ProjectPath getProjectPath(final String proId, XSSFCell paTime, XSSFCell paIncident) {
        ProjectPath path = new ProjectPath();
        path.setProId(proId);
        path.setUpdateTime(new Date());
        path.setPaTime(paTime == null ? null : paTime.getDateCellValue());
        path.setPaIncident(paIncident == null ? null : paIncident.getStringCellValue());
        path.setId(UidUtil.getId());
        return path;
    }

    /**
     * 已上线交易所
     *
     * @param platName
     * @return
     */
    private PlatformInfo getPlatformInfo(XSSFCell platName) {
        PlatformInfo platformInfo = new PlatformInfo();
        platformInfo.setName(platName.getStringCellValue());
        EntityWrapper<PlatformInfo> platformInfoEntityWrapper = new EntityWrapper<>(platformInfo);
        List<PlatformInfo> platformInfoList = btcPlatformInfoService.selectList(platformInfoEntityWrapper);
        if (!CollectionUtils.isEmpty(platformInfoList)) {
            return platformInfoList.get(0);
        }
        return null;
    }

    /**
     * 合作伙伴
     *
     * @param caName
     * @return
     */
    private Capital getCapital(XSSFCell caName) {
        Capital capital = new Capital();
        capital.setCaName(caName.getStringCellValue());
        EntityWrapper<Capital> capitalEntityWrapper = new EntityWrapper<>(capital);
        List<Capital> capitalList = capitalService.selectList(capitalEntityWrapper);
        if (!CollectionUtils.isEmpty(capitalList)) {
            return capitalList.get(0);
        }
        return null;
    }

    /**
     * 核心成员
     *
     * @param proId
     * @param inUserName
     * @param inPosition
     * @param inHeadImage
     * @param inBrief
     * @return
     */
    private BtcProjectInfo getBtcProjectInfo(final String proId, XSSFCell inUserName, XSSFCell inPosition, XSSFCell inHeadImage, XSSFCell inBrief) {
        BtcProjectInfo btcProjectInfo = new BtcProjectInfo();
        btcProjectInfo.setProId(proId);
        btcProjectInfo.setUpdateTime(new Date());
        btcProjectInfo.setId(UidUtil.getId());
        btcProjectInfo.setInBrief(inBrief == null ? null : inBrief.getStringCellValue());
        btcProjectInfo.setInHeadImage(inHeadImage == null ? null : inHeadImage.getStringCellValue());
        btcProjectInfo.setInPosition(inPosition == null ? null : inPosition.getStringCellValue());
        btcProjectInfo.setInUserName(inUserName == null ? null : inUserName.getStringCellValue());
        return btcProjectInfo;
    }

    /**
     * 获取概念板块
     *
     * @param tepName
     * @return
     * @throws BusinessException
     */
    private TepConcept getTepConcept(XSSFCell tepName) throws BusinessException {
        Integer tepId = TepConceptEnum.messageCodeOf(tepName.getStringCellValue());
        TepConcept tepConcept = tepConceptService.selectById(tepId);
        if (tepConcept != null) {
            return tepConcept;
        }
        return null;
    }

    /**
     * 项目库基础信息
     *
     * @param btcProject
     * @param proName
     * @param proLogo
     * @param proTokenNum
     * @param proStage
     * @param proConsesusMechanism
     * @param proTokenAllot
     * @param proIntroduce
     * @param raiseTokenSum
     * @param reTime
     * @param reTimeEnd
     * @param reRatio
     * @param reHardTop
     * @param reLockUp
     * @param reCapitalAllot
     * @param reRemake
     * @param coOfficial
     * @param coBrowser
     * @param coWhite
     * @param coWallet
     * @param coWechat
     * @return
     * @throws BusinessException
     */
    private BtcProject entryProject(BtcProject btcProject, XSSFCell proName, XSSFCell proLogo, XSSFCell proTokenNum, XSSFCell proStage,
                                    XSSFCell proConsesusMechanism, XSSFCell proTokenAllot, XSSFCell proIntroduce, XSSFCell raiseTokenSum,
                                    XSSFCell reTime, XSSFCell reTimeEnd, XSSFCell reRatio, XSSFCell reHardTop, XSSFCell reLockUp,
                                    XSSFCell reCapitalAllot, XSSFCell reRemake, XSSFCell coOfficial, XSSFCell coBrowser,
                                    XSSFCell coWhite, XSSFCell coWallet, XSSFCell coWechat) throws BusinessException {
        btcProject = new BtcProject();
        btcProject.setProName(proName.getStringCellValue());
        btcProject.setProLogo(proLogo == null ? null : proLogo.getStringCellValue());
        btcProject.setProTokenNum(proTokenNum == null ? null : proTokenNum.getStringCellValue());
        if (proStage != null) {
            Integer prostage = ProStageEnum.messageCodeOf(proStage.getStringCellValue());
            btcProject.setProStage(prostage);
        }
        btcProject.setProConsesusMechanism(proConsesusMechanism == null ? null : proConsesusMechanism.getStringCellValue());
        btcProject.setProTokenAllot(proTokenAllot == null ? null : proTokenAllot.getStringCellValue());
        btcProject.setProIntroduce(proIntroduce == null ? null : proIntroduce.getStringCellValue());
        btcProject.setRaiseTokenSum(raiseTokenSum == null ? null : raiseTokenSum.getStringCellValue());
        btcProject.setReTime(reTime == null ? null : DateFormatUtils.format(reTime.getDateCellValue(), "yyyy-MM-dd HH:mm:ss"));
        btcProject.setReTimeEnd(reTimeEnd == null ? null : DateFormatUtils.format(reTimeEnd.getDateCellValue(), "yyyy-MM-dd HH:mm:ss"));
        btcProject.setReRatio(reRatio == null ? null : reRatio.getStringCellValue());
        btcProject.setReHardTop(reHardTop == null ? null : reHardTop.getStringCellValue());
        btcProject.setReLockUp(reLockUp == null ? null : ReLockUpEnum.messageCodeOf(reLockUp.getStringCellValue()));
        btcProject.setReCapitalAllot(reCapitalAllot == null ? null : reCapitalAllot.getStringCellValue());
        btcProject.setReRemake(reRemake == null ? null : reRemake.getStringCellValue());
        btcProject.setCoOfficial(coOfficial == null ? null : coOfficial.getStringCellValue());
        btcProject.setCoBrowser(coBrowser == null ? null : coBrowser.getStringCellValue());
        btcProject.setCoWhite(coWhite == null ? null : coWhite.getStringCellValue());
        btcProject.setCoWallet(coWallet == null ? null : coWallet.getStringCellValue());
        btcProject.setCoWechat(coWechat == null ? null : coWechat.getStringCellValue());
        btcProject.setUpdateTime(new Date());
        btcProject.setId(UidUtil.getId());
        return btcProject;
    }

    /**
     * 保存
     *
     * @param btcProject
     * @param tepConcepts
     * @param btcProjectInfos
     * @param capitals
     * @param platformInfos
     * @param projectPaths
     * @param projectDynamics
     * @throws BusinessException
     */
    private void saveProjectData(BtcProject btcProject, List<TepConcept> tepConcepts, List<BtcProjectInfo> btcProjectInfos,
                                 List<Capital> capitals, List<PlatformInfo> platformInfos, List<ProjectPath> projectPaths, List<ProjectDynamic> projectDynamics) throws BusinessException {
        try {
            if (!btcProjectService.insert(btcProject)) {
                throw new BusinessException(10002);
            }
            if (!CollectionUtils.isEmpty(tepConcepts)) {
                for (TepConcept tepConcept : tepConcepts) {
                    ProTep proTep = new ProTep();
                    proTep.setId(UidUtil.getId());
                    proTep.setTepId(tepConcept.getId());
                    proTep.setProId(btcProject.getId());
                    if (!proTepService.insert(proTep)) {
                        throw new BusinessException(10002);
                    }
                }
            }
            if (!CollectionUtils.isEmpty(btcProjectInfos)) {
                btcProjectInfos.forEach(btcProjectInfo -> btcProjectInfoService.insert(btcProjectInfo));
            }
            if (!CollectionUtils.isEmpty(capitals)) {
                for (Capital capital : capitals) {
                    ProCa proCa = new ProCa();
                    proCa.setCaId(capital.getId());
                    proCa.setProId(btcProject.getId());
                    proCa.setId(UidUtil.getId());
                    if (!proCaService.insert(proCa)) {
                        throw new BusinessException(10002);
                    }
                }
            }
            if (!CollectionUtils.isEmpty(platformInfos)) {
                for (PlatformInfo platformInfo : platformInfos) {
                    ProPlatform proPlatform = new ProPlatform();
                    proPlatform.setId(UidUtil.getId());
                    proPlatform.setProId(btcProject.getId());
                    proPlatform.setPlatId(platformInfo.getName());
                    if (!proPlatformService.insert(proPlatform)) {
                        throw new BusinessException(10002);
                    }
                }
            }
            if (!CollectionUtils.isEmpty(projectPaths)) {
                projectPaths.forEach(projectPath -> projectPathService.insert(projectPath));
            }
            if (!CollectionUtils.isEmpty(projectDynamics)) {
                projectDynamics.forEach(projectDynamic -> projectDynamicService.insert(projectDynamic));
            }
        } catch (Exception e) {
            log.error("import error e:{}", e.getMessage());
            throw new BusinessException(10002);
        }

    }
}
