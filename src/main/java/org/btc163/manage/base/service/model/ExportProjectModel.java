package org.btc163.manage.base.service.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * created by fuyd on 2018/9/3
 */
@Data
@ToString
public class ExportProjectModel implements Serializable {

    private static final long serialVersionUID = 986770565082688285L;
    private String proName;
    /**
     * 项目logo
     */
    private String proLogo;
    /**
     * Token总量
     */
    private String proTokenNum;
    /**
     * 项目阶段
     */
    private Integer proStage;
    /**
     * 共识机制
     */
    private String proConsesusMechanism;
    /**
     * 代币分配
     */
    private String proTokenAllot;
    /**
     * 项目介绍
     */
    private String proIntroduce;
    /**
     * 招募时间
     */
    private String reTime;
    /**
     * 兑换比例
     */
    private String reRatio;
    /**
     * 硬顶
     */
    private String reHardTop;
    /**
     * 锁仓
     */
    private String reLockUp;
    /**
     * 募集资金总数
     */
    private String raiseTokenSum;
    /**
     * 募集资金分配
     */
    private String reCapitalAllot;
    /**
     * 备注
     */
    private String reRemake;
    /**
     * 官网
     */
    private String coOfficial;
    /**
     * 区块链浏览器
     */
    private String coBrowser;
    /**
     * 白皮书
     */
    private String coWhite;
    /**
     * 钱包
     */
    private String coWallet;
    /**
     * 微信
     */
    private String coWechat;

    /**
     * 概念板块ID
     */
    private String tepId;

    /**
     * 交易所
     */
    private String exchange;

    /**
     * 职位-姓名
     */
    private String jobName;

    /**
     * 简介
     */
    private String briefs;

    /**
     * 头像
     */
    private String headImage;

    /**
     * 合作伙伴
     */
    private String caNames;

    /**
     * 项目路径
     */
    private String pathNames;

    /**
     * 项目动态
     */
    private String dyUrl;

    private String dyTitle;

}
