package org.btc163.manage.base.service;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.BaseImport;
import org.btc163.manage.entity.BtcCapitalInfo;
import org.btc163.manage.entity.BtcProject;
import org.btc163.manage.entity.Capital;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.BtcCapitalInfoService;
import org.btc163.manage.service.CapitalService;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * created by fuyd on 2018/8/28
 */
@Slf4j
@Service
public class CapitalImportService extends BaseImport implements ImportService<Capital> {

    private static final String CLASS_NAME = "[ImportService]";

    @Autowired
    private BtcCapitalInfoService btcCapitalInfoService;

    @Autowired
    private CapitalService capitalService;

    @Override
    public Capital excelImport(MultipartFile file) throws IOException, BusinessException {
        XSSFWorkbook workbook = new XSSFWorkbook(file.getInputStream());
        XSSFSheet sheet = workbook.getSheetAt(0);
        List<CellRangeAddress> cras = getCombineCell(sheet);
        int count = sheet.getLastRowNum() + 1;
        // 参投项目
        List<BtcProject> btcProjects = new ArrayList<>();
        // 合伙人
        List<BtcCapitalInfo> btcCapitalInfos = new ArrayList<>();
        // 资本录入
        Capital capital = null;
        for (int i = 1; i < count; i++) {
            boolean pd = isMergedRegion(sheet, i, 0);
            XSSFRow row = sheet.getRow(i);
            XSSFCell caName = row.getCell(0);
            XSSFCell caLogo = row.getCell(1);
            XSSFCell caEstablish = row.getCell(2);
            XSSFCell caLab = row.getCell(3);
            XSSFCell caBrief = row.getCell(4);
            XSSFCell caUrl = row.getCell(5);
            XSSFCell pro = row.getCell(6);
            XSSFCell inUserName = row.getCell(7);
            XSSFCell inPosition = row.getCell(8);
            XSSFCell inHeadImage = row.getCell(9);
            XSSFCell inBrief = row.getCell(10);
            if (pd) {
                int lastRow = getRowNum(cras, sheet.getRow(i).getCell(0), sheet);
                // 资本基础信息
                if (capital == null) {
                    capital = new Capital();
                    capital.setCaName(caName.getStringCellValue());
                    capital.setCaLogo(caLogo.getStringCellValue());
                    capital.setCaLab(caLab.getStringCellValue());
                    capital.setCaBrief(caBrief.getStringCellValue());
                    capital.setCaUrl(caUrl.getStringCellValue());
                    capital.setCaEstablish(caEstablish == null ? null : DateFormatUtils.format(caEstablish.getDateCellValue(), "yyyy-MM-dd"));
                }
                BtcCapitalInfo btcCapitalInfo = new BtcCapitalInfo();
                btcCapitalInfo.setInUserName(inUserName.getStringCellValue());
                btcCapitalInfo.setInHeadImage(inHeadImage.getStringCellValue());
                btcCapitalInfo.setInPosition(inPosition.getStringCellValue());
                btcCapitalInfo.setInBrief(inBrief.getStringCellValue());
                btcCapitalInfos.add(btcCapitalInfo);
                if (i == lastRow) {
                    String caId = UidUtil.getId();
                    capital.setUpdate(new Date());
                    capital.setId(caId);
                    try {
                        if (!capitalService.insert(capital)) {
                            throw new BusinessException(10002);
                        }
                        if (!CollectionUtils.isEmpty(btcCapitalInfos)) {
                            for (BtcCapitalInfo capitalInfo : btcCapitalInfos) {
                                capitalInfo.setCaId(caId);
                                capitalInfo.setId(UidUtil.getId());
                                if (!btcCapitalInfoService.insert(capitalInfo)) {
                                    throw new BusinessException(10002);
                                }
                            }
                        }
                    } catch (Exception e) {
                        throw new BusinessException(10002);
                    }
                    System.out.println(btcCapitalInfos);
                    System.out.println(capital);
                    btcCapitalInfos.clear();
                    capital = null;
                }
            } else {
                capital = new Capital();
                capital.setCaName(caName.getStringCellValue());
                capital.setCaLogo(caLogo.getStringCellValue());
                capital.setCaLab(caLab.getStringCellValue());
                capital.setCaBrief(caBrief.getStringCellValue());
                capital.setCaUrl(caUrl.getStringCellValue());
                capital.setCaEstablish(caEstablish == null ? null : DateFormatUtils.format(caEstablish.getDateCellValue(), "yyyy-MM-dd"));
                String caId = UidUtil.getId();
                capital.setUpdate(new Date());
                capital.setId(caId);
                try {
                    if (!capitalService.insert(capital)) {
                        throw new BusinessException(10002);
                    }
                } catch (Exception e) {
                    throw new BusinessException(10002);
                }
                if (!StringUtils.isEmpty(inUserName.getStringCellValue()) ||
                        !StringUtils.isEmpty(inUserName.getStringCellValue()) ||
                        !StringUtils.isEmpty(inPosition.getStringCellValue()) ||
                        !StringUtils.isEmpty(inBrief.getStringCellValue())) {
                    BtcCapitalInfo btcCapitalInfo = new BtcCapitalInfo();
                    btcCapitalInfo.setInUserName(inUserName.getStringCellValue());
                    btcCapitalInfo.setInHeadImage(inHeadImage.getStringCellValue());
                    btcCapitalInfo.setInPosition(inPosition.getStringCellValue());
                    btcCapitalInfo.setInBrief(inBrief.getStringCellValue());
                    try {
                        btcCapitalInfo.setCaId(caId);
                        btcCapitalInfo.setId(UidUtil.getId());
                        if (!btcCapitalInfoService.insert(btcCapitalInfo)) {
                            throw new BusinessException(10002);
                        }
                    } catch (Exception e) {
                        throw new BusinessException(10002);
                    }
                }

            }

        }
        return new Capital();
    }


}
