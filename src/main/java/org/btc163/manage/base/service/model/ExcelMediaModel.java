package org.btc163.manage.base.service.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * created by fuyd on 2018/9/3
 */
@Data
@ToString
public class ExcelMediaModel implements Serializable {

    private static final long serialVersionUID = 134612128652832110L;
    /**
     * 名称
     */
    private String mtName;
    /**
     * logo
     */
    private String mtLogo;
    /**
     * 分类
     */
    private String mtType;

    private String mtIntroduce;

    private String mtUrl;
}
