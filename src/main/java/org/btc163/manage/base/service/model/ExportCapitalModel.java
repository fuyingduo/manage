package org.btc163.manage.base.service.model;

import lombok.Data;
import lombok.ToString;


/**
 * created by fuyd on 2018/9/3
 */
@Data
@ToString
public class ExportCapitalModel {

    /**
     * 资本名称
     */
    private String caName;
    /**
     * 标签
     */
    private String caLab;
    /**
     * 资本logo
     */
    private String caLogo;
    /**
     * 成立时间
     */
    private String caEstablish;
    /**
     * 简介
     */
    private String caBrief;

    /**
     * 合伙人姓名-职位
     */
    private String partnerNameAndPosition;

    /**
     * 头像
     */
    private String partnerImage;
    /**
     * 合伙人简介
     */
    private String partnerBrief;
    /**
     * 参投项目
     */
    private String projects;


}
