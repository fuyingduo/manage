package org.btc163.manage.base.service.model;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * created by fuyd on 2018/9/3
 */
@Data
@ToString
public class ExcelData<T> implements Serializable {

    private static final long serialVersionUID = 1025766330361298661L;
    // 表头
    private List<String> titles;

    // 数据
    private List<T> rows;

    // 页签名称
    private String name;
}
