package org.btc163.manage.base.service;

import lombok.extern.slf4j.Slf4j;
import org.btc163.manage.base.BaseJob;

/**
 * created by fuyd on 2018/9/12
 */
@Slf4j
public class TestExecuteJob extends BaseJob {

    @Override
    public String getClassName() {
        return TestExecuteJob.class.getSimpleName();
    }

    @Override
    public void executeJob() {
        log.info("！！！-------------------------------------");
    }

}
