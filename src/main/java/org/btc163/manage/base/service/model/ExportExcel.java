package org.btc163.manage.base.service.model;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.xssf.usermodel.extensions.XSSFCellBorder;

import java.awt.Color;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;


/**
 * created by fuyd on 2018/9/3
 */
public class ExportExcel {

    public static void exportExcel(ExcelData data) throws Exception {
        FileOutputStream out = new FileOutputStream(new File("/Volumes/fuydWork/" + data.getName() + ".xlsx"));
        XSSFWorkbook wb = new XSSFWorkbook();
        try {
            String sheetName = data.getName();
            if (null == sheetName) {
                sheetName = "Sheet1";
            }
            XSSFSheet sheet = wb.createSheet(sheetName);
            writeExcel(wb, sheet, data);

            wb.write(out);
        } finally {
            wb.close();
        }
    }

    private static void writeExcel(XSSFWorkbook wb, Sheet sheet, ExcelData data) {

        int rowIndex = 0;
        rowIndex = writeTitlesToExcel(wb, sheet, data.getTitles());
        writeRowsToExcel(wb, sheet, data.getRows(), rowIndex);
        autoSizeColumns(sheet, data.getTitles().size() + 1);

    }

    private static int writeTitlesToExcel(XSSFWorkbook wb, Sheet sheet, List<String> titles) {
        int rowIndex = 0;
        int colIndex = 0;

        Font titleFont = wb.createFont();
        titleFont.setFontName("simsun");
        titleFont.setBold(true);
        // titleFont.setFontHeightInPoints((short) 14);
        titleFont.setColor(IndexedColors.BLACK.index);

        XSSFCellStyle titleStyle = wb.createCellStyle();
        titleStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
        titleStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        titleStyle.setFillForegroundColor(new XSSFColor(new Color(182, 184, 192)));
        titleStyle.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
        titleStyle.setFont(titleFont);
        setBorder(titleStyle, BorderStyle.THIN, new XSSFColor(new Color(0, 0, 0)));

        Row titleRow = sheet.createRow(rowIndex);
        // titleRow.setHeightInPoints(25);
        colIndex = 0;

        for (String field : titles) {
            Cell cell = titleRow.createCell(colIndex);
            cell.setCellValue(field);
            cell.setCellStyle(titleStyle);
            colIndex++;
        }

        rowIndex++;
        return rowIndex;
    }

    private static int writeRowsToExcel(XSSFWorkbook wb, Sheet sheet, List<Object> rows, int rowIndex) {

        Font dataFont = wb.createFont();
        dataFont.setFontName("simsun");
        dataFont.setColor(IndexedColors.BLACK.index);
        XSSFCellStyle dataStyle = wb.createCellStyle();
        dataStyle.setAlignment(XSSFCellStyle.ALIGN_CENTER);
        dataStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
        dataStyle.setFont(dataFont);
        setBorder(dataStyle, BorderStyle.THIN, new XSSFColor(new Color(0, 0, 0)));
        for (Object rowData : rows) {
            Row dataRow = sheet.createRow(rowIndex);
            if (rowData instanceof ExportCapitalModel) {
                ExportCapitalModel exportCapitalModel = (ExportCapitalModel) rowData;
                capitalBean(dataRow, exportCapitalModel);
            } else if (rowData instanceof ExportProjectModel) {
                ExportProjectModel exportProjectModel = (ExportProjectModel) rowData;
                ProjectBean(dataRow, exportProjectModel);
            } else if (rowData instanceof ExcelMediaModel) {
                ExcelMediaModel excelMediaModel = (ExcelMediaModel) rowData;
                mediaBean(dataRow, excelMediaModel);
            }

            rowIndex++;
        }
        return rowIndex;
    }

    private static void mediaBean(Row dataRow, ExcelMediaModel excelMediaModel) {
        Cell cell_0 = dataRow.createCell(0);
        Cell cell_1 = dataRow.createCell(1);
        Cell cell_2 = dataRow.createCell(2);
        Cell cell_3 = dataRow.createCell(3);
        Cell cell_4 = dataRow.createCell(4);
        if (!StringUtils.isEmpty(excelMediaModel.getMtName())) {
            cell_0.setCellValue(excelMediaModel.getMtName());
        } else {
            cell_0.setCellValue("");
        }
        if (!StringUtils.isEmpty(excelMediaModel.getMtLogo())) {
            cell_1.setCellValue(excelMediaModel.getMtLogo());
        } else {
            cell_1.setCellValue("");
        }
        if (!StringUtils.isEmpty(excelMediaModel.getMtType())) {
            cell_2.setCellValue(excelMediaModel.getMtType());
        } else {
            cell_2.setCellValue("");
        }
        if (!StringUtils.isEmpty(excelMediaModel.getMtIntroduce())) {
            cell_3.setCellValue(excelMediaModel.getMtIntroduce());
        } else {
            cell_3.setCellValue("");
        }
        if (!StringUtils.isEmpty(excelMediaModel.getMtUrl())) {
            cell_4.setCellValue(excelMediaModel.getMtUrl());
        } else {
            cell_4.setCellValue("");
        }

    }

    private static void ProjectBean(Row dataRow, ExportProjectModel exportProjectModel) {
        Cell cell_0 = dataRow.createCell(0);
        Cell cell_1 = dataRow.createCell(1);
        Cell cell_2 = dataRow.createCell(2);
        Cell cell_3 = dataRow.createCell(3);
        Cell cell_4 = dataRow.createCell(4);
        Cell cell_5 = dataRow.createCell(5);
        Cell cell_6 = dataRow.createCell(6);
        Cell cell_7 = dataRow.createCell(7);
        Cell cell_8 = dataRow.createCell(8);
        Cell cell_9 = dataRow.createCell(9);
        Cell cell_10 = dataRow.createCell(10);
        Cell cell_11 = dataRow.createCell(11);
        Cell cell_12 = dataRow.createCell(12);
        Cell cell_13 = dataRow.createCell(13);
        Cell cell_14 = dataRow.createCell(14);
        Cell cell_15 = dataRow.createCell(15);
        Cell cell_16 = dataRow.createCell(16);
        Cell cell_17 = dataRow.createCell(17);
        Cell cell_18 = dataRow.createCell(18);
        Cell cell_19 = dataRow.createCell(19);
        Cell cell_20 = dataRow.createCell(20);
        Cell cell_21 = dataRow.createCell(21);
        Cell cell_22 = dataRow.createCell(22);
        Cell cell_23 = dataRow.createCell(23);
        Cell cell_24 = dataRow.createCell(24);
        Cell cell_25 = dataRow.createCell(25);
        Cell cell_26 = dataRow.createCell(26);
        Cell cell_27 = dataRow.createCell(27);
        if (!StringUtils.isEmpty(exportProjectModel.getProName())) {
            cell_0.setCellValue(exportProjectModel.getProName());
        } else {
            cell_0.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getProLogo())) {
            cell_1.setCellValue(exportProjectModel.getProLogo());
        } else {
            cell_1.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getProTokenNum())) {
            cell_2.setCellValue(exportProjectModel.getProTokenNum());
        } else {
            cell_2.setCellValue("");
        }
        if (exportProjectModel.getProStage() != null) {
            cell_3.setCellValue(exportProjectModel.getProStage());
        } else {
            cell_3.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getProConsesusMechanism())) {
            cell_4.setCellValue(exportProjectModel.getProConsesusMechanism());
        } else {
            cell_4.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getProTokenAllot())) {
            cell_5.setCellValue(exportProjectModel.getProTokenAllot());
        } else {
            cell_5.setCellValue("");
        }

        if (!StringUtils.isEmpty(exportProjectModel.getProIntroduce())) {
            cell_6.setCellValue(exportProjectModel.getProIntroduce());
        } else {
            cell_6.setCellValue("");
        }

        if (!StringUtils.isEmpty(exportProjectModel.getReTime())) {
            cell_7.setCellValue(exportProjectModel.getReTime());
        } else {
            cell_7.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getReRatio())) {
            cell_8.setCellValue(exportProjectModel.getReRatio());
        } else {
            cell_8.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getReHardTop())) {
            cell_9.setCellValue(exportProjectModel.getReHardTop());
        } else {
            cell_9.setCellValue("");
        }
        if (exportProjectModel.getReLockUp() != null) {
            cell_10.setCellValue(exportProjectModel.getReLockUp());
        } else {
            cell_10.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getRaiseTokenSum())) {
            cell_11.setCellValue(exportProjectModel.getRaiseTokenSum());
        } else {
            cell_11.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getReCapitalAllot())) {
            cell_12.setCellValue(exportProjectModel.getReCapitalAllot());
        } else {
            cell_12.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getReRemake())) {
            cell_13.setCellValue(exportProjectModel.getReRemake());
        } else {
            cell_13.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getCoOfficial())) {
            cell_14.setCellValue(exportProjectModel.getCoOfficial());
        } else {
            cell_14.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getCoBrowser())) {
            cell_15.setCellValue(exportProjectModel.getCoBrowser());
        } else {
            cell_15.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getCoWhite())) {
            cell_16.setCellValue(exportProjectModel.getCoWhite());
        } else {
            cell_16.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getCoWallet())) {
            cell_17.setCellValue(exportProjectModel.getCoWallet());
        } else {
            cell_17.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getCoWechat())) {
            cell_18.setCellValue(exportProjectModel.getCoWechat());
        } else {
            cell_18.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getTepId())) {
            cell_19.setCellValue(exportProjectModel.getTepId());
        } else {
            cell_19.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getExchange())) {
            cell_20.setCellValue(exportProjectModel.getExchange());
        } else {
            cell_20.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getJobName())) {
            cell_21.setCellValue(exportProjectModel.getJobName());
        } else {
            cell_21.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getBriefs())) {
            cell_22.setCellValue(exportProjectModel.getBriefs());
        } else {
            cell_22.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getHeadImage())) {
            cell_23.setCellValue(exportProjectModel.getHeadImage());
        } else {
            cell_23.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getCaNames())) {
            cell_24.setCellValue(exportProjectModel.getCaNames());
        } else {
            cell_24.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getPathNames())) {
            cell_25.setCellValue(exportProjectModel.getPathNames());
        } else {
            cell_25.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getDyUrl())) {
            cell_26.setCellValue(exportProjectModel.getDyUrl());
        } else {
            cell_26.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportProjectModel.getDyTitle())) {
            cell_27.setCellValue(exportProjectModel.getDyTitle());
        } else {
            cell_27.setCellValue("");
        }

    }

    private static void capitalBean(Row dataRow, ExportCapitalModel exportCapitalModel) {
        Cell cell_0 = dataRow.createCell(0);
        Cell cell_1 = dataRow.createCell(1);
        Cell cell_2 = dataRow.createCell(2);
        Cell cell_3 = dataRow.createCell(3);
        Cell cell_4 = dataRow.createCell(4);
        Cell cell_5 = dataRow.createCell(5);
        Cell cell_6 = dataRow.createCell(6);
        Cell cell_7 = dataRow.createCell(7);
        Cell cell_8 = dataRow.createCell(8);
        if (!StringUtils.isEmpty(exportCapitalModel.getCaName())) {
            cell_0.setCellValue(exportCapitalModel.getCaName());
        } else {
            cell_0.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportCapitalModel.getCaLab())) {
            cell_1.setCellValue(exportCapitalModel.getCaLab());
        } else {
            cell_1.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportCapitalModel.getCaLogo())) {
            cell_2.setCellValue(exportCapitalModel.getCaLogo());
        } else {
            cell_2.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportCapitalModel.getCaEstablish())) {
            cell_3.setCellValue(exportCapitalModel.getCaEstablish());
        } else {
            cell_3.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportCapitalModel.getCaBrief())) {
            cell_4.setCellValue(exportCapitalModel.getCaBrief());
        } else {
            cell_4.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportCapitalModel.getPartnerNameAndPosition())) {
            cell_5.setCellValue(exportCapitalModel.getPartnerNameAndPosition());
        } else {
            cell_5.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportCapitalModel.getPartnerBrief())) {
            cell_6.setCellValue(exportCapitalModel.getPartnerBrief());
        } else {
            cell_6.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportCapitalModel.getPartnerImage())) {
            cell_7.setCellValue(exportCapitalModel.getPartnerImage());
        } else {
            cell_7.setCellValue("");
        }
        if (!StringUtils.isEmpty(exportCapitalModel.getProjects())) {
            cell_8.setCellValue(exportCapitalModel.getProjects());
        } else {
            cell_8.setCellValue("");
        }
    }

    private static void autoSizeColumns(Sheet sheet, int columnNumber) {

        for (int i = 0; i < columnNumber; i++) {
            int orgWidth = sheet.getColumnWidth(i);
            sheet.autoSizeColumn(i, true);
            int newWidth = (int) (sheet.getColumnWidth(i) + 100);
            if (newWidth > orgWidth) {
                sheet.setColumnWidth(i, 10000);
            } else {
                sheet.setColumnWidth(i, 10000);
            }
        }
    }

    private static void setBorder(XSSFCellStyle style, BorderStyle border, XSSFColor color) {
        style.setBorderTop(border);
        style.setBorderLeft(border);
        style.setBorderRight(border);
        style.setBorderBottom(border);
        style.setBorderColor(XSSFCellBorder.BorderSide.TOP, color);
        style.setBorderColor(XSSFCellBorder.BorderSide.LEFT, color);
        style.setBorderColor(XSSFCellBorder.BorderSide.RIGHT, color);
        style.setBorderColor(XSSFCellBorder.BorderSide.BOTTOM, color);
    }

}
