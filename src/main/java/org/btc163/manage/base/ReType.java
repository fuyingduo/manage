package org.btc163.manage.base;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * created by fuyd on 2018/8/21
 */
@Data
@ToString
public class ReType implements Serializable {

    private int code = 0;
    private String msg = "";
    private long count;
    private List<?> data;

    public ReType() {
    }

    public ReType(long count, List<?> data) {
        this.count = count;
        this.data = data;
    }
}
