package org.btc163.manage.base;

import lombok.extern.slf4j.Slf4j;
import org.btc163.manage.exception.BusinessException;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.TimeUnit;

/**
 * created by fuyd on 2018/9/14
 */
@Slf4j
@Component
public class RedisService<T> {

    private static final String CLASS_NAME = "[RedisService]";

    @Resource
    private RedisTemplate<String, T> redisTemplate;

    /**
     * get
     *
     * @param key
     * @return
     * @throws BusinessException
     */
    public T get(String key) throws BusinessException {
        try {
            return redisTemplate.opsForValue().get(key);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[] get error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
    }

    /**
     * set
     *
     * @param key
     * @param data
     * @throws BusinessException
     */
    public void set(String key, T data) throws BusinessException {
        try {
            redisTemplate.opsForValue().set(key, data);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[] set error:{}", e.getMessage());
            throw new BusinessException(10002);
        }
    }

    /**
     * set time
     *
     * @param key
     * @param data
     * @param time
     * @param unit
     * @throws BusinessException
     */
    public void setTime(String key, T data, Integer time, TimeUnit unit) throws BusinessException {
        try {
            redisTemplate.opsForValue().set(key, data, time, unit);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[] set error:{}", e.getMessage());
            throw new BusinessException(10002);
        }
    }
}
