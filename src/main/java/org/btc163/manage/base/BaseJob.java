package org.btc163.manage.base;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * created by fuyd on 2018/9/12
 */
@Slf4j
@Component
public abstract class BaseJob implements Job {

    private static final String CLASS_NAME = "[BaseJob]";

    protected abstract String getClassName();

    protected abstract void executeJob();

    @Override
    public void execute(JobExecutionContext jobExecutionContext) {
        String className = getClassName();
        log.info(CLASS_NAME + "[execute] job name " + className);
        log.info(CLASS_NAME + "[execute] job 执行时间" + DateFormatUtils.format(new Date(), "yyyy-MM-dd HH:mm:ss"));
        this.executeJob();
    }
}
