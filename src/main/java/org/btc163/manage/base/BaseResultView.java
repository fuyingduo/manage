package org.btc163.manage.base;

import lombok.Data;

/**
 * created by fuyd on 2018/8/20
 */
@Data
public class BaseResultView {

    private static final String LOGIN_SESSION = "system/index";
    private static final String LOGIN_ERROR = "login";
    private static final String PREFIX_VIEW = "system/";

    private String view;
    private String message;

    private BaseResultView() {
    }

    public static final String view(String view) {
        return PREFIX_VIEW + view;
    }

    public static final String is_session() {
        return LOGIN_SESSION;
    }

    public static final String is_error() {
        return LOGIN_ERROR;
    }

    public static final String is_main() {
        return "/main/main";
    }

}
