package org.btc163.manage.base;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;
/**
 * created by fuyd on 2018/8/20
 */
@Data
@ToString
public class BaseResult<T> {

    private static final Integer SUCCESS_CODE = 200;
    private static final String SUCCESS_MESSAGE = "ok";

    /**
     * 返回状态码
     */
    @ApiModelProperty(value = "返回状态码")
    private Integer code;

    /**
     * 返回数据
     */
    @ApiModelProperty(value = "返回数据")
    private T data;

    /**
     * 返回信息
     */
    @ApiModelProperty(value = "返回信息")
    private String message;

    @ApiModelProperty(value = "总条数")
    private Integer totalSIze;

    public BaseResult(T data) {
        this.data = data;
        this.code = SUCCESS_CODE;
        this.message = SUCCESS_MESSAGE;
    }

    public BaseResult(T data, Integer code, String message) {
        this.code = code;
        this.data = data;
        this.message = message;
    }

    public BaseResult(T data, Integer code, String message, Integer totalSIze) {
        this.code = code;
        this.data = data;
        this.message = message;
        this.totalSIze = totalSIze;
    }
}
