package org.btc163.manage.base.menu;

import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * created by fuyd on 2018/9/8
 */
@Data
@ToString
public class MenuModel implements Serializable{
    private static final long serialVersionUID = 7358413088365110083L;
    private Long id;
    private String menuTitle;
    private String menuUrl;
    private String menuIcon;
    private Long parentId;
    private String permission;
    private Integer menuType;
    private Integer menuOrder;

    private List<MenuModel> children = new ArrayList<MenuModel>();

    public void addChild(MenuModel sysMenu) {
        children.add(sysMenu);
    }
}
