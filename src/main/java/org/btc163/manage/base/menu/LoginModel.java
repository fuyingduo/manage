package org.btc163.manage.base.menu;

import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * created by fuyd on 2018/8/21
 */
@Data
@ToString
public class LoginModel {
    private String id;
    private String username;
    private List<MenuModel> menuModels;

    public LoginModel() {
    }

    public LoginModel(String id, String username, List<MenuModel> menuModels) {
        this.id = id;
        this.username = username;
        this.menuModels = menuModels;
    }
}
