package org.btc163.manage.base.menu;

import lombok.Data;
import lombok.ToString;

import java.util.ArrayList;
import java.util.List;

/**
 * created by fuyd on 2018/9/8
 */
@Data
@ToString
public class TreeModel {

    /**
     * 级数
     */
    private int layer;
    private Long id;
    private String name;
    private Long pId;
    /**
     * 是否开启 默认开启
     */
    private boolean open = true;
    /**
     * 是否选择 checkbox状态可用 默认未选中
     */
    private boolean checked = false;
    private List<TreeModel> children = new ArrayList<>();
}
