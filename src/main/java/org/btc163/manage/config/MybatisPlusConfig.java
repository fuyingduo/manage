package org.btc163.manage.config;

import com.baomidou.mybatisplus.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.plugins.PerformanceInterceptor;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * created by fuyd on 2018/8/21
 */
@Configuration
@MapperScan("org.btc163.manage.mapper.*")
public class MybatisPlusConfig {

    /**
     * mybatis-plus 分页插件
     *
     * @return
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        paginationInterceptor.setDialectType("mysql");
        return paginationInterceptor;
    }

    @Bean
    public PerformanceInterceptor performanceInterceptordev() {
        PerformanceInterceptor performanceInterceptor = new PerformanceInterceptor();
        // sql格式化输出
        performanceInterceptor.setFormat(true);
        return performanceInterceptor;
    }

}
