package org.btc163.manage.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.ThreadPoolExecutor;

/**
 * created by fuyd on 2018/9/12
 */
@Configuration
public class TaskExecutorConfig {

    @Bean
    public ThreadPoolTaskExecutor taskExecutor() {
        ThreadPoolTaskExecutor threadPoolTaskExecutor = new ThreadPoolTaskExecutor();
        // 线程池最小维护数量
        threadPoolTaskExecutor.setCorePoolSize(3);
        // 允许的空闲时间 秒
        threadPoolTaskExecutor.setKeepAliveSeconds(200);
        // 线程池维护现成的最大数量
        threadPoolTaskExecutor.setMaxPoolSize(10);
        // 缓存队列
        threadPoolTaskExecutor.setQueueCapacity(20);
        // 对拒绝task对处理策略
        threadPoolTaskExecutor.setRejectedExecutionHandler(new ThreadPoolExecutor.CallerRunsPolicy());
        return threadPoolTaskExecutor;
    }
}
