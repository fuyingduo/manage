package org.btc163.manage.config.filter;

import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.web.filter.authz.AuthorizationFilter;
import org.apache.shiro.web.util.WebUtils;
import org.btc163.manage.entity.BUser;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * created by fuyd on 2018/8/21
 */
public class PermissionFilter extends AuthorizationFilter {

    @Override
    protected boolean isAccessAllowed(ServletRequest servletRequest, ServletResponse servletResponse,
                                      Object o) throws Exception {
        Subject sub = getSubject(servletRequest, servletResponse);
        Session session = sub.getSession();
        BUser user = (BUser) session.getAttribute("loginModel");
        if (user == null) {
            return false;
        }
        return true;
    }

    @Override
    protected boolean onAccessDenied(ServletRequest request, ServletResponse response)
            throws IOException {
        saveRequest(request);
        WebUtils.issueRedirect(request, response, "/goLogin");
        return false;
    }
}
