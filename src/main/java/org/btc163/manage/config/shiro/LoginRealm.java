package org.btc163.manage.config.shiro;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.PrincipalCollection;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ByteSource;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.RedisService;
import org.btc163.manage.base.menu.MenuModel;
import org.btc163.manage.base.menu.LoginModel;
import org.btc163.manage.constant.UserConstant;
import org.btc163.manage.entity.BUser;
import org.btc163.manage.entity.BtcUserMenu;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.BtcBUserService;
import org.btc163.manage.service.BtcMenuService;
import org.btc163.manage.service.BtcUserMenuService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * created by fuyd on 2018/8/20
 */
@Slf4j
@Service
public class LoginRealm extends AuthorizingRealm {

    @Autowired
    private BtcBUserService btcBUserService;

    @Autowired
    private BtcUserMenuService btcUserMenuService;

    @Autowired
    private BtcMenuService btcMenuService;

    @Autowired
    private RedisService<List<MenuModel>> redisService;

    /**
     * 获取认证
     *
     * @param principalCollection
     * @return
     */
    @Override
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principalCollection) {
        SimpleAuthorizationInfo info = new SimpleAuthorizationInfo();
        String name = (String) principalCollection.getPrimaryPrincipal();
        //根据用户获取角色 根据角色获取所有按钮权限
        LoginModel loginModel = (LoginModel) ShiroUtil.getSession().getAttribute("loginModel");
        for (MenuModel menuModel : loginModel.getMenuModels()) {
            if (!StringUtils.isEmpty(menuModel.getPermission()))
                info.addStringPermission(menuModel.getPermission());
        }
        return info;
    }

    /**
     * 获取授权
     *
     * @param authenticationToken
     * @return
     * @throws AuthenticationException
     */
    @Override
    protected AuthenticationInfo doGetAuthenticationInfo(AuthenticationToken authenticationToken)
            throws AuthenticationException {
        UsernamePasswordToken upToken = (UsernamePasswordToken) authenticationToken;
        String username = (String) authenticationToken.getPrincipal();
        List<BUser> users = null;
        BUser s = null;
        try {
            Map<String, Object> map = new HashMap<>();
            map.put("username", username);
            users = btcBUserService.selectByMap(map);
            if (CollectionUtils.isEmpty(users)) {
                log.error(" error: 用户不存在!!!");
                throw new UnknownAccountException("用户不存在");
            }
        } catch (Exception e) {
            throw new UnknownAccountException("用户不存在");
        }
        s = users.get(0);
        if (s == null) {
            throw new UnknownAccountException("用户不存在");
        } else {
            BUser user = new BUser(s.getId(), s.getUsername(), s.getPassword(), s.getCreateTime(), s.getUpdateTime(), s.getUserStatus());
            Subject subject = ShiroUtil.getSubject();
            Session session = subject.getSession();
            List<MenuModel> menuList = null;
            try {
                menuList = redisService.get(UserConstant.USER_MENU_INFO + user.getId());
            } catch (BusinessException e) {
                log.error("error:{}", e.getMessage());
            }
            if (CollectionUtils.isEmpty(menuList)) {
                menuList = new ArrayList<>();
                BtcUserMenu btcUserMenu = new BtcUserMenu();
                btcUserMenu.setUserId(user.getId());
                EntityWrapper<BtcUserMenu> userMenuEntityWrapper = new EntityWrapper<>(btcUserMenu);
                List<BtcUserMenu> btcUserMenus = btcUserMenuService.selectList(userMenuEntityWrapper);
                if (!CollectionUtils.isEmpty(btcUserMenus)) {
                    for (BtcUserMenu userMenu : btcUserMenus) {
                        MenuModel menuModel = new MenuModel();
                        BeanUtils.copyProperties(btcMenuService.selectById(userMenu.getMenuId()), menuModel);
                        menuList.add(menuModel);
                    }
                    try {
                        redisService.set(UserConstant.USER_MENU_INFO + user.getId(), menuList);
                    } catch (BusinessException e) {
                        log.error("error:{}", e.getMessage());
                    }
                }
            }
            // 菜单
            LoginModel loginModel = new LoginModel(user.getId(), user.getUsername(), menuList);
            session.setAttribute("loginModel", loginModel);
            List<MenuModel> btcMenus = new ArrayList<>(new HashSet<>(menuList));
            if (!CollectionUtils.isEmpty(btcMenus)) {
                JSONArray menuJsonByUser = btcMenuService.getMenuJsonByUser(btcMenus);
                session.setAttribute("menu", menuJsonByUser);
            }


        }
        ByteSource byteSource = ByteSource.Util.bytes(username);
        return new SimpleAuthenticationInfo(username, s.getPassword(), byteSource, getName());
    }

}
