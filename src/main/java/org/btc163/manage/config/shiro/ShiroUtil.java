package org.btc163.manage.config.shiro;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.btc163.manage.entity.BUser;

/**
 * created by fuyd on 2018/8/21
 */
public class ShiroUtil {

    public static Subject getSubject() {
        return SecurityUtils.getSubject();
    }

    public static Session getSession() {
        return getSubject().getSession();
    }

    public static BUser getBUser() {
        return (BUser) getSession().getAttribute("bUser");
    }
}
