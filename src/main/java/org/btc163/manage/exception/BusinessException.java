package org.btc163.manage.exception;

import lombok.Data;
import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.PropertiesConfiguration;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

@Data
public class BusinessException extends Exception {

    private static final long serialVersionUID = -4648879327187605014L;
    private static Map<Integer, String> CODES_MAP = null;
    private static final String DEFAULT_ERROR_CODE_MESSAGE = "系统内部错误";
    private static final String UNDEFINED_ERROR = "未定义错误";
    private static final String EXCEPTION_MESSAGE_PROPERTIES = "business-exceptions.properties";
    private final static Logger logger = LoggerFactory.getLogger(BusinessException.class);

    /**
     * 本异常错误代码
     */
    private Integer errorCode = 500;

    static {
        BusinessException.init();
    }

    private synchronized static void init() {
        if (null == BusinessException.CODES_MAP) {
            BusinessException.CODES_MAP = new HashMap<Integer, String>();
            BusinessException.CODES_MAP.put(0, BusinessException.DEFAULT_ERROR_CODE_MESSAGE);
            PropertiesConfiguration config = null;
            try {
                config = new PropertiesConfiguration();
                config.setEncoding("utf8");
                config.load(BusinessException.EXCEPTION_MESSAGE_PROPERTIES);
            } catch (ConfigurationException e) {
                BusinessException.logger.error("[business exception] get pro file error:", e);
            }
            BusinessException.logger.info("[business exception] init start...");
            if (config != null) {
                Iterator<?> keyIterator = config.getKeys();
                while (keyIterator.hasNext()) {
                    String key = (String) keyIterator.next();
                    if (NumberUtils.isNumber(key)) {
                        BusinessException.CODES_MAP.put(NumberUtils.createInteger(key), config.getString(key));
                        BusinessException.logger.info("[business exception] add exception:code=" + key + ",message=" + config.getString(key));
                    } else {
                        BusinessException.logger.info("[business exception] add exception:code=" + key + ",message=" + config.getString(key));
                    }
                }
            }
            BusinessException.logger.info("[business exception] init exception map suc.");
        }
    }

    public BusinessException(Integer errorCode) {
        super();
        this.errorCode = errorCode;
    }

    @Override
    public String getMessage() {
        return getLocalizedMessage();
    }

    @Override
    public String getLocalizedMessage() {
        String localMessage = BusinessException.CODES_MAP.get(this.errorCode);
        if (StringUtils.isEmpty(BusinessException.CODES_MAP.get(this.errorCode))) {
            localMessage = UNDEFINED_ERROR;
        }
        return localMessage;
    }
}
