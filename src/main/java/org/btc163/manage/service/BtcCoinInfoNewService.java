package org.btc163.manage.service;

import org.btc163.manage.controller.param.CoinOnfiNewParam;
import org.btc163.manage.entity.CoinInfoNew;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.exception.BusinessException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-21
 */
public interface BtcCoinInfoNewService extends IService<CoinInfoNew> {

    String selectCoinInfoNewsListPage(CoinOnfiNewParam param);

    void addCoinInfo(CoinOnfiNewParam coinOnfiNewParam) throws BusinessException;

    void updateCoinInfo(CoinOnfiNewParam coinOnfiNewParam) throws BusinessException;

    void delCoinInfo(String enKey) throws BusinessException;
}
