package org.btc163.manage.service.base;

import lombok.extern.slf4j.Slf4j;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.utils.FileUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * created by fuyd on 2018/8/22
 */
@Slf4j
@Service
public class ImageUploadService {

    private static final String CLASS_NAME = "[ImageUploadService]";

    @Value("${image.logo.path}")
    private String imagePath;

    public synchronized String imageUpload(MultipartFile multipartFile) throws BusinessException {
        try {
            String imageName = multipartFile.getOriginalFilename();
            long imageSize = multipartFile.getSize();
            InputStream in = multipartFile.getInputStream();
            String time = System.currentTimeMillis() + "";
            String random = (Math.random() * 1000000 + "").substring(0, 4);
            String imName = "LOGO" + time + random + rsImageType(multipartFile.getContentType());
            log.info(CLASS_NAME + "[imageUpload] image upload imName:{}", imName);
            File file = new File(imagePath + imName);
            if (!FileUtil.uploadFileToServer(multipartFile.getInputStream(), imagePath, imName)) {
                throw new BusinessException(10006);
            }
            in.close();
            return imName;
        } catch (IOException e) {
            log.error(CLASS_NAME + "[imageUpdate] error:{}", e.getMessage());
            throw new BusinessException(10006);
        }
    }

    private String rsImageType(String type) {
        if (type.equals("image/png")) {
            return ".png";
        } else {
            return ".jpg";
        }
    }

    public static void main(String[] args) {
        String ss = (Math.random() * 10000 + "").substring(0, 4);
        System.out.println(ss);
    }

}
