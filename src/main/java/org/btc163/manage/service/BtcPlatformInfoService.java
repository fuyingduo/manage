package org.btc163.manage.service;

import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.controller.model.PlatformInfoSelectModel;
import org.btc163.manage.controller.param.PlatformParam;
import org.btc163.manage.entity.PlatformInfo;
import org.btc163.manage.exception.BusinessException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-21
 */
public interface BtcPlatformInfoService extends IService<PlatformInfo> {

    List<PlatformInfoSelectModel> getPlatformInfoSelect() throws BusinessException;

    String getPlatFormList(PlatformParam platformParam);

    Boolean updatePlatformInfo(PlatformInfo info);

}
