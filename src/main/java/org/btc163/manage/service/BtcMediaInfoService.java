package org.btc163.manage.service;

import org.btc163.manage.entity.BtcMediaInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-31
 */
public interface BtcMediaInfoService extends IService<BtcMediaInfo> {

}
