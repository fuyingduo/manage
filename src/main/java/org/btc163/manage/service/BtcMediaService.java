package org.btc163.manage.service;

import org.btc163.manage.app.model.MediaListModel;
import org.btc163.manage.app.model.MediaModel;
import org.btc163.manage.controller.param.MediaParam;
import org.btc163.manage.entity.BtcMedia;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.exception.BusinessException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-31
 */
public interface BtcMediaService extends IService<BtcMedia> {

    Integer getMediaListCount(Integer type) throws BusinessException;

    List<MediaListModel> getMediaList(Integer page, Integer pageSize, Integer type) throws BusinessException;

    MediaModel findMediaInfo(String id) throws BusinessException;

    String getMediaList(MediaParam mediaParam) throws BusinessException;

    void addMediaInfo(MediaParam mediaParam) throws BusinessException;

    void editMediaInfo(MediaParam mediaParam) throws BusinessException;

}
