package org.btc163.manage.service;

import org.btc163.manage.entity.ProCa;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.exception.BusinessException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
public interface ProCaService extends IService<ProCa> {

    void addProAndCaInfo(String caId, List<String> proIds) throws BusinessException;

    void addCaAndProInfo(String proId, List<String> caIds) throws BusinessException;

    List<ProCa> getProCaListByProId(String proId) throws BusinessException;

}
