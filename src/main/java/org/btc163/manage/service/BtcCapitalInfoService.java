package org.btc163.manage.service;

import org.btc163.manage.controller.model.BtcCapitalInfoModel;
import org.btc163.manage.entity.BtcCapitalInfo;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.exception.BusinessException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
public interface BtcCapitalInfoService extends IService<BtcCapitalInfo> {

    void addCapitalInfos(List<BtcCapitalInfoModel> btcCapitalInfoModels, String capitalId) throws BusinessException;

}
