package org.btc163.manage.service;

import org.btc163.manage.entity.BtcMtPm;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-04
 */
public interface BtcMtPmService extends IService<BtcMtPm> {

}
