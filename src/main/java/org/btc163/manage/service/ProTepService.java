package org.btc163.manage.service;

import org.btc163.manage.entity.ProTep;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.exception.BusinessException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
public interface ProTepService extends IService<ProTep> {

    void saveProAndTepData(String proId, List<Long> tepId) throws BusinessException;

    List<Long> getProTepInfosByProId(String proId) throws BusinessException;

    List<ProTep> getProTepsByProId(String proId) throws BusinessException;
}
