package org.btc163.manage.service;

import org.btc163.manage.controller.param.WalletParam;
import org.btc163.manage.entity.BtcWallet;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.exception.BusinessException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-05
 */
public interface BtcWalletService extends IService<BtcWallet> {

    String getWalletPageList(WalletParam walletParam) throws BusinessException;

    void addWalletInfo(WalletParam walletParam) throws BusinessException;

    void editWalletInfo(WalletParam walletParam) throws BusinessException;
}
