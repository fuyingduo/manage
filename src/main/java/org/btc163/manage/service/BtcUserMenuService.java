package org.btc163.manage.service;

import org.btc163.manage.entity.BtcUserMenu;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-08
 */
public interface BtcUserMenuService extends IService<BtcUserMenu> {

}
