package org.btc163.manage.service;

import org.btc163.manage.app.model.ProjectInfoModel;
import org.btc163.manage.app.model.ProjectListModel;
import org.btc163.manage.controller.model.BtcProjectSelectModel;
import org.btc163.manage.controller.param.BtcProjectParam;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.controller.param.ProjectParam;
import org.btc163.manage.entity.BtcProject;
import org.btc163.manage.exception.BusinessException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
public interface BtcProjectService extends IService<BtcProject> {

    void addProject(BtcProjectParam param) throws BusinessException;

    List<BtcProjectSelectModel> getAllProjects() throws BusinessException;

    String getProjectList(ProjectParam projectParam) throws BusinessException;

    List<ProjectListModel> getAppProjectInfos(Integer page, Integer pageSize, List<String> proStages, List<Long> tepNames, String proName) throws BusinessException;

    Integer getAppProjectInCount(List<String> proStages, List<Long> tepNames) throws BusinessException;

    ProjectInfoModel findAppProjectInfos(String proId) throws BusinessException;

    void editProject(BtcProjectParam param) throws BusinessException;
}
