package org.btc163.manage.service;

import org.btc163.manage.controller.param.MillParam;
import org.btc163.manage.entity.BtcMill;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.exception.BusinessException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-05
 */
public interface BtcMillService extends IService<BtcMill> {

    String getMillPageList(MillParam millParam) throws BusinessException;

    void addMillInfo(MillParam millParam) throws BusinessException;

    void editMillInfo(MillParam millParam) throws BusinessException;
}
