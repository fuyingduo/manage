package org.btc163.manage.service;

import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.controller.model.BtcProjectInfoModel;
import org.btc163.manage.entity.BtcProjectInfo;
import org.btc163.manage.exception.BusinessException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
public interface BtcProjectInfoService extends IService<BtcProjectInfo> {

    void addProjectInfos(List<BtcProjectInfoModel> btcProjectInfos, String projectId) throws BusinessException;

    List<BtcProjectInfo> getBtcProjectInfosByProId(String proId) throws BusinessException;

}
