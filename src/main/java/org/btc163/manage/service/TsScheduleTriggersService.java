package org.btc163.manage.service;

import org.btc163.manage.entity.TsScheduleTriggers;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.exception.BusinessException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-13
 */
public interface TsScheduleTriggersService extends IService<TsScheduleTriggers> {

    void refreshTrigger();

    Boolean createScheduleJob(String cron, String jobName) throws BusinessException;
}
