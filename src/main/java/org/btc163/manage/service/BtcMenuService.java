package org.btc163.manage.service;

import com.alibaba.fastjson.JSONArray;
import org.btc163.manage.base.menu.MenuModel;
import org.btc163.manage.entity.BtcMenu;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-03
 */
public interface BtcMenuService extends IService<BtcMenu> {

    JSONArray getMenuJsonByUser(List<MenuModel> menuList);

    JSONArray getTree(String uid);
}
