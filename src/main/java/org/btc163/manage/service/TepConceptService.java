package org.btc163.manage.service;

import org.btc163.manage.entity.TepConcept;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.exception.BusinessException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
public interface TepConceptService extends IService<TepConcept> {

    List<TepConcept> getTepConceptServices() throws BusinessException;

    List<TepConcept> findTepConcepyByIds(List<Long> tepIds) throws BusinessException;

}
