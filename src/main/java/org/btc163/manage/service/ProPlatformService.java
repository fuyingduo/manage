package org.btc163.manage.service;

import org.btc163.manage.entity.ProPlatform;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.exception.BusinessException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-24
 */
public interface ProPlatformService extends IService<ProPlatform> {

    void addProAndPlatformInfos(String proIds, List<String> platforms) throws BusinessException;

    List<ProPlatform> getProPlatformInfos(String proIds) throws BusinessException;
}
