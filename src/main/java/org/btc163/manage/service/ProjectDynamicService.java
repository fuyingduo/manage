package org.btc163.manage.service;

import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.controller.model.ProjectDynamicModel;
import org.btc163.manage.entity.ProjectDynamic;
import org.btc163.manage.exception.BusinessException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
public interface ProjectDynamicService extends IService<ProjectDynamic> {

    void addProjectDynamics(List<ProjectDynamicModel> projectDynamics, String projectId) throws BusinessException;

}
