package org.btc163.manage.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.controller.model.BtcCapitalInfoModel;
import org.btc163.manage.entity.BtcCapitalInfo;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.BtcCapitalInfoMapper;
import org.btc163.manage.service.BtcCapitalInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
@Slf4j
@Service
public class BtcCapitalInfoServiceImpl extends ServiceImpl<BtcCapitalInfoMapper, BtcCapitalInfo> implements BtcCapitalInfoService {

    private static final String CLASS_NAME = "[BtcCapitalInfoServiceImpl]";

    @Autowired
    private BtcCapitalInfoMapper btcCapitalInfoMapper;

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void addCapitalInfos(List<BtcCapitalInfoModel> btcCapitalInfoModels, String capitalId) throws BusinessException {
        if (CollectionUtils.isEmpty(btcCapitalInfoModels)) {
            log.error(CLASS_NAME + "[addCapitalInfos] request is empty");
            return;
        }
        for (BtcCapitalInfoModel btcCapitalInfoModel : btcCapitalInfoModels) {
            if (btcCapitalInfoModel == null) {
                continue;
            }
            if (StringUtils.isEmpty(btcCapitalInfoModel.getInBrief()) &&
                    StringUtils.isEmpty(btcCapitalInfoModel.getInHeadImage()) && StringUtils.isEmpty(btcCapitalInfoModel.getInUserName())) {
                continue;
            }
            BtcCapitalInfo btcCapitalInfo = new BtcCapitalInfo();
            BeanUtils.copyProperties(btcCapitalInfoModel, btcCapitalInfo);
            btcCapitalInfo.setId(UidUtil.getId());
            btcCapitalInfo.setCaId(capitalId);
            try {
                if (btcCapitalInfoMapper.insert(btcCapitalInfo) <= 0) {
                    log.error(CLASS_NAME + "[addCapitalInfos] save data error");
                    throw new BusinessException(10002);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[addCapitalInfos] save data error");
                throw new BusinessException(10002);
            }
        }

    }
}
