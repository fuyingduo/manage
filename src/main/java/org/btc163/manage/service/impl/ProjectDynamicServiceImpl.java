package org.btc163.manage.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.controller.model.ProjectDynamicModel;
import org.btc163.manage.entity.ProjectDynamic;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.ProjectDynamicMapper;
import org.btc163.manage.service.ProjectDynamicService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
@Slf4j
@Service
public class ProjectDynamicServiceImpl extends ServiceImpl<ProjectDynamicMapper, ProjectDynamic> implements ProjectDynamicService {

    private static final String CLASS_NAME = "[ProjectDynamicServiceImpl]";

    @Autowired
    private ProjectDynamicMapper projectDynamicMapper;

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void addProjectDynamics(List<ProjectDynamicModel> projectDynamics, String projectId) throws BusinessException {
        if (CollectionUtils.isEmpty(projectDynamics) || StringUtils.isEmpty(projectId)) {
            log.error(CLASS_NAME + "[addProjectDynamics] request data is empty");
            throw new BusinessException(10001);
        }
        for (ProjectDynamicModel projectDynamic : projectDynamics) {
            if (projectDynamic == null) {
                continue;
            }
            if (StringUtils.isEmpty(projectDynamic.getDyTitle()) && StringUtils.isEmpty(projectDynamic.getDyUrl())) {
                continue;
            }
            ProjectDynamic dynamic = new ProjectDynamic();
            BeanUtils.copyProperties(projectDynamic, dynamic);
            dynamic.setProId(projectId);
            dynamic.setId(UidUtil.getId());
            if (projectDynamicMapper.insert(dynamic) <= 0) {
                log.error(CLASS_NAME + "[addProjectDynamics] data save error projectDynamic:{}", projectDynamic.toString());
                throw new BusinessException(10002);
            }
        }
    }
}
