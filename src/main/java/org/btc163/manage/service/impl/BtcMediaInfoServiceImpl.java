package org.btc163.manage.service.impl;

import org.btc163.manage.entity.BtcMediaInfo;
import org.btc163.manage.mapper.BtcMediaInfoMapper;
import org.btc163.manage.service.BtcMediaInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-31
 */
@Service
public class BtcMediaInfoServiceImpl extends ServiceImpl<BtcMediaInfoMapper, BtcMediaInfo> implements BtcMediaInfoService {

}
