package org.btc163.manage.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.entity.TepConcept;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.TepConceptMapper;
import org.btc163.manage.service.TepConceptService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
@Slf4j
@Service
public class TepConceptServiceImpl extends ServiceImpl<TepConceptMapper, TepConcept> implements TepConceptService {

    private static final String CLASS_NAME = "[TepConceptServiceImpl]";

    @Autowired
    private TepConceptMapper tepConceptMapper;

    @Override
    public List<TepConcept> getTepConceptServices() throws BusinessException {
        List<TepConcept> tepConcepts = null;
        try {
            tepConcepts = tepConceptMapper.selectList(new EntityWrapper<TepConcept>());
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getTepConceptServices] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        return tepConcepts;
    }

    @Override
    public List<TepConcept> findTepConcepyByIds(List<Long> tepIds) throws BusinessException {
        if (CollectionUtils.isEmpty(tepIds)) {
            log.error(CLASS_NAME + "[findTepConcepyByIds] request data error");
            throw new BusinessException(10001);
        }
        List<TepConcept> tepConcepts = new ArrayList<>();
        for (Long tepId : tepIds) {
            try {
                TepConcept tepConcept = tepConceptMapper.selectById(tepId);
                tepConcepts.add(tepConcept);
            } catch (Exception e) {
                log.error(CLASS_NAME + "[] select data error:{}", e.getMessage());
                throw new BusinessException(10003);
            }

        }
        return tepConcepts;
    }
}
