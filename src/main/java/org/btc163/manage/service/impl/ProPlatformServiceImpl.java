package org.btc163.manage.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.entity.ProPlatform;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.ProPlatformMapper;
import org.btc163.manage.service.ProPlatformService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-24
 */
@Slf4j
@Service
public class ProPlatformServiceImpl extends ServiceImpl<ProPlatformMapper, ProPlatform> implements ProPlatformService {

    private static final String CLASS_NAME = "[ProPlatformServiceImpl]";

    @Autowired
    private ProPlatformMapper proPlatformMapper;

    @Override
    public void addProAndPlatformInfos(String proIds, List<String> platforms) throws BusinessException {
        if (StringUtils.isEmpty(proIds) || CollectionUtils.isEmpty(platforms)) {
            log.error(CLASS_NAME + "[addProAndPlatformInfos] request data is empty");
            throw new BusinessException(10001);
        }
        for (String platform : platforms) {
            ProPlatform proPlatform = new ProPlatform();
            proPlatform.setId(UidUtil.getId());
            proPlatform.setProId(proIds);
            proPlatform.setPlatId(platform);
            try {
                if (proPlatformMapper.insert(proPlatform) <= 0) {
                    log.error(CLASS_NAME + "[addProAndPlatformInfos] save data error");
                    throw new BusinessException(10002);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[addProAndPlatformInfos] error:{}", e.getMessage());
                throw new BusinessException(10002);
            }
        }
    }

    @Override
    public List<ProPlatform> getProPlatformInfos(String proIds) throws BusinessException {
        if (StringUtils.isEmpty(proIds)) {
            log.error(CLASS_NAME + "[getProPlatformInfos] request data error");
            throw new BusinessException(10001);
        }
        ProPlatform proPlatform = new ProPlatform();
        proPlatform.setProId(proIds);
        EntityWrapper<ProPlatform> entityWrapper = new EntityWrapper<>(proPlatform);
        try {
            return proPlatformMapper.selectList(entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getProPlatformInfos] error:{}", e.getMessage());
        }
        return null;
    }
}
