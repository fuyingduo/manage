package org.btc163.manage.service.impl;

import org.btc163.manage.entity.BtcWalletInfo;
import org.btc163.manage.mapper.BtcWalletInfoMapper;
import org.btc163.manage.service.BtcWalletInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-05
 */
@Service
public class BtcWalletInfoServiceImpl extends ServiceImpl<BtcWalletInfoMapper, BtcWalletInfo> implements BtcWalletInfoService {

}
