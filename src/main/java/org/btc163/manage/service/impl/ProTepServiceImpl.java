package org.btc163.manage.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.entity.ProTep;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.ProTepMapper;
import org.btc163.manage.service.ProTepService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
@Slf4j
@Service
public class ProTepServiceImpl extends ServiceImpl<ProTepMapper, ProTep> implements ProTepService {

    private static final String CLASS_NAME = "[ProTepServiceImpl]";

    @Autowired
    private ProTepMapper proTepMapper;

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void saveProAndTepData(String proId, List<Long> tepIds) throws BusinessException {
        if (StringUtils.isEmpty(proId) || CollectionUtils.isEmpty(tepIds)) {
            log.error(CLASS_NAME + "[saveProAndTepData] request error data is empty");
            throw new BusinessException(10001);
        }
        try {
            for (Long tepId : tepIds) {
                ProTep proTep = new ProTep();
                proTep.setId(UidUtil.getId());
                proTep.setProId(proId);
                proTep.setTepId(tepId);
                if (proTepMapper.insert(proTep) <= 0) {
                    log.error(CLASS_NAME + "[saveProAndTepData] data save error");
                    throw new BusinessException(10002);
                }
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[saveProAndTepData] error:{}", e.getMessage());
            throw new BusinessException(10002);
        }

    }

    @Override
    public List<Long> getProTepInfosByProId(String proId) throws BusinessException {
        if (StringUtils.isEmpty(proId)) {
            log.error(CLASS_NAME + "[getProTepInfosByProId] request data is empty");
            throw new BusinessException(10001);
        }
        ProTep proTep = new ProTep();
        proTep.setProId(proId);
        EntityWrapper<ProTep> entityWrapper = new EntityWrapper<ProTep>(proTep);
        List<Long> teps = new ArrayList<>();
        try {
            List<ProTep> proTeps = proTepMapper.selectList(entityWrapper);
            for (ProTep tep : proTeps) {
                teps.add(tep.getTepId());
            }
            return teps;
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getProTepInfosByProId] error:{}", e.getMessage());
            return null;
        }
    }

    @Override
    public List<ProTep> getProTepsByProId(String proId) throws BusinessException {
        if (StringUtils.isEmpty(proId)) {
            log.error(CLASS_NAME + "[getProTepsByProId] request data error ");
            throw new BusinessException(10001);
        }
        ProTep proTep = new ProTep();
        proTep.setProId(proId);
        EntityWrapper<ProTep> entityWrapper = new EntityWrapper<ProTep>(proTep);
        try {
            return proTepMapper.selectList(entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getProTepInfosByProId] error:{}", e.getMessage());
            return null;
        }
    }
}
