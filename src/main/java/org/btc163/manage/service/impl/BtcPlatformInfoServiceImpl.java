package org.btc163.manage.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.ReType;
import org.btc163.manage.controller.model.PlatformInfoSelectModel;
import org.btc163.manage.controller.param.PlatformParam;
import org.btc163.manage.entity.PlatformInfo;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.BtcPlatformInfoMapper;
import org.btc163.manage.service.BtcPlatformInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-21
 */
@Slf4j
@Service
public class BtcPlatformInfoServiceImpl extends ServiceImpl<BtcPlatformInfoMapper, PlatformInfo> implements BtcPlatformInfoService {

    private static final String CLASS_NAME = "[BtcPlatformInfoServiceImpl]";

    @Autowired
    private BtcPlatformInfoMapper btcPlatformInfoMapper;

    @Override
    public List<PlatformInfoSelectModel> getPlatformInfoSelect() throws BusinessException {
        List<PlatformInfo> platformInfos = null;
        try {
            platformInfos = btcPlatformInfoMapper.selectList(new EntityWrapper<PlatformInfo>());
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getPlatformInfoSelect] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (CollectionUtils.isEmpty(platformInfos)) {
            log.error(CLASS_NAME + "[getPlatformInfoSelect] select data is empty");
            throw new BusinessException(10003);
        }
        List<PlatformInfoSelectModel> models = new ArrayList<>();
        for (PlatformInfo platformInfo : platformInfos) {
            PlatformInfoSelectModel model = new PlatformInfoSelectModel();
            BeanUtils.copyProperties(platformInfo, model);
            models.add(model);
        }
        return models;
    }

    @Override
    public String getPlatFormList(PlatformParam platformParam) {
        PlatformInfo platformInfo = new PlatformInfo();
        BeanUtils.copyProperties(platformParam, platformInfo);
        Page<PlatformInfo> page = new Page(platformParam.getPage(), platformParam.getLimit());
        EntityWrapper<PlatformInfo> platformInfoEntityWrapper = new EntityWrapper<>(platformInfo);
        Page<PlatformInfo> pages = selectPage(page, platformInfoEntityWrapper);
        List<PlatformInfo> platformInfos = btcPlatformInfoMapper.selectPage(pages, platformInfoEntityWrapper);
        pages.setRecords(platformInfos);
        pages.setTotal(btcPlatformInfoMapper.selectCount(platformInfoEntityWrapper));
        if (CollectionUtils.isEmpty(platformInfos)) {
            return JSON.toJSONString(new ReType(0L, platformInfos));
        }
        ReType reType = new ReType(page.getTotal(), platformInfos);
        return JSON.toJSONString(reType);
    }

    @Override
    public Boolean updatePlatformInfo(PlatformInfo info) {
        try {
            if (btcPlatformInfoMapper.updatePlatformInfo(info) > 0) {
                return true;
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[] error:{}", e.getMessage());
        }
        return false;
    }
}
