package org.btc163.manage.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.menu.MenuModel;
import org.btc163.manage.base.menu.TreeModel;
import org.btc163.manage.entity.BtcMenu;
import org.btc163.manage.entity.BtcUserMenu;
import org.btc163.manage.mapper.BtcMenuMapper;
import org.btc163.manage.service.BtcMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.service.BtcUserMenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-03
 */
@Service
public class BtcMenuServiceImpl extends ServiceImpl<BtcMenuMapper, BtcMenu> implements BtcMenuService {

    @Autowired
    private BtcMenuMapper btcMenuMapper;

    @Autowired
    private BtcUserMenuService btcUserMenuService;

    @Override
    public JSONArray getMenuJsonByUser(List<MenuModel> menuList) {
        JSONArray jsonArr = new JSONArray();
        List<MenuModel> menuModels = new ArrayList<>();
        for (MenuModel menu : menuList) {
            if (menu.getParentId() == null) {
                MenuModel btcMenu = getChilds(menu, menuList);
                menuModels.add(btcMenu);
            }
        }
        if (!CollectionUtils.isEmpty(menuModels)) {
            menuModels.sort((o1, o2) -> o2.getMenuOrder().compareTo(o1.getMenuOrder()));
            menuModels.forEach(menuModel -> jsonArr.add(menuModel));
        }
        return jsonArr;
    }

    @Override
    public JSONArray getTree(String uid) {
        JSONArray jsonArr = new JSONArray();
        BtcMenu btcMenu = new BtcMenu();
        btcMenu.setMenuType(0);
        EntityWrapper<BtcMenu> entityWrapper = new EntityWrapper<>(btcMenu);
        entityWrapper.orderAsc(Arrays.asList("menu_order"));
        List<BtcMenu> menuList = btcMenuMapper.selectList(entityWrapper);
        if (!CollectionUtils.isEmpty(menuList)) {
            for (BtcMenu menu : menuList) {
                TreeModel childByTree = getChildByTree(menu, 0, uid);
                jsonArr.add(childByTree);
            }
        }
        return jsonArr;
    }

    private TreeModel getChildByTree(BtcMenu menu, int layer, String uid) {
        layer++;
        // 子节点
        BtcMenu childMenu = new BtcMenu();
        childMenu.setParentId(menu.getId());
        TreeModel treeModel = new TreeModel();
        treeModel.setId(menu.getId());
        treeModel.setName(menu.getMenuTitle());
        treeModel.setLayer(layer);
        treeModel.setPId(menu.getParentId());
        if (!StringUtils.isEmpty(uid)) {
            BtcUserMenu btcUserMenu = new BtcUserMenu();
            btcUserMenu.setMenuId(menu.getId());
            btcUserMenu.setUserId(uid);
            EntityWrapper<BtcUserMenu> entityWrapper = new EntityWrapper<>(btcUserMenu);
            int count = btcUserMenuService.selectCount(entityWrapper);
            if (count > 0) {
                treeModel.setChecked(true);
            }
        }
        EntityWrapper<BtcMenu> btcMenuEntityWrapper = new EntityWrapper<>(childMenu);
        List<BtcMenu> childMenus = btcMenuMapper.selectList(btcMenuEntityWrapper);
        if (!CollectionUtils.isEmpty(childMenus)) {
            for (BtcMenu childMenu1 : childMenus) {
                TreeModel childByTree = getChildByTree(childMenu1, layer, uid);
                treeModel.getChildren().add(childByTree);
            }
        }
        return treeModel;
    }


    private MenuModel getChilds(MenuModel menu, List<MenuModel> menuList) {
        for (MenuModel menus : menuList) {
            if (menu.getId().equals(menus.getParentId()) && menus.getMenuType() == 1) {
                menu.addChild(menus);
            }
        }
        return menu;

    }
}
