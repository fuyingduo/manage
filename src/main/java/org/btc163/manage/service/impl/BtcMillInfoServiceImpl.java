package org.btc163.manage.service.impl;

import org.btc163.manage.entity.BtcMillInfo;
import org.btc163.manage.mapper.BtcMillInfoMapper;
import org.btc163.manage.service.BtcMillInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-05
 */
@Service
public class BtcMillInfoServiceImpl extends ServiceImpl<BtcMillInfoMapper, BtcMillInfo> implements BtcMillInfoService {

}
