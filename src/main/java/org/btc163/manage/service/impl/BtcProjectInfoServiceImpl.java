package org.btc163.manage.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.controller.model.BtcProjectInfoModel;
import org.btc163.manage.entity.BtcProjectInfo;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.BtcProjectInfoMapper;
import org.btc163.manage.service.BtcProjectInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
@Slf4j
@Service
public class BtcProjectInfoServiceImpl extends ServiceImpl<BtcProjectInfoMapper, BtcProjectInfo> implements BtcProjectInfoService {

    private static final String CLASS_NAME = "[BtcProjectInfoServiceImpl]";

    @Autowired
    private BtcProjectInfoMapper btcProjectInfoMapper;

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void addProjectInfos(List<BtcProjectInfoModel> btcProjectInfos, String projectId) throws BusinessException {
        if (CollectionUtils.isEmpty(btcProjectInfos) || StringUtils.isEmpty(projectId)) {
            log.error(CLASS_NAME + "[addProjectInfos] request param is empty!");
            throw new BusinessException(10001);
        }
        try {
            for (BtcProjectInfoModel btcProjectInfo : btcProjectInfos) {
                if (btcProjectInfo == null) {
                    continue;
                }
                if (StringUtils.isEmpty(btcProjectInfo.getInBrief()) && StringUtils.isEmpty(btcProjectInfo.getInHeadImage())
                        && StringUtils.isEmpty(btcProjectInfo.getInPosition()) && StringUtils.isEmpty(btcProjectInfo.getInUserName())) {
                    continue;
                }
                BtcProjectInfo info = new BtcProjectInfo();
                BeanUtils.copyProperties(btcProjectInfo, info);
                info.setProId(projectId);
                info.setUpdateTime(new Date());
                info.setId(UidUtil.getId());
                if (btcProjectInfoMapper.insert(info) <= 0) {
                    log.error(CLASS_NAME + "[addProjectInfos] data save error btcProjectInfo:{}", btcProjectInfo.toString());
                    throw new BusinessException(10002);
                }
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[addProjectInfos] error:{}", e.getMessage());
            throw new BusinessException(10002);
        }
    }

    @Override
    public List<BtcProjectInfo> getBtcProjectInfosByProId(String proId) throws BusinessException {
        if (StringUtils.isEmpty(proId)) {
            log.error(CLASS_NAME + "[getBtcProjectInfosByProId] request data is empty");
            throw new BusinessException(10001);
        }
        BtcProjectInfo btcProjectInfo = new BtcProjectInfo();
        btcProjectInfo.setProId(proId);
        EntityWrapper<BtcProjectInfo> entityWrapper = new EntityWrapper<>(btcProjectInfo);
        try {
            return btcProjectInfoMapper.selectList(entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getBtcProjectInfosByProId] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
    }

}
