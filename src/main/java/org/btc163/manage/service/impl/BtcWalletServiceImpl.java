package org.btc163.manage.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.ReType;
import org.btc163.manage.controller.model.BtcWalletInfoModel;
import org.btc163.manage.controller.param.WalletParam;
import org.btc163.manage.controller.view.WalletView;
import org.btc163.manage.entity.BtcWallet;
import org.btc163.manage.entity.BtcWalletInfo;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.BtcWalletMapper;
import org.btc163.manage.service.BtcWalletInfoService;
import org.btc163.manage.service.BtcWalletService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-05
 */
@Slf4j
@Service
public class BtcWalletServiceImpl extends ServiceImpl<BtcWalletMapper, BtcWallet> implements BtcWalletService {

    private static final String CLASS_NAME = "[BtcWalletServiceImpl]";

    @Autowired
    private BtcWalletMapper btcWalletMapper;

    @Autowired
    private BtcWalletInfoService btcWalletInfoService;

    @Override
    public String getWalletPageList(WalletParam walletParam) throws BusinessException {
        if (walletParam == null) {
            throw new BusinessException(10001);
        }
        BtcWallet btcWallet = new BtcWallet();
        btcWallet.setWaStatus(0);
        EntityWrapper<BtcWallet> entityWrapper = new EntityWrapper<>(btcWallet);
        if (!StringUtils.isEmpty(walletParam.getWaName())) {
            entityWrapper.like("wa_name", walletParam.getWaName());
        }
        entityWrapper.orderDesc(Arrays.asList("wa_hits", "update_time"));
        Page<BtcWallet> page = new Page<>(walletParam.getPage(), walletParam.getLimit());
        Page<BtcWallet> btcWalletPage = null;
        List<BtcWallet> btcWallets = null;
        try {
            btcWalletPage = selectPage(page);
            btcWallets = btcWalletMapper.selectPage(page, entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getWalletPageList] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (CollectionUtils.isEmpty(btcWallets)) {
            return JSON.toJSONString(new ReType(0, null));
        }
        btcWalletPage.setRecords(btcWallets);
        btcWalletPage.setTotal(btcWalletMapper.selectCount(entityWrapper));
        List<WalletView> walletViews = new ArrayList<>();
        for (BtcWallet wallet : btcWallets) {
            WalletView walletView = new WalletView();
            BeanUtils.copyProperties(wallet, walletView);
            walletView.setUpdateTime(wallet.getUpdateTime() == null ? null : DateFormatUtils.format(wallet.getUpdateTime(), "yyyy-MM-dd HH:mm:ss"));
            final String waId = wallet.getId();
            BtcWalletInfo btcWalletInfo = new BtcWalletInfo();
            btcWalletInfo.setWaId(waId);
            EntityWrapper<BtcWalletInfo> walletInfoEntityWrapper = new EntityWrapper<>(btcWalletInfo);
            List<BtcWalletInfo> btcWalletInfos = btcWalletInfoService.selectList(walletInfoEntityWrapper);
            StringBuilder stringBuilder = new StringBuilder();
            if (!CollectionUtils.isEmpty(btcWalletInfos)) {
                btcWalletInfos.forEach(walletInfo -> stringBuilder.append(walletInfo.getwName() == null ? "" : walletInfo.getwName() + ","));
                if (stringBuilder.length() > 0) {
                    String wNames = stringBuilder.substring(0, stringBuilder.length() - 1);
                    walletView.setPartners(wNames);
                }
            }
            walletViews.add(walletView);
        }
        ReType reType = new ReType(page.getTotal(), walletViews);
        return JSON.toJSONString(reType);
    }

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void addWalletInfo(WalletParam walletParam) throws BusinessException {
        if (walletParam == null) {
            throw new BusinessException(10001);
        }
        BtcWallet btcWallet = new BtcWallet();
        BeanUtils.copyProperties(walletParam, btcWallet);
        btcWallet.setWaHits(StringUtils.isEmpty(walletParam.getWaHits()) ? 0L : Long.valueOf(walletParam.getWaHits()));
        btcWallet.setWaLike(StringUtils.isEmpty(walletParam.getWaLike()) ? 0L : Long.valueOf(walletParam.getWaLike()));
        final String waId = UidUtil.getId();
        btcWallet.setId(waId);
        btcWallet.setUpdateTime(new Date());
        try {
            if (btcWalletMapper.insert(btcWallet) <= 0) {
                throw new BusinessException(10002);
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[addWalletInfo] save BtcWallet error:{}", e.getMessage());
            throw new BusinessException(10002);
        }
        if (!CollectionUtils.isEmpty(walletParam.getBtcWalletInfos())) {
            for (BtcWalletInfoModel btcWalletInfoModel : walletParam.getBtcWalletInfos()) {
                if (!StringUtils.isEmpty(btcWalletInfoModel.getWBrief()) ||
                        !StringUtils.isEmpty(btcWalletInfoModel.getWHeadImage()) ||
                        !StringUtils.isEmpty(btcWalletInfoModel.getWName()) ||
                        !StringUtils.isEmpty(btcWalletInfoModel.getWPosition())) {
                    BtcWalletInfo btcWalletInfo = new BtcWalletInfo();
                    BeanUtils.copyProperties(btcWalletInfoModel, btcWalletInfo);
                    btcWalletInfo.setId(UidUtil.getId());
                    btcWalletInfo.setWaId(waId);
                    try {
                        if (!btcWalletInfoService.insert(btcWalletInfo)) {
                            throw new BusinessException(10002);
                        }
                    } catch (Exception e) {
                        log.error(CLASS_NAME + "[BtcWalletInfo] save BtcWallet error:{}", e.getMessage());
                        throw new BusinessException(10002);
                    }
                }

            }
        }
    }

    @Override
    public void editWalletInfo(WalletParam walletParam) throws BusinessException {
        if (walletParam == null || StringUtils.isEmpty(walletParam.getId())) {
            throw new BusinessException(10001);
        }
        final String waId = walletParam.getId();
        BtcWallet btcWallet = null;
        try {
            btcWallet = btcWalletMapper.selectById(waId);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[editWalletInfo] select BtcWallet error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (btcWallet == null) {
            throw new BusinessException(10003);
        }
        BeanUtils.copyProperties(walletParam, btcWallet);
        btcWallet.setWaHits(StringUtils.isEmpty(walletParam.getWaHits()) ? 0L : Long.valueOf(walletParam.getWaHits()));
        btcWallet.setWaLike(StringUtils.isEmpty(walletParam.getWaLike()) ? 0L : Long.valueOf(walletParam.getWaLike()));
        btcWallet.setUpdateTime(new Date());
        try {
            if (btcWalletMapper.updateById(btcWallet) <= 0) {
                throw new BusinessException(10002);
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[editWalletInfo] save BtcWallet error:{}", e.getMessage());
            throw new BusinessException(10002);
        }
        // 删除绑定
        BtcWalletInfo btcWalletInfo = new BtcWalletInfo();
        btcWalletInfo.setWaId(waId);
        EntityWrapper<BtcWalletInfo> entityWrapper = new EntityWrapper<>(btcWalletInfo);
        List<BtcWalletInfo> btcWalletInfos = null;
        try {
            btcWalletInfos = btcWalletInfoService.selectList(entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[editWalletInfo] select BtcWalletInfo error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (!CollectionUtils.isEmpty(btcWalletInfos)) {
            try {
                btcWalletInfos.forEach(walletInfo -> btcWalletInfoService.deleteById(walletInfo));
            } catch (Exception e) {
                log.error(CLASS_NAME + "[editWalletInfo] delete btcWalletInfo error:{}", e.getMessage());
                throw new BusinessException(10005);
            }
        }
        // 重新绑定
        if (!CollectionUtils.isEmpty(walletParam.getBtcWalletInfos())) {
            for (BtcWalletInfoModel btcWalletInfoModel : walletParam.getBtcWalletInfos()) {
                if (!StringUtils.isEmpty(btcWalletInfoModel.getWBrief()) ||
                        !StringUtils.isEmpty(btcWalletInfoModel.getWHeadImage()) ||
                        !StringUtils.isEmpty(btcWalletInfoModel.getWName()) ||
                        !StringUtils.isEmpty(btcWalletInfoModel.getWPosition())) {
                    BtcWalletInfo WalletInfo = new BtcWalletInfo();
                    BeanUtils.copyProperties(btcWalletInfoModel, WalletInfo);
                    WalletInfo.setId(UidUtil.getId());
                    WalletInfo.setWaId(waId);
                    try {
                        if (!btcWalletInfoService.insert(WalletInfo)) {
                            throw new BusinessException(10002);
                        }
                    } catch (Exception e) {
                        log.error(CLASS_NAME + "[editWalletInfo] save BtcWallet error:{}", e.getMessage());
                        throw new BusinessException(10002);
                    }
                }

            }
        }
    }
}
