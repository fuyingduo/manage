package org.btc163.manage.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.baomidou.mybatisplus.toolkit.CollectionUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.btc163.manage.base.ReType;
import org.btc163.manage.controller.model.BtcMillInfoModel;
import org.btc163.manage.controller.param.MillParam;
import org.btc163.manage.controller.view.MillView;
import org.btc163.manage.entity.BtcMill;
import org.btc163.manage.entity.BtcMillInfo;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.BtcMillMapper;
import org.btc163.manage.service.BtcMillInfoService;
import org.btc163.manage.service.BtcMillService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-05
 */
@Slf4j
@Service
public class BtcMillServiceImpl extends ServiceImpl<BtcMillMapper, BtcMill> implements BtcMillService {

    private static final String CLASS_NAME = "[BtcMillServiceImpl]";

    @Autowired
    private BtcMillMapper btcMillMapper;

    @Autowired
    private BtcMillInfoService btcMillInfoService;

    @Override
    public String getMillPageList(MillParam millParam) throws BusinessException {
        if (millParam == null) {
            throw new BusinessException(10001);
        }
        BtcMill btcMill = new BtcMill();
        btcMill.setMiStatus(0);
        EntityWrapper<BtcMill> entityWrapper = new EntityWrapper<>(btcMill);
        if (!StringUtils.isEmpty(millParam.getMiName())) {
            entityWrapper.like("mi_name", millParam.getMiName());
        }
        entityWrapper.orderDesc(Arrays.asList("mi_hits", "update_time"));
        Page<BtcMill> page = new Page<>(millParam.getPage(), millParam.getLimit());
        Page<BtcMill> btcMillPage = null;
        List<BtcMill> btcMills = null;
        try {
            btcMillPage = selectPage(page);
            btcMills = btcMillMapper.selectPage(page, entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getMillPageList] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (CollectionUtils.isEmpty(btcMills)) {
            return JSON.toJSONString(new ReType(0, null));
        }
        btcMillPage.setRecords(btcMills);
        btcMillPage.setTotal(btcMillMapper.selectCount(entityWrapper));
        List<MillView> millViews = new ArrayList<>();
        for (BtcMill mill : btcMills) {
            MillView millView = new MillView();
            BeanUtils.copyProperties(mill, millView);
            millView.setUpdateTime(mill.getUpdateTime() == null ? null : DateFormatUtils.format(mill.getUpdateTime(), "yyyy-MM-dd HH:mm:ss"));
            final String miId = mill.getId();
            BtcMillInfo btcMillInfo = new BtcMillInfo();
            btcMillInfo.setMiId(miId);
            EntityWrapper<BtcMillInfo> millInfoEntityWrapper = new EntityWrapper<>(btcMillInfo);
            List<BtcMillInfo> btcMillInfos = btcMillInfoService.selectList(millInfoEntityWrapper);
            StringBuilder stringBuilder = new StringBuilder();
            if (!CollectionUtils.isEmpty(btcMillInfos)) {
                btcMillInfos.forEach(millInfo -> stringBuilder.append(millInfo.getmName() == null ? "" : millInfo.getmName() + ","));
                if (stringBuilder.length() > 0) {
                    String mNames = stringBuilder.substring(0, stringBuilder.length() - 1);
                    millView.setPartners(mNames);
                }
            }
            millViews.add(millView);
        }
        ReType reType = new ReType(page.getTotal(), millViews);
        return JSON.toJSONString(reType);
    }

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void addMillInfo(MillParam millParam) throws BusinessException {
        if (millParam == null) {
            throw new BusinessException(10001);
        }
        BtcMill btcMill = new BtcMill();
        BeanUtils.copyProperties(millParam, btcMill);
        btcMill.setMiHits(millParam.getMiHits() == null ? 0L : Long.valueOf(millParam.getMiHits()));
        btcMill.setMiLike(millParam.getMiLike() == null ? 0L : Long.valueOf(millParam.getMiLike()));
        final String miId = UidUtil.getId();
        btcMill.setId(miId);
        btcMill.setUpdateTime(new Date());
        try {
            if (btcMillMapper.insert(btcMill) <= 0) {
                throw new BusinessException(10002);
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[addMillInfo] save BtcMill error:{}", e.getMessage());
            throw new BusinessException(10002);
        }
        if (!CollectionUtils.isEmpty(millParam.getBtcMillInfoModels())) {
            for (BtcMillInfoModel btcMillInfoModel : millParam.getBtcMillInfoModels()) {
                if (!StringUtils.isEmpty(btcMillInfoModel.getMName()) ||
                        !StringUtils.isEmpty(btcMillInfoModel.getMPostition()) ||
                        !StringUtils.isEmpty(btcMillInfoModel.getMHeadImage()) ||
                        !StringUtils.isEmpty(btcMillInfoModel.getMBrief())) {
                    BtcMillInfo btcMillInfo = new BtcMillInfo();
                    BeanUtils.copyProperties(btcMillInfoModel, btcMillInfo);
                    btcMillInfo.setId(UidUtil.getId());
                    btcMillInfo.setMiId(miId);
                    try {
                        if (!btcMillInfoService.insert(btcMillInfo)) {
                            throw new BusinessException(10002);
                        }
                    } catch (Exception e) {
                        log.error(CLASS_NAME + "[addMillInfo] save BtcMillInfo error:{}", e.getMessage());
                        throw new BusinessException(10002);
                    }
                }
            }
        }
    }

    @Override
    public void editMillInfo(MillParam millParam) throws BusinessException {
        if (millParam == null || StringUtils.isEmpty(millParam.getId())) {
            throw new BusinessException(10001);
        }
        final String miId = millParam.getId();
        BtcMill btcMill = null;
        try {
            btcMill = btcMillMapper.selectById(miId);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[editMillInfo] select BtcMill error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (btcMill == null) {
            throw new BusinessException(10003);
        }
        BeanUtils.copyProperties(millParam, btcMill);
        btcMill.setMiHits(millParam.getMiHits() == null ? 0L : Long.valueOf(millParam.getMiHits()));
        btcMill.setMiLike(millParam.getMiLike() == null ? 0L : Long.valueOf(millParam.getMiLike()));
        btcMill.setUpdateTime(new Date());
        try {
            if (btcMillMapper.updateById(btcMill) <= 0) {
                throw new BusinessException(10002);
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[editMillInfo] save BtcMill error:{}", e.getMessage());
            throw new BusinessException(10002);
        }
        // 删除绑定
        BtcMillInfo btcMillInfo = new BtcMillInfo();
        btcMillInfo.setMiId(miId);
        EntityWrapper<BtcMillInfo> entityWrapper = new EntityWrapper<>(btcMillInfo);
        List<BtcMillInfo> btcMillInfos = null;
        try {
            btcMillInfos = btcMillInfoService.selectList(entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[editMillInfo] select BtcMillInfo error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (!CollectionUtils.isEmpty(btcMillInfos)) {
            try {
                btcMillInfos.forEach(millInfo -> btcMillInfoService.deleteById(millInfo));
            } catch (Exception e) {
                log.error(CLASS_NAME + "[editMillInfo] delete BtcMillInfo error:{}", e.getMessage());
                throw new BusinessException(10005);
            }
        }
        // 重新绑定
        if (!CollectionUtils.isEmpty(millParam.getBtcMillInfoModels())) {
            for (BtcMillInfoModel btcMillInfoModel : millParam.getBtcMillInfoModels()) {
                if (!StringUtils.isEmpty(btcMillInfoModel.getMName()) ||
                        !StringUtils.isEmpty(btcMillInfoModel.getMPostition()) ||
                        !StringUtils.isEmpty(btcMillInfoModel.getMHeadImage()) ||
                        !StringUtils.isEmpty(btcMillInfoModel.getMBrief())) {
                    BtcMillInfo millInfo = new BtcMillInfo();
                    BeanUtils.copyProperties(btcMillInfoModel, millInfo);
                    millInfo.setId(UidUtil.getId());
                    millInfo.setMiId(miId);
                    try {
                        if (!btcMillInfoService.insert(millInfo)) {
                            throw new BusinessException(10002);
                        }
                    } catch (Exception e) {
                        log.error(CLASS_NAME + "[editMillInfo] save BtcMillInfo error:{}", e.getMessage());
                        throw new BusinessException(10002);
                    }
                }
            }
        }
    }
}
