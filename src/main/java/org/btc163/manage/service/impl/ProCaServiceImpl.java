package org.btc163.manage.service.impl;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.entity.ProCa;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.ProCaMapper;
import org.btc163.manage.service.ProCaService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
@Slf4j
@Service
public class ProCaServiceImpl extends ServiceImpl<ProCaMapper, ProCa> implements ProCaService {

    private static final String CLASS_NAME = "[ProCaServiceImpl]";

    @Autowired
    private ProCaMapper proCaMapper;

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void addProAndCaInfo(String caId, List<String> proIds) throws BusinessException {
        if (StringUtils.isEmpty(caId) || CollectionUtils.isEmpty(proIds)) {
            log.error(CLASS_NAME + "[addProAndCaInfo] request data is empty ");
            throw new BusinessException(10001);
        }
        for (String proId : proIds) {
            ProCa proCa = new ProCa();
            proCa.setId(UidUtil.getId());
            proCa.setCaId(caId);
            proCa.setProId(proId);
            try {
                if (proCaMapper.insert(proCa) <= 0) {
                    log.error(CLASS_NAME + "[addProAndCaInfo] data save failure");
                    throw new BusinessException(10002);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[addProAndCaInfo] error:{}", e.getMessage());
                throw new BusinessException(10002);
            }
        }

    }

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void addCaAndProInfo(String proId, List<String> caIds) throws BusinessException {
        if (StringUtils.isEmpty(proId) || CollectionUtils.isEmpty(caIds)) {
            log.error(CLASS_NAME + "[addCaAndProInfo] request data is empty");
            throw new BusinessException(10001);
        }
        for (String caId : caIds) {
            ProCa proCa = new ProCa();
            proCa.setProId(proId);
            proCa.setCaId(caId);
            proCa.setId(UidUtil.getId());
            try {
                if (proCaMapper.insert(proCa) <= 0) {
                    log.error(CLASS_NAME + "[addCaAndProInfo] data save failure");
                    throw new BusinessException(10002);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[addCaAndProInfo] error:{}", e.getMessage());
                throw new BusinessException(10002);
            }
        }

    }

    @Override
    public List<ProCa> getProCaListByProId(String proId) throws BusinessException {
        if (StringUtils.isEmpty(proId)) {
            log.error(CLASS_NAME + "[getProCaListByProId] request data error ");
            throw new BusinessException(10001);
        }
        ProCa proCa = new ProCa();
        proCa.setProId(proId);
        EntityWrapper<ProCa> entityWrapper = new EntityWrapper<>();
        try {
            return proCaMapper.selectList(entityWrapper);
        } catch (Exception e) {
            throw new BusinessException(10003);
        }

    }
}
