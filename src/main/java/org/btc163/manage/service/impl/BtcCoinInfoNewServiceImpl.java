package org.btc163.manage.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.RowBounds;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.ReType;
import org.btc163.manage.controller.param.CoinOnfiNewParam;
import org.btc163.manage.entity.CoinInfoNew;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.BtcCoinInfoNewMapper;
import org.btc163.manage.service.BtcCoinInfoNewService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-21
 */
@Slf4j
@Service
public class BtcCoinInfoNewServiceImpl extends ServiceImpl<BtcCoinInfoNewMapper, CoinInfoNew> implements BtcCoinInfoNewService {

    private static final String CLASS_NAME = "[BtcCoinInfoNewServiceImpl]";

    @Autowired
    private BtcCoinInfoNewMapper btcCoinInfoNewMapper;

    @Override
    public String selectCoinInfoNewsListPage(CoinOnfiNewParam param) {
        CoinInfoNew coinInfoNew = new CoinInfoNew();
        BeanUtils.copyProperties(param, coinInfoNew);
        Page<CoinInfoNew> page = new Page<CoinInfoNew>(param.getPage(), param.getLimit());
        EntityWrapper<CoinInfoNew> entityWrapper = new EntityWrapper<>(coinInfoNew);
        Page<CoinInfoNew> pages = selectPage(page, entityWrapper);
        List<CoinInfoNew> coinInfoNews = btcCoinInfoNewMapper.selectPage(pages, entityWrapper);
        pages.setRecords(coinInfoNews);
        pages.setTotal(btcCoinInfoNewMapper.selectCount(entityWrapper));
        if (CollectionUtils.isEmpty(coinInfoNews)) {
            return JSON.toJSONString(new ReType(0L, coinInfoNews));
        }
        ReType reType = new ReType(page.getTotal(), coinInfoNews);
        String view = JSON.toJSONString(reType);
        return view;
    }

    @Override
    public void addCoinInfo(CoinOnfiNewParam coinOnfiNewParam) throws BusinessException {
        if (StringUtils.isEmpty(coinOnfiNewParam.getEnKey())) {
            log.error(CLASS_NAME + "[addCoinInfo] request param error enKey is empty! ");
            throw new BusinessException(10001);
        }
        CoinInfoNew coinInfoNew = new CoinInfoNew();
        BeanUtils.copyProperties(coinOnfiNewParam, coinInfoNew);
        boolean result = false;
        try {
            result = btcCoinInfoNewMapper.insert(coinInfoNew) > 0 ? true : false;
        } catch (Exception e) {
            log.error(CLASS_NAME + "[addCoinInfo] error:{}", e.getMessage());
            throw new BusinessException(10002);
        }
        if (!result) {
            log.error(CLASS_NAME + "[addCoinInfo] data save error !");
            throw new BusinessException(10002);
        }
    }

    @Override
    public void updateCoinInfo(CoinOnfiNewParam coinOnfiNewParam) throws BusinessException {
        if (StringUtils.isEmpty(coinOnfiNewParam.getEnKey())) {
            log.error(CLASS_NAME + "[updateCoinInfo] request param error enKey is empty!");
            throw new BusinessException(10001);
        }
        CoinInfoNew coinInfoNew = btcCoinInfoNewMapper.selectById(coinOnfiNewParam.getEnKey());
        if (coinInfoNew == null) {
            log.error(CLASS_NAME + "[updateCoinInfo] obtain data error [coinInfoNew]");
            throw new BusinessException(10003);
        }
        EntityWrapper<CoinInfoNew> entityWrapper = new EntityWrapper<>(coinInfoNew);
        CoinInfoNew coin = new CoinInfoNew();
        BeanUtils.copyProperties(coinOnfiNewParam, coin);
        try {
            if (btcCoinInfoNewMapper.update(coin, entityWrapper) <= 0) {
                log.error(CLASS_NAME + "[] update data error !");
                throw new BusinessException(10004);
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[updateCoinInfo] error:{}", e.getMessage());
            throw new BusinessException(10004);
        }

    }

    @Override
    public void delCoinInfo(String enKey) throws BusinessException {
        if (StringUtils.isEmpty(enKey)) {
            log.error(CLASS_NAME + "[delCoinInfo] request param error enKey is empty! ");
            throw new BusinessException(10001);
        }
        try {
            if (btcCoinInfoNewMapper.deleteById(enKey) <= 0) {
                log.error(CLASS_NAME + "[delCoinInfo] delete data error ! ");
                throw new BusinessException(10005);
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[delCoinInfo] error:{}", e.getMessage());
            throw new BusinessException(10005);
        }
    }
}
