package org.btc163.manage.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.app.model.MediaInfoModel;
import org.btc163.manage.app.model.MediaListModel;
import org.btc163.manage.app.model.MediaModel;
import org.btc163.manage.base.ReType;
import org.btc163.manage.constant.MediaEnum;
import org.btc163.manage.controller.param.MediaInfoParam;
import org.btc163.manage.controller.param.MediaParam;
import org.btc163.manage.controller.view.MediaView;
import org.btc163.manage.entity.BtcMedia;
import org.btc163.manage.entity.BtcMediaInfo;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.BtcMediaMapper;
import org.btc163.manage.service.BtcMediaInfoService;
import org.btc163.manage.service.BtcMediaService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-31
 */
@Slf4j
@Service
public class BtcMediaServiceImpl extends ServiceImpl<BtcMediaMapper, BtcMedia> implements BtcMediaService {

    private static final String CLASS_NAME = "[BtcMediaServiceImpl]";

    @Autowired
    private BtcMediaMapper btcMediaMapper;

    @Autowired
    private BtcMediaInfoService btcMediaInfoService;

    @Value("${image.logo.read}")
    private String readImage;

    @Override
    public Integer getMediaListCount(Integer type) throws BusinessException {
        if (type == null) {
            throw new BusinessException(30001);
        }
        BtcMedia btcMedia = new BtcMedia();
        btcMedia.setMtType(type);
        EntityWrapper<BtcMedia> mediaEntityWrapper = new EntityWrapper<>(btcMedia);
        try {
            return btcMediaMapper.selectCount(mediaEntityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
    }

    @Override
    public List<MediaListModel> getMediaList(Integer page, Integer pageSize, Integer type) throws BusinessException {
        if (type == null) {
            throw new BusinessException(30001);
        }
        List<MediaListModel> mediaListModels = new ArrayList<>();
        BtcMedia btcMedia = new BtcMedia();
        btcMedia.setMtType(type);
        EntityWrapper<BtcMedia> mediaEntityWrapper = new EntityWrapper<>(btcMedia);
        mediaEntityWrapper.orderDesc(Arrays.asList("mt_read+0"));
        Page<BtcMedia> p = new Page<>(page, pageSize);
        Page<BtcMedia> pages = null;
        List<BtcMedia> btcMedias = null;
        try {
            pages = selectPage(p, mediaEntityWrapper);
            btcMedias = btcMediaMapper.selectPage(pages, mediaEntityWrapper);
            if (!CollectionUtils.isEmpty(btcMedias)) {
                for (BtcMedia media : btcMedias) {
                    MediaListModel mediaListModel = new MediaListModel();
                    BeanUtils.copyProperties(media, mediaListModel);
                    if (!StringUtils.isEmpty(media.getMtLogo())) {
                        mediaListModel.setMtLogo(readImage + media.getMtLogo());
                    }
                    mediaListModels.add(mediaListModel);
                }
            }
            return mediaListModels;
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getMediaList] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
    }

    @Override
    public MediaModel findMediaInfo(String id) throws BusinessException {
        if (StringUtils.isEmpty(id)) {
            log.error(CLASS_NAME + "[findMediaInfo] id is empty ");
            throw new BusinessException(10001);
        }
        BtcMedia btcMedia = btcMediaMapper.selectById(id);
        if (btcMedia == null) {
            throw new BusinessException(10003);
        }
        MediaModel mediaModel = new MediaModel();
        mediaModel.setMtIntroduce(btcMedia.getMtIntroduce());
        mediaModel.setMtUrl(btcMedia.getMtUrl());
        BtcMediaInfo btcMediaInfo = new BtcMediaInfo();
        btcMediaInfo.setmId(btcMedia.getId());
        EntityWrapper<BtcMediaInfo> mediaInfoEntityWrapper = new EntityWrapper<>(btcMediaInfo);
        try {
            List<BtcMediaInfo> btcMediaInfos = btcMediaInfoService.selectList(mediaInfoEntityWrapper);
            List<MediaInfoModel> mediaInfoModels = new ArrayList<>();
            if (!CollectionUtils.isEmpty(btcMediaInfos)) {
                for (BtcMediaInfo mediaInfo : btcMediaInfos) {
                    MediaInfoModel mediaInfoModel = new MediaInfoModel();
                    mediaInfoModel.setInBrief(mediaInfo.getiBrief());
                    mediaInfoModel.setInUserName(mediaInfo.getiNamePosition());
                    if (!StringUtils.isEmpty(mediaInfo.getiLogo())) {
                        mediaInfoModel.setInHeadImage(readImage + mediaInfo.getiLogo());
                    }
                    mediaInfoModels.add(mediaInfoModel);
                }
                mediaModel.setMediaInfoModels(mediaInfoModels);
            }
            return mediaModel;
        } catch (Exception e) {
            log.error(CLASS_NAME + "[findMediaInfo] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
    }

    @Override
    public String getMediaList(MediaParam mediaParam) throws BusinessException {
        BtcMedia btcMedia = new BtcMedia();
        btcMedia.setMtStatus(0);
        if (mediaParam.getMtType() == null) {
            btcMedia.setMtType(0);
        } else if (mediaParam.getMtType() != null) {
            btcMedia.setMtType(mediaParam.getMtType());
        }
        EntityWrapper<BtcMedia> btcMediaEntityWrapper = new EntityWrapper<>(btcMedia);
        if (!StringUtils.isEmpty(mediaParam.getMtName())) {
            btcMediaEntityWrapper.like("mt_name", mediaParam.getMtName());
        }
        btcMediaEntityWrapper.orderDesc(Arrays.asList("mt_read", "update_time"));
        Page<BtcMedia> page = new Page<>(mediaParam.getPage(), mediaParam.getLimit());
        List<BtcMedia> btcMedias = null;
        Page<BtcMedia> pages = null;
        try {
            pages = selectPage(page, btcMediaEntityWrapper);
            btcMedias = btcMediaMapper.selectPage(pages, btcMediaEntityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getMediaList] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (CollectionUtils.isEmpty(btcMedias)) {
            return JSON.toJSONString(new ReType(0L, null));
        }
        pages.setRecords(btcMedias);
        pages.setTotal(btcMediaMapper.selectCount(btcMediaEntityWrapper));
        List<MediaView> mediaViews = new ArrayList<>();
        for (BtcMedia media : btcMedias) {
            MediaView mediaView = new MediaView();
            BeanUtils.copyProperties(media, mediaView);
            mediaView.setMtType(MediaEnum.messageOf(media.getMtType()));
            mediaView.setUpdateTime(DateFormatUtils.format(media.getUpdateTime(), "yyyy-MM-dd HH:mm:ss"));
            mediaView.setMtRead(StringUtils.isEmpty(media.getMtRead()) ? "0" : media.getMtRead());
            mediaView.setMtPraise(StringUtils.isEmpty(media.getMtPraise()) ? "0" : media.getMtPraise());
            mediaViews.add(mediaView);
        }
        ReType reType = new ReType(page.getTotal(), mediaViews);
        return JSON.toJSONString(reType);
    }

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void addMediaInfo(MediaParam mediaParam) throws BusinessException {
        if (mediaParam == null) {
            throw new BusinessException(10001);
        }
        log.info(CLASS_NAME + "[addMediaInfo] mediaParam:{}", mediaParam.toString());
        final String mtId = UidUtil.getId();
        BtcMedia btcMedia = new BtcMedia();
        BeanUtils.copyProperties(mediaParam, btcMedia);
        btcMedia.setId(mtId);
        btcMedia.setUpdateTime(new Date());
        try {
            if (btcMediaMapper.insert(btcMedia) <= 0) {
                throw new BusinessException(10002);
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[addMediaInfo] error:{}", e.getMessage());
            throw new BusinessException(10002);
        }
        if (!CollectionUtils.isEmpty(mediaParam.getBtcMediaInfos())) {
            for (MediaInfoParam mediaInfoParam : mediaParam.getBtcMediaInfos()) {
                if (!StringUtils.isEmpty(mediaInfoParam.getiLogo()) ||
                        !StringUtils.isEmpty(mediaInfoParam.getiPosition()) ||
                        !StringUtils.isEmpty(mediaInfoParam.getiUserName()) ||
                        !StringUtils.isEmpty(mediaInfoParam.getiBrief())) {
                    BtcMediaInfo btcMediaInfo = new BtcMediaInfo();
                    BeanUtils.copyProperties(mediaInfoParam, btcMediaInfo);
                    btcMediaInfo.setiNamePosition(mediaInfoParam.getiUserName() + "," + mediaInfoParam.getiPosition());
                    btcMediaInfo.setmId(mtId);
                    btcMediaInfo.setId(UidUtil.getId());
                    try {
                        if (!btcMediaInfoService.insert(btcMediaInfo)) {
                            throw new BusinessException(10002);
                        }
                    } catch (Exception e) {
                        log.error(CLASS_NAME + "[addMediaInfo] error:{}", e.getMessage());
                        throw new BusinessException(10002);
                    }
                }
            }
        }

    }

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void editMediaInfo(MediaParam mediaParam) throws BusinessException {
        if (mediaParam == null || StringUtils.isEmpty(mediaParam.getId())) {
            throw new BusinessException(10001);
        }
        final String id = mediaParam.getId();
        BtcMedia btcMedia = btcMediaMapper.selectById(id);
        BeanUtils.copyProperties(mediaParam, btcMedia);
        btcMedia.setUpdateTime(new Date());
        if (btcMediaMapper.updateById(btcMedia) <= 0) {
            throw new BusinessException(10002);
        }
        // 删除合伙人
        BtcMediaInfo btcMediaInfo = new BtcMediaInfo();
        btcMediaInfo.setmId(id);
        EntityWrapper<BtcMediaInfo> entityWrapper = new EntityWrapper<>(btcMediaInfo);
        List<BtcMediaInfo> btcMediaInfos = btcMediaInfoService.selectList(entityWrapper);
        if (!CollectionUtils.isEmpty(btcMediaInfos)) {
            btcMediaInfos.forEach(info -> btcMediaInfoService.deleteById(info));
        }

        // 重新录入
        for (MediaInfoParam mediaInfoParam : mediaParam.getBtcMediaInfos()) {
            if (!StringUtils.isEmpty(mediaInfoParam.getiLogo()) ||
                    !StringUtils.isEmpty(mediaInfoParam.getiPosition()) ||
                    !StringUtils.isEmpty(mediaInfoParam.getiUserName()) ||
                    !StringUtils.isEmpty(mediaInfoParam.getiBrief())) {
                BtcMediaInfo mediaInfo = new BtcMediaInfo();
                BeanUtils.copyProperties(mediaInfoParam, mediaInfo);
                mediaInfo.setiNamePosition(mediaInfoParam.getiUserName() + "," + mediaInfoParam.getiPosition());
                mediaInfo.setmId(id);
                mediaInfo.setId(UidUtil.getId());
                if (!StringUtils.isEmpty(mediaInfoParam.getiLogo())) {
                    File file = new File(mediaInfoParam.getiLogo());
                    mediaInfo.setiLogo(file.getName());
                }
                try {
                    if (!btcMediaInfoService.insert(mediaInfo)) {
                        throw new BusinessException(10002);
                    }
                } catch (Exception e) {
                    log.error(CLASS_NAME + "[editMediaInfo] error:{}", e.getMessage());
                    throw new BusinessException(10002);
                }
            }
        }
    }

}
