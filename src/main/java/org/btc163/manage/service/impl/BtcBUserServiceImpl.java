package org.btc163.manage.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.ReType;
import org.btc163.manage.constant.MediaEnum;
import org.btc163.manage.controller.param.UserParam;
import org.btc163.manage.controller.param.UserPasswordParam;
import org.btc163.manage.controller.view.MediaView;
import org.btc163.manage.controller.view.UserView;
import org.btc163.manage.entity.BUser;
import org.btc163.manage.entity.BtcMedia;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.BtcBUserMapper;
import org.btc163.manage.service.BtcBUserService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.utils.Md5Util;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-21
 */
@Slf4j
@Service
public class BtcBUserServiceImpl extends ServiceImpl<BtcBUserMapper, BUser> implements BtcBUserService {

    private static final String CLASS_NAME = "[BtcBUserServiceImpl]";

    @Autowired
    private BtcBUserMapper btcBUserMapper;

    @Override
    public String getUserList(UserParam userParam) throws BusinessException {
        BUser bUser = new BUser();
        if (!StringUtils.isEmpty(userParam.getUsername())) {
            bUser.setUsername(userParam.getUsername());
        }
        bUser.setUserStatus("0");
        EntityWrapper<BUser> entityWrapper = new EntityWrapper<>(bUser);
        Page<BUser> page = new Page<>(userParam.getPage(), userParam.getLimit());
        List<BUser> bUsers = null;
        Page<BUser> pages = null;
        try {
            pages = selectPage(page, entityWrapper);
            bUsers = btcBUserMapper.selectPage(pages, entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getUserList] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (CollectionUtils.isEmpty(bUsers)) {
            return JSON.toJSONString(new ReType(0L, null));
        }
        pages.setRecords(bUsers);
        pages.setTotal(btcBUserMapper.selectCount(entityWrapper));
        List<UserView> userViews = new ArrayList<>();
        for (BUser user : bUsers) {
            UserView userView = new UserView();
            BeanUtils.copyProperties(user, userView);
            userView.setCreateTime(user.getCreateTime() == null ? null : DateFormatUtils.format(user.getCreateTime(), "yyyy-MM-dd"));
            userViews.add(userView);
        }
        ReType reType = new ReType(page.getTotal(), userViews);
        return JSON.toJSONString(reType);
    }

    @Override
    public void editPassword(UserPasswordParam userPasswordParam) throws BusinessException {
        if (userPasswordParam == null || StringUtils.isEmpty(userPasswordParam.getId())) {
            throw new BusinessException(10001);
        }
        BUser bUser = null;
        try {
            bUser = btcBUserMapper.selectById(userPasswordParam.getId());
        } catch (Exception e) {
            log.error(CLASS_NAME + "[] elese BUser error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (bUser == null) {
            throw new BusinessException(10003);
        }
        if (!bUser.getPassword().equals(Md5Util.getMD5(userPasswordParam.getPass(), bUser.getUsername()))) {
            throw new BusinessException(10008);
        }
        if (!userPasswordParam.getNewPwd().equals(userPasswordParam.getReNewPass())) {
            throw new BusinessException(10009);
        }
        bUser.setPassword(Md5Util.getMD5(userPasswordParam.getNewPwd(), bUser.getUsername()));
        try {
            if (btcBUserMapper.updateById(bUser) <= 0) {
                throw new BusinessException(10002);
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[editPassword] save BUser error:{}", e.getMessage());
            throw new BusinessException(10002);
        }
    }
}
