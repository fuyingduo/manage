package org.btc163.manage.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.btc163.manage.entity.TsScheduleTriggers;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.TsScheduleTriggersMapper;
import org.btc163.manage.service.TsScheduleTriggersService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.quartz.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-13
 */
@Slf4j
@Service
public class TsScheduleTriggersServiceImpl extends ServiceImpl<TsScheduleTriggersMapper, TsScheduleTriggers> implements TsScheduleTriggersService {

    private static final String CLASS_NAME = "[TSchedulerTriggersServiceImpl]";

    @Autowired
    @Qualifier("scheduler")
    private Scheduler scheduler;

    @Autowired
    private TsScheduleTriggersMapper tsScheduleTriggersMapper;

   // @Scheduled(cron = "*/10 * * * * ?")
    @Override
    public void refreshTrigger() {
        List<TsScheduleTriggers> jobs = tsScheduleTriggersMapper.selectList(null);
        if (!CollectionUtils.isEmpty(jobs)) {
            for (TsScheduleTriggers job : jobs) {
                Integer status = job.getqStatus();
                TriggerKey triggerKey = TriggerKey.triggerKey(job.getqJobName(), job.getqJobGroup());
                try {
                    CronTrigger trigger = (CronTrigger) scheduler.getTrigger(triggerKey);
                    if (trigger == null) {
                        if (status == 0) {
                            continue;
                        }
                        JobDetail jobDetail = JobBuilder.newJob((Class<? extends Job>) Class.forName(job.getqJobName())).withIdentity(job.getqJobName(), job.getqJobGroup()).build();
                        CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(job.getqCron());
                        trigger = TriggerBuilder.newTrigger().withIdentity(job.getqJobName(), job.getqJobGroup()).withSchedule(scheduleBuilder).build();
                        scheduler.scheduleJob(jobDetail, trigger);
                    } else {
                        if (status == 0) {
                            JobKey jobKey = JobKey.jobKey(job.getqJobName(), job.getqJobGroup());
                            scheduler.deleteJob(jobKey);
                            continue;
                        }
                        String searchCron = job.getqCron();
                        String currentCron = trigger.getCronExpression();
                        if (!searchCron.equals(currentCron)) {
                            CronScheduleBuilder scheduleBuilder = CronScheduleBuilder.cronSchedule(searchCron);
                            trigger = trigger.getTriggerBuilder().withIdentity(triggerKey).withSchedule(scheduleBuilder).build();
                            scheduler.rescheduleJob(triggerKey, trigger);
                        }
                    }
                    log.info(CLASS_NAME + "[refreshTrigger] job refresh success !!!");
                } catch (SchedulerException e) {
                    log.error(CLASS_NAME + "[refreshTrigger] refresh job error:{}", e.getMessage());
                } catch (ClassNotFoundException e) {
                    log.error(CLASS_NAME + "[refreshTrigger] refresh job error:{}", e.getMessage());
                }
            }
        } else {
            log.info(CLASS_NAME + "[refreshTrigger] 未获取到更新任务!!!");
        }
    }

    @Override
    public Boolean createScheduleJob(String cron, String jobName) throws BusinessException {
        if (StringUtils.isEmpty(cron) || StringUtils.isEmpty(jobName)) {
            throw new BusinessException(10003);
        }
        Map<String, Object> params = new HashMap<>();
        params.put("cron", cron);
        params.put("job_name", jobName);
        List<TsScheduleTriggers> triggers = tsScheduleTriggersMapper.selectByMap(params);
        TsScheduleTriggers trigger = null;
        if (!CollectionUtils.isEmpty(triggers)) {
            trigger = triggers.get(0);
            return this.updateScheduleJob(trigger, cron);
        }
        trigger = new TsScheduleTriggers();
        trigger.setqCron(cron);
        trigger.setqJobGroup("group1");
        trigger.setqStatus(1);
        trigger.setqJobName(jobName);
        try {
            boolean result = tsScheduleTriggersMapper.insert(trigger) > 0 ? true : false;
            if (!result) {
                throw new BusinessException(30002);
            }
            return result;
        } catch (Exception e) {
            throw new BusinessException(30002);
        }
    }

    private Boolean updateScheduleJob(TsScheduleTriggers trigger, String cron) throws BusinessException {
        if (trigger == null) {
            throw new BusinessException(10003);
        }
        trigger.setqCron(cron);
        try {
            boolean result = tsScheduleTriggersMapper.updateById(trigger) > 0 ? true : false;
            if (!result) {
                throw new BusinessException(30003);
            }
            return result;
        } catch (Exception e) {
            throw new BusinessException(30003);
        }
    }
}