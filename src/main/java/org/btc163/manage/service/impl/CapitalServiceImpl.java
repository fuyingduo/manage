package org.btc163.manage.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.app.model.CapitalForMoreDetailsModel;
import org.btc163.manage.app.model.CapitalInfoModel;
import org.btc163.manage.app.model.CapitalListModel;
import org.btc163.manage.app.model.ProjectListModel;
import org.btc163.manage.base.ReType;
import org.btc163.manage.constant.ProStageEnum;
import org.btc163.manage.controller.model.BtcCapitalInfoModel;
import org.btc163.manage.controller.model.CapilaInfoSelectModel;
import org.btc163.manage.controller.param.CapitalParam;
import org.btc163.manage.controller.view.CapitalView;
import org.btc163.manage.entity.BtcCapitalInfo;
import org.btc163.manage.entity.BtcProject;
import org.btc163.manage.entity.Capital;
import org.btc163.manage.entity.ProCa;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.CapitalMapper;
import org.btc163.manage.service.BtcCapitalInfoService;
import org.btc163.manage.service.BtcProjectService;
import org.btc163.manage.service.CapitalService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.service.ProCaService;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
@Slf4j
@Service
public class CapitalServiceImpl extends ServiceImpl<CapitalMapper, Capital> implements CapitalService {

    private static final String CLASS_NAME = "[CapitalServiceImpl]";

    @Autowired
    private CapitalMapper capitalMapper;

    @Autowired
    private BtcCapitalInfoService btcCapitalInfoService;

    @Autowired
    private ProCaService proCaService;

    @Autowired
    private BtcProjectService btcProjectService;

    @Value("${image.logo.read}")
    private String readImage;

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void addCapitalInfo(CapitalParam capitalParam) throws BusinessException {
        if (capitalParam == null) {
            log.error(CLASS_NAME + "[addCapitalInfo] request data is null");
            throw new BusinessException(10001);
        }
        Capital capital = new Capital();
        BeanUtils.copyProperties(capitalParam, capital);
        capital.setId(UidUtil.getId());
        capital.setCaEstablish(capitalParam.getCaEstablish());
        capital.setUpdate(new Date());
        if (capitalMapper.insert(capital) <= 0) {
            log.error(CLASS_NAME + "[addCapitalInfo] data save error ");
            throw new BusinessException(10002);
        }
        // 合伙人表录入
        final String capitalId = capital.getId();
        if (!CollectionUtils.isEmpty(capitalParam.getBtcCapitalInfos())) {
            btcCapitalInfoService.addCapitalInfos(capitalParam.getBtcCapitalInfos(), capitalId);
        }
        // 参投项目 中间表录入
        if (!CollectionUtils.isEmpty(capitalParam.getProIds())) {
            proCaService.addProAndCaInfo(capitalId, capitalParam.getProIds());
        }
    }

    @Override
    public List<CapilaInfoSelectModel> getSelectCapilaInfos() throws BusinessException {
        Capital cap = new Capital();
        cap.setCaStatus(0);
        List<Capital> capitals = capitalMapper.selectList(new EntityWrapper<Capital>(cap));
        if (CollectionUtils.isEmpty(capitals)) {
            log.error(CLASS_NAME + "[getSelectCapilaInfos] result is empty");
            throw new BusinessException(10003);
        }
        List<CapilaInfoSelectModel> models = new ArrayList<>();
        for (Capital capital : capitals) {
            CapilaInfoSelectModel model = new CapilaInfoSelectModel();
            BeanUtils.copyProperties(capital, model);
            models.add(model);
        }
        return models;
    }

    @Override
    public List<CapitalListModel> getCapitalPageList(Integer page, Integer pageSize, String caName, List<String> caLab) throws BusinessException {
        if (page == null || pageSize == null) {
            log.error(CLASS_NAME + "[getCapitalPageList] request data error page:{}, pageSize:{}", page, pageSize);
            throw new BusinessException(20001);
        }
        // 获取资本库数据
        Capital capital = new Capital();
        capital.setCaStatus(0);
        EntityWrapper<Capital> entityWrapper = new EntityWrapper<>(capital);
        if (!StringUtils.isEmpty(caName)) {
            entityWrapper.like("ca_name", caName);
//            capital.setCaName(caName);
        }
        Page<Capital> page1 = new Page<>(page, pageSize);

        if (!CollectionUtils.isEmpty(caLab)) {
            entityWrapper.in("ca_lab", caLab);
        }
        Page<Capital> pages = null;
        List<Capital> capitals = null;
        try {
            pages = selectPage(page1, entityWrapper);
            capitals = capitalMapper.selectPage(pages, entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getCapitalPageList] select data error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (CollectionUtils.isEmpty(capitals)) {
            throw new BusinessException(20002);
        }
//        pages.setRecords(capitals);
//        pages.setTotal(capitalMapper.selectCount(entityWrapper));
        List<CapitalListModel> capitalListModels = new ArrayList<>();
        for (Capital capital1 : capitals) {
            CapitalListModel capitalListModel = new CapitalListModel();
            BeanUtils.copyProperties(capital1, capitalListModel);
            if (capital1.getCaEstablish() != null) {
                capitalListModel.setCaEstablish(capital1.getCaEstablish());
            }
            if (!StringUtils.isEmpty(capital1.getCaLogo())) {
                capitalListModel.setCaLogo(readImage + capital1.getCaLogo());
            }
            capitalListModels.add(capitalListModel);
        }
        return capitalListModels;
    }

    @Override
    public Integer getCapiTalSizeTotal(String caName, List<String> caLab) throws BusinessException {
        Capital capital = new Capital();
        capital.setCaStatus(0);
        if (!StringUtils.isEmpty(caName)) {
            capital.setCaName(caName);
        }
        EntityWrapper<Capital> entityWrapper = new EntityWrapper<>(capital);
        if (!CollectionUtils.isEmpty(caLab)) {
            entityWrapper.in("ca_lab", caLab);
        }
        Integer capitals = 0;
        try {
            capitals = capitalMapper.selectCount(entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getCapiTalSizeTotal] select data error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        return capitals;
    }

    @Override
    public CapitalInfoModel getCapitalInfos(final String caId) throws BusinessException {
        if (StringUtils.isEmpty(caId)) {
            log.error(CLASS_NAME + "[getCapitalInfos] request data error");
            throw new BusinessException(10001);
        }
        Capital capital = null;
        try {
            capital = capitalMapper.selectById(caId);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getCapitalInfos] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (capital == null) {
            throw new BusinessException(20002);
        }
        // 合伙人
        BtcCapitalInfo btcCapitalInfo = new BtcCapitalInfo();
        btcCapitalInfo.setCaId(caId);
        EntityWrapper<BtcCapitalInfo> entityWrapper = new EntityWrapper<>(btcCapitalInfo);
        List<CapitalForMoreDetailsModel> capitalForMoreDetailsModels = new ArrayList<>();
        try {
            List<BtcCapitalInfo> btcCapitalInfos = btcCapitalInfo.selectList(entityWrapper);
            if (!CollectionUtils.isEmpty(btcCapitalInfos)) {
                for (BtcCapitalInfo capitalInfo : btcCapitalInfos) {
                    CapitalForMoreDetailsModel capitalForMoreDetailsModel = new CapitalForMoreDetailsModel();
                    BeanUtils.copyProperties(capitalInfo, capitalForMoreDetailsModel);
                    if (!StringUtils.isEmpty(capitalInfo.getInHeadImage())) {
                        capitalForMoreDetailsModel.setInHeadImage(readImage + capitalInfo.getInHeadImage());
                    }
                    capitalForMoreDetailsModels.add(capitalForMoreDetailsModel);
                }
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getCapitalInfos] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        // 投资项目
        ProCa proCa = new ProCa();
        proCa.setCaId(caId);
        EntityWrapper<ProCa> proCaEntityWrapper = new EntityWrapper<>(proCa);
        List<ProjectListModel> btcProjects = new ArrayList<>();
        try {
            List<ProCa> proCas = proCaService.selectList(proCaEntityWrapper);
            if (!CollectionUtils.isEmpty(proCas)) {
                for (ProCa ca : proCas) {
                    BtcProject project = btcProjectService.selectById(ca.getProId());
                    if (project != null) {
                        ProjectListModel projectListModel = new ProjectListModel();
                        BeanUtils.copyProperties(project, projectListModel);
                        if (project.getProStage() != null) {
                            projectListModel.setProStage(ProStageEnum.messageOf(project.getProStage()));
                        }
                        btcProjects.add(projectListModel);
                    }
                }
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getCapitalInfos] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        CapitalInfoModel capitalInfoModel = new CapitalInfoModel();
        BeanUtils.copyProperties(capital, capitalInfoModel);
        capitalInfoModel.setProjectListModels(btcProjects);
        capitalInfoModel.setCapitalForMoreDetails(capitalForMoreDetailsModels);
        return capitalInfoModel;
    }

    @Override
    public String getCapitalList(CapitalParam capitalParam) throws BusinessException {
        if (capitalParam == null) {
            log.error(CLASS_NAME + "[getCapitalList] request data error ");
            throw new BusinessException(10001);
        }
        Capital capital = new Capital();
        capital.setCaStatus(0);
        EntityWrapper<Capital> entityWrapper = new EntityWrapper<>(capital);
        if (!StringUtils.isEmpty(capitalParam.getCaName())) {
            entityWrapper.like("ca_name", capitalParam.getCaName());
        }
        entityWrapper.orderDesc(Arrays.asList("ca_statistics", "update"));
        Page<Capital> page = new Page<>(capitalParam.getPage(), capitalParam.getLimit());
        Page<Capital> pages = null;
        List<Capital> capitals = null;
        try {
            pages = selectPage(page, entityWrapper);
            capitals = capitalMapper.selectPage(pages, entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getCapitalList] select data error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (CollectionUtils.isEmpty(capitals)) {
            return JSON.toJSONString(new ReType(0L, null));
        }
        pages.setRecords(capitals);
        pages.setTotal(capitalMapper.selectCount(entityWrapper));
        List<CapitalView> capitalViews = new ArrayList<>();
        for (Capital capital1 : capitals) {
            CapitalView capitalView = new CapitalView();
            BeanUtils.copyProperties(capital1, capitalView);
            final String caId = capital1.getId();
            // 参投项目
            String referenceForProjects = null;
            ProCa proCa = new ProCa();
            proCa.setCaId(caId);
            EntityWrapper<ProCa> caEntityWrapper = new EntityWrapper<>(proCa);
            List<ProCa> proCas = proCaService.selectList(caEntityWrapper);
            StringBuilder stringBuilder = new StringBuilder();
            if (!CollectionUtils.isEmpty(proCas)) {
                proCas.forEach(proCa1 -> stringBuilder.append(btcProjectService.selectById(proCa1.getProId()).getProName() + ","));
                referenceForProjects = stringBuilder.substring(0, stringBuilder.length() - 1);
            }
            // 合伙人
            String partners = null;
            BtcCapitalInfo btcCapitalInfo = new BtcCapitalInfo();
            btcCapitalInfo.setCaId(caId);
            EntityWrapper<BtcCapitalInfo> infoEntityWrapper = new EntityWrapper<>(btcCapitalInfo);
            List<BtcCapitalInfo> btcCapitalInfos = btcCapitalInfoService.selectList(infoEntityWrapper);
            StringBuilder infoBuilder = new StringBuilder();
            if (!CollectionUtils.isEmpty(btcCapitalInfos)) {
                btcCapitalInfos.forEach(btcCapitalInfo1 -> infoBuilder.append(btcCapitalInfo1.getInUserName() + ","));
                partners = infoBuilder.substring(0, infoBuilder.length() - 1);
            }
            if (StringUtils.isEmpty(referenceForProjects)) {
                capitalView.setReferenceForProjects(capital1.getCaProject());
            } else if (StringUtils.isEmpty(capital1.getCaProject())) {
                capitalView.setReferenceForProjects(referenceForProjects);
            } else {
                capitalView.setReferenceForProjects(referenceForProjects + "," + capital1.getCaProject());
            }
            capitalView.setPartners(partners);
            if (capital1.getUpdate() != null) {
                capitalView.setUpdateTime(DateFormatUtils.format(capital1.getUpdate(), "yyyy-MM-dd HH:mm:ss"));
            }
            capitalView.setCaStatistics(capital1.getCaStatistics() == null ? 0L : capital1.getCaStatistics());
            capitalView.setCaPraise(capital1.getCaPraise() == null ? 0 : capital1.getCaPraise());
            capitalViews.add(capitalView);
        }
        ReType reType = new ReType(page.getTotal(), capitalViews);
        return JSON.toJSONString(reType);
    }

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void editCapitalInfo(CapitalParam capitalParam) throws BusinessException {
        if (capitalParam == null || StringUtils.isEmpty(capitalParam.getId())) {
            log.error(CLASS_NAME + "[editCapitalInfo] request data error ");
            throw new BusinessException(10001);
        }
        final String caId = capitalParam.getId();
        Capital capitai = capitalMapper.selectById(caId);
        if (capitai == null) {
            throw new BusinessException(10004);
        }
        BeanUtils.copyProperties(capitalParam, capitai);
        capitai.setCaEstablish(capitalParam.getCaEstablish());
        if (capitalMapper.updateById(capitai) <= 0) {
            log.error(CLASS_NAME + "[] update data error");
            throw new BusinessException(10004);
        }
        // 删除绑定参投项目
        ProCa proCa = new ProCa();
        proCa.setCaId(caId);
        EntityWrapper<ProCa> entityWrapper = new EntityWrapper<>(proCa);
        List<String> proCaBatchIds = new ArrayList<>();
        List<ProCa> proCas = proCaService.selectList(entityWrapper);
        if (!CollectionUtils.isEmpty(proCas)) {
            proCas.forEach(proCa1 -> proCaBatchIds.add(proCa1.getId()));
            if (!proCaService.deleteBatchIds(proCaBatchIds)) {
                log.error(CLASS_NAME + "[editCapitalInfo] update data error");
                throw new BusinessException(10004);
            }
        }
        // 重新绑定参投项目
        if (!CollectionUtils.isEmpty(capitalParam.getProIds())) {
            List<String> proIds = capitalParam.getProIds();
            proCaService.addProAndCaInfo(caId, proIds);
        }
        // 删除绑定合伙人
        BtcCapitalInfo btcCapitalInfo = new BtcCapitalInfo();
        btcCapitalInfo.setCaId(caId);
        EntityWrapper<BtcCapitalInfo> infoEntityWrapper = new EntityWrapper<>(btcCapitalInfo);
        List<BtcCapitalInfo> btcCapitalInfos = btcCapitalInfoService.selectList(infoEntityWrapper);
        if (!CollectionUtils.isEmpty(btcCapitalInfos)) {
            btcCapitalInfos.forEach(btcCapitalInfo1 -> btcCapitalInfoService.deleteById(btcCapitalInfo1.getId()));
        }
        // 重新绑定合伙人
        List<BtcCapitalInfoModel> btcCapitalInfoModels = capitalParam.getBtcCapitalInfos();
        btcCapitalInfoService.addCapitalInfos(btcCapitalInfoModels, caId);
    }
}
