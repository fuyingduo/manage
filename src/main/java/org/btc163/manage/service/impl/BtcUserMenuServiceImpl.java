package org.btc163.manage.service.impl;

import org.btc163.manage.entity.BtcUserMenu;
import org.btc163.manage.mapper.BtcUserMenuMapper;
import org.btc163.manage.service.BtcUserMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-08
 */
@Service
public class BtcUserMenuServiceImpl extends ServiceImpl<BtcUserMenuMapper, BtcUserMenu> implements BtcUserMenuService {

}
