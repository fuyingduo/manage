package org.btc163.manage.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.ReType;
import org.btc163.manage.controller.param.BtcConferenceParam;
import org.btc163.manage.entity.BtcConference;
import org.btc163.manage.entity.BtcMtPm;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.BtcConferenceMapper;
import org.btc163.manage.service.BtcConferenceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.service.BtcMtPmService;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-04
 */
@Slf4j
@Service
public class BtcConferenceServiceImpl extends ServiceImpl<BtcConferenceMapper, BtcConference> implements BtcConferenceService {

    private static final String CLASS_NAME = "[BtcConferenceServiceImpl]";

    @Autowired
    private BtcMtPmService btcMtPmService;

    @Autowired
    private BtcConferenceMapper btcConferenceMapper;

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void addCon(BtcConferenceParam btcConferenceParam) throws BusinessException {
        if (btcConferenceParam == null) {
            throw new BusinessException(10001);
        }
        final String uid = UidUtil.getId();
        BtcConference btcConference = new BtcConference();
        BeanUtils.copyProperties(btcConferenceParam, btcConference);
        btcConference.setUpdateTime(new Date());
        btcConference.setId(uid);
        if (btcConferenceParam.getPmStartTime() != null) {
            String format = DateFormatUtils.format(btcConferenceParam.getPmStartTime(), "yyyy-MM-dd");
            btcConference.setPmMonth(format);
            btcConference.setPmStartTime(btcConferenceParam.getPmStartTime().getTime() + "");
        }
        if (btcConferenceParam.getPmEndTime() != null) {
            btcConference.setPmEndTime(btcConferenceParam.getPmEndTime().getTime() + "");
        }
        if (!CollectionUtils.isEmpty(btcConferenceParam.getBtcMediaIds())) {
            for (String s : btcConferenceParam.getBtcMediaIds()) {
                BtcMtPm btcMtPm = new BtcMtPm();
                btcMtPm.setPmId(uid);
                btcMtPm.setMtId(s);
                try {
                    if (!btcMtPmService.insert(btcMtPm)) {
                        throw new BusinessException(10002);
                    }
                } catch (Exception e) {
                    log.error(CLASS_NAME + "[addCon] save BtcMtPm error:{}", e.getMessage());
                    throw new BusinessException(10002);
                }
            }
        }
        if (!CollectionUtils.isEmpty(btcConferenceParam.getPmBriefs())) {
            StringBuilder stringBuilder = new StringBuilder();
            btcConferenceParam.getPmBriefs().forEach(s -> stringBuilder.append(s + "^"));
            if (stringBuilder.length() > 0) {
                String briefs = stringBuilder.substring(0, stringBuilder.length() - 1);
                btcConference.setPmBrief(briefs);
            }
        }
        if (!CollectionUtils.isEmpty(btcConferenceParam.getPmHonoreds())) {
            StringBuilder stringBuilder = new StringBuilder();
            btcConferenceParam.getPmHonoreds().forEach(s -> stringBuilder.append(s + "^"));
            if (stringBuilder.length() > 0) {
                String honored = stringBuilder.substring(0, stringBuilder.length() - 1);
                btcConference.setPmHonored(honored);
            }
        }
        if (!CollectionUtils.isEmpty(btcConferenceParam.getPmSchedules())) {
            StringBuilder stringBuilder = new StringBuilder();
            btcConferenceParam.getPmSchedules().forEach(s -> stringBuilder.append(s + "^"));
            if (stringBuilder.length() > 0) {
                String schedule = stringBuilder.substring(0, stringBuilder.length() - 1);
                btcConference.setPmSchedule(schedule);
            }
        }
        try {
            if (btcConferenceMapper.insert(btcConference) <= 0) {
                throw new BusinessException(10002);
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[addCon] save BtcConference error:{}", e.getMessage());
            throw new BusinessException(10002);
        }
    }

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void editCon(BtcConferenceParam btcConferenceParam) throws BusinessException {
        if (btcConferenceParam == null || StringUtils.isEmpty(btcConferenceParam.getId())) {
            throw new BusinessException(10001);
        }
        final String id = btcConferenceParam.getId();
        BtcConference btcConference = null;
        try {
            btcConference = btcConferenceMapper.selectById(id);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[editCon] select BtcConference error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (btcConference == null) {
            throw new BusinessException(10003);
        }
        BeanUtils.copyProperties(btcConferenceParam, btcConference);
        btcConference.setUpdateTime(new Date());
        if (btcConferenceParam.getPmStartTime() != null) {
            String format = DateFormatUtils.format(btcConferenceParam.getPmStartTime(), "yyyy-MM-dd");
            btcConference.setPmMonth(format);
            btcConference.setPmStartTime(btcConferenceParam.getPmStartTime().getTime() + "");
        }
        if (btcConferenceParam.getPmEndTime() != null) {
            btcConference.setPmEndTime(btcConferenceParam.getPmEndTime().getTime() + "");
        }
        if (!CollectionUtils.isEmpty(btcConferenceParam.getPmBriefs())) {
            StringBuilder stringBuilder = new StringBuilder();
            btcConferenceParam.getPmBriefs().forEach(s -> stringBuilder.append(s + "^"));
            if (stringBuilder.length() > 0) {
                String briefs = stringBuilder.substring(0, stringBuilder.length() - 1);
                btcConference.setPmBrief(briefs);
            }
        }
        if (!CollectionUtils.isEmpty(btcConferenceParam.getPmHonoreds())) {
            StringBuilder stringBuilder = new StringBuilder();
            btcConferenceParam.getPmHonoreds().forEach(s -> stringBuilder.append(s + "^"));
            if (stringBuilder.length() > 0) {
                String honored = stringBuilder.substring(0, stringBuilder.length() - 1);
                btcConference.setPmHonored(honored);
            }
        }
        if (!CollectionUtils.isEmpty(btcConferenceParam.getPmSchedules())) {
            StringBuilder stringBuilder = new StringBuilder();
            btcConferenceParam.getPmSchedules().forEach(s -> stringBuilder.append(s + "^"));
            if (stringBuilder.length() > 0) {
                String schedule = stringBuilder.substring(0, stringBuilder.length() - 1);
                btcConference.setPmSchedule(schedule);
            }
        }
        try {
            if (btcConferenceMapper.updateById(btcConference) <= 0) {
                throw new BusinessException(10002);
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[editCon] update btcConference error:{}", e.getMessage());
            throw new BusinessException(10002);
        }
        // 删除绑定媒体
        BtcMtPm btcMtPm = new BtcMtPm();
        btcMtPm.setPmId(id);
        EntityWrapper<BtcMtPm> entityWrapper = new EntityWrapper<>(btcMtPm);
        List<BtcMtPm> btcMtPms = null;
        try {
            btcMtPms = btcMtPmService.selectList(entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[editCon] select BtcMtPms error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (!CollectionUtils.isEmpty(btcMtPms)) {
            btcMtPms.forEach(mtPm -> btcMtPmService.deleteById(mtPm));
        }
        // 重新绑定
        if (!CollectionUtils.isEmpty(btcConferenceParam.getBtcMediaIds())) {
            for (String s : btcConferenceParam.getBtcMediaIds()) {
                BtcMtPm mtPm = new BtcMtPm();
                btcMtPm.setPmId(id);
                btcMtPm.setMtId(s);
                try {
                    if (!btcMtPmService.insert(btcMtPm)) {
                        throw new BusinessException(10002);
                    }
                } catch (Exception e) {
                    log.error(CLASS_NAME + "[editCon] save BtcMtPm error:{}", e.getMessage());
                    throw new BusinessException(10002);
                }
            }
        }

    }

    @Override
    public String getConferenceList(BtcConferenceParam btcConferenceParam) throws BusinessException {
        BtcConference btcConference = new BtcConference();
        btcConference.setPmStatus(0);
        EntityWrapper<BtcConference> entityWrapper = new EntityWrapper<>(btcConference);
        if (!StringUtils.isEmpty(btcConferenceParam.getPmName())) {
            entityWrapper.like("pm_name", btcConferenceParam.getPmName());
        }
        entityWrapper.orderDesc(Arrays.asList("pm_month"));
        Page<BtcConference> page = new Page<>(btcConferenceParam.getPage(), btcConferenceParam.getLimit());
        Page<BtcConference> pages = null;
        List<BtcConference> btcConferences = null;
        try {
            pages = selectPage(page, entityWrapper);
            btcConferences = btcConferenceMapper.selectPage(page, entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getConferenceList] select BtcConference error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (CollectionUtils.isEmpty(btcConferences)) {
            throw new BusinessException(10003);
        }
        pages.setRecords(btcConferences);
        pages.setTotal(btcConferenceMapper.selectCount(entityWrapper));
        ReType reType = new ReType(page.getTotal(), btcConferences);
        return JSON.toJSONString(reType);
    }
}
