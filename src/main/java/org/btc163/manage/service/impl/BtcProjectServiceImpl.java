package org.btc163.manage.service.impl;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.app.model.*;
import org.btc163.manage.base.ReType;
import org.btc163.manage.constant.ProStageEnum;
import org.btc163.manage.constant.ReLockUpEnum;
import org.btc163.manage.controller.model.BtcProjectInfoModel;
import org.btc163.manage.controller.model.BtcProjectSelectModel;
import org.btc163.manage.controller.param.BtcProjectParam;
import org.btc163.manage.controller.param.ProjectParam;
import org.btc163.manage.controller.view.BtcProjectView;
import org.btc163.manage.entity.*;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.BtcProjectMapper;
import org.btc163.manage.service.*;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
@Slf4j
@Service
public class BtcProjectServiceImpl extends ServiceImpl<BtcProjectMapper, BtcProject> implements BtcProjectService {

    private static final String CLASS_NAME = "[BtcProjectServiceImpl]";

    @Autowired
    private BtcProjectMapper btcProjectMapper;

    @Autowired
    private BtcProjectInfoService btcProjectInfoService;

    @Autowired
    private ProjectPathService projectPathService;

    @Autowired
    private ProjectDynamicService projectDynamicService;

    @Autowired
    private ProTepService proTepService;

    @Autowired
    private ProCaService proCaService;

    @Autowired
    private ProPlatformService proPlatformService;

    @Autowired
    private TepConceptService tepConceptService;

    @Autowired
    private BtcPlatformInfoService btcPlatformInfoService;

    @Autowired
    private CapitalService capitalService;

    @Value("${image.logo.read}")
    private String readImage;

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void addProject(BtcProjectParam param) throws BusinessException {
        if (param == null) {
            log.error(CLASS_NAME + "[addProject] request data is null !");
            throw new BusinessException(10001);
        }
        // 1. 储存project表
        BtcProject project = new BtcProject();
        BeanUtils.copyProperties(param, project);
        project.setId(UidUtil.getId());
        project.setUpdateTime(new Date());
        if (btcProjectMapper.insert(project) <= 0) {
            log.error(CLASS_NAME + "[] save data BtcProject failure project:{}", project.toString());
            throw new BusinessException(10002);
        }
        final String projectId = project.getId();
        // 2. 存储btcProjectInfo
        if (!CollectionUtils.isEmpty(param.getBtcProjectInfos())) {
            btcProjectInfoService.addProjectInfos(param.getBtcProjectInfos(), projectId);
        }
        // 3. 存储projectPath
        if (!CollectionUtils.isEmpty(param.getProjectPaths())) {
            projectPathService.addProjectPaths(param.getProjectPaths(), projectId);
        }
        // 4. 存储projectDynamic
        if (!CollectionUtils.isEmpty(param.getProjectDynamics())) {
            projectDynamicService.addProjectDynamics(param.getProjectDynamics(), projectId);
        }
        // 5. 概念板块 存储中间表
        if (!CollectionUtils.isEmpty(param.getProLabel())) {
            proTepService.saveProAndTepData(projectId, param.getProLabel());
        }
        // 6. 合作伙伴
        if (!CollectionUtils.isEmpty(param.getCaIds())) {
            proCaService.addCaAndProInfo(projectId, param.getCaIds());
        }
        // 7.已上线交易所
        if (!CollectionUtils.isEmpty(param.getPlatforms())) {
            proPlatformService.addProAndPlatformInfos(projectId, param.getPlatforms());
        }

    }

    @Override
    public List<BtcProjectSelectModel> getAllProjects() throws BusinessException {
        BtcProject p = new BtcProject();
        p.setProStatus("0");
        List<BtcProject> projects = btcProjectMapper.selectList(new EntityWrapper<BtcProject>(p));
        if (CollectionUtils.isEmpty(projects)) {
            log.error(CLASS_NAME + "[getAllProjects] request data is empty");
            throw new BusinessException(10003);
        }
        List<BtcProjectSelectModel> models = new ArrayList<>();
        for (BtcProject project : projects) {
            BtcProjectSelectModel model = new BtcProjectSelectModel();
            BeanUtils.copyProperties(project, model);
            models.add(model);
        }
        return models;
    }

    @Override
    public String getProjectList(ProjectParam projectParam) throws BusinessException {
        if (projectParam == null) {
            log.error(CLASS_NAME + "[getProjectList] request data error is empty");
            throw new BusinessException(10001);
        }
        BtcProject btcProject = new BtcProject();
        btcProject.setProStatus("0");
        EntityWrapper<BtcProject> entityWrapper = new EntityWrapper<BtcProject>(btcProject);
        if (!StringUtils.isEmpty(projectParam.getProName())) {
            entityWrapper.like("pro_name", projectParam.getProName());
        }
        entityWrapper.orderDesc(Arrays.asList("pro_hits", "update_time"));
        Page<BtcProject> page = new Page<>(projectParam.getPage(), projectParam.getLimit());
        Page<BtcProject> pages = null;
        List<BtcProject> btcProjects = null;
        try {
            pages = selectPage(page, entityWrapper);
            btcProjects = btcProjectMapper.selectPage(pages, entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getProjectList] select data error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (CollectionUtils.isEmpty(btcProjects)) {
            return JSON.toJSONString(new ReType(0L, btcProjects));
        }
        pages.setRecords(btcProjects);
        pages.setTotal(btcProjectMapper.selectCount(entityWrapper));
        List<BtcProjectView> btcProjectViews = new ArrayList<>();
        for (BtcProject project : btcProjects) {
            final String proId = project.getId();
            String proStage = null;
            if (project.getProStage() != null) {
                proStage = ProStageEnum.messageOf(project.getProStage());
            }
            StringBuilder tepConceptName = new StringBuilder();
            List<Long> tepIds = proTepService.getProTepInfosByProId(proId);
            List<TepConcept> tepConcepts = null;
            if (!CollectionUtils.isEmpty(tepIds)) {
                tepConcepts = tepConceptService.findTepConcepyByIds(tepIds);
            }
            String tepName = null;
            if (!CollectionUtils.isEmpty(tepConcepts)) {
                for (TepConcept tepConcept : tepConcepts) {
                    tepConceptName.append(tepConcept.getName() + ",");
                }
                tepName = tepConceptName.toString().substring(0, tepConceptName.toString().length() - 1);
            }
            String infoName = null;
            StringBuilder infobuilders = new StringBuilder();
            List<BtcProjectInfo> infos = btcProjectInfoService.getBtcProjectInfosByProId(proId);
            if (!CollectionUtils.isEmpty(infos)) {
                for (BtcProjectInfo info : infos) {
                    infobuilders.append(info.getInUserName() + ",");
                }
                infoName = infobuilders.toString().substring(0, infobuilders.toString().length() - 1);
            }

            String updateTime = DateFormatUtils.format(project.getUpdateTime(), "yyyy-MM-dd HH:mm:ss");
            BtcProjectView BtcProjectView = new BtcProjectView(project.getId(),
                    project.getProName(),
                    proStage,
                    tepName,
                    project.getProTokenNum(),
                    infoName,
                    updateTime);
            BtcProjectView.setProHits(project.getProHits() == null ? 0L : project.getProHits());
            BtcProjectView.setProPraise(project.getProPraise() == null ? 0 : project.getProPraise());
            btcProjectViews.add(BtcProjectView);
        }
        ReType reType = new ReType(page.getTotal(), btcProjectViews);
        return JSON.toJSONString(reType);
    }

    @Override
    public List<ProjectListModel> getAppProjectInfos(Integer page, Integer pageSize, List<String> proStages, List<Long> tepNames, String proName) throws BusinessException {
        if (page == null || pageSize == null) {
            log.error(CLASS_NAME + "[getAppProjectInfos] request data error page:{}, pageSize:{}", page, pageSize);
            throw new BusinessException(20001);
        }
        int limit_1 = (page - 1) * pageSize;
        int limit_2 = pageSize;
        // 获取proStages code
        List<Integer> stages = new ArrayList<>();
        if (!CollectionUtils.isEmpty(proStages)) {
            for (String proStage : proStages) {
                stages.add(ProStageEnum.messageCodeOf(proStage));
            }
        }
        List<BtcProject> btcProjects = btcProjectMapper.getProjectInfosByParams(limit_1, limit_2, stages, tepNames, proName);
        if (CollectionUtils.isEmpty(btcProjects)) {
            log.error(CLASS_NAME + "[getAppProjectInfos] gain data is empty ");
            throw new BusinessException(20002);
        }
        List<ProjectListModel> projectListModels = new ArrayList<>();
        for (BtcProject btcProject : btcProjects) {
            final String proId = btcProject.getId();
            List<ProTep> proTeps = proTepService.getProTepsByProId(proId);
            List<Long> tepIds = new ArrayList<>();
            List<TepConcept> tepConcepts = null;
            if (!CollectionUtils.isEmpty(proTeps)) {
                proTeps.forEach(proTep -> tepIds.add(proTep.getTepId()));
                tepConcepts = tepConceptService.findTepConcepyByIds(tepIds);
            }
            List<TepConceptModel> tepConceptModels = new ArrayList<>();
            if (!CollectionUtils.isEmpty(tepConcepts)) {
                TepConceptModel tepConceptModel = new TepConceptModel();
                tepConcepts.forEach(tepConcept -> BeanUtils.copyProperties(tepConcept, tepConceptModel));
                tepConceptModels.add(tepConceptModel);
            }
            ProjectListModel projectListModel = new ProjectListModel(proId, btcProject.getProName(), btcProject.getProLogo() == null ? null : readImage + btcProject.getProLogo(),
                    btcProject.getProStage() == null ? "" : ProStageEnum.messageOf(btcProject.getProStage()), tepConceptModels);
            projectListModels.add(projectListModel);
        }
        return projectListModels;
    }

    @Override
    public Integer getAppProjectInCount(List<String> proStages, List<Long> tepNames) throws BusinessException {
        List<Integer> stages = new ArrayList<>();
        if (!CollectionUtils.isEmpty(proStages)) {
            for (String proStage : proStages) {
                stages.add(ProStageEnum.messageCodeOf(proStage));
            }
        }
        Integer count = 0;
        try {
            count = btcProjectMapper.getProjectCount(stages, tepNames);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[getAppProjectInCount] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        return count;
    }

    @Override
    public ProjectInfoModel findAppProjectInfos(final String proId) throws BusinessException {
        if (StringUtils.isEmpty(proId)) {
            log.error(CLASS_NAME + "[findAppProjectInfos] request data error");
            throw new BusinessException(10001);
        }
        BtcProject project = btcProjectMapper.selectById(proId);
        if (project == null) {
            log.error(CLASS_NAME + "[findAppProjectInfos] result data BtcProject is null");
            throw new BusinessException(10003);
        }
        project.setProHits(project.getProHits() == null ? 1L : project.getProHits() + 1L);
        btcProjectMapper.updateById(project);
        // 项目详情
        ProjectInfoModel projectInfoModel = new ProjectInfoModel();
        BeanUtils.copyProperties(project, projectInfoModel);
        // 概念板块
        ProTep proTep = new ProTep();
        proTep.setProId(proId);
        List<ProTep> proTeps = proTepService.selectList(new EntityWrapper<>(proTep));
        List<Long> tepIds = new ArrayList<>();
        if (!CollectionUtils.isEmpty(proTeps)) {
            proTeps.forEach(pro -> tepIds.add(pro.getTepId()));
        }
        List<TepConceptModel> tepConceptModels = new ArrayList<>();
        if (!CollectionUtils.isEmpty(tepIds)) {
            List<TepConcept> tepConcepts = tepConceptService.findTepConcepyByIds(tepIds);
            if (!CollectionUtils.isEmpty(tepConcepts)) {
                for (TepConcept tepConcept : tepConcepts) {
                    TepConceptModel tepConceptModel = new TepConceptModel();
                    BeanUtils.copyProperties(tepConcept, tepConceptModel);
                    tepConceptModels.add(tepConceptModel);
                }
            }
        }
        // 已上线交易所
        StringBuilder platformName = new StringBuilder();
        List<ProPlatform> proPlatforms = proPlatformService.getProPlatformInfos(proId);
        if (!CollectionUtils.isEmpty(proPlatforms)) {
            for (ProPlatform proPlatform : proPlatforms) {
                PlatformInfo platformInfo = new PlatformInfo();
                platformInfo.setName(proPlatform.getPlatId());
                List<PlatformInfo> btcPlatformInfos = btcPlatformInfoService.selectList(new EntityWrapper<PlatformInfo>(platformInfo));
                if (!CollectionUtils.isEmpty(btcPlatformInfos)) {
                    PlatformInfo btcPlatformInfo = btcPlatformInfos.get(0);
                    platformName.append(btcPlatformInfo.getName() + ",");
                }
            }
        }
        String platNames = "";
        if (platformName.length() > 0) {
            platNames = platformName.substring(0, platformName.length() - 1);
        }
        // 合作伙伴
        List<ProCa> proCas = proCaService.getProCaListByProId(proId);
        List<CapitalListModel> capitals = new ArrayList<>();
        if (!CollectionUtils.isEmpty(proCas)) {
            for (ProCa proCa : proCas) {
                CapitalListModel capitalListModel = new CapitalListModel();
                Capital capital = capitalService.selectById(proCa.getCaId());
                if (capital != null) {
                    BeanUtils.copyProperties(capital, capitalListModel);
                    if (capital.getCaEstablish() != null) {
                        capitalListModel.setCaEstablish(capital.getCaEstablish());
                    }
                    capitals.add(capitalListModel);
                }
            }
        }
        // 项目路线
        List<ProjectPathModel> projectPathModels = new ArrayList<>();
        ProjectPath path = new ProjectPath();
        path.setProId(proId);
        List<ProjectPath> projectPaths = projectPathService.selectList(new EntityWrapper<ProjectPath>(path));
        if (!CollectionUtils.isEmpty(projectPaths)) {
            for (ProjectPath projectPath : projectPaths) {
                ProjectPathModel projectPathModel = new ProjectPathModel();
                BeanUtils.copyProperties(projectPath, projectPathModel);
                if (projectPath.getPaTime() != null) {
                    projectPathModel.setPaTime(DateFormatUtils.format(projectPath.getPaTime(), "yyyy-MM-dd HH:mm:ss"));
                }
                projectPathModels.add(projectPathModel);
            }
        }
        // 更多详情
        List<ProjectForMoreDetailsModel> projectForMoreDetailsModels = new ArrayList<>();
        BtcProjectInfo btcProjectInfo = new BtcProjectInfo();
        btcProjectInfo.setProId(proId);
        List<BtcProjectInfo> btcProjectInfos = btcProjectInfoService.selectList(new EntityWrapper<BtcProjectInfo>(btcProjectInfo));
        if (!CollectionUtils.isEmpty(btcProjectInfos)) {
            for (BtcProjectInfo projectInfo : btcProjectInfos) {
                ProjectForMoreDetailsModel projectForMoreDetailsModel = new ProjectForMoreDetailsModel();
                BeanUtils.copyProperties(projectInfo, projectForMoreDetailsModel);
                if (!StringUtils.isEmpty(projectInfo.getInHeadImage())) {
                    projectForMoreDetailsModel.setInHeadImage(readImage + projectInfo.getInHeadImage());
                }
                projectForMoreDetailsModels.add(projectForMoreDetailsModel);
            }
        }
        // 相关新闻
        List<ProjectDynamicModel> projectDynamicModels = new ArrayList<>();
        ProjectDynamic projectDynamic = new ProjectDynamic();
        projectDynamic.setProId(proId);
        List<ProjectDynamic> projectDynamics = projectDynamicService.selectList(new EntityWrapper<ProjectDynamic>(projectDynamic));
        if (!CollectionUtils.isEmpty(projectDynamics)) {
            for (ProjectDynamic dynamic : projectDynamics) {
                ProjectDynamicModel projectDynamicModel = new ProjectDynamicModel();
                BeanUtils.copyProperties(dynamic, projectDynamicModel);
                projectDynamicModels.add(projectDynamicModel);
            }
        }
        if (project.getReTime() != null) {
            projectInfoModel.setReTime(project.getReTime());
        }
        projectInfoModel.setReLockUp(ReLockUpEnum.messageOf(project.getReLockUp()));
        projectInfoModel.setProStage(ProStageEnum.messageOf(project.getProStage()));
        projectInfoModel.setRaiseTokenNum(project.getRaiseTokenSum());
        projectInfoModel.setPopExchange(platNames);
        projectInfoModel.setTepConcepts(tepConceptModels);
        projectInfoModel.setProjectForMoreDetails(projectForMoreDetailsModels);
        projectInfoModel.setCapitalListModels(capitals);
        projectInfoModel.setProjectPathModels(projectPathModels);
        projectInfoModel.setProjectDynamics(projectDynamicModels);
        return projectInfoModel;
    }

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void editProject(BtcProjectParam param) throws BusinessException {
        if (param == null || param.getId() == null) {
            log.error(CLASS_NAME + "[editProject] request data error");
            throw new BusinessException(10001);
        }
        final String proId = param.getId();
        BtcProject btcProject = null;
        try {
            btcProject = btcProjectMapper.selectById(proId);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[editProject] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (btcProject == null) {
            log.error(CLASS_NAME + "[] select data BtcProject is null");
            throw new BusinessException(10003);
        }
        BeanUtils.copyProperties(param, btcProject);
        // 修改项目表
        try {
            if (btcProjectMapper.updateById(btcProject) <= 0) {
                log.error(CLASS_NAME + "[editProject] update btcProject error");
                throw new BusinessException(10002);
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[editProject] error:{}", e.getMessage());
            throw new BusinessException(10002);
        }
        // 核心成员
        this.updateProjectInfos(proId, param.getBtcProjectInfos());
        // 概念板块
        this.updateProTeps(proId, param.getProLabel());
        // 合作伙伴
        this.updateProCas(proId, param.getCaIds());
        // 已上线交易所
        this.updateProPlatform(proId, param.getPlatforms());
        // 项目路径
        this.updateProjectPaths(proId, param.getProjectPaths());
        // 新闻标题
        this.updateProjectDynamics(proId, param.getProjectDynamics());

    }


    /**
     * 核心成员
     *
     * @param proId
     * @throws BusinessException
     */
    private void updateProjectInfos(String proId, List<BtcProjectInfoModel> projectInfos) throws BusinessException {
        // 删除绑定核心成员
        BtcProjectInfo btcProjectInfo = new BtcProjectInfo();
        btcProjectInfo.setProId(proId);
        EntityWrapper<BtcProjectInfo> infoEntityWrapper = new EntityWrapper<>(btcProjectInfo);
        List<BtcProjectInfo> btcProjectInfos = null;
        try {
            btcProjectInfos = btcProjectInfoService.selectList(infoEntityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[updateProjectInfos] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (!CollectionUtils.isEmpty(btcProjectInfos)) {
            List<String> proInfos = new ArrayList<>();
            btcProjectInfos.forEach(btcProjectInfo1 -> proInfos.add(btcProjectInfo1.getId()));
            if (!btcProjectInfoService.deleteBatchIds(proInfos)) {
                log.error(CLASS_NAME + "[updateProjectInfos] delete data error");
                throw new BusinessException(10002);
            }
        }
        // 重新绑定核心成员
        if (CollectionUtils.isEmpty(projectInfos)) {
            return;
        }
        for (BtcProjectInfoModel projectInfo : projectInfos) {
            BtcProjectInfo info = new BtcProjectInfo();
            BeanUtils.copyProperties(projectInfo, info);
            info.setUpdateTime(new Date());
            info.setId(UidUtil.getId());
            info.setProId(proId);
            try {
                if (!btcProjectInfoService.insert(info)) {
                    log.error(CLASS_NAME + "[updateProjectInfos] data save error");
                    throw new BusinessException(10002);
                }
            } catch (Exception e) {
                throw new BusinessException(10002);
            }
        }
    }

    /**
     * 概念板块
     *
     * @param proId
     * @param proLabel
     * @throws BusinessException
     */
    private void updateProTeps(String proId, List<Long> proLabel) throws BusinessException {
        // 删除绑定概念板块
        ProTep proTep = new ProTep();
        proTep.setProId(proId);
        EntityWrapper<ProTep> proTepEntityWrapper = new EntityWrapper<>(proTep);
        List<ProTep> proTeps = null;
        try {
            proTeps = proTepService.selectList(proTepEntityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[updateProTeps] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (!CollectionUtils.isEmpty(proTeps)) {
            try {
                List<String> proTepIds = new ArrayList<>();
                proTeps.forEach(proTep1 -> proTepIds.add(proTep1.getId()));
                if (!proTepService.deleteBatchIds(proTepIds)) {
                    throw new BusinessException(10005);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[updateProTeps] error:{}", e.getMessage());
                throw new BusinessException(10005);
            }
        }
        // 绑定概念模块
        if (CollectionUtils.isEmpty(proLabel)) {
            return;
        }
        for (Long tepId : proLabel) {
            ProTep p = new ProTep();
            p.setProId(proId);
            p.setTepId(tepId);
            p.setId(UidUtil.getId());
            try {
                if (!proTepService.insert(p)) {
                    throw new BusinessException(10002);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[updateProTeps] error:{}", e.getMessage());
                throw new BusinessException(10002);
            }
        }
    }

    /**
     * 合作伙伴
     *
     * @param proId
     * @param caIds
     * @throws BusinessException
     */
    private void updateProCas(String proId, List<String> caIds) throws BusinessException {
        // 删除绑定
        ProCa proCa = new ProCa();
        proCa.setProId(proId);
        EntityWrapper<ProCa> proCaEntityWrapper = new EntityWrapper<>(proCa);
        List<ProCa> proCas = null;
        try {
            proCas = proCaService.selectList(proCaEntityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[updateProCas] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (!CollectionUtils.isEmpty(proCas)) {
            try {
                List<String> proCaIds = new ArrayList<>();
                proCas.forEach(proCa1 -> proCaIds.add(proCa1.getId()));
                if (!proCaService.deleteBatchIds(proCaIds)) {
                    log.error(CLASS_NAME + "[updateProCas] delete data error ");
                    throw new BusinessException(10005);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[updateProCas] error:{}", e.getMessage());
                throw new BusinessException(10005);
            }
        }
        // 重新绑定
        if (CollectionUtils.isEmpty(caIds)) {
            return;
        }
        for (String caId : caIds) {
            ProCa p = new ProCa();
            p.setId(UidUtil.getId());
            p.setProId(proId);
            p.setCaId(caId);
            try {
                if (!proCaService.insert(p)) {
                    log.error(CLASS_NAME + "[updateProCas] data save error");
                    throw new BusinessException(10002);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[updateProCas] error:{}", e.getMessage());
                throw new BusinessException(10002);
            }
        }
    }

    /**
     * 交易所
     *
     * @param proId
     * @param platforms
     * @throws BusinessException
     */
    private void updateProPlatform(String proId, List<String> platforms) throws BusinessException {
        // 删除绑定
        ProPlatform proPlatform = new ProPlatform();
        proPlatform.setProId(proId);
        EntityWrapper<ProPlatform> proPlatformEntityWrapper = new EntityWrapper<>(proPlatform);
        List<ProPlatform> proPlatforms = null;
        try {
            proPlatforms = proPlatformService.selectList(proPlatformEntityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[] error:{}", e.getMessage());
            throw new BusinessException(10003);
        }
        if (!CollectionUtils.isEmpty(proPlatforms)) {
            List<String> proPlatIds = new ArrayList<>();
            proPlatforms.forEach(proPlatform1 -> proPlatIds.add(proPlatform1.getId()));
            try {
                if (!proPlatformService.deleteBatchIds(proPlatIds)) {
                    log.error(CLASS_NAME + "[] data proPlatform save error ");
                    throw new BusinessException(10005);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[updateProPlatform] error:{}", e.getMessage());
                throw new BusinessException(10005);
            }
        }
        // 重新绑定
        if (CollectionUtils.isEmpty(platforms)) {
            return;
        }
        for (String platform : platforms) {
            ProPlatform p = new ProPlatform();
            p.setProId(proId);
            p.setPlatId(platform);
            p.setId(UidUtil.getId());
            try {
                if (!proPlatformService.insert(p)) {
                    log.error(CLASS_NAME + "[updateProPlatform] ProPlatform save error ");
                    throw new BusinessException(10002);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[updateProPlatform] error:{}", e.getMessage());
                throw new BusinessException(10002);
            }
        }
    }

    /**
     * 新闻标题
     *
     * @param proId
     * @param paths
     * @throws BusinessException
     */
    private void updateProjectPaths(String proId, List<org.btc163.manage.controller.model.ProjectPathModel> paths) throws BusinessException {
        // 删除绑定
        ProjectPath projectPath = new ProjectPath();
        projectPath.setProId(proId);
        EntityWrapper<ProjectPath> pathEntityWrapper = new EntityWrapper<>(projectPath);
        List<ProjectPath> projectPaths = null;
        try {
            projectPaths = projectPathService.selectList(pathEntityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[updateProjectPaths] select data ProjectPath error ");
            throw new BusinessException(10003);
        }
        if (!CollectionUtils.isEmpty(projectPaths)) {
            List<String> pathIds = new ArrayList<>();
            projectPaths.forEach(projectPath1 -> pathIds.add(projectPath1.getId()));
            try {
                if (!projectPathService.deleteBatchIds(pathIds)) {
                    log.error(CLASS_NAME + "[updateProjectPaths] delete data projectPaths error");
                    throw new BusinessException(10005);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[updateProjectPaths] error:[]", e.getMessage());
                throw new BusinessException(10005);
            }
        }
        // 重新绑定
        if (CollectionUtils.isEmpty(paths)) {
            return;
        }
        for (org.btc163.manage.controller.model.ProjectPathModel path : paths) {
            ProjectPath p = new ProjectPath();
            BeanUtils.copyProperties(path, p);
            p.setUpdateTime(new Date());
            p.setProId(proId);
            p.setId(UidUtil.getId());
            try {
                if (!projectPathService.insert(p)) {
                    log.error(CLASS_NAME + "[] save data ProjectPath error ");
                    throw new BusinessException(10002);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[updateProjectPaths] error:{}", e.getMessage());
                throw new BusinessException(10002);
            }
        }
    }

    /**
     * 相关新闻
     *
     * @param proId
     * @param dynamics
     * @throws BusinessException
     */
    private void updateProjectDynamics(String proId, List<org.btc163.manage.controller.model.ProjectDynamicModel> dynamics) throws BusinessException {
        // 删除绑定
        ProjectDynamic projectDynamic = new ProjectDynamic();
        projectDynamic.setProId(proId);
        EntityWrapper<ProjectDynamic> projectDynamicEntityWrapper = new EntityWrapper<>(projectDynamic);
        List<ProjectDynamic> projectDynamics = null;
        try {
            projectDynamics = projectDynamicService.selectList(projectDynamicEntityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[updateProjectDynamics] select data ProjectDynamic error ");
            throw new BusinessException(10003);
        }
        if (!CollectionUtils.isEmpty(projectDynamics)) {
            List<String> dynamicIds = new ArrayList<>();
            projectDynamics.forEach(projectDynamic1 -> dynamicIds.add(projectDynamic1.getId()));
            try {
                if (!projectDynamicService.deleteBatchIds(dynamicIds)) {
                    log.error(CLASS_NAME + "[updateProjectDynamics] delete data projectDynamics error ");
                    throw new BusinessException(10005);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[updateProjectDynamics] error:{}", e.getMessage());
                throw new BusinessException(10005);
            }
        }
        //重新绑定
        if (CollectionUtils.isEmpty(dynamics)) {
            return;
        }
        for (org.btc163.manage.controller.model.ProjectDynamicModel dynamic : dynamics) {
            ProjectDynamic p = new ProjectDynamic();
            BeanUtils.copyProperties(dynamic, p);
            p.setProId(proId);
            p.setId(UidUtil.getId());
            try {
                if (!projectDynamicService.insert(p)) {
                    log.error(CLASS_NAME + "[updateProjectDynamics] save data ProjectDynamic error ");
                    throw new BusinessException(10002);
                }
            } catch (Exception e) {
                log.error(CLASS_NAME + "[updateProjectDynamics] error:{}", e.getMessage());
                throw new BusinessException(10002);
            }
        }
    }
}
