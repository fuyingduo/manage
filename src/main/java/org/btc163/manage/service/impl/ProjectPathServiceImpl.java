package org.btc163.manage.service.impl;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.controller.model.ProjectPathModel;
import org.btc163.manage.entity.ProjectPath;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.mapper.ProjectPathMapper;
import org.btc163.manage.service.ProjectPathService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
@Slf4j
@Service
public class ProjectPathServiceImpl extends ServiceImpl<ProjectPathMapper, ProjectPath> implements ProjectPathService {

    private static final String CLASS_NAME = "[ProjectPathServiceImpl]";

    @Autowired
    private ProjectPathMapper projectPathMapper;

    @Transactional(rollbackFor = BusinessException.class)
    @Override
    public void addProjectPaths(List<ProjectPathModel> projectPaths, String projectId) throws BusinessException {
        if (CollectionUtils.isEmpty(projectPaths) || StringUtils.isEmpty(projectId)) {
            log.error(CLASS_NAME + "[addProjectPaths] request data error projectPaths is empty");
            throw new BusinessException(10001);
        }
        for (ProjectPathModel projectPath : projectPaths) {
            if (projectPath == null) {
                continue;
            }
            if (projectPath.getPaTime() == null && StringUtils.isEmpty(projectPath.getPaIncident())) {
                continue;
            }
            ProjectPath path = new ProjectPath();
            BeanUtils.copyProperties(projectPath, path);
            path.setProId(projectId);
            path.setId(UidUtil.getId());
            path.setUpdateTime(new Date());
            if (projectPathMapper.insert(path) <= 0) {
                log.error(CLASS_NAME + "[addProjectPaths] data save error projectPath:{}", projectPath.toString());
                throw new BusinessException(10002);
            }
        }
    }
}
