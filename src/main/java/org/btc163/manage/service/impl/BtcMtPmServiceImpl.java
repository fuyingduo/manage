package org.btc163.manage.service.impl;

import org.btc163.manage.entity.BtcMtPm;
import org.btc163.manage.mapper.BtcMtPmMapper;
import org.btc163.manage.service.BtcMtPmService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-04
 */
@Service
public class BtcMtPmServiceImpl extends ServiceImpl<BtcMtPmMapper, BtcMtPm> implements BtcMtPmService {

}
