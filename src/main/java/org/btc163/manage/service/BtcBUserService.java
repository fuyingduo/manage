package org.btc163.manage.service;

import org.btc163.manage.controller.param.UserParam;
import org.btc163.manage.controller.param.UserPasswordParam;
import org.btc163.manage.entity.BUser;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.exception.BusinessException;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-21
 */
public interface BtcBUserService extends IService<BUser> {

    String getUserList(UserParam userParam) throws BusinessException;

    void editPassword(UserPasswordParam userPasswordParam) throws BusinessException;
}
