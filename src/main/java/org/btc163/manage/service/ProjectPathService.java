package org.btc163.manage.service;

import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.controller.model.ProjectPathModel;
import org.btc163.manage.entity.ProjectPath;
import org.btc163.manage.exception.BusinessException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
public interface ProjectPathService extends IService<ProjectPath> {

    void addProjectPaths(List<ProjectPathModel> projectPaths, String projectId) throws BusinessException;

}
