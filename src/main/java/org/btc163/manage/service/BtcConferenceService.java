package org.btc163.manage.service;

import org.btc163.manage.controller.param.BtcConferenceParam;
import org.btc163.manage.entity.BtcConference;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.exception.BusinessException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-09-04
 */
public interface BtcConferenceService extends IService<BtcConference> {

    void addCon(BtcConferenceParam btcConferenceParam) throws BusinessException;

    void editCon(BtcConferenceParam btcConferenceParam) throws BusinessException;

    String getConferenceList(BtcConferenceParam btcConferenceParam) throws BusinessException;
}
