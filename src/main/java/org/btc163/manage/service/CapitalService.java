package org.btc163.manage.service;

import org.btc163.manage.app.model.CapitalInfoModel;
import org.btc163.manage.app.model.CapitalListModel;
import org.btc163.manage.controller.model.CapilaInfoSelectModel;
import org.btc163.manage.controller.param.CapitalParam;
import org.btc163.manage.entity.Capital;
import com.baomidou.mybatisplus.service.IService;
import org.btc163.manage.exception.BusinessException;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
public interface CapitalService extends IService<Capital> {

    void addCapitalInfo(CapitalParam capitalParam) throws BusinessException;

    List<CapilaInfoSelectModel> getSelectCapilaInfos() throws BusinessException;

    List<CapitalListModel> getCapitalPageList(Integer page, Integer pageSize, String caName, List<String> caLab) throws BusinessException;

    Integer getCapiTalSizeTotal(String caName, List<String> caLab) throws BusinessException;

    CapitalInfoModel getCapitalInfos(String caId) throws BusinessException;

    String getCapitalList(CapitalParam capitalParam) throws BusinessException;

    void editCapitalInfo(CapitalParam capitalParam) throws BusinessException;

}
