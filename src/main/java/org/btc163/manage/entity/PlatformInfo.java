package org.btc163.manage.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-08-21
 */
@TableName("platform_info")
public class PlatformInfo extends Model<PlatformInfo> {

    private static final long serialVersionUID = 1L;

    private String name;
    private String content1;
    private String content2;
    @TableField("file_name")
    private String fileName;
    private String logo;
    private String microblog;
    private String url;
    @TableField("trad_type")
    private String tradType;
    private String country;
    private Integer pid;
    @TableField("star_num")
    private String starNum;
    private BigDecimal h24;
    @TableField("symbol_number")
    private String symbolNumber;


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContent1() {
        return content1;
    }

    public void setContent1(String content1) {
        this.content1 = content1;
    }

    public String getContent2() {
        return content2;
    }

    public void setContent2(String content2) {
        this.content2 = content2;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getMicroblog() {
        return microblog;
    }

    public void setMicroblog(String microblog) {
        this.microblog = microblog;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getTradType() {
        return tradType;
    }

    public void setTradType(String tradType) {
        this.tradType = tradType;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }

    public String getStarNum() {
        return starNum;
    }

    public void setStarNum(String starNum) {
        this.starNum = starNum;
    }

    public BigDecimal getH24() {
        return h24;
    }

    public void setH24(BigDecimal h24) {
        this.h24 = h24;
    }

    public String getSymbolNumber() {
        return symbolNumber;
    }

    public void setSymbolNumber(String symbolNumber) {
        this.symbolNumber = symbolNumber;
    }

    @Override
    protected Serializable pkVal() {
        return this.name;
    }

    @Override
    public String toString() {
        return "PlatformInfo{" +
        ", name=" + name +
        ", content1=" + content1 +
        ", content2=" + content2 +
        ", fileName=" + fileName +
        ", logo=" + logo +
        ", microblog=" + microblog +
        ", url=" + url +
        ", tradType=" + tradType +
        ", country=" + country +
        ", pid=" + pid +
        ", starNum=" + starNum +
        ", h24=" + h24 +
        ", symbolNumber=" + symbolNumber +
        "}";
    }
}
