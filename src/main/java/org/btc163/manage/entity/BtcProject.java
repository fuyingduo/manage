package org.btc163.manage.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author fuyd
 * @since 2018-08-24
 */
@TableName("btc_project")
public class BtcProject extends Model<BtcProject> {

    private static final long serialVersionUID = 1L;

    /**
     * ID
     */
    private String id;
    /**
     * 项目名称
     */
    @TableField("pro_name")
    private String proName;
    /**
     * 项目logo
     */
    @TableField("pro_logo")
    private String proLogo;
    /**
     * Token总量
     */
    @TableField("pro_token_num")
    private String proTokenNum;
    /**
     * 项目阶段
     */
    @TableField("pro_stage")
    private Integer proStage;
    /**
     * 共识机制
     */
    @TableField("pro_consesus_mechanism")
    private String proConsesusMechanism;
    /**
     * 代币分配
     */
    @TableField("pro_token_allot")
    private String proTokenAllot;
    /**
     * 标签
     */
    @TableField("pro_label")
    private String proLabel;
    /**
     * 项目介绍
     */
    @TableField("pro_introduce")
    private String proIntroduce;
    /**
     * 合作伙伴
     */
    @TableField("pop_exchange")
    private String popExchange;
    /**
     * 招募时间
     */
    @TableField("re_time")
    private String reTime;
    /**
     * 招募时间结束
     */
    @TableField("re_time_end")
    private String reTimeEnd;
    /**
     * 兑换比例
     */
    @TableField("re_ratio")
    private String reRatio;
    /**
     * 硬顶
     */
    @TableField("re_hard_top")
    private String reHardTop;
    /**
     * 锁仓
     */
    @TableField("re_lock_up")
    private Integer reLockUp;
    /**
     * 募集资金总数
     */
    @TableField("raise_token_sum")
    private String raiseTokenSum;
    /**
     * 募集资金分配
     */
    @TableField("re_capital_allot")
    private String reCapitalAllot;
    /**
     * 备注
     */
    @TableField("re_remake")
    private String reRemake;
    /**
     * 官网
     */
    @TableField("co_official")
    private String coOfficial;
    /**
     * 区块链浏览器
     */
    @TableField("co_browser")
    private String coBrowser;
    /**
     * 白皮书
     */
    @TableField("co_white")
    private String coWhite;
    /**
     * 钱包
     */
    @TableField("co_wallet")
    private String coWallet;
    /**
     * 微信
     */
    @TableField("co_wechat")
    private String coWechat;
    /**
     * 状态
     */
    @TableField("pro_status")
    private String proStatus;
    @TableField("create_time")
    private Date createTime;
    @TableField("update_time")
    private Date updateTime;

    @TableField("pro_hits")
    private Long proHits;

    @TableField("pro_praise")
    private Integer proPraise;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProName() {
        return proName;
    }

    public void setProName(String proName) {
        this.proName = proName;
    }

    public String getProLogo() {
        return proLogo;
    }

    public void setProLogo(String proLogo) {
        this.proLogo = proLogo;
    }

    public String getProTokenNum() {
        return proTokenNum;
    }

    public void setProTokenNum(String proTokenNum) {
        this.proTokenNum = proTokenNum;
    }

    public Integer getProStage() {
        return proStage;
    }

    public void setProStage(Integer proStage) {
        this.proStage = proStage;
    }

    public String getProConsesusMechanism() {
        return proConsesusMechanism;
    }

    public void setProConsesusMechanism(String proConsesusMechanism) {
        this.proConsesusMechanism = proConsesusMechanism;
    }

    public String getProTokenAllot() {
        return proTokenAllot;
    }

    public void setProTokenAllot(String proTokenAllot) {
        this.proTokenAllot = proTokenAllot;
    }

    public String getProLabel() {
        return proLabel;
    }

    public void setProLabel(String proLabel) {
        this.proLabel = proLabel;
    }

    public String getProIntroduce() {
        return proIntroduce;
    }

    public void setProIntroduce(String proIntroduce) {
        this.proIntroduce = proIntroduce;
    }

    public String getPopExchange() {
        return popExchange;
    }

    public void setPopExchange(String popExchange) {
        this.popExchange = popExchange;
    }

    public String getReTime() {
        return reTime;
    }

    public void setReTime(String reTime) {
        this.reTime = reTime;
    }

    public String getReTimeEnd() {
        return reTimeEnd;
    }

    public void setReTimeEnd(String reTimeEnd) {
        this.reTimeEnd = reTimeEnd;
    }

    public String getReRatio() {
        return reRatio;
    }

    public void setReRatio(String reRatio) {
        this.reRatio = reRatio;
    }

    public String getReHardTop() {
        return reHardTop;
    }

    public void setReHardTop(String reHardTop) {
        this.reHardTop = reHardTop;
    }

    public Integer getReLockUp() {
        return reLockUp;
    }

    public void setReLockUp(Integer reLockUp) {
        this.reLockUp = reLockUp;
    }

    public String getRaiseTokenSum() {
        return raiseTokenSum;
    }

    public void setRaiseTokenSum(String raiseTokenSum) {
        this.raiseTokenSum = raiseTokenSum;
    }

    public String getReCapitalAllot() {
        return reCapitalAllot;
    }

    public void setReCapitalAllot(String reCapitalAllot) {
        this.reCapitalAllot = reCapitalAllot;
    }

    public String getReRemake() {
        return reRemake;
    }

    public void setReRemake(String reRemake) {
        this.reRemake = reRemake;
    }

    public String getCoOfficial() {
        return coOfficial;
    }

    public void setCoOfficial(String coOfficial) {
        this.coOfficial = coOfficial;
    }

    public String getCoBrowser() {
        return coBrowser;
    }

    public void setCoBrowser(String coBrowser) {
        this.coBrowser = coBrowser;
    }

    public String getCoWhite() {
        return coWhite;
    }

    public void setCoWhite(String coWhite) {
        this.coWhite = coWhite;
    }

    public String getCoWallet() {
        return coWallet;
    }

    public void setCoWallet(String coWallet) {
        this.coWallet = coWallet;
    }

    public String getCoWechat() {
        return coWechat;
    }

    public void setCoWechat(String coWechat) {
        this.coWechat = coWechat;
    }

    public String getProStatus() {
        return proStatus;
    }

    public void setProStatus(String proStatus) {
        this.proStatus = proStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getProHits() {
        return proHits;
    }

    public void setProHits(Long proHits) {
        this.proHits = proHits;
    }

    public Integer getProPraise() {
        return proPraise;
    }

    public void setProPraise(Integer proPraise) {
        this.proPraise = proPraise;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BtcProject{" +
                "id='" + id + '\'' +
                ", proName='" + proName + '\'' +
                ", proLogo='" + proLogo + '\'' +
                ", proTokenNum='" + proTokenNum + '\'' +
                ", proStage=" + proStage +
                ", proConsesusMechanism='" + proConsesusMechanism + '\'' +
                ", proTokenAllot='" + proTokenAllot + '\'' +
                ", proLabel='" + proLabel + '\'' +
                ", proIntroduce='" + proIntroduce + '\'' +
                ", popExchange='" + popExchange + '\'' +
                ", reTime='" + reTime + '\'' +
                ", reTimeEnd='" + reTimeEnd + '\'' +
                ", reRatio='" + reRatio + '\'' +
                ", reHardTop='" + reHardTop + '\'' +
                ", reLockUp=" + reLockUp +
                ", raiseTokenSum='" + raiseTokenSum + '\'' +
                ", reCapitalAllot='" + reCapitalAllot + '\'' +
                ", reRemake='" + reRemake + '\'' +
                ", coOfficial='" + coOfficial + '\'' +
                ", coBrowser='" + coBrowser + '\'' +
                ", coWhite='" + coWhite + '\'' +
                ", coWallet='" + coWallet + '\'' +
                ", coWechat='" + coWechat + '\'' +
                ", proStatus='" + proStatus + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", proHits=" + proHits +
                ", proPraise=" + proPraise +
                '}';
    }
}
