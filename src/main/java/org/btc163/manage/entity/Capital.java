package org.btc163.manage.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author fuyd
 * @since 2018-08-24
 */
@TableName("capital")
public class Capital extends Model<Capital> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 资本名称
     */
    @TableField("ca_name")
    private String caName;
    /**
     * 资本logo
     */
    @TableField("ca_logo")
    private String caLogo;
    /**
     * 成立时间
     */
    @TableField("ca_establish")
    private String caEstablish;
    /**
     * 简介
     */
    @TableField("ca_brief")
    private String caBrief;
    /**
     * 标签
     */
    @TableField("ca_lab")
    private String caLab;
    /**
     * 网址
     */
    @TableField("ca_url")
    private String caUrl;
    /**
     * 状态0正常
     */
    @TableField("ca_status")
    private Integer caStatus;
    /**
     * 创建时间
     */
    @TableField("create_time")
    private Date createTime;
    /**
     * 修改时间
     */
    private Date update;

    /**
     * 参投项目
     */
    @TableField("ca_project")
    private String caProject;

    /**
     * 点击量
     */
    @TableField("ca_statistics")
    private Long caStatistics;

    /**
     * 点赞
     */
    @TableField("ca_praise")
    private Integer caPraise;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCaName() {
        return caName;
    }

    public void setCaName(String caName) {
        this.caName = caName;
    }

    public String getCaLogo() {
        return caLogo;
    }

    public void setCaLogo(String caLogo) {
        this.caLogo = caLogo;
    }

    public String getCaEstablish() {
        return caEstablish;
    }

    public void setCaEstablish(String caEstablish) {
        this.caEstablish = caEstablish;
    }

    public String getCaBrief() {
        return caBrief;
    }

    public void setCaBrief(String caBrief) {
        this.caBrief = caBrief;
    }

    public String getCaLab() {
        return caLab;
    }

    public void setCaLab(String caLab) {
        this.caLab = caLab;
    }

    public String getCaUrl() {
        return caUrl;
    }

    public void setCaUrl(String caUrl) {
        this.caUrl = caUrl;
    }

    public Integer getCaStatus() {
        return caStatus;
    }

    public void setCaStatus(Integer caStatus) {
        this.caStatus = caStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdate() {
        return update;
    }

    public void setUpdate(Date update) {
        this.update = update;
    }

    public String getCaProject() {
        return caProject;
    }

    public void setCaProject(String caProject) {
        this.caProject = caProject;
    }

    public Long getCaStatistics() {
        return caStatistics;
    }

    public void setCaStatistics(Long caStatistics) {
        this.caStatistics = caStatistics;
    }

    public Integer getCaPraise() {
        return caPraise;
    }

    public void setCaPraise(Integer caPraise) {
        this.caPraise = caPraise;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "Capital{" +
                "id='" + id + '\'' +
                ", caName='" + caName + '\'' +
                ", caLogo='" + caLogo + '\'' +
                ", caEstablish='" + caEstablish + '\'' +
                ", caBrief='" + caBrief + '\'' +
                ", caLab='" + caLab + '\'' +
                ", caUrl='" + caUrl + '\'' +
                ", caStatus=" + caStatus +
                ", createTime=" + createTime +
                ", update=" + update +
                ", caProject='" + caProject + '\'' +
                ", caStatistics=" + caStatistics +
                ", caPraise=" + caPraise +
                '}';
    }
}
