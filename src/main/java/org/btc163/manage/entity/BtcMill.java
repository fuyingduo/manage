package org.btc163.manage.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-09-05
 */
@TableName("btc_mill")
public class BtcMill extends Model<BtcMill> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 名称
     */
    @TableField("mi_name")
    private String miName;
    /**
     * logo
     */
    @TableField("mi_logo")
    private String miLogo;
    /**
     * 简介
     */
    @TableField("mi_brief")
    private String miBrief;
    /**
     * 官网
     */
    @TableField("mi_website")
    private String miWebsite;
    /**
     * 状态
     */
    @TableField("mi_status")
    private Integer miStatus;
    /**
     * 点击量
     */
    @TableField("mi_hits")
    private Long miHits;
    /**
     * 赞
     */
    @TableField("mi_like")
    private Long miLike;
    @TableField("create_time")
    private Date createTime;
    @TableField("update_time")
    private Date updateTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMiName() {
        return miName;
    }

    public void setMiName(String miName) {
        this.miName = miName;
    }

    public String getMiLogo() {
        return miLogo;
    }

    public void setMiLogo(String miLogo) {
        this.miLogo = miLogo;
    }

    public String getMiBrief() {
        return miBrief;
    }

    public void setMiBrief(String miBrief) {
        this.miBrief = miBrief;
    }

    public String getMiWebsite() {
        return miWebsite;
    }

    public void setMiWebsite(String miWebsite) {
        this.miWebsite = miWebsite;
    }

    public Integer getMiStatus() {
        return miStatus;
    }

    public void setMiStatus(Integer miStatus) {
        this.miStatus = miStatus;
    }

    public Long getMiHits() {
        return miHits;
    }

    public void setMiHits(Long miHits) {
        this.miHits = miHits;
    }

    public Long getMiLike() {
        return miLike;
    }

    public void setMiLike(Long miLike) {
        this.miLike = miLike;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BtcMill{" +
        ", id=" + id +
        ", miName=" + miName +
        ", miLogo=" + miLogo +
        ", miBrief=" + miBrief +
        ", miWebsite=" + miWebsite +
        ", miStatus=" + miStatus +
        ", miHits=" + miHits +
        ", miLike=" + miLike +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
