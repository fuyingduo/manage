package org.btc163.manage.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-08-24
 */
@TableName("pro_platform")
public class ProPlatform extends Model<ProPlatform> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 交易所ID
     */
    @TableField("plat_id")
    private String platId;
    /**
     * 项目ID
     */
    @TableField("pro_id")
    private String proId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPlatId() {
        return platId;
    }

    public void setPlatId(String platId) {
        this.platId = platId;
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ProPlatform{" +
        ", id=" + id +
        ", platId=" + platId +
        ", proId=" + proId +
        "}";
    }
}
