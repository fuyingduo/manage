package org.btc163.manage.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-09-04
 */
@TableName("btc_conference")
public class BtcConference extends Model<BtcConference> {

    private static final long serialVersionUID = 1L;

    private String id;
    @TableField("pm_month")
    private String pmMonth;
    @TableField("pm_city_name")
    private String pmCityName;
    @TableField("pm_desc")
    private String pmDesc;
    @TableField("pm_start_time")
    private String pmStartTime;
    @TableField("pm_end_time")
    private String pmEndTime;
    @TableField("pm_name")
    private String pmName;
    @TableField("pm_province_name")
    private String pmProvinceName;
    @TableField("pm_status")
    private Integer pmStatus;
    @TableField("create_time")
    private Date createTime;
    @TableField("update_time")
    private Date updateTime;
    @TableField("pm_site")
    private String pmSite;
    @TableField("pm_brief")
    private String pmBrief;
    @TableField("pm_honored")
    private String pmHonored;
    @TableField("pm_schedule")
    private String pmSchedule;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPmMonth() {
        return pmMonth;
    }

    public void setPmMonth(String pmMonth) {
        this.pmMonth = pmMonth;
    }

    public String getPmCityName() {
        return pmCityName;
    }

    public void setPmCityName(String pmCityName) {
        this.pmCityName = pmCityName;
    }

    public String getPmDesc() {
        return pmDesc;
    }

    public void setPmDesc(String pmDesc) {
        this.pmDesc = pmDesc;
    }

    public String getPmStartTime() {
        return pmStartTime;
    }

    public void setPmStartTime(String pmStartTime) {
        this.pmStartTime = pmStartTime;
    }

    public String getPmEndTime() {
        return pmEndTime;
    }

    public void setPmEndTime(String pmEndTime) {
        this.pmEndTime = pmEndTime;
    }

    public String getPmName() {
        return pmName;
    }

    public void setPmName(String pmName) {
        this.pmName = pmName;
    }

    public String getPmProvinceName() {
        return pmProvinceName;
    }

    public void setPmProvinceName(String pmProvinceName) {
        this.pmProvinceName = pmProvinceName;
    }

    public Integer getPmStatus() {
        return pmStatus;
    }

    public void setPmStatus(Integer pmStatus) {
        this.pmStatus = pmStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getPmSite() {
        return pmSite;
    }

    public void setPmSite(String pmSite) {
        this.pmSite = pmSite;
    }

    public String getPmBrief() {
        return pmBrief;
    }

    public void setPmBrief(String pmBrief) {
        this.pmBrief = pmBrief;
    }

    public String getPmHonored() {
        return pmHonored;
    }

    public void setPmHonored(String pmHonored) {
        this.pmHonored = pmHonored;
    }

    public String getPmSchedule() {
        return pmSchedule;
    }

    public void setPmSchedule(String pmSchedule) {
        this.pmSchedule = pmSchedule;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BtcConference{" +
        ", id=" + id +
        ", pmMonth=" + pmMonth +
        ", pmCityName=" + pmCityName +
        ", pmDesc=" + pmDesc +
        ", pmStartTime=" + pmStartTime +
        ", pmEndTime=" + pmEndTime +
        ", pmName=" + pmName +
        ", pmProvinceName=" + pmProvinceName +
        ", pmStatus=" + pmStatus +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", pmSite=" + pmSite +
        ", pmBrief=" + pmBrief +
        ", pmHonored=" + pmHonored +
        ", pmSchedule=" + pmSchedule +
        "}";
    }
}
