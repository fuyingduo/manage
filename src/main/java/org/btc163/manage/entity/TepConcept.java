package org.btc163.manage.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
@TableName("tep_concept")
public class TepConcept extends Model<TepConcept> {

    private static final long serialVersionUID = 1L;

    private Long id;
    /**
     * 概念模版
     */
    private String name;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TepConceptEnum{" +
        ", id=" + id +
        ", name=" + name +
        "}";
    }
}
