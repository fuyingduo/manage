package org.btc163.manage.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-09-13
 */
@TableName("ts_schedule_triggers")
public class TsScheduleTriggers extends Model<TsScheduleTriggers> {

    private static final long serialVersionUID = 1L;

    private Long id;
    @TableField("q_cron")
    private String qCron;
    @TableField("q_job_name")
    private String qJobName;
    @TableField("q_status")
    private Integer qStatus;
    @TableField("q_job_group")
    private String qJobGroup;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getqCron() {
        return qCron;
    }

    public void setqCron(String qCron) {
        this.qCron = qCron;
    }

    public String getqJobName() {
        return qJobName;
    }

    public void setqJobName(String qJobName) {
        this.qJobName = qJobName;
    }

    public Integer getqStatus() {
        return qStatus;
    }

    public void setqStatus(Integer qStatus) {
        this.qStatus = qStatus;
    }

    public String getqJobGroup() {
        return qJobGroup;
    }

    public void setqJobGroup(String qJobGroup) {
        this.qJobGroup = qJobGroup;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "TsScheduleTriggers{" +
        ", id=" + id +
        ", qCron=" + qCron +
        ", qJobName=" + qJobName +
        ", qStatus=" + qStatus +
        ", qJobGroup=" + qJobGroup +
        "}";
    }
}
