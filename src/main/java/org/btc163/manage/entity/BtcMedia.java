package org.btc163.manage.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-08-31
 */
@TableName("btc_media")
public class BtcMedia extends Model<BtcMedia> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 名称
     */
    @TableField("mt_name")
    private String mtName;
    /**
     * logo
     */
    @TableField("mt_logo")
    private String mtLogo;
    /**
     * 标签
     */
    @TableField("mt_spell")
    private String mtSpell;
    /**
     * 阅读量
     */
    @TableField("mt_read")
    private String mtRead;
    /**
     * 点赞
     */
    @TableField("mt_praise")
    private String mtPraise;
    @TableField("mt_status")
    private Integer mtStatus;
    @TableField("create_time")
    private Date createTime;
    /**
     * 分类
     */
    @TableField("mt_type")
    private Integer mtType;
    @TableField("update_time")
    private Date updateTime;

    @TableField("mt_introduce")
    private String mtIntroduce;

    @TableField("mt_url")
    private String mtUrl;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMtName() {
        return mtName;
    }

    public void setMtName(String mtName) {
        this.mtName = mtName;
    }

    public String getMtLogo() {
        return mtLogo;
    }

    public void setMtLogo(String mtLogo) {
        this.mtLogo = mtLogo;
    }

    public String getMtSpell() {
        return mtSpell;
    }

    public void setMtSpell(String mtSpell) {
        this.mtSpell = mtSpell;
    }

    public String getMtRead() {
        return mtRead;
    }

    public void setMtRead(String mtRead) {
        this.mtRead = mtRead;
    }

    public String getMtPraise() {
        return mtPraise;
    }

    public void setMtPraise(String mtPraise) {
        this.mtPraise = mtPraise;
    }

    public Integer getMtStatus() {
        return mtStatus;
    }

    public void setMtStatus(Integer mtStatus) {
        this.mtStatus = mtStatus;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getMtType() {
        return mtType;
    }

    public void setMtType(Integer mtType) {
        this.mtType = mtType;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getMtIntroduce() {
        return mtIntroduce;
    }

    public void setMtIntroduce(String mtIntroduce) {
        this.mtIntroduce = mtIntroduce;
    }

    public String getMtUrl() {
        return mtUrl;
    }

    public void setMtUrl(String mtUrl) {
        this.mtUrl = mtUrl;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BtcMedia{" +
                "id='" + id + '\'' +
                ", mtName='" + mtName + '\'' +
                ", mtLogo='" + mtLogo + '\'' +
                ", mtSpell='" + mtSpell + '\'' +
                ", mtRead='" + mtRead + '\'' +
                ", mtPraise='" + mtPraise + '\'' +
                ", mtStatus=" + mtStatus +
                ", createTime=" + createTime +
                ", mtType=" + mtType +
                ", updateTime=" + updateTime +
                ", mtIntroduce='" + mtIntroduce + '\'' +
                ", mtUrl='" + mtUrl + '\'' +
                '}';
    }
}
