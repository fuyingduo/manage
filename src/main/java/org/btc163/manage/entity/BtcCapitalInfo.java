package org.btc163.manage.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 *
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
@TableName("btc_capital_info")
public class BtcCapitalInfo extends Model<BtcCapitalInfo> {

    private static final long serialVersionUID = 1L;

    private String id;
    @TableField("in_user_name")
    private String inUserName;
    @TableField("in_head_image")
    private String inHeadImage;
    @TableField("in_position")
    private String inPosition;
    @TableField("in_brief")
    private String inBrief;
    @TableField("ca_id")
    private String caId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInUserName() {
        return inUserName;
    }

    public void setInUserName(String inUserName) {
        this.inUserName = inUserName;
    }

    public String getInHeadImage() {
        return inHeadImage;
    }

    public void setInHeadImage(String inHeadImage) {
        this.inHeadImage = inHeadImage;
    }

    public String getInPosition() {
        return inPosition;
    }

    public void setInPosition(String inPosition) {
        this.inPosition = inPosition;
    }

    public String getInBrief() {
        return inBrief;
    }

    public void setInBrief(String inBrief) {
        this.inBrief = inBrief;
    }

    public String getCaId() {
        return caId;
    }

    public void setCaId(String caId) {
        this.caId = caId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BtcCapitalInfo{" +
                ", id=" + id +
                ", inUserName=" + inUserName +
                ", inHeadImage=" + inHeadImage +
                ", inPosition=" + inPosition +
                ", inBrief=" + inBrief +
                ", caId=" + caId +
                "}";
    }
}
