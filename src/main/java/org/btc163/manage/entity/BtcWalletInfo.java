package org.btc163.manage.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-09-05
 */
@TableName("btc_wallet_info")
public class BtcWalletInfo extends Model<BtcWalletInfo> {

    private static final long serialVersionUID = 1L;

    private String id;
    @TableField("w_name")
    private String wName;
    @TableField("w_position")
    private String wPosition;
    @TableField("w_head_image")
    private String wHeadImage;
    @TableField("w_brief")
    private String wBrief;
    @TableField("wa_id")
    private String waId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getwName() {
        return wName;
    }

    public void setwName(String wName) {
        this.wName = wName;
    }

    public String getwPosition() {
        return wPosition;
    }

    public void setwPosition(String wPosition) {
        this.wPosition = wPosition;
    }

    public String getwHeadImage() {
        return wHeadImage;
    }

    public void setwHeadImage(String wHeadImage) {
        this.wHeadImage = wHeadImage;
    }

    public String getwBrief() {
        return wBrief;
    }

    public void setwBrief(String wBrief) {
        this.wBrief = wBrief;
    }

    public String getWaId() {
        return waId;
    }

    public void setWaId(String waId) {
        this.waId = waId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BtcWalletInfo{" +
        ", id=" + id +
        ", wName=" + wName +
        ", wPosition=" + wPosition +
        ", wHeadImage=" + wHeadImage +
        ", wBrief=" + wBrief +
        ", waId=" + waId +
        "}";
    }
}
