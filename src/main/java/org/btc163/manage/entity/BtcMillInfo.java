package org.btc163.manage.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-09-05
 */
@TableName("btc_mill_info")
public class BtcMillInfo extends Model<BtcMillInfo> {

    private static final long serialVersionUID = 1L;

    private String id;
    @TableField("m_name")
    private String mName;
    @TableField("m_postition")
    private String mPostition;
    @TableField("m_head_image")
    private String mHeadImage;
    @TableField("m_brief")
    private String mBrief;
    @TableField("mi_id")
    private String miId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmPostition() {
        return mPostition;
    }

    public void setmPostition(String mPostition) {
        this.mPostition = mPostition;
    }

    public String getmHeadImage() {
        return mHeadImage;
    }

    public void setmHeadImage(String mHeadImage) {
        this.mHeadImage = mHeadImage;
    }

    public String getmBrief() {
        return mBrief;
    }

    public void setmBrief(String mBrief) {
        this.mBrief = mBrief;
    }

    public String getMiId() {
        return miId;
    }

    public void setMiId(String miId) {
        this.miId = miId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BtcMillInfo{" +
        ", id=" + id +
        ", mName=" + mName +
        ", mPostition=" + mPostition +
        ", mHeadImage=" + mHeadImage +
        ", mBrief=" + mBrief +
        ", miId=" + miId +
        "}";
    }
}
