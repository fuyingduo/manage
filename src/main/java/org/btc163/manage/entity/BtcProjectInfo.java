package org.btc163.manage.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
@TableName("btc_project_info")
public class BtcProjectInfo extends Model<BtcProjectInfo> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 姓名
     */
    @TableField("in_user_name")
    private String inUserName;
    /**
     * 头像
     */
    @TableField("in_head_image")
    private String inHeadImage;
    /**
     * 职位
     */
    @TableField("in_position")
    private String inPosition;
    /**
     * 简介
     */
    @TableField("in_brief")
    private String inBrief;
    /**
     * 项目ID
     */
    @TableField("pro_id")
    private String proId;
    @TableField("create_time")
    private Date createTime;
    @TableField("update_time")
    private Date updateTime;
    /**
     * 状态
     */
    @TableField("in_status")
    private String inStatus;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getInUserName() {
        return inUserName;
    }

    public void setInUserName(String inUserName) {
        this.inUserName = inUserName;
    }

    public String getInHeadImage() {
        return inHeadImage;
    }

    public void setInHeadImage(String inHeadImage) {
        this.inHeadImage = inHeadImage;
    }

    public String getInPosition() {
        return inPosition;
    }

    public void setInPosition(String inPosition) {
        this.inPosition = inPosition;
    }

    public String getInBrief() {
        return inBrief;
    }

    public void setInBrief(String inBrief) {
        this.inBrief = inBrief;
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getInStatus() {
        return inStatus;
    }

    public void setInStatus(String inStatus) {
        this.inStatus = inStatus;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BtcProjectInfo{" +
        ", id=" + id +
        ", inUserName=" + inUserName +
        ", inHeadImage=" + inHeadImage +
        ", inPosition=" + inPosition +
        ", inBrief=" + inBrief +
        ", proId=" + proId +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", inStatus=" + inStatus +
        "}";
    }
}
