package org.btc163.manage.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-08-31
 */
@TableName("btc_media_info")
public class BtcMediaInfo extends Model<BtcMediaInfo> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 媒体ID
     */
    @TableField("m_id")
    private String mId;
    /**
     * 姓名职位
     */
    @TableField("i_name_position")
    private String iNamePosition;
    /**
     * logo
     */
    @TableField("i_logo")
    private String iLogo;
    /**
     * 简介
     */
    @TableField("i_brief")
    private String iBrief;
    @TableField("create_time")
    private Date createTime;
    @TableField("i_status")
    private Integer iStatus;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getiNamePosition() {
        return iNamePosition;
    }

    public void setiNamePosition(String iNamePosition) {
        this.iNamePosition = iNamePosition;
    }

    public String getiLogo() {
        return iLogo;
    }

    public void setiLogo(String iLogo) {
        this.iLogo = iLogo;
    }

    public String getiBrief() {
        return iBrief;
    }

    public void setiBrief(String iBrief) {
        this.iBrief = iBrief;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Integer getiStatus() {
        return iStatus;
    }

    public void setiStatus(Integer iStatus) {
        this.iStatus = iStatus;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BtcMediaInfo{" +
        ", id=" + id +
        ", mId=" + mId +
        ", iNamePosition=" + iNamePosition +
        ", iLogo=" + iLogo +
        ", iBrief=" + iBrief +
        ", createTime=" + createTime +
        ", iStatus=" + iStatus +
        "}";
    }
}
