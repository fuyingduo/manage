package org.btc163.manage.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
@TableName("project_dynamic")
public class ProjectDynamic extends Model<ProjectDynamic> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 新闻标题
     */
    @TableField("dy_title")
    private String dyTitle;
    /**
     * 新闻地址
     */
    @TableField("dy_url")
    private String dyUrl;
    /**
     * 项目ID
     */
    @TableField("pro_id")
    private String proId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDyTitle() {
        return dyTitle;
    }

    public void setDyTitle(String dyTitle) {
        this.dyTitle = dyTitle;
    }

    public String getDyUrl() {
        return dyUrl;
    }

    public void setDyUrl(String dyUrl) {
        this.dyUrl = dyUrl;
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ProjectDynamic{" +
        ", id=" + id +
        ", dyTitle=" + dyTitle +
        ", dyUrl=" + dyUrl +
        ", proId=" + proId +
        "}";
    }
}
