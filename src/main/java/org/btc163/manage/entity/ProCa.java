package org.btc163.manage.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
@TableName("pro_ca")
public class ProCa extends Model<ProCa> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 项目主键
     */
    @TableField("pro_id")
    private String proId;
    /**
     * 资本主键
     */
    @TableField("ca_id")
    private String caId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public String getCaId() {
        return caId;
    }

    public void setCaId(String caId) {
        this.caId = caId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ProCa{" +
        ", id=" + id +
        ", proId=" + proId +
        ", caId=" + caId +
        "}";
    }
}
