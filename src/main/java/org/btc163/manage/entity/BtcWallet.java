package org.btc163.manage.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-09-05
 */
@TableName("btc_wallet")
public class BtcWallet extends Model<BtcWallet> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 钱包名称
     */
    @TableField("wa_name")
    private String waName;
    /**
     * logo
     */
    @TableField("wa_logo")
    private String waLogo;
    /**
     * 简介
     */
    @TableField("wa_brief")
    private String waBrief;
    /**
     * 官网
     */
    @TableField("wa_website")
    private String waWebsite;
    /**
     * 状态
     */
    @TableField("wa_status")
    private Integer waStatus;
    /**
     * 点击量
     */
    @TableField("wa_hits")
    private Long waHits;
    /**
     * 点赞
     */
    @TableField("wa_like")
    private Long waLike;
    @TableField("create_time")
    private Date createTime;
    @TableField("update_time")
    private Date updateTime;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getWaName() {
        return waName;
    }

    public void setWaName(String waName) {
        this.waName = waName;
    }

    public String getWaLogo() {
        return waLogo;
    }

    public void setWaLogo(String waLogo) {
        this.waLogo = waLogo;
    }

    public String getWaBrief() {
        return waBrief;
    }

    public void setWaBrief(String waBrief) {
        this.waBrief = waBrief;
    }

    public String getWaWebsite() {
        return waWebsite;
    }

    public void setWaWebsite(String waWebsite) {
        this.waWebsite = waWebsite;
    }

    public Integer getWaStatus() {
        return waStatus;
    }

    public void setWaStatus(Integer waStatus) {
        this.waStatus = waStatus;
    }

    public Long getWaHits() {
        return waHits;
    }

    public void setWaHits(Long waHits) {
        this.waHits = waHits;
    }

    public Long getWaLike() {
        return waLike;
    }

    public void setWaLike(Long waLike) {
        this.waLike = waLike;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "BtcWallet{" +
        ", id=" + id +
        ", waName=" + waName +
        ", waLogo=" + waLogo +
        ", waBrief=" + waBrief +
        ", waWebsite=" + waWebsite +
        ", waStatus=" + waStatus +
        ", waHits=" + waHits +
        ", waLike=" + waLike +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        "}";
    }
}
