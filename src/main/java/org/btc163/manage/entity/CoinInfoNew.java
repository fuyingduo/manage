package org.btc163.manage.entity;

import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-08-21
 */
@TableName("coin_info_new")
public class CoinInfoNew extends Model<CoinInfoNew> {

    private static final long serialVersionUID = 1L;

    @TableId("en_key")
    private String enKey;
    @TableField("blog_url")
    private String blogUrl;
    @TableField("break_group")
    private String breakGroup;
    @TableField("break_url")
    private String breakUrl;
    @TableField("cn_name")
    private String cnName;
    private String context;
    @TableField("crowd_funding_average_price")
    private String crowdFundingAveragePrice;
    @TableField("crowd_funding_count_amount")
    private String crowdFundingCountAmount;
    @TableField("crowd_funding_price")
    private String crowdFundingPrice;
    @TableField("crowd_funding_success_amount")
    private String crowdFundingSuccessAmount;
    @TableField("crowd_funding_success_number")
    private String crowdFundingSuccessNumber;
    @TableField("crowd_funding_target")
    private String crowdFundingTarget;
    @TableField("crowd_funding_type")
    private String crowdFundingType;
    @TableField("en_name")
    private String enName;
    @TableField("exchange_num")
    private String exchangeNum;
    @TableField("for_sale_url")
    private String forSaleUrl;
    @TableField("ico_all_num")
    private String icoAllNum;
    @TableField("ico_allot")
    private String icoAllot;
    @TableField("ico_end_time")
    private String icoEndTime;
    @TableField("ico_flag")
    private String icoFlag;
    @TableField("ico_open_price")
    private String icoOpenPrice;
    @TableField("ico_platform")
    private String icoPlatform;
    @TableField("ico_sell_num")
    private String icoSellNum;
    @TableField("ico_start_time")
    private String icoStartTime;
    @TableField("investor_ratio")
    private String investorRatio;
    @TableField("issue_date")
    private String issueDate;
    private String jurisdiction;
    @TableField("legal_counsel")
    private String legalCounsel;
    @TableField("legal_form")
    private String legalForm;
    @TableField("related_notion")
    private String relatedNotion;
    @TableField("safety_audit")
    private String safetyAudit;
    @TableField("totla_volume")
    private String totlaVolume;
    private String trait;
    @TableField("turn_value")
    private String turnValue;
    private String url;
    @TableField("white_book")
    private String whiteBook;
    @TableField("file_name")
    private String fileName;


    public String getEnKey() {
        return enKey;
    }

    public void setEnKey(String enKey) {
        this.enKey = enKey;
    }

    public String getBlogUrl() {
        return blogUrl;
    }

    public void setBlogUrl(String blogUrl) {
        this.blogUrl = blogUrl;
    }

    public String getBreakGroup() {
        return breakGroup;
    }

    public void setBreakGroup(String breakGroup) {
        this.breakGroup = breakGroup;
    }

    public String getBreakUrl() {
        return breakUrl;
    }

    public void setBreakUrl(String breakUrl) {
        this.breakUrl = breakUrl;
    }

    public String getCnName() {
        return cnName;
    }

    public void setCnName(String cnName) {
        this.cnName = cnName;
    }

    public String getContext() {
        return context;
    }

    public void setContext(String context) {
        this.context = context;
    }

    public String getCrowdFundingAveragePrice() {
        return crowdFundingAveragePrice;
    }

    public void setCrowdFundingAveragePrice(String crowdFundingAveragePrice) {
        this.crowdFundingAveragePrice = crowdFundingAveragePrice;
    }

    public String getCrowdFundingCountAmount() {
        return crowdFundingCountAmount;
    }

    public void setCrowdFundingCountAmount(String crowdFundingCountAmount) {
        this.crowdFundingCountAmount = crowdFundingCountAmount;
    }

    public String getCrowdFundingPrice() {
        return crowdFundingPrice;
    }

    public void setCrowdFundingPrice(String crowdFundingPrice) {
        this.crowdFundingPrice = crowdFundingPrice;
    }

    public String getCrowdFundingSuccessAmount() {
        return crowdFundingSuccessAmount;
    }

    public void setCrowdFundingSuccessAmount(String crowdFundingSuccessAmount) {
        this.crowdFundingSuccessAmount = crowdFundingSuccessAmount;
    }

    public String getCrowdFundingSuccessNumber() {
        return crowdFundingSuccessNumber;
    }

    public void setCrowdFundingSuccessNumber(String crowdFundingSuccessNumber) {
        this.crowdFundingSuccessNumber = crowdFundingSuccessNumber;
    }

    public String getCrowdFundingTarget() {
        return crowdFundingTarget;
    }

    public void setCrowdFundingTarget(String crowdFundingTarget) {
        this.crowdFundingTarget = crowdFundingTarget;
    }

    public String getCrowdFundingType() {
        return crowdFundingType;
    }

    public void setCrowdFundingType(String crowdFundingType) {
        this.crowdFundingType = crowdFundingType;
    }

    public String getEnName() {
        return enName;
    }

    public void setEnName(String enName) {
        this.enName = enName;
    }

    public String getExchangeNum() {
        return exchangeNum;
    }

    public void setExchangeNum(String exchangeNum) {
        this.exchangeNum = exchangeNum;
    }

    public String getForSaleUrl() {
        return forSaleUrl;
    }

    public void setForSaleUrl(String forSaleUrl) {
        this.forSaleUrl = forSaleUrl;
    }

    public String getIcoAllNum() {
        return icoAllNum;
    }

    public void setIcoAllNum(String icoAllNum) {
        this.icoAllNum = icoAllNum;
    }

    public String getIcoAllot() {
        return icoAllot;
    }

    public void setIcoAllot(String icoAllot) {
        this.icoAllot = icoAllot;
    }

    public String getIcoEndTime() {
        return icoEndTime;
    }

    public void setIcoEndTime(String icoEndTime) {
        this.icoEndTime = icoEndTime;
    }

    public String getIcoFlag() {
        return icoFlag;
    }

    public void setIcoFlag(String icoFlag) {
        this.icoFlag = icoFlag;
    }

    public String getIcoOpenPrice() {
        return icoOpenPrice;
    }

    public void setIcoOpenPrice(String icoOpenPrice) {
        this.icoOpenPrice = icoOpenPrice;
    }

    public String getIcoPlatform() {
        return icoPlatform;
    }

    public void setIcoPlatform(String icoPlatform) {
        this.icoPlatform = icoPlatform;
    }

    public String getIcoSellNum() {
        return icoSellNum;
    }

    public void setIcoSellNum(String icoSellNum) {
        this.icoSellNum = icoSellNum;
    }

    public String getIcoStartTime() {
        return icoStartTime;
    }

    public void setIcoStartTime(String icoStartTime) {
        this.icoStartTime = icoStartTime;
    }

    public String getInvestorRatio() {
        return investorRatio;
    }

    public void setInvestorRatio(String investorRatio) {
        this.investorRatio = investorRatio;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getJurisdiction() {
        return jurisdiction;
    }

    public void setJurisdiction(String jurisdiction) {
        this.jurisdiction = jurisdiction;
    }

    public String getLegalCounsel() {
        return legalCounsel;
    }

    public void setLegalCounsel(String legalCounsel) {
        this.legalCounsel = legalCounsel;
    }

    public String getLegalForm() {
        return legalForm;
    }

    public void setLegalForm(String legalForm) {
        this.legalForm = legalForm;
    }

    public String getRelatedNotion() {
        return relatedNotion;
    }

    public void setRelatedNotion(String relatedNotion) {
        this.relatedNotion = relatedNotion;
    }

    public String getSafetyAudit() {
        return safetyAudit;
    }

    public void setSafetyAudit(String safetyAudit) {
        this.safetyAudit = safetyAudit;
    }

    public String getTotlaVolume() {
        return totlaVolume;
    }

    public void setTotlaVolume(String totlaVolume) {
        this.totlaVolume = totlaVolume;
    }

    public String getTrait() {
        return trait;
    }

    public void setTrait(String trait) {
        this.trait = trait;
    }

    public String getTurnValue() {
        return turnValue;
    }

    public void setTurnValue(String turnValue) {
        this.turnValue = turnValue;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getWhiteBook() {
        return whiteBook;
    }

    public void setWhiteBook(String whiteBook) {
        this.whiteBook = whiteBook;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    protected Serializable pkVal() {
        return this.enKey;
    }

    @Override
    public String toString() {
        return "CoinInfoNew{" +
        ", enKey=" + enKey +
        ", blogUrl=" + blogUrl +
        ", breakGroup=" + breakGroup +
        ", breakUrl=" + breakUrl +
        ", cnName=" + cnName +
        ", context=" + context +
        ", crowdFundingAveragePrice=" + crowdFundingAveragePrice +
        ", crowdFundingCountAmount=" + crowdFundingCountAmount +
        ", crowdFundingPrice=" + crowdFundingPrice +
        ", crowdFundingSuccessAmount=" + crowdFundingSuccessAmount +
        ", crowdFundingSuccessNumber=" + crowdFundingSuccessNumber +
        ", crowdFundingTarget=" + crowdFundingTarget +
        ", crowdFundingType=" + crowdFundingType +
        ", enName=" + enName +
        ", exchangeNum=" + exchangeNum +
        ", forSaleUrl=" + forSaleUrl +
        ", icoAllNum=" + icoAllNum +
        ", icoAllot=" + icoAllot +
        ", icoEndTime=" + icoEndTime +
        ", icoFlag=" + icoFlag +
        ", icoOpenPrice=" + icoOpenPrice +
        ", icoPlatform=" + icoPlatform +
        ", icoSellNum=" + icoSellNum +
        ", icoStartTime=" + icoStartTime +
        ", investorRatio=" + investorRatio +
        ", issueDate=" + issueDate +
        ", jurisdiction=" + jurisdiction +
        ", legalCounsel=" + legalCounsel +
        ", legalForm=" + legalForm +
        ", relatedNotion=" + relatedNotion +
        ", safetyAudit=" + safetyAudit +
        ", totlaVolume=" + totlaVolume +
        ", trait=" + trait +
        ", turnValue=" + turnValue +
        ", url=" + url +
        ", whiteBook=" + whiteBook +
        ", fileName=" + fileName +
        "}";
    }
}
