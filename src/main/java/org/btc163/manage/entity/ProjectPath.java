package org.btc163.manage.entity;

import java.util.Date;
import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
@TableName("project_path")
public class ProjectPath extends Model<ProjectPath> {

    private static final long serialVersionUID = 1L;

    private String id;
    /**
     * 时间
     */
    @TableField("pa_time")
    private Date paTime;
    /**
     * 事件
     */
    @TableField("pa_incident")
    private String paIncident;
    /**
     * 项目Id
     */
    @TableField("pro_id")
    private String proId;
    @TableField("create_time")
    private Date createTime;
    @TableField("update_time")
    private Date updateTime;
    @TableField("pa_status")
    private String paStatus;
    /**
     * 排序
     */
    private Integer sort;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getPaTime() {
        return paTime;
    }

    public void setPaTime(Date paTime) {
        this.paTime = paTime;
    }

    public String getPaIncident() {
        return paIncident;
    }

    public void setPaIncident(String paIncident) {
        this.paIncident = paIncident;
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getPaStatus() {
        return paStatus;
    }

    public void setPaStatus(String paStatus) {
        this.paStatus = paStatus;
    }

    public Integer getSort() {
        return sort;
    }

    public void setSort(Integer sort) {
        this.sort = sort;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ProjectPath{" +
                "id='" + id + '\'' +
                ", paTime=" + paTime +
                ", paIncident='" + paIncident + '\'' +
                ", proId='" + proId + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", paStatus='" + paStatus + '\'' +
                ", sort=" + sort +
                '}';
    }
}
