package org.btc163.manage.entity;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.annotations.Version;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
@TableName("pro_tep")
public class ProTep extends Model<ProTep> {

    private static final long serialVersionUID = 1L;

    private String id;
    @TableField("pro_id")
    private String proId;
    @TableField("tep_id")
    private Long tepId;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getProId() {
        return proId;
    }

    public void setProId(String proId) {
        this.proId = proId;
    }

    public Long getTepId() {
        return tepId;
    }

    public void setTepId(Long tepId) {
        this.tepId = tepId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "ProTep{" +
        ", id=" + id +
        ", proId=" + proId +
        ", tepId=" + tepId +
        "}";
    }
}
