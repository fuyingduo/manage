package org.btc163.manage.constant;

import org.apache.commons.lang3.StringUtils;
import org.btc163.manage.exception.BusinessException;

/**
 * created by fuyd on 2018/8/25
 */
public enum ReLockUpEnum {
    UP_YES(0, "是"), UP_NO(1, "否");

    private Integer code;
    private String message;

    ReLockUpEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String messageOf(Integer code) throws BusinessException {
        if (code == null) {
            throw new BusinessException(10001);
        }
        for (ReLockUpEnum reLockUpEnum : ReLockUpEnum.values()) {
            if (reLockUpEnum.code.equals(code)) {
                return reLockUpEnum.message;
            }
        }
        throw new BusinessException(10007);
    }

    public static Integer messageCodeOf(String message) throws BusinessException {
        if (StringUtils.isEmpty(message)) {
            throw new BusinessException(20003);
        }
        for (ReLockUpEnum reLockUpEnum : ReLockUpEnum.values()) {
            if (message.equals(reLockUpEnum.message)) {
                return reLockUpEnum.getCode();
            }

        }
        throw new BusinessException(20003);
    }
}
