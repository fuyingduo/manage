package org.btc163.manage.constant;

import org.apache.commons.lang3.StringUtils;
import org.btc163.manage.exception.BusinessException;

/**
 * created by fuyd on 2018/8/24
 */
public enum ProStageEnum {

    PROPAGANDA(0, "宣传期"), COLLECT_PERIOD(1, "募集期"), STAY_ONLINE(2, "待上线"), HAS_BEEN_LAUNCHED(3, "已上线"), ALREADY_DELIST(4, "已退市");

    private Integer code;
    private String message;

    ProStageEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String messageOf(Integer code) throws BusinessException {
        if (code == null) {
            throw new BusinessException(10001);
        }
        for (ProStageEnum proStageEnum : ProStageEnum.values()) {
            if (proStageEnum.code.equals(code)) {
                return proStageEnum.message;
            }
        }
        throw new BusinessException(10007);
    }

    public static Integer messageCodeOf(String message) throws BusinessException {
        if (StringUtils.isEmpty(message)) {
            throw new BusinessException(20003);
        }
        for (ProStageEnum proStageEnum : ProStageEnum.values()) {
            if (message.equals(proStageEnum.message)) {
                return proStageEnum.getCode();
            }

        }
        throw new BusinessException(20003);
    }

}
