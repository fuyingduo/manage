package org.btc163.manage.constant;

import org.apache.commons.lang3.StringUtils;
import org.btc163.manage.exception.BusinessException;

/**
 * created by fuyd on 2018/8/29
 */
public enum TepConceptEnum {

    ANONYMITY(0, "匿名链"), BASICS(1, "基础链"), DATA(3, "数据存储"), FINANCIAL(4, "金融服务"), INTERNET(5, "物联网");

    private Integer code;

    private String message;

    TepConceptEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String messageOf(Integer code) throws BusinessException {
        if (code == null) {
            throw new BusinessException(10001);
        }
        for (TepConceptEnum tepConceptEnum : TepConceptEnum.values()) {
            if (tepConceptEnum.code.equals(code)) {
                return tepConceptEnum.message;
            }
        }
        throw new BusinessException(10007);
    }

    public static Integer messageCodeOf(String message) throws BusinessException {
        if (StringUtils.isEmpty(message)) {
            throw new BusinessException(20003);
        }
        for (TepConceptEnum tepConceptEnum : TepConceptEnum.values()) {
            if (message.equals(tepConceptEnum.message)) {
                return tepConceptEnum.getCode();
            }

        }
        throw new BusinessException(20003);
    }
}
