package org.btc163.manage.constant;

/**
 * created by fuyd on 2018/9/14
 */
public class UserConstant {

    /**
     * 用户key
     */
    public static final String USER_INFO_KEY = "user_info_key_";

    /**
     * 用户菜单key
     */
    public static final String USER_MENU_INFO = "user_menu_key_";
}
