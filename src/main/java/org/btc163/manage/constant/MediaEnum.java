package org.btc163.manage.constant;

/**
 * created by fuyd on 2018/8/31
 */
public enum  MediaEnum {

    WEB(0, "web端"), MICROBLOG(1, "微博"), MICROLETTER(2, "微信");

    private Integer code;
    private String message;

    MediaEnum(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public static String messageOf(Integer code) {
        MediaEnum[] mediaEnums = MediaEnum.values();
        for (MediaEnum mediaEnum : mediaEnums) {
            if (code == mediaEnum.getCode()) {
                return mediaEnum.getMessage();
            }
        }
        return null;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
