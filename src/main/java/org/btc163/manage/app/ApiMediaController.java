package org.btc163.manage.app;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.btc163.manage.app.model.MediaInfoModel;
import org.btc163.manage.app.model.MediaListModel;
import org.btc163.manage.app.model.MediaModel;
import org.btc163.manage.base.BaseResult;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.BtcMediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * created by fuyd on 2018/8/31
 */
@Slf4j
@Controller
@RequestMapping(value = "/api/mt")
public class ApiMediaController {

    private static final String CLASS_NAME = "[ApiMediaController]";

    @Autowired
    private BtcMediaService btcMediaService;

    @ApiOperation(value = "媒体库列表", notes = "媒体库列表")
    @GetMapping(value = "/all/list")
    @ResponseBody
    public BaseResult<List<MediaListModel>> getMediaList(@ApiParam(value = "页数") @RequestParam(value = "page") Integer page,
                                                         @ApiParam(value = "条数/页") @RequestParam(value = "pageSize") Integer pageSize,
                                                         @ApiParam(value = "分类") @RequestParam(value = "type") Integer type) {
        try {
            Integer count = btcMediaService.getMediaListCount(type);
            List<MediaListModel> mediaListModels = btcMediaService.getMediaList(page, pageSize, type);
            return new BaseResult<>(mediaListModels, 0, "ok", count);
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[getMediaList] error:{}", e.getMessage());
            return new BaseResult<>(null, e.getErrorCode(), e.getMessage());
        }
    }

    @ApiOperation(value = "媒体库详情", notes = "媒体库详情")
    @GetMapping(value = "/info/particulars")
    @ResponseBody
    public BaseResult<MediaModel> findMediaInfo(@RequestParam(value = "mtId") String mtId) {
        try {
            MediaModel mediaInfo = btcMediaService.findMediaInfo(mtId);
            return new BaseResult<>(mediaInfo, 0, "ok");
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[findMediaInfo] error:{}", e.getMessage());
            return new BaseResult<>(null, e.getErrorCode(), e.getMessage());
        }
    }
}
