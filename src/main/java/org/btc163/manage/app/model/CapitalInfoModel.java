package org.btc163.manage.app.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * created by fuyd on 2018/8/24
 */
@Data
@ToString
@ApiModel(value = "CapitalInfoModel", description = "资本详情")
public class CapitalInfoModel {
    // TODO 资本库新增标签字段
    @ApiModelProperty(value = "标签")
    private String caLab;
    @ApiModelProperty(value = "简介")
    private String caBrief;
    @ApiModelProperty(value = "合伙人")
    private List<CapitalForMoreDetailsModel> capitalForMoreDetails;
    @ApiModelProperty(value = "投资项目")
    private List<ProjectListModel> projectListModels;
    // TODO 筛选 名称 阶段 概念板块
}
