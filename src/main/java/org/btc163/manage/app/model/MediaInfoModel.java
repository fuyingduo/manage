package org.btc163.manage.app.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/31
 */
@ApiModel(value = "MediaInfoModel", description = "媒体库详情")
@Data
@ToString
public class MediaInfoModel {

    @ApiModelProperty(value = "头像")
    private String inHeadImage;
    @ApiModelProperty(value = "介绍")
    private String inBrief;
    @ApiModelProperty(value = "姓名职位")
    private String inUserName;
    private String inPosition;

}
