package org.btc163.manage.app.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/24
 */
@Data
@ToString
@ApiModel(value = "ProjectToCollectDataModel", description = "募集资料")
public class ProjectToCollectDataModel {
    // TODO 数据库新加字段 募集Token总量
    @ApiModelProperty(value = "募集Token总量")
    private Integer raiseTokenNum;
    @ApiModelProperty(value = "招募时间")
    private String reTime;
    @ApiModelProperty(value = "兑换比例")
    private String reRatio;
    @ApiModelProperty(value = "募集资金分配")
    private String reCapitalAllot;
    @ApiModelProperty(value = "硬顶")
    private String reHardTop;
    @ApiModelProperty(value = "锁仓")
    private Integer reLockUp;
    @ApiModelProperty(value = "备注")
    private String reRemake;
}
