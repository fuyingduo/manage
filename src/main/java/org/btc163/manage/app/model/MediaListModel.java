package org.btc163.manage.app.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/31
 */
@Data
@ToString
@ApiModel(value = "MediaListModel", description = "媒体库")
public class MediaListModel {
    @ApiModelProperty(value = "主键")
    private String id;
    @ApiModelProperty(value = "名称")
    private String mtName;
    @ApiModelProperty(value = "logo")
    private String mtLogo;
    @ApiModelProperty(value = "简介")
    private String mtIntroduce;

}
