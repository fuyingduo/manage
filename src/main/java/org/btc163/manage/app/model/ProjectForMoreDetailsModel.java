package org.btc163.manage.app.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/24
 */
@Data
@ToString
@ApiModel(value = "ProjectForMoreDetailsModel", description = "更多详情")
public class ProjectForMoreDetailsModel {
    @ApiModelProperty(value = "姓名")
    private String inUserName;
    @ApiModelProperty(value = "头像")
    private String inHeadImage;
    @ApiModelProperty(value = "职位")
    private String inPosition;
    @ApiModelProperty(value = "简介")
    private String inBrief;
}
