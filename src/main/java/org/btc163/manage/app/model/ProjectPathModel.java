package org.btc163.manage.app.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;


/**
 * created by fuyd on 2018/8/24
 */
@Data
@ToString
@ApiModel(value = "ProjectPathModel", description = "项目路径图")
public class ProjectPathModel {
    @ApiModelProperty(value = "时间")
    private String paTime;
    @ApiModelProperty(value = "事件")
    private String paIncident;
}
