package org.btc163.manage.app.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/24
 */
@Data
@ToString
@ApiModel(value = "ProjectContactModel", description = "联系方式")
public class ProjectContactModel {
    @ApiModelProperty(value = "官网")
    private String coOfficial;
    @ApiModelProperty(value = "区块链浏览器")
    private String coBrowser;
    @ApiModelProperty(value = "白皮书")
    private String coWhite;
    @ApiModelProperty(value = "钱包")
    private String coWallet;
    @ApiModelProperty(value = "微信")
    private String coWechat;
}
