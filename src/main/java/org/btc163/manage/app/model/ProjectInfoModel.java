package org.btc163.manage.app.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * created by fuyd on 2018/8/24
 */
@Data
@ToString
@ApiModel(value = "ProjectInfoModel", description = "项目库详情")
public class ProjectInfoModel {
    @ApiModelProperty(value = "简介")
    private String proIntroduce;
    @ApiModelProperty(value = "token总量")
    private String proTokenNum;
    @ApiModelProperty(value = "代币分配")
    private String proTokenAllot;
    @ApiModelProperty(value = "共识机制")
    private String proConsesusMechanism;

    //    @ApiModelProperty(value = "token募集总量")
    //    private ProjectToCollectDataModel projectToCollectData;
    @ApiModelProperty(value = "募集Token总量")
    private String raiseTokenNum;
    @ApiModelProperty(value = "兑换比例")
    private String reRatio;
    @ApiModelProperty(value = "募集资金分配")
    private String reCapitalAllot;
    @ApiModelProperty(value = "硬顶")
    private String reHardTop;
    @ApiModelProperty(value = "锁仓")
    private String reLockUp;
    @ApiModelProperty(value = "备注")
    private String reRemake;

    // 联系方式
    //    @ApiModelProperty(value = "联系方式")
    //    private List<ProjectContactModel> projectContacts;
    @ApiModelProperty(value = "官网")
    private String coOfficial;
    @ApiModelProperty(value = "区块链浏览器")
    private String coBrowser;
    @ApiModelProperty(value = "白皮书")
    private String coWhite;
    @ApiModelProperty(value = "钱包")
    private String coWallet;
    @ApiModelProperty(value = "微信")
    private String coWechat;

    // TODO 新增字段 已上线交易所
    @ApiModelProperty(value = "招募时间")
    private String reTime;
    @ApiModelProperty(value = "项目阶段")
    private String proStage;
    @ApiModelProperty(value = "概念模版")
    private List<TepConceptModel> tepConcepts;
    @ApiModelProperty(value = "已上线交易所")
    private String popExchange;
    @ApiModelProperty(value = "更多详情")
    private List<ProjectForMoreDetailsModel> ProjectForMoreDetails;
    @ApiModelProperty(value = "合作伙伴")
    private List<CapitalListModel> capitalListModels;
    @ApiModelProperty(value = "项目路径图")
    private List<ProjectPathModel> projectPathModels;
    @ApiModelProperty(value = "相关新闻")
    private List<ProjectDynamicModel> ProjectDynamics;
}
