package org.btc163.manage.app.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;


/**
 * created by fuyd on 2018/8/24
 */
@Data
@ToString
@ApiModel(value = "CapitalListModel", description = "资本库")
public class CapitalListModel {
    @ApiModelProperty(value = "主键")
    private String id;
    @ApiModelProperty(value = "资本名称")
    private String caName;
    @ApiModelProperty(value = "资本logo")
    private String caLogo;
    @ApiModelProperty(value = "成立时间")
    private String caEstablish;
}
