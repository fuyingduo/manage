package org.btc163.manage.app.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/24
 */
@Data
@ToString
@ApiModel(value = "TepConceptModel", description = "概念板块")
public class TepConceptModel {
    @ApiModelProperty(value = "主键ID")
    private Long id;
    @ApiModelProperty(value = "概念板块名称")
    private String name;
}
