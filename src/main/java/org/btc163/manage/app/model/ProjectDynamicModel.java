package org.btc163.manage.app.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/24
 */
@Data
@ToString
@ApiModel(value = "ProjectDynamicModel", description = "相关新闻")
public class ProjectDynamicModel {
    @ApiModelProperty(value = "新闻标题")
    private String dyTitle;
    @ApiModelProperty(value = "新闻地址")
    private String dyUrl;
}
