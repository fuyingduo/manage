package org.btc163.manage.app.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;
import java.util.List;

/**
 * created by fuyd on 2018/8/31
 */
@ApiModel(value = "MediaModel", description = "媒体库详情")
@Data
@ToString
public class MediaModel implements Serializable{
    private static final long serialVersionUID = 4184118815884609872L;
    @ApiModelProperty(value = "简介")
    private String mtIntroduce;
    @ApiModelProperty(value = "网址")
    private String mtUrl;
    @ApiModelProperty(value = "成员")
    private List<MediaInfoModel> mediaInfoModels;
}
