package org.btc163.manage.app.model;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.ToString;

import java.util.List;

/**
 * created by fuyd on 2018/8/24
 */
@Data
@ToString
@ApiModel(value = "ProjectListModel", description = "项目库")
public class ProjectListModel {
    @ApiModelProperty(value = "主键ID")
    private String id;
    @ApiModelProperty(value = "项目名称")
    private String proName;
    @ApiModelProperty(value = "项目LOGO")
    private String proLogo;
    @ApiModelProperty(value = "项目阶段")
    private String proStage;
    @ApiModelProperty(value = "概念板块")
    private List<TepConceptModel> tepConcepts;

    public ProjectListModel() {

    }

    public ProjectListModel(String id, String proName, String proLogo, String proStage, List<TepConceptModel> tepConcepts) {
        this.id = id;
        this.proName = proName;
        this.proLogo = proLogo;
        this.proStage = proStage;
        this.tepConcepts = tepConcepts;
    }
}
