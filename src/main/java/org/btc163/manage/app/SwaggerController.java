package org.btc163.manage.app;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.btc163.manage.entity.ProjectPath;
import org.springframework.web.bind.annotation.*;

/**
 * created by fuyd on 2018/8/23
 */
@RestController
@RequestMapping("/test")
@Api(tags = "测试类", value = "测试类")
public class SwaggerController {
    @ApiOperation(value = "测试", notes = "测试")
    @RequestMapping(value = "/test/{id}", method = RequestMethod.GET)
    public ProjectPath test(@PathVariable Integer id, ProjectPath projectPath) {
        System.out.println(id);
        return null;
    }
}
