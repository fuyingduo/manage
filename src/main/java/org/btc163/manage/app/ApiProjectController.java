package org.btc163.manage.app;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.btc163.manage.app.model.ProjectInfoModel;
import org.btc163.manage.app.model.ProjectListModel;
import org.btc163.manage.base.BaseResult;
import org.btc163.manage.entity.TepConcept;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.BtcProjectService;
import org.btc163.manage.service.TepConceptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * created by fuyd on 2018/8/24
 */
@Api(value = "项目库", description = "项目库")
@Slf4j
@RestController
@RequestMapping(value = "/api/pro")
public class ApiProjectController {

    private static final String CLASS_NAME = "[ApiProjectController]";

    @Autowired
    private BtcProjectService btcProjectService;

    @Autowired
    private TepConceptService tepConceptService;

    @ApiOperation(value = "项目库列表", notes = "项目库列表")
    @GetMapping(value = "/all/list")
    @ResponseBody
    public BaseResult<List<ProjectListModel>> getProjectList(@ApiParam(value = "页数") @RequestParam(value = "page") Integer page,
                                                             @ApiParam(value = "条数/页") @RequestParam(value = "pageSize") Integer pageSize,
                                                             @ApiParam(value = "关键字") @RequestParam(value = "proName", required = false) String proName,
                                                             @ApiParam(value = "所处阶段") @RequestParam(value = "proStages", required = false) String proStages,
                                                             @ApiParam(value = "概念板块") @RequestParam(value = "tepNames", required = false) String tepNames) {
        try {
            List<String> stages = this.getProStagesList(proStages);
            List<Long> names = this.getTepNamesList(tepNames);
            Integer count = btcProjectService.getAppProjectInCount(stages, names);
            List<ProjectListModel> projects = btcProjectService.getAppProjectInfos(page, pageSize, stages, names, proName);
            return new BaseResult<>(projects, 0, "ok", count);
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[getProjectList] error:{}", e.getMessage());
            return new BaseResult<>(null, e.getErrorCode(), e.getMessage(), 0);
        }
    }

    @ApiOperation(value = "项目库详情", notes = "项目库详情")
    @GetMapping(value = "/info/information")
    @ResponseBody
    public BaseResult<ProjectInfoModel> findProjectInfoByKey(@ApiParam(value = "项目主键") @RequestParam(value = "proId") String proId) {
        try {
            ProjectInfoModel projectInfo = btcProjectService.findAppProjectInfos(proId);
            return new BaseResult<>(projectInfo, 0, "ok");
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[findProjectInfoByKey] error:{}", e.getMessage());
            return new BaseResult<>(null, e.getErrorCode(), e.getMessage());
        }
    }

    @ApiOperation(value = "概念板块下拉", notes = "概念板块下拉")
    @GetMapping(value = "/concept/select")
    @ResponseBody
    public BaseResult<List<TepConcept>> filterBoxProject() {
        try {
            List<TepConcept> tepConcepts = tepConceptService.getTepConceptServices();
            return new BaseResult<>(tepConcepts, 0, "ok");
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[filterBoxProject] error:{}", e.getMessage());
            return new BaseResult<>(null, e.getErrorCode(), e.getMessage());
        }
    }

    private List<String> getProStagesList(String proStages) {
        List<String> stages = new ArrayList<>();
        if (!StringUtils.isEmpty(proStages)) {
            if (proStages.indexOf(",") < 0) {
                stages.add(proStages);
            } else {
                String[] proArr = proStages.split(",");
                for (String s : proArr) {
                    stages.add(s);
                }
            }
        }
        return stages;
    }

    private List<Long> getTepNamesList(String tepNames) {
        List<Long> names = new ArrayList<>();
        if (!StringUtils.isEmpty(tepNames)) {
            if (tepNames.indexOf(",") < 0) {
                names.add(Long.valueOf(tepNames));
            } else {
                String[] nameArr = tepNames.split(",");
                for (String s : nameArr) {
                    names.add(Long.valueOf(s));
                }
            }
        }
        return names;
    }
}
