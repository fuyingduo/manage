package org.btc163.manage.app;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.btc163.manage.app.model.CapitalInfoModel;
import org.btc163.manage.app.model.CapitalListModel;
import org.btc163.manage.base.BaseResult;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.CapitalService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * created by fuyd on 2018/8/24
 */
@Api(value = "资本库", description = "资本库")
@Slf4j
@RestController
@RequestMapping(value = "/api/ca")
public class ApiCapitalController {
    private static final String CLASS_NAME = "[ApiCapitalController]";

    @Autowired
    private CapitalService capitalService;

    @ApiOperation(value = "资本库列表", notes = "资本库列表")
    @GetMapping(value = "/all/list")
    @ResponseBody
    public BaseResult<List<CapitalListModel>> getProjectList(@ApiParam(value = "页数") @RequestParam(value = "page") Integer page,
                                                             @ApiParam(value = "条数/页") @RequestParam(value = "pageSize") Integer pageSize,
                                                             @ApiParam(value = "资本名称") @RequestParam(value = "caName", required = false) String caName,
                                                             @ApiParam(value = "标签") @RequestParam(value = "caLab", required = false) String caLab) {
        try {
            List<String> caLabs = this.getCaLabList(caLab);
            Integer count = capitalService.getCapiTalSizeTotal(caName, caLabs);
            List<CapitalListModel> capitals = capitalService.getCapitalPageList(page, pageSize, caName, caLabs);
            return new BaseResult<>(capitals, 0, "ok", count);
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[getProjectList] error", e.getMessage());
            return new BaseResult<>(null, e.getErrorCode(), e.getMessage(), 0);
        }
    }

    @ApiOperation(value = "资本库详情", notes = "资本库详情")
    @GetMapping(value = "/info/information")
    @ResponseBody
    public BaseResult<CapitalInfoModel> findCapitalInfoByKey(@ApiParam(value = "资本主键") @RequestParam(value = "caId") String caId) {
        try {
            CapitalInfoModel capitalInfo = capitalService.getCapitalInfos(caId);
            return new BaseResult<>(capitalInfo, 0, "ok");
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[findCapitalInfoByKey] error:{}", e.getMessage());
            return new BaseResult<>(null, e.getErrorCode(), e.getMessage());
        }
    }

    private List<String> getCaLabList(String caLab) {
        List<String> caLabs = new ArrayList<>();
        if (!StringUtils.isEmpty(caLab)) {
            if (caLab.indexOf(",") < 0) {
                caLabs.add(caLab);
            } else {
                String[] caLabArr = caLab.split(",");
                if (caLabArr != null && caLabArr.length > 0) {
                    for (String str : caLabArr) {
                        caLabs.add(str);
                    }
                }
            }
        }
        return caLabs;
    }

}
