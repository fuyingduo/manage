package org.btc163.manage.controller;


import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.btc163.manage.base.BaseResult;
import org.btc163.manage.entity.TepConcept;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.TepConceptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
@Slf4j
@Controller
@RequestMapping("/tep")
public class TepConceptController {

    private static final String CLASS_NAME = "[TepConceptController]";

    @Autowired
    private TepConceptService tepConceptService;

    @PostMapping(value = "findData")
    @ResponseBody
    public String getTepConcepts() {
        try {
            List<TepConcept> teps = tepConceptService.getTepConceptServices();
            return JSONObject.toJSONString(new BaseResult<List<TepConcept>>(teps));
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[getTepConcepts] error:{}", e.getMessage());
            return JSONObject.toJSONString(new BaseResult<TepConcept>(null, e.getErrorCode(), e.getMessage()));
        }
    }

}

