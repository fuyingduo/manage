package org.btc163.manage.controller.view;


import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/9/3
 */
@Data
@ToString
public class MediaView {

    private String id;
    /**
     * 名称
     */
    private String mtName;
    /**
     * logo
     */
    private String mtLogo;
    /**
     * 标签
     */
    private String mtSpell;
    /**
     * 阅读量
     */
    private String mtRead;
    /**
     * 点赞
     */
    private String mtPraise;

    /**
     * 分类
     */
    private String mtType;

    private String updateTime;

    private String mtIntroduce;

    private String mtUrl;
}
