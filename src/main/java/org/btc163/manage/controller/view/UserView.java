package org.btc163.manage.controller.view;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/9/8
 */
@Data
@ToString
public class UserView {
    private String id;
    private String username;
    private String createTime;
}
