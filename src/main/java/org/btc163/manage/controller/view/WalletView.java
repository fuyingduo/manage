package org.btc163.manage.controller.view;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/9/6
 */
@Data
@ToString
public class WalletView {

    private String id;
    /**
     * 钱包名称
     */
    private String waName;
    /**
     * logo
     */
    private String waLogo;
    /**
     * 简介
     */
    private String waBrief;
    /**
     * 官网
     */
    private String waWebsite;
    /**
     * 状态
     */
    private Integer waStatus;
    /**
     * 点击量
     */
    private Long waHits;
    /**
     * 点赞
     */
    private Long waLike;

    /**
     * 修改时间
     */
    private String updateTime;

    /**
     * 合伙人
     */
    private String partners;
}
