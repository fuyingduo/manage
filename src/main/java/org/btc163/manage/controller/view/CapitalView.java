package org.btc163.manage.controller.view;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/27
 */
@Data
@ToString
public class CapitalView {

    /**
     * ID
     */
    private String id;

    /**
     * 资本名称
     */
    private String caName;

    /**
     * 参投项目
     */
    private String referenceForProjects;

    /**
     * 合作伙伴
     */
    private String partners;

    /**
     * 更新时间
     */
    private String updateTime;

    private Long caStatistics;

    /**
     * 点赞
     */
    private Integer caPraise;
}
