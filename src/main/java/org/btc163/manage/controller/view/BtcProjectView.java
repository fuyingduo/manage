package org.btc163.manage.controller.view;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/24
 */
@Data
@ToString
public class BtcProjectView {

    private String id;

    /**
     * 项目名称
     */
    private String proName;

    /**
     * 项目阶段
     */
    private String proStage;

    /**
     * 概念板块
     */
    private String theConceptOfPlate;

    /**
     * token总量
     */
    private String tokenNum;

    /**
     * 核心成员
     */
    private String kernelMember;

    /**
     * 更新时间
     */
    private String updateTime;

    private Long proHits;

    private Integer proPraise;

    public BtcProjectView(String id, String proName, String proStage, String theConceptOfPlate, String tokenNum, String kernelMember, String updateTime) {
        this.id = id;
        this.proName = proName;
        this.proStage = proStage;
        this.theConceptOfPlate = theConceptOfPlate;
        this.tokenNum = tokenNum;
        this.kernelMember = kernelMember;
        this.updateTime = updateTime;
    }
}
