package org.btc163.manage.controller.view;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/9/7
 */
@Data
@ToString
public class MillView {

    private String id;
    /**
     * 名称
     */
    private String miName;
    /**
     * logo
     */
    private String miLogo;
    /**
     * 简介
     */
    private String miBrief;
    /**
     * 官网
     */
    private String miWebsite;
    /**
     * 点击量
     */
    private Long miHits;
    /**
     * 赞
     */
    private Long miLike;

    /**
     * 修改时间
     */
    private String updateTime;

    /**
     * 合伙人
     */
    private String partners;
}
