package org.btc163.manage.controller;

import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.BaseResult;
import org.btc163.manage.controller.model.FileModel;
import org.btc163.manage.service.base.ImageUploadService;
import org.btc163.manage.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.ArrayList;
import java.util.List;

/**
 * created by fuyd on 2018/8/22
 */
@Slf4j
@RestController
@RequestMapping(value = "upload")
public class UploadImageController {

    private static final String CLASS_NAME = "[UploadImageController]";

    @Autowired
    private ImageUploadService imageUploadService;

    @PostMapping(value = "image")
    @ResponseBody
    public String upload(MultipartFile file) {
        try {
            return JSONObject.toJSONString(new FileModel(0, "ok", imageUploadService.imageUpload(file)));
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[upload] error:{}", e.getMessage());
            return JSONObject.toJSONString(new FileModel(-1, "error", ""));
        }
    }
}
