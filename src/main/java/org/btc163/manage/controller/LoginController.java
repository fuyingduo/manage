package org.btc163.manage.controller;

import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.ExcessiveAttemptsException;
import org.apache.shiro.authc.IncorrectCredentialsException;
import org.apache.shiro.authc.UnknownAccountException;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.session.Session;
import org.apache.shiro.subject.Subject;
import org.btc163.manage.base.BaseResultView;
import org.btc163.manage.config.shiro.ShiroUtil;
import org.btc163.manage.controller.param.BUserParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import javax.servlet.ServletRequest;

/**
 * created by fuyd on 2018/8/21
 */
@Slf4j
@Controller
public class LoginController {

    @GetMapping(value = "/goLogin")
    public String goLogin(Model model, ServletRequest request) {
        Subject sub = SecurityUtils.getSubject();
        if (sub.isAuthenticated()) {
            return BaseResultView.is_main();
        } else {
            model.addAttribute("message", "请重新登录");
            return BaseResultView.is_error();
        }
    }

    @GetMapping(value = "/login")
    public String loginCheck() {
        Subject sub = SecurityUtils.getSubject();
        Boolean flag2 = sub.isRemembered();
        boolean flag = sub.isAuthenticated() || flag2;
        if (flag) {
            return BaseResultView.is_main();
        }
        return BaseResultView.is_error();
    }

    @PostMapping(value = "/login")
    public String login(BUserParam user, Model model) {
        UsernamePasswordToken token = new UsernamePasswordToken(user.getUsername().trim(),
                user.getPassword());
        Subject subject = ShiroUtil.getSubject();
        String msg = null;
        try {
            log.info("login token is :{}", token.toString());
            subject.login(token);
            if (subject.isAuthenticated()) {
                return BaseResultView.is_main();
            }
        } catch (UnknownAccountException e) {
            msg = "用户名/密码错误";
        } catch (IncorrectCredentialsException e) {
            msg = "用户名/密码错误";
        } catch (ExcessiveAttemptsException e) {
            msg = "登录失败多次，账户锁定10分钟";
        }
        if (msg != null) {
            model.addAttribute("message", msg);
        }
        return BaseResultView.is_error();
    }
}
