package org.btc163.manage.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.BaseResult;
import org.btc163.manage.base.BaseResultView;
import org.btc163.manage.controller.model.*;
import org.btc163.manage.controller.param.BtcProjectParam;
import org.btc163.manage.controller.param.ProjectParam;
import org.btc163.manage.entity.*;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.*;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
@Slf4j
@Controller
@RequestMapping("/btc")
public class BtcProjectController {

    private static final String CLASS_NAME = "[]";

    @Autowired
    private BtcProjectService btcProjectService;

    @Autowired
    private ProTepService proTepService;

    @Autowired
    private BtcProjectInfoService btcProjectInfoService;

    @Autowired
    private ProPlatformService proPlatformService;

    @Autowired
    private ProCaService proCaService;

    @Autowired
    private ProjectPathService projectPathService;

    @Autowired
    private ProjectDynamicService projectDynamicService;

    @Value("${image.logo.read}")
    private String readImage;

    @GetMapping(value = "jumpView")
    public String jumpProjectView() {
        return BaseResultView.view("pro/btc_project_list");
    }

    @GetMapping(value = "jump")
    public String jumpProjectAdd() {
        return BaseResultView.view("pro/btc_project_add");
    }

    @PostMapping(value = "add")
    @ResponseBody
    public String addProject(BtcProjectParam btcProjectParam) {
        try {
            btcProjectService.addProject(btcProjectParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[addProject] error:{}", e.getMessage());
            return null;
        }
    }


    @PostMapping(value = "/select")
    @ResponseBody
    public String getAllProjects() {
        try {
            List<BtcProjectSelectModel> models = btcProjectService.getAllProjects();
            return JSONObject.toJSONString(new BaseResult<List<BtcProjectSelectModel>>(models));
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[getAllProjects] error:{}", e.getMessage());
            return JSONObject.toJSONString(new BaseResult<List<BtcProjectSelectModel>>(null, e.getErrorCode(), e.getMessage()));
        }
    }

    @GetMapping(value = "getProjectList")
    @ResponseBody
    public String getProjectLists(ProjectParam projectParam) {
        try {
            return btcProjectService.getProjectList(projectParam);
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[getProjectLists] error:{}", e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "jumpEdit")
    public String jumpEdit(@RequestParam(value = "id") final String id, Model model) {
        if (StringUtils.isEmpty(id)) {
            return "error/403";
        }
        BtcProject btcProject = btcProjectService.selectById(id);
        if (btcProject == null) {
            log.error(CLASS_NAME + "[jumpEdit] select data error");
            return "error/403";
        }
        BtcProjectModel btcProjectModel = new BtcProjectModel();
        BeanUtils.copyProperties(btcProject, btcProjectModel);
        if (btcProject.getReTime() != null) {
            btcProjectModel.setReTime(btcProject.getReTime());
        }
        if (btcProject.getReTimeEnd() != null) {
            btcProjectModel.setReTimeEnd(btcProject.getReTimeEnd());
        }
        if (btcProject.getProTokenNum() != null) {
            btcProjectModel.setProTokenNum(btcProject.getProTokenNum().toString());
        }
        if (btcProject.getRaiseTokenSum() != null) {
            btcProjectModel.setRaiseTokenSum(btcProject.getRaiseTokenSum().toString());
        }
        if (!StringUtils.isEmpty(btcProject.getProLogo())) {
            btcProjectModel.setProLogo(btcProject.getProLogo());
        }
        btcProjectModel.setProhits(btcProject.getProHits() == null ? "0" : btcProject.getProHits().toString());
        btcProjectModel.setProPraise(btcProject.getProPraise() == null ? "0" : btcProject.getProPraise().toString());
        // 项目列表
        model.addAttribute("btcProject", btcProjectModel);
        // 概念板块
        String tepIds = null;
        ProTep proTep = new ProTep();
        proTep.setProId(id);
        EntityWrapper<ProTep> proTepEntityWrapper = new EntityWrapper<>(proTep);
        StringBuilder tepBuilder = new StringBuilder();
        List<ProTep> proTeps = proTepService.selectList(proTepEntityWrapper);
        if (!CollectionUtils.isEmpty(proTeps)) {
            proTeps.forEach(proTep1 -> tepBuilder.append(proTep1.getTepId() + "|"));
            tepIds = tepBuilder.substring(0, tepBuilder.length() - 1);

        }
        model.addAttribute("tepIds", tepIds);
        model.addAttribute("readImage", readImage);
        // 核心成员
        try {
            List<BtcProjectInfo> btcProjectInfos = btcProjectInfoService.getBtcProjectInfosByProId(id);
            if (!CollectionUtils.isEmpty(btcProjectInfos)) {
                btcProjectInfos.forEach(btcProjectInfo -> btcProjectInfo.setInHeadImage(btcProjectInfo.getInHeadImage()));
                model.addAttribute("btcProjectInfos", btcProjectInfos);
            }
        } catch (BusinessException e) {
            e.printStackTrace();
        }
        // 合作伙伴
        ProCa proCa = new ProCa();
        proCa.setProId(id);
        EntityWrapper<ProCa> proCaEntityWrapper = new EntityWrapper<>(proCa);
        List<ProCa> proCas = proCaService.selectList(proCaEntityWrapper);
        String caIds = null;
        StringBuilder proCaBulider = new StringBuilder();
        if (!CollectionUtils.isEmpty(proCas)) {
            proCas.forEach(proCa1 -> proCaBulider.append(proCa1.getCaId() + "|"));
            caIds = proCaBulider.substring(0, proCaBulider.length() - 1);
        }
        model.addAttribute("caIds", caIds);
        // 已上线交易所
        String platformIds = null;
        try {
            List<ProPlatform> proPlatforms = proPlatformService.getProPlatformInfos(id);
            if (!CollectionUtils.isEmpty(proPlatforms)) {
                StringBuilder platformBuilder = new StringBuilder();
                proPlatforms.forEach(proPlatform -> platformBuilder.append(proPlatform.getPlatId() + "|"));
                platformIds = platformBuilder.substring(0, platformBuilder.length() - 1);
            }
        } catch (BusinessException e) {
            e.printStackTrace();
        }
        model.addAttribute("platformIds", platformIds);
        // 项目路径
        ProjectPath projectPath = new ProjectPath();
        projectPath.setProId(id);
        EntityWrapper<ProjectPath> pathEntityWrapper = new EntityWrapper<>(projectPath);
        List<ProjectPath> projectPaths = projectPathService.selectList(pathEntityWrapper);
        if (!CollectionUtils.isEmpty(projectPaths)) {
            List<PathModel> pathModels = new ArrayList<>();
            for (ProjectPath path : projectPaths) {
                PathModel pathModel = new PathModel();
                pathModel.setPaIncident(path.getPaIncident());
                if (path.getPaTime() != null) {
                    pathModel.setPaTime(DateFormatUtils.format(path.getPaTime(), "yyyy-MM-dd HH:mm:ss"));
                    pathModels.add(pathModel);
                }
            }
            model.addAttribute("projectPaths", pathModels);
        }
        // 相关新闻
        ProjectDynamic projectDynamic = new ProjectDynamic();
        projectDynamic.setProId(id);
        EntityWrapper<ProjectDynamic> dynamicEntityWrapper = new EntityWrapper<>(projectDynamic);
        List<ProjectDynamic> projectDynamics = projectDynamicService.selectList(dynamicEntityWrapper);
        if (!CollectionUtils.isEmpty(projectDynamics)) {
            model.addAttribute("projectDynamics", projectDynamics);
        }
        System.out.println(btcProject.toString());
        return BaseResultView.view("pro/btc_project_edit");
    }

    @PostMapping(value = "/edit")
    @ResponseBody
    public String editProject(BtcProjectParam btcProjectParam) {
        try {
            btcProjectService.editProject(btcProjectParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[editProject] error:{}", e.getMessage());
            return "操作失败";
        }
    }

    @PostMapping(value = "/del")
    @ResponseBody
    public String delProject(@RequestParam(value = "id") String id) {
        if (StringUtils.isEmpty(id)) {
            return "操作失败";
        }
        try {
            BtcProject btcProject = btcProjectService.selectById(id);
            if (btcProject == null) {
                return "操作失败";
            }
            btcProject.setProStatus("1");
            btcProject.setUpdateTime(new Date());
            if (!btcProjectService.updateById(btcProject)) {
                return "操作失败";
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[delProject] error:{}", e.getMessage());
            return "操作失败";
        }
        return "操作成功";
    }


}

