package org.btc163.manage.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.BaseResultView;
import org.btc163.manage.controller.model.BtcMediaSelectModel;
import org.btc163.manage.controller.param.BtcConferenceParam;
import org.btc163.manage.entity.BtcConference;
import org.btc163.manage.entity.BtcMedia;
import org.btc163.manage.entity.BtcMtPm;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.BtcConferenceService;
import org.btc163.manage.service.BtcMediaService;
import org.btc163.manage.service.BtcMtPmService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * created by fuyd on 2018/9/10
 */
@Slf4j
@Controller
@RequestMapping(value = "con")
public class BtcConferenceController {

    @Autowired
    private BtcMediaService btcMediaService;

    @Autowired
    private BtcConferenceService btcConferenceService;

    @Autowired
    private BtcMtPmService btcMtPmService;

    @Value("${image.logo.read}")
    private String readImage;


    private static final String CLASS_NAME = "[BtcConferenceController]";

    @GetMapping(value = "jumpConView")
    public String jumpConView() {
        return BaseResultView.view("con/conference_list");
    }

    @GetMapping(value = "addConView")
    public String addConView(Model model) {
        BtcMedia btcMedia = new BtcMedia();
        btcMedia.setMtStatus(0);
        EntityWrapper<BtcMedia> entityWrapper = new EntityWrapper<>(btcMedia);
        List<BtcMedia> btcMedias = btcMediaService.selectList(entityWrapper);
        if (!CollectionUtils.isEmpty(btcMedias)) {
            List<BtcMediaSelectModel> btcMediaSelectModels = new ArrayList<>();
            btcMedias.forEach(media -> btcMediaSelectModels.add(new BtcMediaSelectModel(media.getId(), media.getMtName())));
            model.addAttribute("btcMedia", btcMedias);
        }
        return BaseResultView.view("con/conference_add");
    }

    @PostMapping(value = "addCon")
    @ResponseBody
    public String addCon(BtcConferenceParam btcConferenceParam) {
        try {
            btcConferenceService.addCon(btcConferenceParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[addCon] error:{}", e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "editCon")
    @ResponseBody
    public String editCon(BtcConferenceParam btcConferenceParam) {
        try {
            btcConferenceService.editCon(btcConferenceParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[editCon] error:{}", e.getMessage());
            return null;
        }
    }

    @GetMapping(value = "getConferenceList")
    @ResponseBody
    public String getConferenceList(BtcConferenceParam btcConferenceParam) {
        try {
            return btcConferenceService.getConferenceList(btcConferenceParam);
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[getConferenceList] error:{}", e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "del")
    @ResponseBody
    public String delConference(@RequestParam(value = "id") String id) {
        if (StringUtils.isEmpty(id)) {
            return "操作失败";
        }
        try {
            BtcConference btcConference = btcConferenceService.selectById(id);
            if (btcConference == null) {
                return "操作失败";
            }
            btcConference.setPmStatus(1);
            if (!btcConferenceService.updateById(btcConference)) {
                return "操作失败";
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[delConference] error:{}", e.getMessage());
            return "操作失败";
        }
        return "操作成功";
    }

    @GetMapping(value = "jumpEdit")
    public String jumpEditConference(@RequestParam(value = "id") String id, Model model) {
        if (StringUtils.isEmpty(id)) {
            return BaseResultView.view("error/403");
        }
        try {
            BtcConference btcConference = btcConferenceService.selectById(id);
            btcConference.setPmStartTime(StringUtils.isEmpty(btcConference.getPmStartTime()) ? null : DateFormatUtils.format(Long.valueOf(btcConference.getPmStartTime()), "yyyy-MM-dd"));
            btcConference.setPmEndTime(StringUtils.isEmpty(btcConference.getPmEndTime()) ? null : DateFormatUtils.format(Long.valueOf(btcConference.getPmEndTime()), "yyyy-MM-dd"));
            model.addAttribute("btcConference", btcConference);
            model.addAttribute("readImage", readImage);
            // 媒体库
            BtcMtPm btcMtPm = new BtcMtPm();
            btcMtPm.setPmId(btcConference.getId());
            EntityWrapper<BtcMtPm> entityWrapper = new EntityWrapper<>(btcMtPm);
            List<BtcMtPm> btcMtPms = btcMtPmService.selectList(entityWrapper);
            if (!CollectionUtils.isEmpty(btcMtPms)) {
                StringBuilder stringBuilder = new StringBuilder();
                for (BtcMtPm mtPm : btcMtPms) {
                    stringBuilder.append(mtPm.getMtId() + ",");
                }
                if (stringBuilder.length() > 0) {
                    String mtIds = stringBuilder.substring(0, stringBuilder.length() - 1);
                    model.addAttribute("mtIds", mtIds);
                }
            }
            if (!StringUtils.isEmpty(btcConference.getPmBrief())) {
                List<String> briefs = new ArrayList<>();
                if (btcConference.getPmBrief().indexOf("^") > 0) {
                    String[] split = btcConference.getPmBrief().split("\\^");
                    for (String s : split) {
                        briefs.add(s);
                    }
                } else {
                    briefs.add(btcConference.getPmBrief());
                }
                model.addAttribute("briefs", briefs);
            }
            if (!StringUtils.isEmpty(btcConference.getPmHonored())) {
                List<String> honoreds = new ArrayList<>();
                if (btcConference.getPmHonored().indexOf("^") > 0) {
                    String[] split = btcConference.getPmHonored().split("\\^");
                    for (String s : split) {
                        honoreds.add(s);
                    }
                } else {
                    honoreds.add(btcConference.getPmHonored());
                }
                model.addAttribute("honoreds", honoreds);
            }
            if (!StringUtils.isEmpty(btcConference.getPmSchedule())) {
                List<String> schedules = new ArrayList<>();
                if (btcConference.getPmSchedule().indexOf("^") > 0) {
                    String[] split = btcConference.getPmSchedule().split("\\^");
                    for (String s : split) {
                        schedules.add(s);
                    }
                } else {
                    schedules.add(btcConference.getPmSchedule());
                }
                model.addAttribute("schedules", schedules);
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[jumpEditConference] error:{}", e.getMessage());
            return BaseResultView.view("error/403");
        }
        return BaseResultView.view("con/conference_edit");
    }


}
