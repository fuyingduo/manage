package org.btc163.manage.controller;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.BaseResult;
import org.btc163.manage.base.BaseResultView;
import org.btc163.manage.controller.model.PlatformInfoSelectModel;
import org.btc163.manage.controller.param.PlatformParam;
import org.btc163.manage.entity.PlatformInfo;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.BtcPlatformInfoService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * created by fuyd on 2018/8/24
 */
@Slf4j
@Controller
@RequestMapping(value = "plat")
public class PlatformInfoController {

    private static final String CLASS_NAME = "[PlatformInfoController]";

    @Autowired
    private BtcPlatformInfoService btcPlatformInfoService;


    @PostMapping(value = "/select")
    @ResponseBody
    public String getPlatformSelect() {
        try {
            List<PlatformInfoSelectModel> models = btcPlatformInfoService.getPlatformInfoSelect();
            return JSONObject.toJSONString(new BaseResult<List<PlatformInfoSelectModel>>(models));
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[getPlatformSelect] error:{}", e.getMessage());
            return JSONObject.toJSONString(new BaseResult<List<PlatformInfoSelectModel>>(null, e.getErrorCode(), e.getMessage()));
        }
    }

    @GetMapping(value = "/info")
    public String jumpPlatform() {
        return BaseResultView.view("platform/plat_formln_info_list");
    }

    @GetMapping(value = "/jumpAdd")
    public String jumpAdd() {
        return BaseResultView.view("platform/plat_formln_info_add");
    }

    @GetMapping(value = "/jumpEdit")
    public String jumpEdit(@RequestParam(value = "name") String name, Model model) {
        if (StringUtils.isEmpty(name)) {
            return "/error/404";
        }
        PlatformInfo platformInfo = new PlatformInfo();
        platformInfo.setName(name);
        EntityWrapper<PlatformInfo> entityWrapper = new EntityWrapper<>(platformInfo);
        List<PlatformInfo> platformInfos = btcPlatformInfoService.selectList(entityWrapper);
        if (!CollectionUtils.isEmpty(platformInfos)) {
            PlatformInfo platform = platformInfos.get(0);
            model.addAttribute("platform", platform);
            return BaseResultView.view("platform/plat_formln_info_edit");
        }
        return "/error/404";
    }

    @GetMapping(value = "/list")
    @ResponseBody
    public String getPlatformList(PlatformParam platformParam) {
        return btcPlatformInfoService.getPlatFormList(platformParam);
    }

    @PostMapping(value = "add")
    @ResponseBody
    public String addPlatformInfo(PlatformParam platformParam) {
        if (StringUtils.isEmpty(platformParam.getName())) {
            return "操作失败";
        }
        PlatformInfo verify = new PlatformInfo();
        verify.setName(platformParam.getName());
        EntityWrapper<PlatformInfo> platformParamEntityWrapper = new EntityWrapper<>(verify);
        try {
            if (!CollectionUtils.isEmpty(btcPlatformInfoService.selectList(platformParamEntityWrapper))) {
                return "交易所已经存在";
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[addPlatformInfo] error:{}", e.getMessage());
            return "操作失败";
        }
        PlatformInfo platformInfo = new PlatformInfo();
        BeanUtils.copyProperties(platformParam, platformInfo);
        try {
            if (!btcPlatformInfoService.insert(platformInfo)) {
                return "操作失败";
            }
            return "操作成功";
        } catch (Exception e) {
            log.error(CLASS_NAME + "[addPlatformInfo] error:{}", e.getMessage());
            return "操作失败";
        }
    }

    @PostMapping(value = "edit")
    @ResponseBody
    public String editPlatformInfo(PlatformParam platformParam) {
        if (StringUtils.isEmpty(platformParam.getName())) {
            return "操作失败";
        }
        final String name = platformParam.getName();
        PlatformInfo platformInfo = new PlatformInfo();
        platformInfo.setName(name);
        EntityWrapper<PlatformInfo> entityWrapper = new EntityWrapper<>(platformInfo);
        List<PlatformInfo> platformInfos = null;
        try {
            platformInfos = btcPlatformInfoService.selectList(entityWrapper);
        } catch (Exception e) {
            return "操作失败";
        }
        if (CollectionUtils.isEmpty(platformInfos)) {
            return "操作失败";
        }
        PlatformInfo info = platformInfos.get(0);
        BeanUtils.copyProperties(platformParam, info);
        try {
            if (!btcPlatformInfoService.updatePlatformInfo(info)) {
                return "操作失败";
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[editPlatformInfo] error:{}", e.getMessage());
            return "操作失败";
        }
        return "操作成功";
    }

    @PostMapping(value = "del")
    @ResponseBody
    public String delPlatformInfo(@RequestParam(value = "name") String name) {
        if (StringUtils.isEmpty(name)) {
            return "操作失败";
        }
        PlatformInfo platformInfo = new PlatformInfo();
        platformInfo.setName(name);
        EntityWrapper<PlatformInfo> entityWrapper = new EntityWrapper<>(platformInfo);
        List<PlatformInfo> platformInfos = null;
        try {
            platformInfos = btcPlatformInfoService.selectList(entityWrapper);
        } catch (Exception e) {
            log.error(CLASS_NAME + "[delPlatformInfo] error:{}", e.getMessage());
            return "操作失败";
        }
        if (platformInfos == null) {
            return "操作失败";
        }
        PlatformInfo info = platformInfos.get(0);
        if (!btcPlatformInfoService.delete(new EntityWrapper<PlatformInfo>(info))) {
            return "操作失败";
        }
        return "操作成功";
    }
}
