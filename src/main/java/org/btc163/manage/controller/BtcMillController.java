package org.btc163.manage.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.BaseResultView;
import org.btc163.manage.controller.model.BtcMillModel;
import org.btc163.manage.controller.param.MillParam;
import org.btc163.manage.entity.BtcMill;
import org.btc163.manage.entity.BtcMillInfo;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.BtcMillInfoService;
import org.btc163.manage.service.BtcMillService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * created by fuyd on 2018/9/7
 */
@Slf4j
@Controller
@RequestMapping(value = "/mi")
public class BtcMillController {

    private static final String CLASS_NAME = "[BtcMillController]";

    @Autowired
    private BtcMillService btcMillService;

    @Autowired
    private BtcMillInfoService btcMillInfoService;

    @Value("${image.logo.read}")
    private String readImage;

    @GetMapping(value = "jumpView")
    public String jumpListView() {
        return BaseResultView.view("mi/mill_list");
    }

    @GetMapping(value = "jumpAdd")
    public String jumpAddView() {
        return BaseResultView.view("mi/mill_add");
    }

    @GetMapping(value = "jumpEdit")
    public String jumpEditView(@RequestParam(value = "id") String id, Model model) {
        BtcMill btcMill = btcMillService.selectById(id);
        BtcMillModel btcMillModel = new BtcMillModel();
        BeanUtils.copyProperties(btcMill, btcMillModel);
        btcMillModel.setMiHits(btcMill.getMiHits().toString());
        btcMillModel.setMiLike(btcMill.getMiLike().toString());
        model.addAttribute("btcMill", btcMillModel);
        BtcMillInfo btcMillInfo = new BtcMillInfo();
        btcMillInfo.setMiId(id);
        EntityWrapper<BtcMillInfo> entityWrapper = new EntityWrapper<>(btcMillInfo);
        List<BtcMillInfo> btcMillInfs = btcMillInfoService.selectList(entityWrapper);
        model.addAttribute("readImage", readImage);
        if (!CollectionUtils.isEmpty(btcMillInfs)) {
            model.addAttribute("btcMillInfs", btcMillInfs);
        }
        return BaseResultView.view("mi/mill_edit");
    }

    @GetMapping(value = "getMillList")
    @ResponseBody
    public String getMillList(MillParam millParam) {
        try {
            return btcMillService.getMillPageList(millParam);
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[getMillList] error:{}", e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "add")
    @ResponseBody
    public String addMillInfo(MillParam millParam) {
        try {
            btcMillService.addMillInfo(millParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[addMillInfo] error:{}", e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "del")
    @ResponseBody
    public String delMillInfo(@RequestParam(value = "id") String id) {
        if (StringUtils.isEmpty(id)) {
            return "操作失败";
        }
        try {
            BtcMill btcMill = btcMillService.selectById(id);
            if (btcMill != null) {
                btcMill.setMiStatus(1);
                if (!btcMillService.updateById(btcMill)) {
                    return "操作失败";
                }
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[delMillInfo] error:{}", e.getMessage());
            return "操作失败";
        }
        return "操作成功";
    }

    @PostMapping(value = "edit")
    @ResponseBody
    public String editMillInfo(MillParam millParam) {
        try {
            btcMillService.editMillInfo(millParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[editMillInfo] error:{}", e.getMessage());
            return null;
        }
    }
}
