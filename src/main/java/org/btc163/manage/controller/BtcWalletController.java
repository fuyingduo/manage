package org.btc163.manage.controller;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.BaseResultView;
import org.btc163.manage.controller.model.BtcWalletInfoModel;
import org.btc163.manage.controller.model.BtcWalletModel;
import org.btc163.manage.controller.param.WalletParam;
import org.btc163.manage.entity.BtcWallet;
import org.btc163.manage.entity.BtcWalletInfo;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.BtcWalletInfoService;
import org.btc163.manage.service.BtcWalletService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

/**
 * created by fuyd on 2018/9/6
 */
@Slf4j
@Controller
@RequestMapping(value = "/wa")
public class BtcWalletController {

    private static final String CLASS_NAME = "[BtcWalletController]";

    @Autowired
    private BtcWalletService btcWalletService;

    @Autowired
    private BtcWalletInfoService btcWalletInfoService;

    @Value("${image.logo.read}")
    private String readImage;

    @GetMapping(value = "jumpView")
    public String jumpListView() {
        return BaseResultView.view("wa/wallet_list");
    }

    @GetMapping(value = "jumpAdd")
    public String jumpAddView() {
        return BaseResultView.view("wa/wallet_add");
    }

    @GetMapping(value = "jumpEdit")
    public String jumpEditView(@RequestParam(value = "id") String id, Model model) {
        BtcWallet btcWallet = btcWalletService.selectById(id);
        BtcWalletModel btcWalletModel = new BtcWalletModel();
        BeanUtils.copyProperties(btcWallet, btcWalletModel);
        btcWalletModel.setWaHits(btcWallet.getWaHits().toString());
        btcWalletModel.setWaLike(btcWallet.getWaLike().toString());
        model.addAttribute("btcWallet", btcWalletModel);
        BtcWalletInfo btcWalletInfo = new BtcWalletInfo();
        btcWalletInfo.setWaId(id);
        EntityWrapper<BtcWalletInfo> entityWrapper = new EntityWrapper<>(btcWalletInfo);
        List<BtcWalletInfo> btcWalletInfos = btcWalletInfoService.selectList(entityWrapper);
        model.addAttribute("readImage", readImage);
        if (!CollectionUtils.isEmpty(btcWalletInfos)) {
            model.addAttribute("btcWalletInfoModels", btcWalletInfos);
        }
        return BaseResultView.view("wa/wallet_edit");
    }

    @GetMapping(value = "getWalletList")
    @ResponseBody
    public String getWalletList(WalletParam walletParam) {
        try {
            return btcWalletService.getWalletPageList(walletParam);
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[getWalletList] error:{}", e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "add")
    @ResponseBody
    public String addWalletInfo(WalletParam walletParam) {
        try {
            btcWalletService.addWalletInfo(walletParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[addWallet] error:{}", e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "del")
    @ResponseBody
    public String delWalletInfo(@RequestParam(value = "id") String id) {
        if (StringUtils.isEmpty(id)) {
            return "操作失败";
        }
        try {
            BtcWallet btcWallet = btcWalletService.selectById(id);
            if (btcWallet != null) {
                btcWallet.setWaStatus(1);
                if (!btcWalletService.updateById(btcWallet)) {
                    return "操作失败";
                }
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[delWalletInfo] error:{}", e.getMessage());
            return "操作失败";
        }
        return "操作成功";
    }

    @PostMapping(value = "edit")
    @ResponseBody
    public String editWalletInfo(WalletParam walletParam) {
        try {
            btcWalletService.editWalletInfo(walletParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[editWalletInfo] error:{}", e.getMessage());
            return null;
        }
    }
}
