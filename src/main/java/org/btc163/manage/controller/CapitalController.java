package org.btc163.manage.controller;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.app.model.CapitalInfoModel;
import org.btc163.manage.base.BaseResult;
import org.btc163.manage.base.BaseResultView;
import org.btc163.manage.controller.model.BtcProjectEditModel;
import org.btc163.manage.controller.model.BtcProjectInfoModel;
import org.btc163.manage.controller.model.BtcProjectSelectModel;
import org.btc163.manage.controller.model.CapilaInfoSelectModel;
import org.btc163.manage.controller.param.CapitalParam;
import org.btc163.manage.entity.BtcCapitalInfo;
import org.btc163.manage.entity.BtcProject;
import org.btc163.manage.entity.Capital;
import org.btc163.manage.entity.ProCa;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.BtcCapitalInfoService;
import org.btc163.manage.service.BtcProjectService;
import org.btc163.manage.service.CapitalService;
import org.btc163.manage.service.ProCaService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author fuyd
 * @since 2018-08-23
 */
@Slf4j
@Controller
@RequestMapping("/ca")
public class CapitalController {

    private static final String CLASS_NAME = "[CapitalController]";

    @Autowired
    private CapitalService capitalService;

    @Autowired
    private BtcCapitalInfoService btcCapitalInfoService;

    @Autowired
    private ProCaService proCaService;

    @Value("${image.logo.read}")
    private String readImage;

    @GetMapping(value = "jumpView")
    public String jumpCapitalView() {
        return BaseResultView.view("ca/capital_list");
    }

    @GetMapping(value = "jump")
    public String jumpCapitalAdd() {
        return BaseResultView.view("ca/capital_add");
    }

    @GetMapping(value = "jumpEdit")
    public String jumpCapitalEdit(@RequestParam(value = "id") final String id, Model model) {
        if (StringUtils.isEmpty(id)) {
            model.addAttribute("message", "参数异常");
            return "error/403";
        }
        // 资本列表
        Capital capital = capitalService.selectById(id);
        BtcProjectEditModel btcProjectEditModel = new BtcProjectEditModel();
        BeanUtils.copyProperties(capital, btcProjectEditModel);
        if (capital.getCaEstablish() != null) {
            btcProjectEditModel.setCaEstablish(capital.getCaEstablish().replace(".", "-"));
        }
        btcProjectEditModel.setCaStatistics(capital.getCaStatistics() == null ? "0" : capital.getCaStatistics().toString());
        btcProjectEditModel.setCaPraise(capital.getCaPraise() == null ? "0" : capital.getCaPraise().toString());
        model.addAttribute("capital", btcProjectEditModel);
        model.addAttribute("readImage", readImage);
        // 合伙人
        BtcCapitalInfo btcCapitalInfo = new BtcCapitalInfo();
        btcCapitalInfo.setCaId(id);
        EntityWrapper<BtcCapitalInfo> entityWrapper = new EntityWrapper<>(btcCapitalInfo);
        List<BtcCapitalInfo> btcCapitalInfos = btcCapitalInfoService.selectList(entityWrapper);
        model.addAttribute("btcCapitalInfos", btcCapitalInfos);
        // 参投项目
        ProCa proCa = new ProCa();
        proCa.setCaId(id);
        StringBuilder stringBuilder = new StringBuilder();
        EntityWrapper<ProCa> proCaEntityWrapper = new EntityWrapper<>(proCa);
        List<ProCa> proCas = proCaService.selectList(proCaEntityWrapper);
        String caIds = null;
        if (!CollectionUtils.isEmpty(proCas)) {
            for (ProCa ca : proCas) {
                stringBuilder.append(ca.getProId() + "|");
            }
            caIds = stringBuilder.substring(0, stringBuilder.length() - 1);
        }
        model.addAttribute("caIds", caIds);
        return BaseResultView.view("ca/capital_edit");
    }

    @PostMapping(value = "add")
    @ResponseBody
    public String addCapitalInfo(CapitalParam capitalParam) {
        try {
            capitalService.addCapitalInfo(capitalParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[addCapitalInfo] error:{}", e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "select")
    @ResponseBody
    public String getSelectCapilaInfos() {
        try {
            List<CapilaInfoSelectModel> models = capitalService.getSelectCapilaInfos();
            return JSONObject.toJSONString(new BaseResult<List<CapilaInfoSelectModel>>(models));
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[getSelectCapilaInfos] error:{}", e.getMessage());
            return JSONObject.toJSONString(new BaseResult<List<CapilaInfoSelectModel>>(null, e.getErrorCode(), e.getMessage()));
        }

    }

    @GetMapping(value = "getCapitalList")
    @ResponseBody
    public String getCapitalList(CapitalParam capitalParam) {
        try {
            return capitalService.getCapitalList(capitalParam);
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[getCapitalList] error:{}", e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "/edit")
    @ResponseBody
    public String editCapital(CapitalParam capitalParam) {
        try {
            capitalService.editCapitalInfo(capitalParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[editCapital] error:{}", e.getMessage());
        }
        return "操作失败";
    }

    @PostMapping(value = "/del")
    @ResponseBody
    public String delCapital(@RequestParam(value = "id") String id) {
        if (StringUtils.isEmpty(id)) {
            return "操作失败";
        }
        try {
            Capital capital = capitalService.selectById(id);
            if (capital == null) {
                return "操作失败";
            }
            capital.setCaStatus(1);
            capital.setUpdate(new Date());
            if (!capitalService.updateById(capital)) {
                return "操作失败";
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[delCapital] error:{}", e.getMessage());
            return "操作失败";
        }
        return "操作成功";
    }

}

