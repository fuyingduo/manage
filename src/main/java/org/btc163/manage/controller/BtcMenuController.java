package org.btc163.manage.controller;

import com.alibaba.fastjson.JSONArray;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.BaseResultView;
import org.btc163.manage.base.RedisService;
import org.btc163.manage.base.menu.JsonUtil;
import org.btc163.manage.base.menu.MenuModel;
import org.btc163.manage.constant.UserConstant;
import org.btc163.manage.controller.param.UserParam;
import org.btc163.manage.controller.param.UserPasswordParam;
import org.btc163.manage.entity.BUser;
import org.btc163.manage.entity.BtcMenu;
import org.btc163.manage.entity.BtcUserMenu;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.BtcBUserService;
import org.btc163.manage.service.BtcMenuService;
import org.btc163.manage.service.BtcUserMenuService;
import org.btc163.manage.utils.Md5Util;
import org.btc163.manage.utils.UidUtil;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * created by fuyd on 2018/9/8
 */
@Slf4j
@Controller
@RequestMapping(value = "/menu")
public class BtcMenuController {

    private static final String CLASS_NAME = "[BtcMenuController]";

    @Autowired
    private BtcMenuService btcMenuService;

    @Autowired
    private BtcBUserService btcBUserService;

    @Autowired
    private BtcUserMenuService btcUserMenuService;

    @Autowired
    private RedisService<List<MenuModel>> redisService;

    @GetMapping(value = "addMenuManageView")
    public String addMenuManageView(@RequestParam(value = "uid") String uid, Model model) {
        if (StringUtils.isEmpty(uid)) {
            return BaseResultView.view("error/403");
        }
        JSONArray tree = btcMenuService.getTree(uid);
        model.addAttribute("uid", uid);
        model.addAttribute("menus", tree.toJSONString());
        return BaseResultView.view("menu/menu_add");
    }

    @GetMapping(value = "jumpList")
    public String userListView() {
        return BaseResultView.view("menu/menu_list");
    }

    @GetMapping(value = "showUserList")
    @ResponseBody
    public String showUserList(UserParam userParam) {
        try {
            return btcBUserService.getUserList(userParam);
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[showUserList] error:{}", e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "addUserMenu")
    @ResponseBody
    public String addUserMenu(UserParam userParam, Long[] menus) {
        if (StringUtils.isEmpty(userParam.getId())) {
            return "操作失败";
        }
        BUser bUser = btcBUserService.selectById(userParam.getId());
        if (bUser == null) {
            return "用户不存在";
        }
        // 删除绑定菜单
        BtcUserMenu oldMenu = new BtcUserMenu();
        oldMenu.setUserId(userParam.getId());
        EntityWrapper<BtcUserMenu> entityWrapper = new EntityWrapper<>(oldMenu);
        List<BtcUserMenu> btcUserMenus = btcUserMenuService.selectList(entityWrapper);
        if (!CollectionUtils.isEmpty(btcUserMenus)) {
            btcUserMenus.forEach(btcUserMenu -> btcUserMenuService.deleteById(btcUserMenu));
        }
        // 重新绑定
        BtcUserMenu btcUserMenu = new BtcUserMenu();
        btcUserMenu.setUserId(userParam.getId());
        if (!ArrayUtils.isEmpty(menus)) {
            for (Long menu : menus) {
                btcUserMenu.setMenuId(menu);
                btcUserMenuService.insert(btcUserMenu);
            }
        }

        BtcMenu btcMenu = new BtcMenu();
        EntityWrapper<BtcMenu> menuEntityWrapper = new EntityWrapper<>(btcMenu);
        menuEntityWrapper.in("id", menus);
        List<BtcMenu> menuLists = btcMenuService.selectList(menuEntityWrapper);
        List<MenuModel> menuModels = new ArrayList<>();
        for (BtcMenu menuList : menuLists) {
            MenuModel menuModel = new MenuModel();
            BeanUtils.copyProperties(menuList, menuModel);
            menuModels.add(menuModel);
        }
        // 更新redis中菜单
        try {
            redisService.set(UserConstant.USER_MENU_INFO + bUser.getId(), menuModels);
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[addUserMenu] error:{}", e.getMessage());
            return "操作失败";
        }
        return "操作成功";
    }

    @GetMapping(value = "addUserView")
    public String addUserView() {
        return BaseResultView.view("menu/user_add");
    }

    @GetMapping(value = "checkUser")
    @ResponseBody
    public JsonUtil checkUser(String uname) {
        JsonUtil j = new JsonUtil();
        j.setFlag(Boolean.FALSE);
        if (StringUtils.isEmpty(uname)) {
            j.setMsg("获取数据失败");
            return j;
        }
        BUser bUser = new BUser();
        bUser.setUsername(uname);
        EntityWrapper<BUser> entityWrapper = new EntityWrapper<>(bUser);
        int result = btcBUserService.selectCount(entityWrapper);
        if (result > 0) {
            j.setMsg("用户名已存在");
            return j;
        }
        j.setFlag(true);
        return j;
    }

    @PostMapping(value = "addUser")
    @ResponseBody
    public String addUser(@RequestParam(value = "username") String username) {
        if (StringUtils.isEmpty(username)) {
            return "用户名为空";
        }
        BUser bUser = new BUser();
        bUser.setUsername(username);
        EntityWrapper<BUser> entityWrapper = new EntityWrapper<>(bUser);
        int result = btcBUserService.selectCount(entityWrapper);
        if (result > 0) {
            return "用户已存在";
        }
        bUser.setId(UidUtil.getId());
        bUser.setUserStatus("0");
        String md5 = Md5Util.getMD5("123456", username);
        bUser.setPassword(md5);
        btcBUserService.insert(bUser);
        return "操作成功";
    }

    @PostMapping(value = "password")
    @ResponseBody
    public String password(@RequestParam(value = "id") String id) {
        if (StringUtils.isEmpty(id)) {
            return "请求参数异常";
        }
        BUser bUser = btcBUserService.selectById(id);
        if (bUser == null) {
            return "操作失败";
        }
        bUser.setPassword(Md5Util.getMD5("123456", bUser.getUsername()));
        btcBUserService.updateById(bUser);
        return "操作成功";
    }

    @PostMapping(value = "del")
    @ResponseBody
    public String del(@RequestParam(value = "id") String id) {
        if (StringUtils.isEmpty(id)) {
            return "请求参数异常";
        }
        BUser bUser = btcBUserService.selectById(id);
        if (bUser == null) {
            return "操作失败";
        }
        bUser.setUserStatus("1");
        btcBUserService.updateById(bUser);
        return "操作成功";
    }

    @GetMapping(value = "editPasswordView")
    public String editPasswordView(@RequestParam(value = "id") String id, Model model) {
        if (StringUtils.isEmpty(id)) {
            return BaseResultView.view("error/403");
        }
        BUser bUser = btcBUserService.selectById(id);
        model.addAttribute("user", bUser);
        return BaseResultView.view("menu/re-pass");
    }

    @PostMapping(value = "editPassword")
    @ResponseBody
    public String updatePassword(UserPasswordParam userPasswordParam) {
        try {
            btcBUserService.editPassword(userPasswordParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[updatePassword] error:{}", e.getMessage());
            return null;
        }
    }

}
