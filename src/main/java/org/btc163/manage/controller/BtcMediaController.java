package org.btc163.manage.controller;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.mapper.EntityWrapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.util.CollectionUtils;
import org.btc163.manage.base.BaseResultView;
import org.btc163.manage.controller.model.BtcMediaSelectModel;
import org.btc163.manage.controller.param.MediaInfoParam;
import org.btc163.manage.controller.param.MediaParam;
import org.btc163.manage.entity.BtcMedia;
import org.btc163.manage.entity.BtcMediaInfo;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.BtcMediaInfoService;
import org.btc163.manage.service.BtcMediaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * created by fuyd on 2018/9/3
 */
@Slf4j
@Controller
@RequestMapping(value = "/me")
public class BtcMediaController {

    private static final String CLASS_NAME = "[BtcMediaController]";

    @Autowired
    private BtcMediaService btcMediaService;

    @Autowired
    private BtcMediaInfoService btcMediaInfoService;

    @Value("${image.logo.read}")
    private String readImage;

    @GetMapping(value = "/jumpView")
    public String jumpMediaListView() {
        return BaseResultView.view("media/media_list");
    }

    @GetMapping(value = "/addjump")
    public String jumpMediaAddView() {
        return BaseResultView.view("media/media_add");
    }

    @GetMapping(value = "/jumpEdit")
    public String jumpMediaEditView(@RequestParam(value = "id") String id, Model model) {
        if (StringUtils.isEmpty(id)) {
            BaseResultView.view("error/403");
        }
        try {
            BtcMedia btcMedia = btcMediaService.selectById(id);
            if (!StringUtils.isEmpty(btcMedia.getMtLogo())) {
                btcMedia.setMtLogo(btcMedia.getMtLogo());
            }
            btcMedia.setMtRead(StringUtils.isEmpty(btcMedia.getMtRead()) ? "0" : btcMedia.getMtRead());
            btcMedia.setMtPraise(StringUtils.isEmpty(btcMedia.getMtPraise()) ? "0" : btcMedia.getMtPraise());
            model.addAttribute("readImage", readImage);
            model.addAttribute("btcMedia", btcMedia);
            final String mId = btcMedia.getId();
            BtcMediaInfo btcMediaInfo = new BtcMediaInfo();
            btcMediaInfo.setmId(mId);
            EntityWrapper<BtcMediaInfo> entityWrapper = new EntityWrapper<>(btcMediaInfo);
            entityWrapper.orderAsc(Arrays.asList("create_time"));
            List<BtcMediaInfo> btcMediaInfoList = btcMediaInfoService.selectList(entityWrapper);
            List<MediaInfoParam> btcMediaInfos = new ArrayList<>();
            if (!CollectionUtils.isEmpty(btcMediaInfoList)) {
                for (BtcMediaInfo mediaInfo : btcMediaInfoList) {
                    MediaInfoParam mediaInfoParam = new MediaInfoParam();
                    if (!StringUtils.isEmpty(mediaInfo.getiNamePosition())) {
                        String[] iNamePositions = mediaInfo.getiNamePosition().split(",");
                        mediaInfoParam.setiUserName(iNamePositions[0]);
                        mediaInfoParam.setiPosition(iNamePositions[1]);
                    }
                    mediaInfoParam.setiBrief(mediaInfo.getiBrief());
                    if (!StringUtils.isEmpty(mediaInfo.getiLogo())) {
                        mediaInfoParam.setiLogo(mediaInfo.getiLogo());
                    }
                    btcMediaInfos.add(mediaInfoParam);
                }
            }
            model.addAttribute("btcMediaInfos", btcMediaInfos);
            // TODO 需要切割姓名和职位
            return BaseResultView.view("media/media_edit");
        } catch (Exception e) {
            log.error(CLASS_NAME + "[] error:{}", e.getMessage());
            return BaseResultView.view("error/403");
        }
    }

    @GetMapping(value = "getMediaList")
    @ResponseBody
    public String getMediaList(MediaParam mediaParam) {
        try {
            return btcMediaService.getMediaList(mediaParam);
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[] error :{getMediaList}", e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "del")
    @ResponseBody
    public String delMediaInfo(@RequestParam(value = "id") String id) {
        if (StringUtils.isEmpty(id)) {
            return "操作失败";
        }
        try {
            BtcMedia btcMedia = btcMediaService.selectById(id);
            if (btcMedia == null) {
                return "操作失败";
            }
            btcMedia.setMtStatus(1);
            if (!btcMediaService.updateById(btcMedia)) {
                return "操作失败";
            }
        } catch (Exception e) {
            log.error(CLASS_NAME + "[] error:{}", e.getMessage());
            return "操作失败";
        }
        return "操作成功";
    }

    @PostMapping(value = "add")
    @ResponseBody
    public String addMediaInfo(MediaParam mediaParam) {
        try {
            btcMediaService.addMediaInfo(mediaParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[addMediaInfo] error:{}", e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "edit")
    @ResponseBody
    public String editMedia(MediaParam mediaParam) {
        try {
            btcMediaService.editMediaInfo(mediaParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[editMedia] error:{}", e.getMessage());
            return null;
        }
    }

    @PostMapping(value = "select")
    @ResponseBody
    public String getMediaSelect() {
        BtcMedia btcMedia = new BtcMedia();
        btcMedia.setMtStatus(0);
        EntityWrapper<BtcMedia> entityWrapper = new EntityWrapper<>(btcMedia);
        List<BtcMedia> btcMediaList = btcMediaService.selectList(entityWrapper);
        List<BtcMediaSelectModel> btcMediaSelectModels = new ArrayList<>();
        if (!CollectionUtils.isEmpty(btcMediaList)) {
            btcMediaList.forEach(media -> btcMediaSelectModels.add(new BtcMediaSelectModel(media.getId(), media.getMtName())));
        }
        return JSON.toJSONString(btcMediaSelectModels);
    }
}
