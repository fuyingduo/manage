package org.btc163.manage.controller;

import org.btc163.manage.base.BaseResultView;
import org.btc163.manage.base.service.ImportService;
import org.btc163.manage.entity.BtcProject;
import org.btc163.manage.exception.BusinessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * created by fuyd on 2018/8/28
 */
@Controller
@RequestMapping(value = "im")
public class ImportController {

    @Autowired
    private ImportService<BtcProject> importService;

    @GetMapping(value = "view")
    public String test() {
        return BaseResultView.view("import/import");
    }

    @PostMapping(value = "import")
    @ResponseBody
    public void importExcel(MultipartFile file) {
        try {
            importService.excelImport(file);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (BusinessException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        String irname = "王文宇 博士 联合创始人";
        String username = irname.substring(0, irname.indexOf(" "));
        String zw = irname.substring(irname.indexOf(" ") + 1, irname.length());
        System.out.println(zw);
    }
}
