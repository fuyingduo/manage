package org.btc163.manage.controller.model;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/27
 */
@Data
@ToString
public class BtcProjectEditModel {
    private String id;
    private String caName;
    private String caLogo;
    private String caEstablish;
    private String caLab;
    private String caBrief;
    private String caUrl;
    private String caProject;
    private String caStatistics;
    private String caPraise;
}
