package org.btc163.manage.controller.model;

import lombok.Data;
import lombok.ToString;


/**
 * created by fuyd on 2018/9/7
 */
@Data
@ToString
public class BtcMillModel {

    private String id;

    private String miName;

    private String miLogo;

    private String miHits;

    private String miLike;

    private String miBrief;

    private String miWebsite;

}
