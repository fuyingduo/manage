package org.btc163.manage.controller.model;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/9/6
 */
@Data
@ToString
public class BtcWalletModel {

    private String id;

    private String waName;

    private String waLogo;

    private String waHits;

    private String waLike;

    private String waBrief;

    private String waWebsite;
}
