package org.btc163.manage.controller.model;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/27
 */
@Data
@ToString
public class PathModel {
    private String paTime;
    private String paIncident;
}
