package org.btc163.manage.controller.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;

import java.io.Serializable;

/**
 * created by fuyd on 2018/8/22
 */
@Data
@ToString
@AllArgsConstructor
public class FileModel implements Serializable {
    private static final long serialVersionUID = 2035279367638081280L;
    private Integer code;
    private String message;
    private String fileName;
}
