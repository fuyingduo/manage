package org.btc163.manage.controller.model;


/**
 * created by fuyd on 2018/8/23
 */
public class BtcProjectInfoModel {

    private String id;

    private String inUserName;
    /**
     * 头像
     */
    private String inHeadImage;
    /**
     * 职位
     */
    private String inPosition;
    /**
     * 简介
     */
    private String inBrief;

    public String getInUserName() {
        return inUserName;
    }

    public void setInUserName(String inUserName) {
        this.inUserName = inUserName;
    }

    public String getInHeadImage() {
        return inHeadImage;
    }

    public void setInHeadImage(String inHeadImage) {
        this.inHeadImage = inHeadImage;
    }

    public String getInPosition() {
        return inPosition;
    }

    public void setInPosition(String inPosition) {
        this.inPosition = inPosition;
    }

    public String getInBrief() {
        return inBrief;
    }

    public void setInBrief(String inBrief) {
        this.inBrief = inBrief;
    }

    @Override
    public String toString() {
        return "BtcProjectInfoModel{" +
                "inUserName='" + inUserName + '\'' +
                ", inHeadImage='" + inHeadImage + '\'' +
                ", inPosition='" + inPosition + '\'' +
                ", inBrief='" + inBrief + '\'' +
                '}';
    }
}
