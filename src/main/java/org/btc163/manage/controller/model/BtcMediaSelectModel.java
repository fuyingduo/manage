package org.btc163.manage.controller.model;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/9/10
 */
@Data
@ToString
public class BtcMediaSelectModel {

    private String id;
    private String mtName;

    public BtcMediaSelectModel(String id, String mtName) {
        this.id = id;
        this.mtName = mtName;
    }
}
