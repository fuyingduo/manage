package org.btc163.manage.controller.model;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/9/7
 */
@Data
@ToString
public class BtcMillInfoModel {
    private String mName;
    private String mHeadImage;
    private String mBrief;
    private String mPostition;
}
