package org.btc163.manage.controller.model;

import lombok.Data;

/**
 * created by fuyd on 2018/8/23
 */
public class ProjectDynamicModel {

    /**
     * 新闻标题
     */
    private String dyTitle;
    /**
     * 新闻地址
     */
    private String dyUrl;

    public String getDyTitle() {
        return dyTitle;
    }

    public void setDyTitle(String dyTitle) {
        this.dyTitle = dyTitle;
    }

    public String getDyUrl() {
        return dyUrl;
    }

    public void setDyUrl(String dyUrl) {
        this.dyUrl = dyUrl;
    }

    @Override
    public String toString() {
        return "ProjectDynamicModel{" +
                "dyTitle='" + dyTitle + '\'' +
                ", dyUrl='" + dyUrl + '\'' +
                '}';
    }

}
