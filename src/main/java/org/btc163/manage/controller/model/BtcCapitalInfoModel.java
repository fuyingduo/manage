package org.btc163.manage.controller.model;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/23
 */
@Data
@ToString
public class BtcCapitalInfoModel {
    private String inUserName;
    private String inHeadImage;
    private String inBrief;
    private String inPosition;
}
