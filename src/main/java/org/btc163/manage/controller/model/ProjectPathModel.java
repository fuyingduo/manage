package org.btc163.manage.controller.model;


import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * created by fuyd on 2018/8/23
 */
public class ProjectPathModel {

    /**
     * 时间
     */
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date paTime;
    /**
     * 事件
     */
    private String paIncident;

    public Date getPaTime() {
        return paTime;
    }

    public void setPaTime(Date paTime) {
        this.paTime = paTime;
    }

    public String getPaIncident() {
        return paIncident;
    }

    public void setPaIncident(String paIncident) {
        this.paIncident = paIncident;
    }

    @Override
    public String toString() {
        return "ProjectPathModel{" +
                "paTime=" + paTime +
                ", paIncident='" + paIncident + '\'' +
                '}';
    }
}
