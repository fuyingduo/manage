package org.btc163.manage.controller.model;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/24
 */
@Data
@ToString
public class PlatformInfoSelectModel {
    private String name;
}
