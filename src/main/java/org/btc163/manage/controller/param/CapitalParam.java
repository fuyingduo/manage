package org.btc163.manage.controller.param;

import lombok.Data;
import lombok.ToString;
import org.btc163.manage.base.BasePage;
import org.btc163.manage.controller.model.BtcCapitalInfoModel;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * created by fuyd on 2018/8/23
 */
@Data
@ToString
public class CapitalParam extends BasePage {

    private String id;

    /**
     * 资本名称
     */
    private String caName;
    /**
     * 资本logo
     */
    private String caLogo;
    /**
     * 成立时间
     */
    private String caEstablish;
    /**
     * 简介
     */
    private String caBrief;
    /**
     * 网址
     */
    private String caUrl;

    /**
     * 项目ID
     */
    private List<String> proIds;

    /**
     * 标签
     */
    private String caLab;

    /**
     * 项目字符串
     */
    private String caProject;

    /**
     * 点击量
     */
    private Long caStatistics;

    /**
     * 点赞
     */
    private Integer caPraise;

    /**
     * 合伙人
     */
    private List<BtcCapitalInfoModel> btcCapitalInfos;

    public CapitalParam() {

    }

    public CapitalParam(String id, String caName, String caLogo, String caEstablish, String caBrief, String caUrl, List<String> proIds, String caLab, String caProject, Long caStatistics, Integer caPraise, List<BtcCapitalInfoModel> btcCapitalInfos) {
        this.id = id;
        this.caName = caName;
        this.caLogo = caLogo;
        this.caEstablish = caEstablish;
        this.caBrief = caBrief;
        this.caUrl = caUrl;
        this.proIds = proIds;
        this.caLab = caLab;
        this.caProject = caProject;
        this.caStatistics = caStatistics;
        this.caPraise = caPraise;
        this.btcCapitalInfos = btcCapitalInfos;
    }
}
