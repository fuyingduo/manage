package org.btc163.manage.controller.param;

import lombok.Data;
import lombok.ToString;
import org.btc163.manage.base.BasePage;
import org.btc163.manage.controller.model.BtcMillInfoModel;

import java.util.List;

/**
 * created by fuyd on 2018/9/7
 */
@Data
@ToString
public class MillParam extends BasePage{

    private String id;
    /**
     * 名称
     */
    private String miName;
    /**
     * logo
     */
    private String miLogo;
    /**
     * 简介
     */
    private String miBrief;
    /**
     * 官网
     */
    private String miWebsite;
    /**
     * 点击量
     */
    private Long miHits;
    /**
     * 赞
     */
    private Long miLike;

    private List<BtcMillInfoModel> btcMillInfoModels;

    public MillParam() {
    }

    public MillParam(String id, String miName, String miLogo, String miBrief, String miWebsite, Long miHits, Long miLike) {
        this.id = id;
        this.miName = miName;
        this.miLogo = miLogo;
        this.miBrief = miBrief;
        this.miWebsite = miWebsite;
        this.miHits = miHits;
        this.miLike = miLike;
    }
}
