package org.btc163.manage.controller.param;

import lombok.Data;
import lombok.ToString;
import org.btc163.manage.base.BasePage;
import org.btc163.manage.controller.model.BtcWalletInfoModel;

import java.util.List;

/**
 * created by fuyd on 2018/9/6
 */
@Data
@ToString
public class WalletParam extends BasePage {

    private String id;

    private String waName;

    private String waLogo;

    private String waHits;

    private String waLike;

    private String waBrief;

    private String waWebsite;

    private List<BtcWalletInfoModel> btcWalletInfos;

    public WalletParam() {
    }

    public WalletParam(String id, String waName, String waLogo, String waHits, String waLike, String waBrief, String waWebsite, List<BtcWalletInfoModel> btcWalletInfos) {
        this.id = id;
        this.waName = waName;
        this.waLogo = waLogo;
        this.waHits = waHits;
        this.waLike = waLike;
        this.waBrief = waBrief;
        this.waWebsite = waWebsite;
        this.btcWalletInfos = btcWalletInfos;
    }
}
