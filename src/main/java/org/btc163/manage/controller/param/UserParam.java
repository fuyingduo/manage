package org.btc163.manage.controller.param;

import lombok.Data;
import lombok.ToString;
import org.btc163.manage.base.BasePage;

/**
 * created by fuyd on 2018/9/8
 */
@Data
@ToString
public class UserParam extends BasePage{
    private String id;
    private String username;
}
