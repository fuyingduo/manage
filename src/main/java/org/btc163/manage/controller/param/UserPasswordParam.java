package org.btc163.manage.controller.param;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/9/10
 */
@Data
@ToString
public class UserPasswordParam {
    private String id;
    private String pass;
    private String newPwd;
    private String reNewPass;
}
