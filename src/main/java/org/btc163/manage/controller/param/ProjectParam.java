package org.btc163.manage.controller.param;

import lombok.Data;
import lombok.ToString;
import org.btc163.manage.base.BasePage;

/**
 * created by fuyd on 2018/8/24
 */
@Data
@ToString
public class ProjectParam extends BasePage{
    private String proName;
}
