package org.btc163.manage.controller.param;


import lombok.Data;
import lombok.ToString;
import org.btc163.manage.controller.model.BtcProjectInfoModel;
import org.btc163.manage.controller.model.ProjectDynamicModel;
import org.btc163.manage.controller.model.ProjectPathModel;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * created by fuyd on 2018/8/22
 */
@Data
@ToString
public class BtcProjectParam {

    private String id;

    /**
     * 项目名称
     */
    private String proName;
    /**
     * 项目logo
     */
    private String proLogo;
    /**
     * Token总量
     */
    private String proTokenNum;
    /**
     * 项目阶段
     */
    private Integer proStage;
    /**
     * 共识机制
     */
    private String proConsesusMechanism;
    /**
     * 代币分配
     */
    private String proTokenAllot;
    /**
     * 标签
     */
    private List<Long> proLabel;
    /**
     * 项目介绍
     */
    private String proIntroduce;
    /**
     * 合作伙伴
     */
    private String proPartner;
    /**
     * 招募时间
     */
    private String reTime;
    private String reTimeEnd;
    /**
     * 兑换比例
     */
    private String reRatio;
    /**
     * 硬顶
     */
    private String reHardTop;
    /**
     * 锁仓
     */
    private Integer reLockUp;
    /**
     * 募集资金分配
     */
    private String reCapitalAllot;
    /**
     * 备注
     */
    private String reRemake;
    /**
     * 官网
     */
    private String coOfficial;
    /**
     * 区块链浏览器
     */
    private String coBrowser;
    /**
     * 白皮书
     */
    private String coWhite;
    /**
     * 钱包
     */
    private String coWallet;
    /**
     * 微信
     */
    private String coWechat;

    /**
     * 合作伙伴
     */
    private List<String> caIds;

    /**
     * 交易所ids
     */
    private List<String> platforms;

    /**
     * 募集token总量
     */
    private String raiseTokenSum;

    /**
     * 点击量
     */
    private Long prohits;

    /**
     * 点赞
     */
    private Integer proPraise;

    private List<ProjectPathModel> projectPaths;

    private List<ProjectDynamicModel> projectDynamics;

    private List<BtcProjectInfoModel> btcProjectInfos;

}
