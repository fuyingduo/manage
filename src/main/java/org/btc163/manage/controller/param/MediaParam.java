package org.btc163.manage.controller.param;

import lombok.Data;
import lombok.ToString;
import org.btc163.manage.base.BasePage;

import java.util.List;

/**
 * created by fuyd on 2018/9/3
 */
@Data
@ToString
public class MediaParam extends BasePage {

    private String id;
    /**
     * 媒体名称
     */
    private String mtName;

    /**
     * 媒体类型
     */
    private Integer mtType;

    /**
     * 媒体logo
     */
    private String mtLogo;

    /**
     * 网址
     */
    private String mtUrl;

    /**
     * 简介
     */
    private String mtIntroduce;

    /**
     * 点击量
     */
    private String mtRead;
    /**
     * 点赞
     */
    private String mtPraise;

    /**
     * 核心成员
     */
    private List<MediaInfoParam> btcMediaInfos;

}


