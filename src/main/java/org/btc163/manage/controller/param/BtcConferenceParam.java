package org.btc163.manage.controller.param;

import lombok.Data;
import lombok.ToString;
import org.btc163.manage.base.BasePage;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * created by fuyd on 2018/9/10
 */
@Data
@ToString
public class BtcConferenceParam extends BasePage {
    private String id;
    private String pmName;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date pmStartTime;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date pmEndTime;
    private String pmProvinceName;
    private String pmCityName;
    private String pmSite;
    private String pmDesc;
    private List<String> btcMediaIds;
    private List<String> pmBriefs;
    private List<String> pmHonoreds;
    private List<String> pmSchedules;
}
