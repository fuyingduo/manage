package org.btc163.manage.controller.param;

import lombok.Data;
import lombok.ToString;
import org.btc163.manage.base.BasePage;

/**
 * created by fuyd on 2018/8/21
 */
@Data
@ToString
public class CoinOnfiNewParam extends BasePage{
    private String enKey;
}
