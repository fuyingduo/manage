package org.btc163.manage.controller.param;

import java.io.Serializable;

public class MediaInfoParam implements Serializable {

    private static final long serialVersionUID = 666279342941061028L;
    private String iUserName;

    private String iPosition;

    private String iLogo;

    private String iBrief;

    public String getiUserName() {
        return iUserName;
    }

    public void setiUserName(String iUserName) {
        this.iUserName = iUserName;
    }

    public String getiPosition() {
        return iPosition;
    }

    public void setiPosition(String iPosition) {
        this.iPosition = iPosition;
    }

    public String getiLogo() {
        return iLogo;
    }

    public void setiLogo(String iLogo) {
        this.iLogo = iLogo;
    }

    public String getiBrief() {
        return iBrief;
    }

    public void setiBrief(String iBrief) {
        this.iBrief = iBrief;
    }

    @Override
    public String toString() {
        return "MediaInfoParam{" +
                "iUserName='" + iUserName + '\'' +
                ", iPosition='" + iPosition + '\'' +
                ", iLogo='" + iLogo + '\'' +
                ", iBrief='" + iBrief + '\'' +
                '}';
    }
}
