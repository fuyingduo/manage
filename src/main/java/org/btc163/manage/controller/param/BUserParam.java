package org.btc163.manage.controller.param;

import lombok.Data;
import lombok.ToString;

/**
 * created by fuyd on 2018/8/21
 */
@Data
@ToString
public class BUserParam {

    private String username;
    private String password;
    private String code;
}
