package org.btc163.manage.controller;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.btc163.manage.base.BaseResult;
import org.btc163.manage.base.BaseResultView;
import org.btc163.manage.controller.param.CoinOnfiNewParam;
import org.btc163.manage.entity.CoinInfoNew;
import org.btc163.manage.exception.BusinessException;
import org.btc163.manage.service.BtcCoinInfoNewService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;


/**
 * <p>
 * 前端控制器
 * </p>
 *
 * @author fuyd
 * @since 2018-08-21
 */
@Slf4j
@Controller
@RequestMapping(value = "/coin")
public class BtcCoinInfoNewController {

    private static final String CLASS_NAME = "[BtcCoinInfoNewController]";

    @Autowired
    private BtcCoinInfoNewService btcCoinInfoNewService;

    @GetMapping(value = "/info")
    public String jumpCoinView() {
        return BaseResultView.view("coin/coin_info_new_list");
    }

    @GetMapping(value = "/getCoinInfoList")
    @ResponseBody
    public String getCoinInfoList(CoinOnfiNewParam coinOnfiNewParam) {
        String coinInfoNews = btcCoinInfoNewService.selectCoinInfoNewsListPage(coinOnfiNewParam);
        return coinInfoNews;
    }

    @GetMapping(value = "/jumpAddCoinView")
    public String jumpAddCoinView() {
        return BaseResultView.view("coin/coin_info_new_add");
    }

    @PostMapping(value = "/addCoinInfo")
    @ResponseBody
    public String addCoinInfo(CoinOnfiNewParam coinOnfiNewParam) {
        try {
            btcCoinInfoNewService.addCoinInfo(coinOnfiNewParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[addCoinInfo] error:{}", e.getMessage());
            return e.getMessage();
        }
    }

    @GetMapping(value = "/jumpEditCoinView")
    public String jumpEditCoinView(@RequestParam(value = "enKey") String enKey, Model model) {
        if (StringUtils.isEmpty(enKey)) {
            model.addAttribute("message", "参数异常");
            return "error/403";
        }
        CoinInfoNew coinInfonew = btcCoinInfoNewService.selectById(enKey);
        model.addAttribute("coinInfonew", coinInfonew);
        return BaseResultView.view("coin/coin_info_new_edit");
    }

    @PostMapping(value = "/editCoinInfo")
    @ResponseBody
    public String editCoinInfo(CoinOnfiNewParam coinOnfiNewParam) {
        try {
            btcCoinInfoNewService.updateCoinInfo(coinOnfiNewParam);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[editCoinInfo] error:{}", e.getMessage());
            return e.getMessage();
        }
    }

    @PostMapping(value = "delCoinInfo")
    @ResponseBody
    public String delCoinInfo(@RequestParam(value = "enKey") String enKey) {
        try {
            btcCoinInfoNewService.delCoinInfo(enKey);
            return "操作成功";
        } catch (BusinessException e) {
            log.error(CLASS_NAME + "[delCoinInfo] error:{}", e.getMessage());
            return "操作失败";
        }
    }

}

