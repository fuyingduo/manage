package org.btc163.manage.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.btc163.manage.entity.ProPlatform;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fuyd
 * @since 2018-08-24
 */
@Mapper
public interface ProPlatformMapper extends BaseMapper<ProPlatform> {

}
