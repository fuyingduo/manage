package org.btc163.manage.mapper;

import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.btc163.manage.entity.BtcProject;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
@Mapper
public interface BtcProjectMapper extends BaseMapper<BtcProject> {
    List<BtcProject> getProjectInfosByParams(@Param(value = "page") int page,
                                             @Param(value = "pageSize") int pageSize,
                                             @Param(value = "proStages") List<Integer> proStages,
                                             @Param(value = "tepNames") List<Long> tepNames,
                                             @Param(value = "proName") String proName);
    Integer getProjectCount(@Param(value = "proStages") List<Integer> proStages,
                            @Param(value = "tepNames") List<Long> tepNames);
}
