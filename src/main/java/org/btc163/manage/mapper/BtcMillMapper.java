package org.btc163.manage.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.btc163.manage.entity.BtcMill;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fuyd
 * @since 2018-09-05
 */
@Mapper
public interface BtcMillMapper extends BaseMapper<BtcMill> {

}
