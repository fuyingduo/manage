package org.btc163.manage.mapper;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.btc163.manage.entity.PlatformInfo;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author fuyd
 * @since 2018-08-21
 */
@Mapper
public interface BtcPlatformInfoMapper extends BaseMapper<PlatformInfo> {
    int updatePlatformInfo(PlatformInfo info);
}
