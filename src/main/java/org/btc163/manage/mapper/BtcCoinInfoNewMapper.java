package org.btc163.manage.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.btc163.manage.entity.CoinInfoNew;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fuyd
 * @since 2018-08-21
 */
@Mapper
public interface BtcCoinInfoNewMapper extends BaseMapper<CoinInfoNew> {

}
