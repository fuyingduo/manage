package org.btc163.manage.mapper;

import org.apache.ibatis.annotations.Mapper;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.btc163.manage.entity.BtcProjectInfo;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fuyd
 * @since 2018-08-22
 */
@Mapper
public interface BtcProjectInfoMapper extends BaseMapper<BtcProjectInfo> {

}
