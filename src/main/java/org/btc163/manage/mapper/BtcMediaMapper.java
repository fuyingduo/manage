package org.btc163.manage.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.btc163.manage.entity.BtcMedia;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author fuyd
 * @since 2018-08-31
 */
@Mapper
public interface BtcMediaMapper extends BaseMapper<BtcMedia> {

}
