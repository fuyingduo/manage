package org.btc163.manage.utils;

import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.DbType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * <p>
 * 代码生成器
 * </p>
 * created by fuyd on 2018/06/06
 */
public class MpGenerator {

    private static final Logger log = LoggerFactory.getLogger(MpGenerator.class);

    private static final String BD_URL = "jdbc:mysql://127.0.0.1:3306/quartz?zeroDateTimeBehavior=convertToNull";
    private static final String DB_DRIVER_NAME = "com.mysql.jdbc.Driver";
    private static final String DB_USER_NAME = "root";
    private static final String DB_PASS_WORD = "root";
    private static final String[] DB_TABLE_FREFIX = new String[] { "" };
    private static final String[] DB_GENERATE_TABLE = new String[] { "ts_schedule_triggers" };
    private static final String AUTHOR = "fuyd";
    private static final String OUT_PUT_DIR = "manage/src/main/java/";
    private static final String ENTITY_CLASS_BASE = "org.btc163.manage.entity.BaseEntity";
    private static final String MAPPER_CLASS_BASE = "org.btc163.manage.mapper.BaseMapper";
    private static final String PACKAGE_PARENT = "org.btc163.manage";
    private static final String PACKAGE_MODULE_NAME = "";
    private static final String VERSION = "version";

    public static void main(String[] args) {
        System.out.println("start");
        AutoGenerator mpg = new AutoGenerator();
        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        // 输出位置
        String uri = getProjectUrl();
        System.out.println(uri + OUT_PUT_DIR);
        gc.setOutputDir(uri + OUT_PUT_DIR);
        // 文件覆盖
        gc.setFileOverride(true);
        // 活动记录
        gc.setActiveRecord(true);
        // 二级缓存
        gc.setEnableCache(false);
        // resultMap
        gc.setBaseResultMap(true);
        // columList
        gc.setBaseColumnList(false);
        // 生成后打开文件夹
        gc.setOpen(false);
        // 作者
        gc.setAuthor(AUTHOR);
        // 自定义文件命名
        gc.setMapperName("%sMapper");
        gc.setXmlName("%sMapper");
        gc.setServiceName("%sService");
        gc.setServiceImplName("%sServiceImpl");
        gc.setControllerName("%sController");
        mpg.setGlobalConfig(gc);
        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        // 数据库类型
        dsc.setDbType(DbType.MYSQL);
        dsc.setTypeConvert(new MySqlTypeConvert() {
            // 自定义数据库字段类型转换
            @Override
            public DbColumnType processTypeConvert(String fieldType) {
                log.info("[MpGenerator] [main] fieldType:{}", fieldType);
                return super.processTypeConvert(fieldType);
            }
        });
        dsc.setDriverName(DB_DRIVER_NAME);
        dsc.setUsername(DB_USER_NAME);
        dsc.setPassword(DB_PASS_WORD);
        dsc.setUrl(BD_URL);
        mpg.setDataSource(dsc);
        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        // 表前缀
        strategy.setTablePrefix(DB_TABLE_FREFIX);
        // 生成策略 (驼峰式)
        strategy.setNaming(NamingStrategy.underline_to_camel);
        // 生成实体
        strategy.setInclude(DB_GENERATE_TABLE);
        strategy.setVersionFieldName(VERSION);
        //        // 自定义实体父类
        //        strategy.setSuperEntityClass(ENTITY_CLASS_BASE);
        //        // 自定义mapper父类
        //        strategy.setSuperMapperClass(MAPPER_CLASS_BASE);
        mpg.setStrategy(strategy);
        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(PACKAGE_PARENT);
        pc.setModuleName(PACKAGE_MODULE_NAME);
        pc.setController("controller");

        mpg.setPackageInfo(pc);
        //        TemplateConfig tc = new TemplateConfig();
        //        tc.setXml("src/main/resources/mybatis");
        mpg.execute();
    }

    private static String getProjectUrl() {
        String uri = null;
        String url = MpGenerator.class.getResource("/").getPath();
        if (url.indexOf("manage") > 0) {
            uri = url.toString().substring(1, url.indexOf("manage"));
        }
        if (StringUtils.isEmpty(uri)) {
            uri = "D:\\work\\package";
        }
        log.info("create table url:{}", uri);
        return uri;
    }

}
