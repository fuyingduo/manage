package org.btc163.manage.utils;

import java.util.UUID;

/**
 * created by fuyd on 2018/8/22
 */
public class UidUtil {

    public final static String getId() {
        return UUID.randomUUID().toString().replace("-", "");
    }
}
