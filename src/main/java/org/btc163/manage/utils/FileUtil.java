package org.btc163.manage.utils;

import lombok.extern.slf4j.Slf4j;

import java.io.*;

/**
 * created by fuyd on 2018/8/22
 */
@Slf4j
public class FileUtil {

    public static boolean uploadFileToServer(InputStream formFile, String fileDir, String fileName) {
        InputStream streamIn = null;
        OutputStream streamOut = null;
        try {
            File fileDirs = new File(fileDir);
            if (!fileDirs.isDirectory()) {
                fileDirs.mkdirs();
            }
            File htmlFile = new File(fileDir + "/" + fileName);
            if (htmlFile.exists()) {
                if (htmlFile.delete()) {
                    System.out.println("删除成功");
                } else {
                    System.out.println("删除失败");

                }
            }
            streamIn = formFile;
            streamOut = new FileOutputStream(htmlFile);
            int bytesRead = 0;
            byte[] buffer = new byte[8192];
            while ((bytesRead = streamIn.read(buffer, 0, 8192)) != -1) {
                streamOut.write(buffer, 0, bytesRead);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
            return false;
        } catch (IOException e) {
            e.printStackTrace();
            log.error(e.getMessage(), e);
            return false;
        } finally {
            try {
                streamOut.close();
                streamIn.close();
            } catch (Exception e) {
                e.printStackTrace();
                log.error(e.getMessage(), e);
            }
        }
        return true;
    }

}
