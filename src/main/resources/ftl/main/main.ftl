<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <title>tokenbus后台管理系统</title>
    <link rel="stylesheet" href="${re.contextPath}/plugin/plugins/layui/css/layui.css" media="all"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/plugins/font-awesome/css/font-awesome.min.css" media="all"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/build/css/app.css" media="all"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/build/css/themes/default.css" media="all" id="skin" kit-skin/>
    <style>
        .layui-footer {
            background-color: #2F4056;
        }

        .layui-side-scroll {
            border-right: 3px solid #009688;
        }
    </style>
</head>

<body class="kit-theme">
<div class="layui-layout layui-layout-admin kit-layout-admin">
    <div class="layui-header">
        <div class="layui-logo">
            tokenbus后台管理系统
        </div>
        <div class="layui-logo kit-logo-mobile"></div>
        <ul class="layui-nav layui-layout-right kit-nav">
            <li class="layui-nav-item">
                <a href="javascript:;">
        <#assign loginModel = Session["loginModel"]>
                ${loginModel.username}
                </a>
                <dl class="layui-nav-child">
                    <dd><a href="javascript:;"
                           onclick="rePwd('修改密码','/menu/editPasswordView?id=${loginModel.id}',500,350)">修改密码</a></dd>
                </dl>
            </li>
            <li class="layui-nav-item"><a href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i> 注销</a></li>
        </ul>
    </div>
<#--<div class="layui-side layui-nav-tree layui-bg-black kit-side">
<div class="layui-side-scroll">
  <div class="kit-side-fold"><i class="fa fa-navicon" aria-hidden="true"></i></div>
    <ul class="layui-nav layui-nav-tree" lay-filter="test">
        <!-- 侧边导航: <ul class="layui-nav layui-nav-tree layui-nav-side"> &ndash;&gt;
        <li class="layui-nav-item layui-nav-itemed">
            <a href="javascript:;">管理</a>
            <dl class="layui-nav-child">
                <dd>
                    <a href="javascript:;" kit-target data-options="{url:'/coin/info',icon:'',title:'币种管理',id:'1'}">
                        <i class="layui-icon"></i><span>币种管理</span></a>
                </dd>
                <dd>
                    <a href="javascript:;" kit-target data-options="{url:'/plat/info',icon:'',title:'交易所管理',id:'2'}">
                        <i class="layui-icon"></i><span>交易所管理</span></a>
                </dd>
                &lt;#&ndash;<dd>&ndash;&gt;
                    &lt;#&ndash;<a href="javascript:;" kit-target data-options="{url:'/im/view',icon:'',title:'导入',id:'7'}">&ndash;&gt;
                        &lt;#&ndash;<i class="layui-icon"></i><span>导入</span></a>&ndash;&gt;
                &lt;#&ndash;</dd>&ndash;&gt;
            </dl>
        </li>
        <li class="layui-nav-item layui-nav-itemed">
            <a href="javascript:;">项目管理</a>
            <dl class="layui-nav-child">
                <dd>
                    <a href="javascript:;" kit-target data-options="{url:'/btc/jumpView',icon:'',title:'项目列表',id:'3'}">
                        <i class="layui-icon"></i><span>项目列表</span></a>
                </dd>
                <dd>
                    <a href="javascript:;" kit-target data-options="{url:'/btc/jump',icon:'',title:'项目录入',id:'4'}">
                        <i class="layui-icon"></i><span>项目录入</span></a>
                </dd>
            </dl>
        </li>
        <li class="layui-nav-item layui-nav-itemed">
            <a href="javascript:;">资本管理</a>
            <dl class="layui-nav-child">
                <dd>
                    <a href="javascript:;" kit-target data-options="{url:'/ca/jumpView',icon:'',title:'资本列表',id:'5'}">
                        <i class="layui-icon"></i><span>资本列表</span></a>
                </dd>
                <dd>
                    <a href="javascript:;" kit-target data-options="{url:'/ca/jump',icon:'',title:'资本录入',id:'6'}">
                        <i class="layui-icon"></i><span>资本录入</span></a>
                </dd>
            </dl>
        </li>
        <li class="layui-nav-item layui-nav-itemed">
            <a href="javascript:;">媒体管理</a>
            <dl class="layui-nav-child">
                <dd>
                    <a href="javascript:;" kit-target data-options="{url:'/me/jumpView',icon:'',title:'媒体列表',id:'7'}">
                        <i class="layui-icon"></i><span>媒体库列表</span></a>
                </dd>
            </dl>
        </li>
        <li class="layui-nav-item layui-nav-itemed">
            <a href="javascript:;">钱包管理</a>
            <dl class="layui-nav-child">
                <dd>
                    <a href="javascript:;" kit-target data-options="{url:'/wa/jumpView',icon:'',title:'钱包列表',id:'8'}">
                        <i class="layui-icon"></i><span>钱包库列表</span></a>
                </dd>
            </dl>
        </li>
        <li class="layui-nav-item layui-nav-itemed">
            <a href="javascript:;">开发管理</a>
            <dl class="layui-nav-child">
                <dd>
                    <a href="javascript:;" kit-target data-options="{url:'/ex/jumpView',icon:'',title:'开发列表',id:'9'}">
                        <i class="layui-icon"></i><span>开发库列表</span></a>
                </dd>
            </dl>
        </li>
        <li class="layui-nav-item layui-nav-itemed">
            <a href="javascript:;">矿机管理</a>
            <dl class="layui-nav-child">
                <dd>
                    <a href="javascript:;" kit-target data-options="{url:'/mi/jumpView',icon:'',title:'矿机列表',id:'10'}">
                        <i class="layui-icon"></i><span>矿机库列表</span></a>
                </dd>
            </dl>
        </li>
    </ul>
</div>
</div>-->

<#macro tree data start end>
    <#if (start=="start")>
      <div class="layui-side layui-nav-tree layui-bg-black kit-side">
      <div class="layui-side-scroll">
        <div class="kit-side-fold"><i class="fa fa-navicon" aria-hidden="true"></i></div>
      <ul class="layui-nav layui-nav-tree" lay-filter="kitNavbar" kit-navbar>
    </#if>
    <#list data as child>
        <#if child.children?size gt 0>
              <li class="layui-nav-item "><#--layui-nav-itemed-->
                  <a class="" href="javascript:;"><i aria-hidden="true"
                                                     class="layui-icon">${child.menuIcon}</i><span> ${child.menuTitle}</span></a>
                  <dl class="layui-nav-child">
                  <@tree data=child.children start="" end=""/>
                  </dl>
              </li>
        <#else>
              <dd>
                  <a href="javascript:;" kit-target
                     data-options="{url:'${child.menuUrl}',icon:'${child.menuIcon}',title:'${child.menuTitle}',id:'${child.menuOrder?c}'}">
                      <i class="layui-icon">${child.menuIcon}</i><span> ${child.menuTitle}</span></a>
              </dd>
        </#if>
    </#list>
    <#if (end=="end")>
      </ul>
      </div>
      </div>
    </#if>
</#macro>
<@tree data=menu start="start" end="end"/>

    <div class="layui-body" id="container">
        <!-- 内容主体区域 -->
        <div style="padding: 15px;"><i class="layui-icon layui-anim layui-anim-rotate layui-anim-loop">&#xe63e;</i>
            请稍等...
        </div>
    </div>

    <div class="layui-footer">
        <!-- 底部固定区域 -->

    </div>
</div>
<script src="${re.contextPath}/plugin/plugins/layui/layui.js"></script>
<script src="${re.contextPath}/plugin/tools/main.js"></script>
<script>

    function rePwd(title, url, w, h) {
        if (title == null || title == '') {
            title = false;
        }
        ;
        if (url == null || url == '') {
            url = "404.html";
        }
        ;
        if (w == null || w == '') {
            w = ($(window).width() * 0.9);
        }
        ;
        if (h == null || h == '') {
            h = ($(window).height() - 50);
        }
        ;
        layer.open({
            id: 'user-rePwd',
            type: 2,
            area: [w + 'px', h + 'px'],
            fix: false,
            maxmin: true,
            shadeClose: true,
            shade: 0.4,
            title: title,
            content: url,
        });
    }
</script>
</body>

</html>
