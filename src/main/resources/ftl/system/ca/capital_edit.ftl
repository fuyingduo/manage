<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>资本编辑</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css">
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui_1/css/layui.css">
    <link rel="stylesheet" href="${re.contextPath}/plugin/ztree/css/metroStyle/metroStyle.css">
    <style>
        /* 下拉多选样式 需要引用*/
        select[multiple] + .layui-form-select > .layui-select-title > input.layui-input {
            border-bottom: 0
        }

        select[multiple] + .layui-form-select dd {
            padding: 0;
        }

        select[multiple] + .layui-form-select .layui-form-checkbox[lay-skin=primary] {
            margin: 0 !important;
            display: block;
            line-height: 36px !important;
            position: relative;
            padding-left: 26px;
        }

        select[multiple] + .layui-form-select .layui-form-checkbox[lay-skin=primary] span {
            line-height: 36px !important;
            float: none;
        }

        select[multiple] + .layui-form-select .layui-form-checkbox[lay-skin=primary] i {
            position: absolute;
            left: 10px;
            top: 0;
            margin-top: 9px;
        }

        .multiSelect {
            line-height: normal;
            height: auto;
            padding: 4px 10px;
            overflow: hidden;
            min-height: 38px;
            margin-top: -38px;
            left: 0;
            z-index: 99;
            position: relative;
            background: none;
        }

        .multiSelect a {
            padding: 2px 5px;
            background: #908e8e;
            border-radius: 2px;
            color: #fff;
            display: block;
            line-height: 20px;
            height: 20px;
            margin: 2px 5px 2px 0;
            float: left;
        }

        .multiSelect a span {
            float: left;
        }

        .multiSelect a i {
            float: left;
            display: block;
            margin: 2px 0 0 2px;
            border-radius: 2px;
            width: 8px;
            height: 8px;
            padding: 4px;
            position: relative;
            -webkit-transition: all .3s;
            transition: all .3s
        }

        .multiSelect a i:before, .multiSelect a i:after {
            position: absolute;
            left: 8px;
            top: 2px;
            content: '';
            height: 12px;
            width: 1px;
            background-color: #fff
        }

        .multiSelect a i:before {
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg)
        }

        .multiSelect a i:after {
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg)
        }

        .multiSelect a i:hover {
            background-color: #545556;
        }

        .multiOption {
            display: inline-block;
            padding: 0 5px;
            cursor: pointer;
            color: #999;
        }

        .multiOption:hover {
            color: #5FB878
        }

        @font-face {
            font-family: "iconfont";
            src: url('data:application/x-font-woff;charset=utf-8;base64,d09GRgABAAAAAAaoAAsAAAAACfwAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABHU1VCAAABCAAAADMAAABCsP6z7U9TLzIAAAE8AAAARAAAAFZW7kokY21hcAAAAYAAAABwAAABsgdU06BnbHlmAAAB8AAAAqEAAAOUTgbbS2hlYWQAAASUAAAALwAAADYR+R9jaGhlYQAABMQAAAAcAAAAJAfeA4ZobXR4AAAE4AAAABMAAAAUE+kAAGxvY2EAAAT0AAAADAAAAAwB/gLGbWF4cAAABQAAAAAfAAAAIAEVAGhuYW1lAAAFIAAAAUUAAAJtPlT+fXBvc3QAAAZoAAAAPQAAAFBD0CCqeJxjYGRgYOBikGPQYWB0cfMJYeBgYGGAAJAMY05meiJQDMoDyrGAaQ4gZoOIAgCKIwNPAHicY2Bk/s04gYGVgYOpk+kMAwNDP4RmfM1gxMjBwMDEwMrMgBUEpLmmMDgwVLwwZ27438AQw9zA0AAUZgTJAQAokgyoeJzFkTEOgCAQBOdAjTH+wtbezvggKyteTPyFLpyFvsC9DNnbHIEA0AJRzKIBOzCKdqVW88hQ84ZN/UBPUKU85fVcrkvZ27tMc17FR+0NMh2/yf47+quxrtvT6cVJD7pinpzyI3l1ysy5OIQbzBsVxHicZVM9aBRBFJ43c7szyeV2s/97m9zP3ppb5ZID72+9iJfDnyIiGImCMZWFXaKdaSyuESJYCFZpRZBUCpaJcCCKaexsRVHQytrC2/Pt5ZSIy+z3vvnemwfvY4ZIhAw/s33mEoMcJyfJebJCCMgVKCk0B37YqNIKWL5kOabCwiD0eVCqsjPglGTTrrUaZUfmsgoK5KHu11phlYbQbHToaajZOYDsjLeqz83q7BFMumH+fnyRPgGrEMyqnYV4eX7JrBUNsTWl61ldfyhkSRKUplQFNh17QpqYlOOnkupZ+4UTtABT2dC7tJYpzug3txu3c3POBECvB8ZMUXm2pHkarnuebehZPp0RrpcJjpmw9TXtGlO58heCXwpnfcVes7PExknPkVWctFxSIUxANgs4Q9RaglYjjIKwCqGvANfy4NQtBL8DkYaipAVVaGqNVuTnoQBYg8NzHzNaJ7HAdpjFXfF2DSEjxF2ui7T8ifP2CsBiZTCsLCbxCv4UDvlgp+kFgQcHXgAQP64s0gdQdOOKWwSM8CGJz4V4c11gQwc70hTlH4XLv12dbwO052OotGHMYYj8VrwDJQ/eeSXA2Ib24Me42XvX993ECxm96LM+6xKdBCRCNy6TdfSDoxmJFXYBaokV5RL7K/0nOHZ9rBl+chcCP7kVMML6SGHozx8Od3ZvCEvlm5KQ0nxPTJtiLHD7ny1jsnxYsAF7imkq8QVEOBgF5Yh0yNkpPIenN2QAsSdMNX6xu85VC/tiE3Mat6P8JqWM73NLhZ9mzjBy5uAlAlJYBiMRDPQleQ+9FEFfJJImGnHQHWIEmm/5UB8h8uaIIzrc4SEPozByel3oDvFcN+4D+dU/uou/L2xv/1mUQBdTCIN+jGUEgV47UkB+Aw7YpAMAAAB4nGNgZGBgAGLbQwYd8fw2Xxm4WRhA4HrO20sI+n8DCwOzE5DLwcAEEgUAPX4LPgB4nGNgZGBgbvjfwBDDwgACQJKRARWwAgBHCwJueJxjYWBgYH7JwMDCgMAADpsA/QAAAAAAAHYA/AGIAcp4nGNgZGBgYGWIYWBjAAEmIOYCQgaG/2A+AwASVwF+AHicZY9NTsMwEIVf+gekEqqoYIfkBWIBKP0Rq25YVGr3XXTfpk6bKokjx63UA3AejsAJOALcgDvwSCebNpbH37x5Y08A3OAHHo7fLfeRPVwyO3INF7gXrlN/EG6QX4SbaONVuEX9TdjHM6bCbXRheYPXuGL2hHdhDx18CNdwjU/hOvUv4Qb5W7iJO/wKt9Dx6sI+5l5XuI1HL/bHVi+cXqnlQcWhySKTOb+CmV7vkoWt0uqca1vEJlODoF9JU51pW91T7NdD5yIVWZOqCas6SYzKrdnq0AUb5/JRrxeJHoQm5Vhj/rbGAo5xBYUlDowxQhhkiMro6DtVZvSvsUPCXntWPc3ndFsU1P9zhQEC9M9cU7qy0nk6T4E9XxtSdXQrbsuelDSRXs1JErJCXta2VELqATZlV44RelzRiT8oZ0j/AAlabsgAAAB4nGNgYoAALgbsgJWRiZGZkYWRlZGNgbGCuzw1MykzMb8kU1eXs7A0Ma8CiA05CjPz0rPz89IZGADc3QvXAAAA') format('woff')
        }

        .iconfont {
            font-family: "iconfont" !important;
            font-size: 16px;
            font-style: normal;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .icon-fanxuan:before {
            content: "\e837";
        }

        .icon-quanxuan:before {
            content: "\e623";
        }

        .icon-qingkong:before {
            content: "\e63e";
        }

        /* 下面是页面内样式，无需引用 */
        .layui-block {
            margin-bottom: 10px;
        }

        .layui-form-label {
            width: 180px;
        }

        .code {
            color: gray;
            margin-left: 10px;
        }

        .unshow > #result {
            display: none;
        }

        pre {
            padding: 5px;
            margin: 5px;
        }

        .string {
            color: green;
        }

        .number {
            color: darkorange;
        }

        .boolean {
            color: blue;
        }

        .null {
            color: magenta;
        }

        .key {
            color: red;
        }
    </style>
    <script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
<#--<script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js" charset="utf-8"></script>-->
    <script type="text/javascript" src="${re.contextPath}/plugin/ztree/js/jquery.ztree.core.js"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/layui_1/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/ztree/js/jquery.ztree.excheck.js"
            charset="utf-8"></script>
</head>
<body onload="getData()">
<form class="layui-form layui-form-pane" style="margin-left: 40px;margin-right: 100px;">
    <input type="hidden" id="projectLogo" name="caLogo" value="${capital.caLogo}"/>
    <input type="hidden" name="id" value="${capital.id}"/>
    <input type="hidden" id="ca_ids" value="${caIds}"/>
    <div class="layui-row layui-col-space10">
        <div class="layui-col-md6">
            <div class="layui-form-item">
                <label class="layui-form-label">资本名称：</label>
                <div class="layui-input-block">
                    <input type="text" name="caName" value="${capital.caName}" placeholder="请输入资本名称" class="layui-input"
                           lay-verify="caName"
                           autocomplete="off">
                </div>
            </div>
        </div>
        <div class="layui-col-md6">
            <div class="layui-form-item">
                <label class="layui-form-label">资本logo</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn layui-btn-primary" id="upLoadLogo">
                        <i class="layui-icon">&#xe660;</i>
                    </button>
                    <span style="color: #aaaaaa; font-size: x-small" id="show_project_logo">
                        <#if capital.caLogo == null>请上传头像<#else><img src="${readImage}${capital.caLogo}"
                                                                       style="width: 100px; height: 38px;"></#if>
                    </span>
                </div>
            </div>
        </div>
    </div>

    <div class="layui-row layui-col-space10">
        <div class="layui-form-item">
            <label class="layui-form-label">成立时间：</label>
            <div class="layui-input-block">
                <div class="layui-inline">
                    <input type="text" readonly placeholder="yyyy-mm-dd" value="${capital.caEstablish}" class="layui-input paTime"
                           name="caEstablish"
                           id="paTime" lay-verify="caEstablish"
                           autocomplete="off">
                </div>
            </div>
        </div>
        <div class="layui-col-md4">
            <div class="layui-form-item">
                <label class="layui-form-label">标签：</label>
                <div class="layui-input-block">
                    <input type="text" name="caLab" value="${capital.caLab}" placeholder="请输入标签" class="layui-input"
                           lay-verify="caLab"
                           autocomplete="off">
                </div>
            </div>
        </div>
        <div class="layui-col-md4">
            <div class="layui-form-item">
                <label class="layui-form-label">点击量：</label>
                <div class="layui-input-block">
                    <input type="text" name="caStatistics" value="${capital.caStatistics}" placeholder="请输入点击量" class="layui-input"
                           lay-verify="caStatistics"
                           autocomplete="off">
                </div>
            </div>
        </div>
        <div class="layui-col-md4">
            <div class="layui-form-item">
                <label class="layui-form-label">点赞：</label>
                <div class="layui-input-block">
                    <input type="text" name="caPraise" value="${capital.caPraise}" placeholder="请输入点击量" class="layui-input"
                           lay-verify="caPraise"
                           autocomplete="off">
                </div>
            </div>
        </div>
        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">资本介绍</label>
            <div class="layui-input-block">
                    <textarea placeholder="200字以内" name="caBrief" class="layui-textarea"
                              lay-verify="caBrief">${capital.caBrief}</textarea>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">资本网址：</label>
            <div class="layui-input-block">
                <input type="text" name="caUrl" value="${capital.caUrl}" placeholder="http://" class="layui-input"
                       lay-verify="caUrl"
                       autocomplete="off">
            </div>
        </div>

        <div class="layui-block">
            <label class="layui-form-label">参投项目：</label>
            <div class="layui-input-inline">
                <select name="proIds" multiple lay-search id="select_get_data">
                    <option value="">请选择</option>
                <#-- <option value="0">基础链</option>
                 <option value="1">匿名链</option>
                 <option value="2">数据存储链</option>
                 <option value="3">金融服务</option>
                 <option value="4">物联网</option>-->
                </select>
            </div>
        </div>

        <div class="layui-col-md4">
            <div class="layui-form-item">
                <label class="layui-form-label">参投项目：</label>
                <div class="layui-input-block">
                    <input type="text" name="caProject" value="${capital.caProject}" placeholder="请输入投资项目" class="layui-input"
                           lay-verify="caProject"
                           autocomplete="off">
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <fieldset class="layui-elem-field layui-field-title" style="margin-top: 10px;">
                <legend style="font-size:16px;">合伙人</legend>
            </fieldset>
        </div>
        <div>
            <span class="layui-btn layui-btn-xs" id="add_core_member">新增</span>
            <div class="num" num="0">
                <div class="layui-form-item">
                    <label class="layui-form-label">姓名：</label>
                    <div class="layui-input-block">
                        <input type="text" name="btcCapitalInfos[0].inUserName" value="${btcCapitalInfos[0].inUserName}"
                               placeholder="请输入姓名"
                               class="layui-input"
                               autocomplete="off">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">职位：</label>
                    <div class="layui-input-block">
                        <input type="text" name="btcCapitalInfos[0].inPosition" value="${btcCapitalInfos[0].inPosition}"
                               placeholder="请输入职位"
                               class="layui-input"
                               autocomplete="off">
                    </div>
                </div>
                <div class="layui-col-md8">
                    <div class="layui-form-item">
                        <label class="layui-form-label">头像：</label>
                        <div class="layui-input-block">
                            <button type="button" class="layui-btn layui-btn-primary UploadheadImage" id="">
                                <i class="layui-icon">&#xe660;</i>
                            </button>
                            <span style="color: #aaaaaa; font-size: x-small" class="show_head_logo"
                                  id="">
                                <#if btcCapitalInfos[0].inHeadImage == null>请上传头像<#else><img src="${readImage}${btcCapitalInfos[0].inHeadImage}"
                                                                             style="width: 38px; height: 38px;"></#if>
                            </span>
                            <input type="hidden" class="projectHead" id=""
                                   name="btcCapitalInfos[0].inHeadImage"
                                   value="${btcCapitalInfos[0].inHeadImage}"/>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item layui-form-text">
                    <label class="layui-form-label">简介</label>
                    <div class="layui-input-block">
                                    <textarea placeholder="200字以内" name="btcCapitalInfos[0].inBrief"
                                              class="layui-textarea">${btcCapitalInfos[0].inBrief}</textarea>
                    </div>
                </div>
            </div>
        <#if btcCapitalInfos?? && (btcCapitalInfos?size > 1)>
            <#list btcCapitalInfos as item>
                        <#if item_index gt 0>
                        <div class="num" num="${item_index}">
                            <span class="layui-btn layui-btn-xs layui-btn-danger del_project">删除</span>
                            <div class="layui-form-item">
                                <label class="layui-form-label">姓名：</label>
                                <div class="layui-input-block">
                                    <input type="text" name="btcCapitalInfos[${item_index}].inUserName" value="${item.inUserName}"
                                           placeholder="请输入姓名"
                                           class="layui-input"
                                           autocomplete="off">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">职位：</label>
                                <div class="layui-input-block">
                                    <input type="text" name="btcCapitalInfos[${item_index}].inPosition" value="${item.inPosition}"
                                           placeholder="请输入职位"
                                           class="layui-input"
                                           autocomplete="off">
                                </div>
                            </div>
                            <div class="layui-col-md8">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">头像：</label>
                                    <div class="layui-input-block">
                                        <button type="button" class="layui-btn layui-btn-primary UploadheadImage" id="">
                                            <i class="layui-icon">&#xe660;</i>
                                        </button>
                                        <span style="color: #aaaaaa; font-size: x-small" class="show_head_logo"
                                              id="">
                                            <#if item.inHeadImage == null>请上传头像<#else><img src="${readImage}${item.inHeadImage}"
                                                                                                         style="width: 38px; height: 38px;"></#if>
                                        </span>
                                        <input type="hidden" class="projectHead" id=""
                                               name="btcCapitalInfos[${item_index}].inHeadImage"
                                               value="${item.inHeadImage}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item layui-form-text">
                                <label class="layui-form-label">简介</label>
                                <div class="layui-input-block">
                                    <textarea placeholder="200字以内" name="btcCapitalInfos[${item_index}].inBrief"
                                              class="layui-textarea">${item.inBrief}</textarea>
                                </div>
                            </div>
                        </div>
                        </#if>
                </#list>
            </#if>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <a class="layui-btn" type="button" lay-filter="add" lay-submit>保存</a>
            <a href="javascript:history.back(-1)" class="layui-btn layui-btn-primary">取消</a>
        </div>
    </div>
</form>

<script type="text/javascript">
    function getData() {
        $.post("/btc/select", function (result) {
            var jsonRS = JSON.parse(result);
            console.log(jsonRS);
            if (jsonRS.code == 200) {
                var caIds = $('#ca_ids').val();
                var data = jsonRS.data;
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    var id = data[i].id;
                    var proName = data[i].proName;
                    if (caIds != null && caIds != '') {
                        console.log(caIds.indexOf("|"));
                        if (caIds.indexOf("|") < 0) {
                            console.log(data[i].id);
                            console.log(caIds);
                            if (caIds == data[i].id) {
                                $('#select_get_data').append('<option value="' + id + '" selected>' + proName + '</option>');
                                break;
                            }
                        } else {
                            var caArr = caIds.split("|");
                            for (var j = 0; j < caArr.length; j++) {
                                if (caArr[j] == data[i].id) {
                                    $('#select_get_data').append('<option value="' + id + '" selected>' + proName + '</option>');
                                    break;
                                } else if (j == caArr.length - 1) {
                                    $('#select_get_data').append('<option value="' + id + '">' + proName + '</option>');
                                }
                            }
                        }
                    }
                    $('#select_get_data').append('<option value="' + id + '">' + proName + '</option>');
                }
                var form = layui.form;
                form.render();
            }
        });
    }
</script>
<script>
    layui.use(['layer', 'form', 'upload', 'laydate'], function () {
        var upload = layui.upload;
        $ = layui.jquery;
        var form = layui.form
                , layer = layui.layer;
        var laydate = layui.laydate;

        // 下拉框 多选 复选框渲染(如果不加不显示)
        form.render();
        //自定义验证规则
        form.verify({
            caName: function (value) {
                if (value.trim() == "") {
                    return "资本名称不能为空";
                }
            }
        });


        $('#add_core_member').click(function () {
            var maxdiv = $('.num')[$('.num').length - 1];
            var max_str = maxdiv.attributes[1].nodeValue;
            var max_num = Number(max_str) + 1;
            console.log(max_num);
            $(this).parent('div').append('' +
                    '     <div class="num" num="' + max_num + '"> ' +
                    '           <span class="layui-btn layui-btn-xs layui-btn-danger del_project">删除</span>' +
                    '         <div class="layui-form-item">' +
                    '           <label class="layui-form-label">姓名：</label>' +
                    '           <div class="layui-input-block">' +
                    '               <input type="text" name="btcCapitalInfos[' + max_num + '].inUserName" value="" placeholder="请输入名称" class="layui-input"' +
                    '               autocomplete="off">' +
                    '           </div>' +
                    '         </div>' +
                    '         <div class="layui-form-item">' +
                    '           <label class="layui-form-label">职位：</label>' +
                    '           <div class="layui-input-block">' +
                    '              <input type="text" name="btcCapitalInfos[' + max_num + '].inPosition" value="" placeholder="请输入职位名称" class="layui-input"' +
                    '               autocomplete="off">' +
                    '           </div>' +
                    '         </div>' +
                    '         <div class="layui-col-md8">' +
                    '           <div class="layui-form-item">' +
                    '                <label class="layui-form-label">头像：</label>' +
                    '                <div class="layui-input-block">' +
                    '                   <button type="button" class="layui-btn layui-btn-primary UploadheadImage" id="">' +
                    '                       <i class="layui-icon">&#xe660;</i>' +
                    '                    </button>' +
                    '                    <span style="color: #aaaaaa; font-size: x-small" class="show_head_logo"' +
                    '                   id="">请上传头像</span>' +
                    '                   <input type="hidden" class="projectHead" id="" name="btcCapitalInfos[' + max_num + '].inHeadImage" value=""/>' +
                    '                </div>' +
                    '           </div>' +
                    '           </div>' +
                    '           <div class="layui-form-item layui-form-text">' +
                    '               <label class="layui-form-label">简介</label>' +
                    '               <div class="layui-input-block">' +
                    '                   <textarea placeholder="200字以内" name="btcCapitalInfos[' + max_num + '].inBrief"' +
                    '                   class="layui-textarea"></textarea>' +
                    '               </div>' +
                    '           </div>' +
                    '        </div>');

            $('.del_project').click(function () {
                $(this).parent('div').remove();
            });
            layui.each($(".UploadheadImage"), function (index, elem) {
                layui.use('upload', function () {
                    var upload = layui.upload;
                    upload.render({
                        elem: elem
                        , url: '/upload/image/'
                        , auto: true //选择文件后不自动上传
                        , bindAction: '' //指向一个按钮触发上传
                        , choose: function (obj) {
                            //将每次选择的文件追加到文件队列
                            var doc = this.item;
                            var files = obj.pushFile();
                            //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                            obj.preview(function (index, file, result) {
                                doc.parent('div').find('.show_head_logo').html('<img style="width: 38px; height: 38px;" src="' + result + '" >');
                                // $('.show_head_logo').html('<img style="width: 100px; height: 100px;" src="' + result + '" >');
                            });
                        }
                        , done: function (res, index, upload) {
                            var doc = this.item;
                            console.log(res);
                            //假设code=0代表上传成功
                            if (res.code == 0) {
                                doc.parent('div').find('.projectHead').val(res.fileName);
                            }
                            if (res.code == -1) {
                                window.top.layer.msg("上传失败", {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                                // uploadInst.upload();  重新上传
                            }
                        }

                    });
                });
            });

        });

        // 日期
        laydate.render({
            elem: '#paTime'
            // type: 'datetime'
        });

        // LOGO
        upload.render({
            elem: '#upLoadLogo'
            , url: '/upload/image/'
            , auto: true //选择文件后不自动上传
            , bindAction: '' //指向一个按钮触发上传
            , choose: function (obj) {
                //将每次选择的文件追加到文件队列
                var files = obj.pushFile();
                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function (index, file, result) {
                    $('#show_project_logo').html('<img style="width: 100px; height: 38px;" src="' + result + '" >');
                });
            }
            , done: function (res, index, upload) {
                console.log(res);
                //假设code=0代表上传成功
                if (res.code == 0) {
                    $('#projectLogo').val(res.fileName);
                }
                if (res.code == -1) {
                    window.top.layer.msg("上传失败", {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                    // uploadInst.upload();  重新上传
                }
            }
        });

        var head = upload.render({
            elem: '.UploadheadImage'
            , url: '/upload/image/'
            , auto: true //选择文件后不自动上传
            , bindAction: '' //指向一个按钮触发上传
            , choose: function (obj) {
                //将每次选择的文件追加到文件队列
                var doc = this.item;
                var files = obj.pushFile();
                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function (index, file, result) {
                    doc.parent('div').find('.show_head_logo').html('<img style="width: 38px; height: 38px;" src="' + result + '" >');
                    // $('.show_head_logo').html('<img style="width: 100px; height: 100px;" src="' + result + '" >');
                });
            }
            , done: function (res, index, upload) {
                var doc = this.item;
                console.log(res);
                //假设code=0代表上传成功
                if (res.code == 0) {
                    doc.parent('div').find('.projectHead').val(res.fileName);
                }
                if (res.code == -1) {
                    window.top.layer.msg("上传失败", {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                    // uploadInst.upload();  重新上传
                }
            }
        });

        //监听提交
        form.on('submit(add)', function (data) {
            console.log(data.field);
            $.ajax({
                url: '/ca/edit',
                type: 'post',
                data: data.field,
                async: false, traditional: true,
                success: function (data) {
                    window.top.layer.msg(data, {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                    window.location.href = "/ca/jumpView";
                }, error: function () {
                    layer.alert("请求失败", {icon: 6}, function () {
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                    });
                }
            });
            return false;
        });
    });
</script>
</body>
</html>