<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>交易所列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js"
            charset="utf-8"></script>
    <style>
        .layui-input {
            height: 30px;
            width: 120px;
        }
        .x-nav {
            padding: 0 20px;
            position: relative;
            z-index: 99;
            border-bottom: 1px solid #e5e5e5;
            height: 32px;
            overflow: hidden;
        }
    </style>
</head>

<body>
<br/>
<div class="x-nav">
</div>

<div class="layui-col-md12" style="height:40px;margin-top:3px;">
    <div class="layui-btn-group">
        <@shiro.hasPermission name="plat:insert">
            <button class="layui-btn layui-btn-sm" data-type="add">
                新增
            </button>
        </@shiro.hasPermission>
    </div>
</div>
<table id="platPageList" class="layui-hide" lay-filter="user"></table>



<script type="text/html" id="toolBar">
    <@shiro.hasPermission name="plat:edit">
           <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    </@shiro.hasPermission>
     <@shiro.hasPermission name="plat:del">
             <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
     </@shiro.hasPermission>
</script>
<script type="text/html" id="indexTpl">
    {{d.LAY_TABLE_INDEX+1}}
</script>
<script>
    layui.laytpl.toDateString = function(d, format){
        var date = new Date(d || new Date())
                ,ymd = [
            this.digit(date.getFullYear(), 4)
            ,this.digit(date.getMonth() + 1)
            ,this.digit(date.getDate())
        ]
                ,hms = [
            this.digit(date.getHours())
            ,this.digit(date.getMinutes())
            ,this.digit(date.getSeconds())
        ];

        format = format || 'yyyy-MM-dd HH:mm:ss';

        return format.replace(/yyyy/g, ymd[0])
                .replace(/MM/g, ymd[1])
                .replace(/dd/g, ymd[2])
                .replace(/HH/g, hms[0])
                .replace(/mm/g, hms[1])
                .replace(/ss/g, hms[2]);
    };

    //数字前置补零
    layui.laytpl.digit = function (num, length, end) {
        var str = '';
        num = String(num);
        length = length || 2;
        for (var i = num.length; i < length; i++) {
            str += '0';
        }
        return num < Math.pow(10, length) ? str + (num | 0) : num;
    };

    document.onkeydown = function (e) { // 回车提交表单
        var theEvent = window.event || e;
        var code = theEvent.keyCode || theEvent.which;
        if (code == 13) {
            $(".select .select-on").click();
        }
    }
    layui.use('table', function () {
        var table = layui.table;
        //方法级渲染
        table.render({
            id: 'platPageList',
            elem: '#platPageList'
            , url: 'list'
            , cols: [[
                {title: '序号',templet: '#indexTpl', width: '5%'}
                , {field: 'name', title: '名称', width: '20%'}
                ,{field: 'name', title: '操作', width: '15%', toolbar: "#toolBar"}
            ]]
            , page: true
            ,  height: 'full-105'
        });

        var $ = layui.$, active = {
            reload:function(){
                table.reload('platPageList', {
                });
            },
            add: function () {
                add("/plat/jumpAdd");
            }
        };

        //监听工具条
        table.on('tool(user)', function (obj) {
            var data = obj.data;
            console.log(data);
            if (obj.event === 'del') {
                layer.confirm('确定删除交易所[<label style="color: #00AA91;">' + data.name + '</label>]?', function(){
                    del(data.name);
                });
            } else if (obj.event === 'edit') {
                update('jumpEdit?name=' + data.name);
            }
        });

        $('.layui-col-md12 .layui-btn').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        $('.select .layui-btn').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

    });
    function del(name) {
        $.ajax({
            url: "del",
            type: "post",
            data: {name: name},
            success: function (msg) {
                layer.msg(msg,{icon:6,offset: 'rb',area:['120px','80px'],anim:2});
                layui.table.reload('platPageList');
            }
        });
    }

    /**
     * 更新交易所
     * @param url
     */
    function update(url) {
        if (url == null || url == '') {
            return false;
        }
        window.location.href=url;
    }

    /**
     * 新增交易所
     * @param url
     */
    function add(url) {
        if (url == null || url == '') {
            return false;
        }
        window.location.href = url;
    }

</script>
</body>

</html>