<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>会议编辑</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css">
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui_1/css/layui.css">
    <link rel="stylesheet" href="${re.contextPath}/plugin/ztree/css/metroStyle/metroStyle.css">
    <style>
        /* 下拉多选样式 需要引用*/
        select[multiple] + .layui-form-select > .layui-select-title > input.layui-input {
            border-bottom: 0
        }

        select[multiple] + .layui-form-select dd {
            padding: 0;
        }

        select[multiple] + .layui-form-select .layui-form-checkbox[lay-skin=primary] {
            margin: 0 !important;
            display: block;
            line-height: 36px !important;
            position: relative;
            padding-left: 26px;
        }

        select[multiple] + .layui-form-select .layui-form-checkbox[lay-skin=primary] span {
            line-height: 36px !important;
            float: none;
        }

        select[multiple] + .layui-form-select .layui-form-checkbox[lay-skin=primary] i {
            position: absolute;
            left: 10px;
            top: 0;
            margin-top: 9px;
        }

        .multiSelect {
            line-height: normal;
            height: auto;
            padding: 4px 10px;
            overflow: hidden;
            min-height: 38px;
            margin-top: -38px;
            left: 0;
            z-index: 99;
            position: relative;
            background: none;
        }

        .multiSelect a {
            padding: 2px 5px;
            background: #908e8e;
            border-radius: 2px;
            color: #fff;
            display: block;
            line-height: 20px;
            height: 20px;
            margin: 2px 5px 2px 0;
            float: left;
        }

        .multiSelect a span {
            float: left;
        }

        .multiSelect a i {
            float: left;
            display: block;
            margin: 2px 0 0 2px;
            border-radius: 2px;
            width: 8px;
            height: 8px;
            padding: 4px;
            position: relative;
            -webkit-transition: all .3s;
            transition: all .3s
        }

        .multiSelect a i:before, .multiSelect a i:after {
            position: absolute;
            left: 8px;
            top: 2px;
            content: '';
            height: 12px;
            width: 1px;
            background-color: #fff
        }

        .multiSelect a i:before {
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg)
        }

        .multiSelect a i:after {
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg)
        }

        .multiSelect a i:hover {
            background-color: #545556;
        }

        .multiOption {
            display: inline-block;
            padding: 0 5px;
            cursor: pointer;
            color: #999;
        }

        .multiOption:hover {
            color: #5FB878
        }

        @font-face {
            font-family: "iconfont";
            src: url('data:application/x-font-woff;charset=utf-8;base64,d09GRgABAAAAAAaoAAsAAAAACfwAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABHU1VCAAABCAAAADMAAABCsP6z7U9TLzIAAAE8AAAARAAAAFZW7kokY21hcAAAAYAAAABwAAABsgdU06BnbHlmAAAB8AAAAqEAAAOUTgbbS2hlYWQAAASUAAAALwAAADYR+R9jaGhlYQAABMQAAAAcAAAAJAfeA4ZobXR4AAAE4AAAABMAAAAUE+kAAGxvY2EAAAT0AAAADAAAAAwB/gLGbWF4cAAABQAAAAAfAAAAIAEVAGhuYW1lAAAFIAAAAUUAAAJtPlT+fXBvc3QAAAZoAAAAPQAAAFBD0CCqeJxjYGRgYOBikGPQYWB0cfMJYeBgYGGAAJAMY05meiJQDMoDyrGAaQ4gZoOIAgCKIwNPAHicY2Bk/s04gYGVgYOpk+kMAwNDP4RmfM1gxMjBwMDEwMrMgBUEpLmmMDgwVLwwZ27438AQw9zA0AAUZgTJAQAokgyoeJzFkTEOgCAQBOdAjTH+wtbezvggKyteTPyFLpyFvsC9DNnbHIEA0AJRzKIBOzCKdqVW88hQ84ZN/UBPUKU85fVcrkvZ27tMc17FR+0NMh2/yf47+quxrtvT6cVJD7pinpzyI3l1ysy5OIQbzBsVxHicZVM9aBRBFJ43c7szyeV2s/97m9zP3ppb5ZID72+9iJfDnyIiGImCMZWFXaKdaSyuESJYCFZpRZBUCpaJcCCKaexsRVHQytrC2/Pt5ZSIy+z3vvnemwfvY4ZIhAw/s33mEoMcJyfJebJCCMgVKCk0B37YqNIKWL5kOabCwiD0eVCqsjPglGTTrrUaZUfmsgoK5KHu11phlYbQbHToaajZOYDsjLeqz83q7BFMumH+fnyRPgGrEMyqnYV4eX7JrBUNsTWl61ldfyhkSRKUplQFNh17QpqYlOOnkupZ+4UTtABT2dC7tJYpzug3txu3c3POBECvB8ZMUXm2pHkarnuebehZPp0RrpcJjpmw9TXtGlO58heCXwpnfcVes7PExknPkVWctFxSIUxANgs4Q9RaglYjjIKwCqGvANfy4NQtBL8DkYaipAVVaGqNVuTnoQBYg8NzHzNaJ7HAdpjFXfF2DSEjxF2ui7T8ifP2CsBiZTCsLCbxCv4UDvlgp+kFgQcHXgAQP64s0gdQdOOKWwSM8CGJz4V4c11gQwc70hTlH4XLv12dbwO052OotGHMYYj8VrwDJQ/eeSXA2Ib24Me42XvX993ECxm96LM+6xKdBCRCNy6TdfSDoxmJFXYBaokV5RL7K/0nOHZ9rBl+chcCP7kVMML6SGHozx8Od3ZvCEvlm5KQ0nxPTJtiLHD7ny1jsnxYsAF7imkq8QVEOBgF5Yh0yNkpPIenN2QAsSdMNX6xu85VC/tiE3Mat6P8JqWM73NLhZ9mzjBy5uAlAlJYBiMRDPQleQ+9FEFfJJImGnHQHWIEmm/5UB8h8uaIIzrc4SEPozByel3oDvFcN+4D+dU/uou/L2xv/1mUQBdTCIN+jGUEgV47UkB+Aw7YpAMAAAB4nGNgZGBgAGLbQwYd8fw2Xxm4WRhA4HrO20sI+n8DCwOzE5DLwcAEEgUAPX4LPgB4nGNgZGBgbvjfwBDDwgACQJKRARWwAgBHCwJueJxjYWBgYH7JwMDCgMAADpsA/QAAAAAAAHYA/AGIAcp4nGNgZGBgYGWIYWBjAAEmIOYCQgaG/2A+AwASVwF+AHicZY9NTsMwEIVf+gekEqqoYIfkBWIBKP0Rq25YVGr3XXTfpk6bKokjx63UA3AejsAJOALcgDvwSCebNpbH37x5Y08A3OAHHo7fLfeRPVwyO3INF7gXrlN/EG6QX4SbaONVuEX9TdjHM6bCbXRheYPXuGL2hHdhDx18CNdwjU/hOvUv4Qb5W7iJO/wKt9Dx6sI+5l5XuI1HL/bHVi+cXqnlQcWhySKTOb+CmV7vkoWt0uqca1vEJlODoF9JU51pW91T7NdD5yIVWZOqCas6SYzKrdnq0AUb5/JRrxeJHoQm5Vhj/rbGAo5xBYUlDowxQhhkiMro6DtVZvSvsUPCXntWPc3ndFsU1P9zhQEC9M9cU7qy0nk6T4E9XxtSdXQrbsuelDSRXs1JErJCXta2VELqATZlV44RelzRiT8oZ0j/AAlabsgAAAB4nGNgYoAALgbsgJWRiZGZkYWRlZGNgbGCuzw1MykzMb8kU1eXs7A0Ma8CiA05CjPz0rPz89IZGADc3QvXAAAA') format('woff')
        }

        .iconfont {
            font-family: "iconfont" !important;
            font-size: 16px;
            font-style: normal;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .icon-fanxuan:before {
            content: "\e837";
        }

        .icon-quanxuan:before {
            content: "\e623";
        }

        .icon-qingkong:before {
            content: "\e63e";
        }

        /* 下面是页面内样式，无需引用 */
        .layui-block {
            margin-bottom: 10px;
        }

        .layui-form-label {
            width: 180px;
        }

        .code {
            color: gray;
            margin-left: 10px;
        }

        .unshow > #result {
            display: none;
        }

        pre {
            padding: 5px;
            margin: 5px;
        }

        .string {
            color: green;
        }

        .number {
            color: darkorange;
        }

        .boolean {
            color: blue;
        }

        .null {
            color: magenta;
        }

        .key {
            color: red;
        }
    </style>
    <script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/ztree/js/jquery.ztree.core.js"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/layui_1/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/ztree/js/jquery.ztree.excheck.js"
            charset="utf-8"></script>

</head>
<body onload="mediaSelect();">
<form class="layui-form layui-form-pane" style="margin-left: 40px;margin-right: 100px;">
    <input type="hidden" name="id" value="${btcConference.id}"/>
    <input type="hidden" id="mtIds" name="" value="${mtIds}"/>
    <div class="brief_div">
    </div>

    <div class="layui-row layui-col-space10">

        <div class="layui-col-md4">
            <label class="layui-form-label">会议名称：</label>
            <div class="layui-input-block">
                <input type="text" name="pmName" value="${btcConference.pmName}" placeholder="请输入会议名称" class="layui-input"
                       lay-verify="pmName"
                       autocomplete="off">
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">会议时间</label>
            <div class="layui-input-block">
                <div class="layui-inline">
                    <input type="text" readonly placeholder="start" value="${btcConference.pmStartTime}" class="layui-input" name="pmStartTime"
                           id="reTime" lay-verify="pmStartTime"
                           autocomplete="off">
                </div>&nbsp;&nbsp;-&nbsp;&nbsp;
                <div class="layui-inline">
                    <input type="text" readonly placeholder="end" value="${btcConference.pmEndTime}" class="layui-input" name="pmEndTime"
                           id="reTimeEnd" lay-verify="pmEndTime"
                           autocomplete="off">
                </div>
            </div>
        </div>

        <div class="layui-col-md4">
            <label class="layui-form-label">省：</label>
            <div class="layui-input-block">
                <input type="text" name="pmProvinceName" value="${btcConference.pmProvinceName}" placeholder="请输入省份" class="layui-input"
                       lay-verify="pmProvinceName"
                       autocomplete="off">
            </div>
        </div>

        <div class="layui-col-md4">
            <label class="layui-form-label">市：</label>
            <div class="layui-input-block">
                <input type="text" name="pmCityName" value="${btcConference.pmCityName}" placeholder="请输入市" class="layui-input"
                       lay-verify="pmCityName"
                       autocomplete="off">
            </div>
        </div>

        <div class="layui-col-md4">
            <label class="layui-form-label">地点：</label>
            <div class="layui-input-block">
                <input type="text" name="pmSite" value="${btcConference.pmSite}" placeholder="请输入会议地址" class="layui-input"
                       lay-verify="pmSite"
                       autocomplete="off">
            </div>
        </div>

        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">介绍</label>
            <div class="layui-input-block">
                    <textarea placeholder="200字以内" name="pmDesc" class="layui-textarea"
                              lay-verify="pmDesc">${btcConference.pmDesc}</textarea>
            </div>
        </div>


        <div class="layui-block">
            <label class="layui-form-label">支持媒体：</label>
            <div class="layui-input-inline">
                <select name="btcMediaIds" multiple lay-search id="select_get_data">
                    <option value="">请选择</option>
                </select>
            </div>
        </div>
    </div>

    <div class="layui-form-item">
        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 10px;">
            <legend style="font-size:16px;">简介</legend>
        </fieldset>
    </div>
    <div class="layui-row layui-col-space10">
        <div class="layui-upload">
            <button type="button" class="layui-btn layui-btn-primary" id="briefList">选择图片</button>
            <button type="button" class="layui-btn" id="briefListAction">开始上传</button>
            <div class="layui-upload-list">
                <table class="layui-table">
                    <thead>
                    <th>图片</th>
                    <th>状态</th>
                    <th>操作</th>
                    </thead>
                    <tbody id="briefdemoList">
                    <#list briefs as brief>
                    <tr id="upload-${brief_index}">
                        <td><img src="${readImage}${brief}" alt="${brief}" class="layui-upload-img"></td>
                        <td><span style="color: #5FB878;">上传成功</span><input type="hidden" name="pmBriefs[]"  value="${brief}"/></td>
                        <td><span class="layui-btn layui-btn-xs layui-btn-danger brief-delete">删除</span></td>
                    </tr>
                    </#list>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="layui-form-item">
        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 10px;">
            <legend style="font-size:16px;">参会嘉宾</legend>
        </fieldset>
    </div>
    <div class="layui-row layui-col-space10">
        <div class="layui-upload">
            <button type="button" class="layui-btn layui-btn-primary" id="honoredList">选择图片</button>
            <button type="button"  class="layui-btn" id="honoredListAction">开始上传</button>
            <div class="layui-upload-list">
                <table class="layui-table">
                    <thead>
                    <th>图片</th>
                    <th>状态</th>
                    <th>操作</th>
                    </thead>
                    <tbody id="honoreddemoList">
                     <#list honoreds as honored>
                     <tr id="upload-${honored_index}">
                         <td><img src="${readImage}${honored}" alt="${honored}" class="layui-upload-img"></td>
                         <td><span style="color: #5FB878;">上传成功</span><input type="hidden" name="pmHonoreds[]"  value="${honored}"/></td>
                         <td><span class="layui-btn layui-btn-xs layui-btn-danger honored-delete">删除</span></td>
                     </tr>
                     </#list>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <div class="layui-form-item">
        <fieldset class="layui-elem-field layui-field-title" style="margin-top: 10px;">
            <legend style="font-size:16px;">会议日程</legend>
        </fieldset>
    </div>
    <div class="layui-row layui-col-space10">
        <div class="layui-upload">
            <button type="button" class="layui-btn layui-btn-primary" id="scheduleList">选择图片</button>
            <button type="button" class="layui-btn" id="scheduleListAction">开始上传</button>
            <div class="layui-upload-list">
                <table class="layui-table">
                    <thead>
                    <th>图片</th>
                    <th>状态</th>
                    <th>操作</th>
                    </thead>
                    <tbody id="scheduledemoList">
                    <#list schedules as schedule>
                    <tr id="upload-${schedule_index}">
                        <td><img src="${readImage}${schedule}" alt="${schedule}" class="layui-upload-img"></td>
                        <td><span style="color: #5FB878;">上传成功</span><input type="hidden" name="pmSchedules[]"  value="${schedule}"/></td>
                        <td><span class="layui-btn layui-btn-xs layui-btn-danger schedule-delete">删除</span></td>
                    </tr>
                    </#list>
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    <div class="layui-form-item">
        <div class="layui-input-block">
            <a class="layui-btn" type="button" lay-filter="add" lay-submit>保存</a>
            <a href="javascript:history.back(-1)" class="layui-btn layui-btn-primary">取消</a>
        </div>
    </div>
</form>
<script type="text/javascript">
    function mediaSelect() {
        var mtIds = $('#mtIds').val();
        $.ajax({
            url: '/me/select',
            type: 'post',
            async: false, traditional: true,
            success: function (data) {
                if (data != null) {
                    var jsonData = JSON.parse(data);
                    for (var i = 0; i < jsonData.length; i++) {
                        if (mtIds != null && mtIds != '') {
                            if (mtIds.indexOf(",") < 0) {
                                if (mtIds == jsonData[i].id) {
                                    $('#select_get_data').append('<option value="' + jsonData[i].id + '" selected>' + jsonData[i].mtName + '</option>');
                                    break;
                                }
                            } else {
                                var mtIdArr = mtIds.split(",");
                                for (var j = 0; j < mtIdArr.length; j++) {
                                    if (mtIdArr[j] == jsonData[i].id) {
                                        $('#select_get_data').append('<option value="' + jsonData[i].id + '" selected>' + jsonData[i].mtName + '</option>');
                                        break;
                                    } else if (j == mtIdArr.length - 1) {
                                        $('#select_get_data').append('<option value="' + jsonData[i].id + '">' + jsonData[i].mtName + '</option>');
                                    }
                                }
                            }
                        } else {
                            $('#select_get_data').append('<option value="' + jsonData[i].id + '">' + jsonData[i].mtName + '</option>');
                        }
                    }
                }
            }
        });
        var form = layui.form;
        form.render();
    }
</script>
<script>
    layui.use(['layer', 'form', 'upload', 'laydate'], function () {
        var upload = layui.upload;
        $ = layui.jquery;
        var form = layui.form
                , layer = layui.layer;
        var laydate = layui.laydate;


        // 下拉框 多选 复选框渲染(如果不加不显示)
        form.render();
        //自定义验证规则
        form.verify({
            waName: function (value) {
                if (value.trim() == "") {
                    return "钱包名称不能为空";
                }
            }
        });

        $('.brief-delete').click(function () {
            $(this).parent().parent("tr").remove();
        });

        $('.honored-delete').click(function () {
            $(this).parent().parent("tr").remove();
        });

        $('.schedule-delete').click(function () {
            $(this).parent().parent("tr").remove();
        });



        // 日期
        laydate.render({
            elem: '#reTime'
            // type: 'datetime'
        });
        // 日期
        laydate.render({
            elem: '#reTimeEnd'
            // type: 'datetime'
        });

        //多文件列表示例
        var briefdemoView = $('#briefdemoList')
                , uploadListIns = upload.render({
            elem: '#briefList'
            , url: '/upload/image'
            , multiple: true
            , auto: false
            , bindAction: '#briefListAction'
            , choose: function (obj) {
                var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                //读取本地文件
                obj.preview(function (index, file, result) {
                    var tr = $(['<tr id="upload-' + index + '">'
                        , '<td><img src="' + result + '" alt="' + file.name + '" class="layui-upload-img"></td>'
                        , '<td>' + (file.size / 1014).toFixed(1) + 'kb</td>'
                        , '<td>等待上传</td>'
                        , '<td>'
                        , '<button class="layui-btn layui-btn-xs demo-reload layui-hide">重传</button>'
                        , '<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>'
                        , '</td>'
                        , '</tr>'].join(''));
                    $('.pmBriefs').remove();
                    //单个重传
                    tr.find('.demo-reload').on('click', function () {
                        obj.upload(index, file);
                    });

                    //删除
                    tr.find('.demo-delete').on('click', function () {
                        delete files[index]; //删除对应的文件
                        tr.remove();
                        uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                    });

                    briefdemoView.append(tr);
                });
            }
            , done: function (res, index, upload) {
                if (res.code == 0) { //上传成功
                    console.log(res.fileName);
                    http://localhost:8090/con/addConView
                            $('.brief_div').append('<input type="text" name="pmBriefs[]" value="' + res.fileName + '"/>');
                    var tr = briefdemoView.find('tr#upload-' + index)
                            , tds = tr.children();
                    tds.eq(2).html('<span style="color: #5FB878;">上传成功</span>');
                    tds.eq(3).html(''); //清空操作
                    return delete this.files[index]; //删除文件队列已经上传成功的文件
                }
                this.error(index, upload);
            }
            , error: function (index, upload) {
                var tr = briefdemoView.find('tr#upload-' + index)
                        , tds = tr.children();
                tds.eq(2).html('<span style="color: #FF5722;">上传失败</span>');
                tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
            }
        });


        //多文件列表示例
        var honoreddemoView = $('#honoreddemoList')
                , uploadListIns = upload.render({
            elem: '#honoredList'
            , url: '/upload/image'
            , multiple: true
            , auto: false
            , bindAction: '#honoredListAction'
            , choose: function (obj) {
                var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                //读取本地文件
                obj.preview(function (index, file, result) {
                    var tr = $(['<tr id="upload-' + index + '">'
                        , '<td><img src="' + result + '" alt="' + file.name + '" class="layui-upload-img"></td>'
                        , '<td>' + (file.size / 1014).toFixed(1) + 'kb</td>'
                        , '<td>等待上传</td>'
                        , '<td>'
                        , '<button class="layui-btn layui-btn-xs demo-reload layui-hide">重传</button>'
                        , '<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>'
                        , '</td>'
                        , '</tr>'].join(''));
                    //单个重传
                    tr.find('.demo-reload').on('click', function () {
                        obj.upload(index, file);
                    });

                    //删除
                    tr.find('.demo-delete').on('click', function () {
                        delete files[index]; //删除对应的文件
                        tr.remove();
                        uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                    });

                    honoreddemoView.append(tr);
                });
            }
            , done: function (res, index, upload) {
                if (res.code == 0) { //上传成功
                    console.log(res.fileName);
                    http://localhost:8090/con/addConView
                            $('.brief_div').append('<input type="text" name="pmHonoreds[]" value="' + res.fileName + '"/>');
                    var tr = honoreddemoView.find('tr#upload-' + index)
                            , tds = tr.children();
                    tds.eq(2).html('<span style="color: #5FB878;">上传成功</span>');
                    tds.eq(3).html(''); //清空操作
                    return delete this.files[index]; //删除文件队列已经上传成功的文件
                }
                this.error(index, upload);
            }
            , error: function (index, upload) {
                var tr = honoreddemoView.find('tr#upload-' + index)
                        , tds = tr.children();
                tds.eq(2).html('<span style="color: #FF5722;">上传失败</span>');
                tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
            }
        });


        //多文件列表示例
        var scheduledemoView = $('#scheduledemoList')
                , uploadListIns = upload.render({
            elem: '#scheduleList'
            , url: '/upload/image'
            , multiple: true
            , auto: false
            , bindAction: '#scheduleListAction'
            , choose: function (obj) {
                var files = this.files = obj.pushFile(); //将每次选择的文件追加到文件队列
                //读取本地文件
                obj.preview(function (index, file, result) {
                    var tr = $(['<tr id="upload-' + index + '">'
                        , '<td><img src="' + result + '" alt="' + file.name + '" class="layui-upload-img"></td>'
                        , '<td>' + (file.size / 1014).toFixed(1) + 'kb</td>'
                        , '<td>等待上传</td>'
                        , '<td>'
                        , '<button class="layui-btn layui-btn-xs demo-reload layui-hide">重传</button>'
                        , '<button class="layui-btn layui-btn-xs layui-btn-danger demo-delete">删除</button>'
                        , '</td>'
                        , '</tr>'].join(''));
                    //单个重传
                    tr.find('.demo-reload').on('click', function () {
                        obj.upload(index, file);
                    });

                    //删除
                    tr.find('.demo-delete').on('click', function () {
                        delete files[index]; //删除对应的文件
                        tr.remove();
                        uploadListIns.config.elem.next()[0].value = ''; //清空 input file 值，以免删除后出现同名文件不可选
                    });

                    scheduledemoView.append(tr);
                });
            }
            , done: function (res, index, upload) {
                if (res.code == 0) { //上传成功
                    console.log(res.fileName);
                    http://localhost:8090/con/addConView
                            $('.brief_div').append('<input type="text" name="pmSchedules[]" value="' + res.fileName + '"/>');
                    var tr = scheduledemoView.find('tr#upload-' + index)
                            , tds = tr.children();
                    tds.eq(2).html('<span style="color: #5FB878;">上传成功</span>');
                    tds.eq(3).html(''); //清空操作
                    return delete this.files[index]; //删除文件队列已经上传成功的文件
                }
                this.error(index, upload);
            }
            , error: function (index, upload) {
                var tr = scheduledemoView.find('tr#upload-' + index)
                        , tds = tr.children();
                tds.eq(2).html('<span style="color: #FF5722;">上传失败</span>');
                tds.eq(3).find('.demo-reload').removeClass('layui-hide'); //显示重传
            }
        });


        //监听提交
        form.on('submit(add)', function (data) {
            console.log(data.field);
            $.ajax({
                url: '/con/editCon',
                type: 'post',
                data: data.field,
                async: false, traditional: true,
                success: function (data) {
                    window.top.layer.msg(data, {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                    window.location.href = "/con/jumpConView";
                }, error: function () {
                    layer.alert("请求失败", {icon: 6}, function () {
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                    });
                }
            });
            return false;
        });
    });
</script>
</body>
</html>