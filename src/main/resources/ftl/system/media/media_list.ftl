<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>媒体库列表</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css">
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui_1/css/layui.css">
    <link rel="stylesheet" href="${re.contextPath}/plugin/ztree/css/metroStyle/metroStyle.css">
    <script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js"
            charset="utf-8"></script>
    <style>

        .x-nav {
            padding: 0 20px;
            position: relative;
            z-index: 99;
            border-bottom: 1px solid #e5e5e5;
            height: 32px;
            overflow: hidden;
        }

    </style>
</head>

<body>
<br/>
<div class="select">
 <@shiro.hasPermission name="me:insert">
             <button class="layui-btn layui-btn-sm" data-type="add">
                 新增
             </button>
 </@shiro.hasPermission>
    媒体名称：
    <div class="layui-inline">
        <input class="layui-input" placeholder="媒体名称" height="20px" id="mtName" autocomplete="off">
    </div>
    <div class="layui-input-inline layui-form">
        <select id="mtType" lay-verify="required" lay-search="">
            <option value="0" selected>WEB</option>
            <option value="1">微博</option>
            <option value="2">微信</option>
        </select>
    </div>
    <button class="select-on layui-btn layui-btn-sm" data-type="select"><i class="layui-icon"></i>
    </button>
    <button class="layui-btn layui-btn-sm" id="refresh" style="float: right;"
            data-type="reload">
        <i class="layui-icon">ဂ</i>
    </button>
</div>
<table id="medialPageList" class="layui-hide" lay-filter="user"></table>
<script type="text/html" id="toolBar">
    <@shiro.hasPermission name="me:edit">
            <a class="layui-btn layui-btn-xs" lay-event="edit">编辑</a>
    </@shiro.hasPermission>
     <@shiro.hasPermission name="me:del">
            <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a>
     </@shiro.hasPermission>
</script>
<script type="text/html" id="indexTpl">
    {{d.LAY_TABLE_INDEX+1}}
</script>
<script>
    var form = layui.form
            , layer = layui.layer;
    form.render();
    layui.laytpl.toDateString = function (d, format) {
        var date = new Date(d || new Date())
                , ymd = [
            this.digit(date.getFullYear(), 4)
            , this.digit(date.getMonth() + 1)
            , this.digit(date.getDate())
        ]
                , hms = [
            this.digit(date.getHours())
            , this.digit(date.getMinutes())
            , this.digit(date.getSeconds())
        ];

        layui.table.reload('medialPageList');

        format = format || 'yyyy-MM-dd HH:mm:ss';

        return format.replace(/yyyy/g, ymd[0])
                .replace(/MM/g, ymd[1])
                .replace(/dd/g, ymd[2])
                .replace(/HH/g, hms[0])
                .replace(/mm/g, hms[1])
                .replace(/ss/g, hms[2]);
    };

    //数字前置补零
    layui.laytpl.digit = function (num, length, end) {
        var str = '';
        num = String(num);
        length = length || 2;
        for (var i = num.length; i < length; i++) {
            str += '0';
        }
        return num < Math.pow(10, length) ? str + (num | 0) : num;
    };

    document.onkeydown = function (e) { // 回车提交表单
        var theEvent = window.event || e;
        var code = theEvent.keyCode || theEvent.which;
        if (code == 13) {
            $(".select .select-on").click();
        }
    }
    layui.use('table', function () {
        var table = layui.table;
        //方法级渲染
        table.render({
            id: 'medialPageList',
            elem: '#medialPageList'
            , url: 'getMediaList'
            , cols: [[
                {title: '序号', templet: '#indexTpl', width: '5%'}
                , {field: 'mtName', title: '媒体名称', width: '10%'}
                , {field: 'mtType', title: '媒体种类', width: '10%'}
                , {field: 'mtRead', title: '点击量', width: '10%', sort: true}
                , {field: 'mtPraise', title: '点赞', width: '10%', sort: true}
                , {field: 'mtUrl', title: '网址', width: '10%'}
                , {field: 'mtIntroduce', title: '简介', width: '20%'}
                , {field: 'updateTime', title: '更新时间', width: '15%', sort: true}
                , {field: 'id', title: '操作', width: '10%', toolbar: "#toolBar"}
            ]]
            , page: true
            , height: 'full-105'
        });

        var $ = layui.$, active = {
            select: function () {
                var mtName = $('#mtName').val();
                var mtType = $('#mtType').find("option:selected").val();
                console.log(mtType);
                table.reload('medialPageList', {
                    where: {
                        mtName: mtName,
                        mtType: mtType
                    }
                });
            },
            reload: function () {
                $('#mtName').val("");
                $('#mtType').val(0);
                form.render();
                // $('#mtType').find("option[value='0']").attr("selected", "selected");
                table.reload('medialPageList', {
                    where: {
                        mtName: null,
                        mtType: null
                    }
                });
            },
            add: function () {
                add("/me/addjump");
            }
        };

        //监听工具条
        table.on('tool(user)', function (obj) {
            var data = obj.data;
            console.log(obj);
            if (obj.event === 'del') {
                layer.confirm('确定删除媒体库[<label style="color: #00AA91;">' + data.mtName + '</label>]?', function () {
                    del(data.id);
                });
            } else if (obj.event === 'edit') {
                update('jumpEdit?id=' + data.id);
            }
        });

        $('.layui-col-md12 .layui-btn').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });
        $('.select .layui-btn').on('click', function () {
            var type = $(this).data('type');
            active[type] ? active[type].call(this) : '';
        });

    });

    function del(id) {
        $.ajax({
            url: "del",
            type: "post",
            data: {id: id},
            success: function (msg) {
                layer.msg(msg, {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                layui.table.reload('medialPageList');
            }
        });
    }

    /**
     * 编辑资本
     * @param url
     */
    function update(url) {
        window.location.href = url;
    }

    /**
     * 新增资本
     * @param url
     */
    function add(url) {
        window.location.href = url;
    }

</script>
</body>

</html>