<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>媒体编辑</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css">
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui_1/css/layui.css">
    <link rel="stylesheet" href="${re.contextPath}/plugin/ztree/css/metroStyle/metroStyle.css">
    <style>
        /* 下拉多选样式 需要引用*/
        select[multiple] + .layui-form-select > .layui-select-title > input.layui-input {
            border-bottom: 0
        }

        select[multiple] + .layui-form-select dd {
            padding: 0;
        }

        select[multiple] + .layui-form-select .layui-form-checkbox[lay-skin=primary] {
            margin: 0 !important;
            display: block;
            line-height: 36px !important;
            position: relative;
            padding-left: 26px;
        }

        select[multiple] + .layui-form-select .layui-form-checkbox[lay-skin=primary] span {
            line-height: 36px !important;
            float: none;
        }

        select[multiple] + .layui-form-select .layui-form-checkbox[lay-skin=primary] i {
            position: absolute;
            left: 10px;
            top: 0;
            margin-top: 9px;
        }

        .multiSelect {
            line-height: normal;
            height: auto;
            padding: 4px 10px;
            overflow: hidden;
            min-height: 38px;
            margin-top: -38px;
            left: 0;
            z-index: 99;
            position: relative;
            background: none;
        }

        .multiSelect a {
            padding: 2px 5px;
            background: #908e8e;
            border-radius: 2px;
            color: #fff;
            display: block;
            line-height: 20px;
            height: 20px;
            margin: 2px 5px 2px 0;
            float: left;
        }

        .multiSelect a span {
            float: left;
        }

        .multiSelect a i {
            float: left;
            display: block;
            margin: 2px 0 0 2px;
            border-radius: 2px;
            width: 8px;
            height: 8px;
            padding: 4px;
            position: relative;
            -webkit-transition: all .3s;
            transition: all .3s
        }

        .multiSelect a i:before, .multiSelect a i:after {
            position: absolute;
            left: 8px;
            top: 2px;
            content: '';
            height: 12px;
            width: 1px;
            background-color: #fff
        }

        .multiSelect a i:before {
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg)
        }

        .multiSelect a i:after {
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg)
        }

        .multiSelect a i:hover {
            background-color: #545556;
        }

        .multiOption {
            display: inline-block;
            padding: 0 5px;
            cursor: pointer;
            color: #999;
        }

        .multiOption:hover {
            color: #5FB878
        }

        @font-face {
            font-family: "iconfont";
            src: url('data:application/x-font-woff;charset=utf-8;base64,d09GRgABAAAAAAaoAAsAAAAACfwAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABHU1VCAAABCAAAADMAAABCsP6z7U9TLzIAAAE8AAAARAAAAFZW7kokY21hcAAAAYAAAABwAAABsgdU06BnbHlmAAAB8AAAAqEAAAOUTgbbS2hlYWQAAASUAAAALwAAADYR+R9jaGhlYQAABMQAAAAcAAAAJAfeA4ZobXR4AAAE4AAAABMAAAAUE+kAAGxvY2EAAAT0AAAADAAAAAwB/gLGbWF4cAAABQAAAAAfAAAAIAEVAGhuYW1lAAAFIAAAAUUAAAJtPlT+fXBvc3QAAAZoAAAAPQAAAFBD0CCqeJxjYGRgYOBikGPQYWB0cfMJYeBgYGGAAJAMY05meiJQDMoDyrGAaQ4gZoOIAgCKIwNPAHicY2Bk/s04gYGVgYOpk+kMAwNDP4RmfM1gxMjBwMDEwMrMgBUEpLmmMDgwVLwwZ27438AQw9zA0AAUZgTJAQAokgyoeJzFkTEOgCAQBOdAjTH+wtbezvggKyteTPyFLpyFvsC9DNnbHIEA0AJRzKIBOzCKdqVW88hQ84ZN/UBPUKU85fVcrkvZ27tMc17FR+0NMh2/yf47+quxrtvT6cVJD7pinpzyI3l1ysy5OIQbzBsVxHicZVM9aBRBFJ43c7szyeV2s/97m9zP3ppb5ZID72+9iJfDnyIiGImCMZWFXaKdaSyuESJYCFZpRZBUCpaJcCCKaexsRVHQytrC2/Pt5ZSIy+z3vvnemwfvY4ZIhAw/s33mEoMcJyfJebJCCMgVKCk0B37YqNIKWL5kOabCwiD0eVCqsjPglGTTrrUaZUfmsgoK5KHu11phlYbQbHToaajZOYDsjLeqz83q7BFMumH+fnyRPgGrEMyqnYV4eX7JrBUNsTWl61ldfyhkSRKUplQFNh17QpqYlOOnkupZ+4UTtABT2dC7tJYpzug3txu3c3POBECvB8ZMUXm2pHkarnuebehZPp0RrpcJjpmw9TXtGlO58heCXwpnfcVes7PExknPkVWctFxSIUxANgs4Q9RaglYjjIKwCqGvANfy4NQtBL8DkYaipAVVaGqNVuTnoQBYg8NzHzNaJ7HAdpjFXfF2DSEjxF2ui7T8ifP2CsBiZTCsLCbxCv4UDvlgp+kFgQcHXgAQP64s0gdQdOOKWwSM8CGJz4V4c11gQwc70hTlH4XLv12dbwO052OotGHMYYj8VrwDJQ/eeSXA2Ib24Me42XvX993ECxm96LM+6xKdBCRCNy6TdfSDoxmJFXYBaokV5RL7K/0nOHZ9rBl+chcCP7kVMML6SGHozx8Od3ZvCEvlm5KQ0nxPTJtiLHD7ny1jsnxYsAF7imkq8QVEOBgF5Yh0yNkpPIenN2QAsSdMNX6xu85VC/tiE3Mat6P8JqWM73NLhZ9mzjBy5uAlAlJYBiMRDPQleQ+9FEFfJJImGnHQHWIEmm/5UB8h8uaIIzrc4SEPozByel3oDvFcN+4D+dU/uou/L2xv/1mUQBdTCIN+jGUEgV47UkB+Aw7YpAMAAAB4nGNgZGBgAGLbQwYd8fw2Xxm4WRhA4HrO20sI+n8DCwOzE5DLwcAEEgUAPX4LPgB4nGNgZGBgbvjfwBDDwgACQJKRARWwAgBHCwJueJxjYWBgYH7JwMDCgMAADpsA/QAAAAAAAHYA/AGIAcp4nGNgZGBgYGWIYWBjAAEmIOYCQgaG/2A+AwASVwF+AHicZY9NTsMwEIVf+gekEqqoYIfkBWIBKP0Rq25YVGr3XXTfpk6bKokjx63UA3AejsAJOALcgDvwSCebNpbH37x5Y08A3OAHHo7fLfeRPVwyO3INF7gXrlN/EG6QX4SbaONVuEX9TdjHM6bCbXRheYPXuGL2hHdhDx18CNdwjU/hOvUv4Qb5W7iJO/wKt9Dx6sI+5l5XuI1HL/bHVi+cXqnlQcWhySKTOb+CmV7vkoWt0uqca1vEJlODoF9JU51pW91T7NdD5yIVWZOqCas6SYzKrdnq0AUb5/JRrxeJHoQm5Vhj/rbGAo5xBYUlDowxQhhkiMro6DtVZvSvsUPCXntWPc3ndFsU1P9zhQEC9M9cU7qy0nk6T4E9XxtSdXQrbsuelDSRXs1JErJCXta2VELqATZlV44RelzRiT8oZ0j/AAlabsgAAAB4nGNgYoAALgbsgJWRiZGZkYWRlZGNgbGCuzw1MykzMb8kU1eXs7A0Ma8CiA05CjPz0rPz89IZGADc3QvXAAAA') format('woff')
        }

        .iconfont {
            font-family: "iconfont" !important;
            font-size: 16px;
            font-style: normal;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .icon-fanxuan:before {
            content: "\e837";
        }

        .icon-quanxuan:before {
            content: "\e623";
        }

        .icon-qingkong:before {
            content: "\e63e";
        }

        /* 下面是页面内样式，无需引用 */
        .layui-block {
            margin-bottom: 10px;
        }

        .layui-form-label {
            width: 180px;
        }

        .code {
            color: gray;
            margin-left: 10px;
        }

        .unshow > #result {
            display: none;
        }

        pre {
            padding: 5px;
            margin: 5px;
        }

        .string {
            color: green;
        }

        .number {
            color: darkorange;
        }

        .boolean {
            color: blue;
        }

        .null {
            color: magenta;
        }

        .key {
            color: red;
        }
    </style>
    <script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/ztree/js/jquery.ztree.core.js"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/layui_1/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/ztree/js/jquery.ztree.excheck.js"
            charset="utf-8"></script>

</head>
<body>
<form class="layui-form layui-form-pane" style="margin-left: 40px;margin-right: 100px;">
    <input type="hidden" id="mtLogo" name="mtLogo" value="${btcMedia.mtLogo}"/>
    <input type="hidden" name="id" value="${btcMedia.id}"/>
    <div class="layui-row layui-col-space10">
        <div class="layui-col-md4">
            <div class="layui-form-item">
                <label class="layui-form-label">媒体名称：</label>
                <div class="layui-input-block">
                    <input type="text" name="mtName" value="${btcMedia.mtName}" placeholder="请输入媒体名称" class="layui-input"
                           lay-verify="mtName"
                           autocomplete="off">
                </div>
            </div>
        </div>

        <div class="layui-col-md8">
            <div class="layui-form-item">
                <label class="layui-form-label">媒体logo</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn layui-btn-primary" id="upLoadLogo">
                        <i class="layui-icon">&#xe660;</i>
                    </button>
                    <span style="color: #aaaaaa; font-size: x-small" id="show_project_logo"><#if btcMedia.mtLogo == null>请上传图片<#else><img src="${readImage}${btcMedia.mtLogo}" style="width: 100px; height: 38px;"></#if></span>
                </div>
            </div>
        </div>

        <div class="layui-col-md4">
            <label class="layui-form-label">媒体类型</label>
            <div class="layui-input-block">
                <select name="mtType" lay-filter="mtType">
                    <option <#if btcMedia.mtType=='0'>selected</#if> value="0">WEB</option>
                    <option <#if btcMedia.mtType=='1'>selected</#if> value="1">微博</option>
                    <option <#if btcMedia.mtType=='2'>selected</#if> value="2">微信</option>
                </select>
            </div>
        </div>

        <div class="layui-form-item">
            <label class="layui-form-label">网址：</label>
            <div class="layui-input-block">
                <input type="text" name="mtUrl" value="${btcMedia.mtUrl}"
                       class="layui-input"
                       autocomplete="off">
            </div>
        </div>

        <div class="layui-form-item layui-form-text">
            <label class="layui-form-label">简介</label>
            <div class="layui-input-block">
                            <textarea placeholder="200字以内" name="mtIntroduce"
                                      class="layui-textarea">${btcMedia.mtIntroduce}</textarea>
            </div>
        </div>
        <div class="layui-col-md6">
            <div class="layui-form-item">
                <label class="layui-form-label">点击量：</label>
                <div class="layui-input-block">
                    <input type="text" name="mtRead" value="${btcMedia.mtRead}" placeholder="请输入点击量" class="layui-input"
                           lay-verify="mtRead"
                           autocomplete="off">
                </div>
            </div>
        </div>
        <div class="layui-col-md6">
            <div class="layui-form-item">
                <label class="layui-form-label">点赞：</label>
                <div class="layui-input-block">
                    <input type="text" name="mtPraise" value="${btcMedia.mtPraise}" placeholder="请输入点赞量" class="layui-input"
                           lay-verify="mtPraise"
                           autocomplete="off">
                </div>
            </div>
        </div>
        <div>
            <span class="layui-btn layui-btn-xs" id="add_core_member">新增</span>
            <div class="num" num="0">
                <div class="layui-form-item">
                    <label class="layui-form-label">姓名：</label>
                    <div class="layui-input-block">
                        <input type="text" name="btcMediaInfos[0].iUserName" value="${btcMediaInfos[0].iUserName}" placeholder="请输入姓名"
                               class="layui-input"
                               autocomplete="off">
                    </div>
                </div>
                <div class="layui-form-item">
                    <label class="layui-form-label">职位：</label>
                    <div class="layui-input-block">
                        <input type="text" name="btcMediaInfos[0].iPosition" value="${btcMediaInfos[0].iPosition}" placeholder="请输入职位"
                               class="layui-input"
                               autocomplete="off">
                    </div>
                </div>
                <div class="layui-col-md8">
                    <div class="layui-form-item">
                        <label class="layui-form-label">头像：</label>
                        <div class="layui-input-block">
                            <button type="button" class="layui-btn layui-btn-primary UploadheadImage" id="">
                                <i class="layui-icon">&#xe660;</i>
                            </button>
                            <span style="color: #aaaaaa; font-size: x-small" class="show_head_logo"
                                  id="">
                                <#if btcMediaInfos[0].iLogo == null>请上传头像<#else><img src="${readImage}${btcMediaInfos[0].iLogo}" style="width: 38px; height: 38px;"></#if>
                            </span>
                            <input type="hidden" class="projectHead" id="" name="btcMediaInfos[0].iLogo"
                                   value="${btcMediaInfos[0].iLogo}"/>
                        </div>
                    </div>
                </div>
                <div class="layui-form-item layui-form-text">
                    <label class="layui-form-label">简介</label>
                    <div class="layui-input-block">
                            <textarea placeholder="200字以内" name="btcMediaInfos[0].iBrief"
                                      class="layui-textarea">${btcMediaInfos[0].iBrief}</textarea>
                    </div>
                </div>
            </div>
            <#if btcMediaInfos?? && (btcMediaInfos?size > 1)>
                <#list btcMediaInfos as item>
                    <#if item_index gt 0>
                        <div class="num" num="${item_index}">
                            <span class="layui-btn layui-btn-xs layui-btn-danger del_project">删除</span>
                            <div class="layui-form-item">
                                <label class="layui-form-label">姓名：</label>
                                <div class="layui-input-block">
                                    <input type="text" name="btcMediaInfos[${item_index}].iUserName" value="${item.iUserName}"
                                           placeholder="请输入姓名"
                                           class="layui-input"
                                           autocomplete="off">
                                </div>
                            </div>
                            <div class="layui-form-item">
                                <label class="layui-form-label">职位：</label>
                                <div class="layui-input-block">
                                    <input type="text" name="btcMediaInfos[${item_index}].iPosition" value="${item.iPosition}"
                                           placeholder="请输入职位"
                                           class="layui-input"
                                           autocomplete="off">
                                </div>
                            </div>
                            <div class="layui-col-md8">
                                <div class="layui-form-item">
                                    <label class="layui-form-label">头像：</label>
                                    <div class="layui-input-block">
                                        <button type="button" class="layui-btn layui-btn-primary UploadheadImage" id="">
                                            <i class="layui-icon">&#xe660;</i>
                                        </button>
                                        <span style="color: #aaaaaa; font-size: x-small" class="show_head_logo"
                                              id="">
                                            <#if item.iLogo == null>请上传头像<#else><img src="${readImage}${item.iLogo}" style="width: 38px; height: 38px;"></#if>
                                        </span>
                                        <input type="hidden" class="projectHead" id=""
                                               name="btcMediaInfos[${item_index}].iLogo"
                                               value="${item.iLogo}"/>
                                    </div>
                                </div>
                            </div>
                            <div class="layui-form-item layui-form-text">
                                <label class="layui-form-label">简介</label>
                                <div class="layui-input-block">
                                    <textarea placeholder="200字以内" name="btcMediaInfos[${item_index}].iBrief"
                                              class="layui-textarea">${item.iBrief}</textarea>
                                </div>
                            </div>
                        </div>
                    </#if>
                </#list>
            </#if>
        </div>
    </div>
    <div class="layui-form-item">
        <div class="layui-input-block">
            <a class="layui-btn" type="button" lay-filter="add" lay-submit>保存</a>
            <a href="javascript:history.back(-1)" class="layui-btn layui-btn-primary">取消</a>
        </div>
    </div>
</form>

<script>
    layui.use(['layer', 'form', 'upload', 'laydate'], function () {
        var upload = layui.upload;
        $ = layui.jquery;
        var form = layui.form
                , layer = layui.layer;
        var laydate = layui.laydate;


        // 下拉框 多选 复选框渲染(如果不加不显示)
        form.render();
        //自定义验证规则
        form.verify({
            mtName: function (value) {
                if (value.trim() == "") {
                    return "媒体名称不能为空";
                }
            }
        });


        $('#add_core_member').click(function () {
            var maxdiv = $('.num')[$('.num').length - 1];
            var max_str = maxdiv.attributes[1].nodeValue;
            var max_num = Number(max_str) + 1;
            console.log(max_num);
            $(this).parent('div').after('' +
                    '     <div class="num" num="' + max_num + '"> ' +
                    '           <span class="layui-btn layui-btn-xs layui-btn-danger del_project">删除</span>' +
                    '         <div class="layui-form-item">' +
                    '           <label class="layui-form-label">姓名：</label>' +
                    '           <div class="layui-input-block">' +
                    '               <input type="text" name="btcMediaInfos[' + max_num + '].iUserName" value="" placeholder="请输入名称" class="layui-input"' +
                    '               autocomplete="off">' +
                    '           </div>' +
                    '         </div>' +
                    '         <div class="layui-form-item">' +
                    '           <label class="layui-form-label">职位：</label>' +
                    '           <div class="layui-input-block">' +
                    '              <input type="text" name="btcMediaInfos[' + max_num + '].iPosition" value="" placeholder="请输入职位名称" class="layui-input"' +
                    '               autocomplete="off">' +
                    '           </div>' +
                    '         </div>' +
                    '         <div class="layui-col-md8">' +
                    '           <div class="layui-form-item">' +
                    '                <label class="layui-form-label">头像：</label>' +
                    '                <div class="layui-input-block">' +
                    '                   <button type="button" class="layui-btn layui-btn-primary UploadheadImage" id="">' +
                    '                       <i class="layui-icon">&#xe660;</i>' +
                    '                    </button>' +
                    '                    <span style="color: #aaaaaa; font-size: x-small" class="show_head_logo"' +
                    '                   id="">请上传头像</span>' +
                    '                   <input type="hidden" class="projectHead" id="" name="btcMediaInfos[' + max_num + '].iLogo" value=""/>' +
                    '                </div>' +
                    '           </div>' +
                    '           </div>' +
                    '           <div class="layui-form-item layui-form-text">' +
                    '               <label class="layui-form-label">简介</label>' +
                    '               <div class="layui-input-block">' +
                    '                   <textarea placeholder="200字以内" name="btcMediaInfos[' + max_num + '].iBrief"' +
                    '                   class="layui-textarea"></textarea>' +
                    '               </div>' +
                    '           </div>' +
                    '        </div>');

            $('.del_project').click(function () {
                $(this).parent('div').remove();
            });
            layui.each($(".UploadheadImage"), function (index, elem) {
                layui.use('upload', function () {
                    var upload = layui.upload;
                    upload.render({
                        elem: elem
                        , url: '/upload/image/'
                        , auto: true //选择文件后不自动上传
                        , bindAction: '' //指向一个按钮触发上传
                        , choose: function (obj) {
                            //将每次选择的文件追加到文件队列
                            var doc = this.item;
                            var files = obj.pushFile();
                            //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                            obj.preview(function (index, file, result) {
                                doc.parent('div').find('.show_head_logo').html('<img style="width: 38px; height: 38px;" src="' + result + '" >');
                                // $('.show_head_logo').html('<img style="width: 100px; height: 100px;" src="' + result + '" >');
                            });
                        }
                        , done: function (res, index, upload) {
                            var doc = this.item;
                            console.log(res);
                            //假设code=0代表上传成功
                            if (res.code == 0) {
                                doc.parent('div').find('.projectHead').val(res.fileName);
                            }
                            if (res.code == -1) {
                                window.top.layer.msg("上传失败", {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                                // uploadInst.upload();  重新上传
                            }
                        }

                    });
                });
            });

        });

        // LOGO
        upload.render({
            elem: '#upLoadLogo'
            , url: '/upload/image/'
            , auto: true //选择文件后不自动上传
            , bindAction: '' //指向一个按钮触发上传
            , choose: function (obj) {
                //将每次选择的文件追加到文件队列
                var files = obj.pushFile();
                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function (index, file, result) {
                    $('#show_project_logo').html('<img style="width: 100px; height: 38px;" src="' + result + '" >');
                });
            }
            , done: function (res, index, upload) {
                console.log(res);
                //假设code=0代表上传成功
                if (res.code == 0) {
                    $('#mtLogo').val(res.fileName);
                }
                if (res.code == -1) {
                    window.top.layer.msg("上传失败", {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                    // uploadInst.upload();  重新上传
                }
            }
        });

        var head = upload.render({
            elem: '.UploadheadImage'
            , url: '/upload/image/'
            , auto: true //选择文件后不自动上传
            , bindAction: '' //指向一个按钮触发上传
            , choose: function (obj) {
                //将每次选择的文件追加到文件队列
                var doc = this.item;
                var files = obj.pushFile();
                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function (index, file, result) {
                    doc.parent('div').find('.show_head_logo').html('<img style="width: 38px; height: 38px;" src="' + result + '" >');
                    // $('.show_head_logo').html('<img style="width: 100px; height: 100px;" src="' + result + '" >');
                });
            }
            , done: function (res, index, upload) {
                var doc = this.item;
                console.log(res);
                //假设code=0代表上传成功
                if (res.code == 0) {
                    doc.parent('div').find('.projectHead').val(res.fileName);
                }
                if (res.code == -1) {
                    window.top.layer.msg("上传失败", {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                    // uploadInst.upload();  重新上传
                }
            }
        });

        //监听提交
        form.on('submit(add)', function (data) {
            console.log(data.field);
            $.ajax({
                url: '/me/edit',
                type: 'post',
                data: data.field,
                async: false, traditional: true,
                success: function (data) {
                    window.top.layer.msg(data, {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                    window.location.href = "/me/jumpView";
                }, error: function () {
                    layer.alert("请求失败", {icon: 6}, function () {
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                    });
                }
            });
            return false;
        });
    });
</script>
</body>
</html>