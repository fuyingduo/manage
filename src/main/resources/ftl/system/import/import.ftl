<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>新增币种</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport"
          content="width=device-width,user-scalable=yes, minimum-scale=0.4, initial-scale=0.8,target-densitydpi=low-dpi"/>
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css">
    <script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js"></script>
</head>

<body>
<div class="x-body">
    <form class="layui-form layui-form-pane" style="margin-left: 20px;">
        <div style="width:100%;height:400px;overflow: auto;">
            <div class="layui-form-item">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 10px;">
                    <legend style="font-size:16px;">基础信息</legend>
                </fieldset>
            </div>
            <button type="button" class="layui-btn" id="uploadExcel"><i class="layui-icon"></i>上传文件</button>
        </div>
        <div style="width: 100%;height: 55px;background-color: white;border-top:1px solid #e6e6e6;
  position: fixed;bottom: 1px;margin-left:-20px;">
            <div class="layui-form-item" style=" float: right;margin-right: 30px;margin-top: 8px">
                <button class="layui-btn" lay-filter="add" lay-submit="add">
                    增加
                </button>
                <a class="layui-btn layui-btn-primary" id="close">
                    取消
                </a>
            </div>
        </div>
    </form>
</div>
<script>
    layui.use(['form', 'layer', 'laydate', 'element', 'upload'], function () {
        $ = layui.jquery;
        var form = layui.form
                , layer = layui.layer;
        var element = layui.element;
        var upload = layui.upload;

        upload.render({
            elem: '#uploadExcel',
            url: '/im/import',
            accept: 'file',
            multiple: true,
            done: function (res) {
                console.log(res);
            }
        });

        $('#close').click(function () {
            window.history.back(-1);
        });
        //监听提交
        form.on('submit(add)', function (data) {
            $.ajax({
                url: 'addCoinInfo',
                type: 'post',
                data: data.field,
                async: false, traditional: true,
                success: function (data) {
                    window.top.layer.msg(data, {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                    window.location.href = "info";
                }, error: function () {
                    var index = parent.layer.getFrameIndex(window.name);
                    window.top.layer.msg('请求失败', {icon: 5, offset: 'rb', area: ['120px', '80px'], anim: 2});
                    parent.layer.close(index);
                }
            });
            return false;
        });
        form.render();
    });

</script>
</body>

</html>
