<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>项目编辑</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui/css/layui.css">
    <link rel="stylesheet" href="${re.contextPath}/plugin/layui_1/css/layui.css">
    <link rel="stylesheet" href="${re.contextPath}/plugin/ztree/css/metroStyle/metroStyle.css">
    <style>
        /* 下拉多选样式 需要引用*/
        select[multiple] + .layui-form-select > .layui-select-title > input.layui-input {
            border-bottom: 0
        }

        select[multiple] + .layui-form-select dd {
            padding: 0;
        }

        select[multiple] + .layui-form-select .layui-form-checkbox[lay-skin=primary] {
            margin: 0 !important;
            display: block;
            line-height: 36px !important;
            position: relative;
            padding-left: 26px;
        }

        select[multiple] + .layui-form-select .layui-form-checkbox[lay-skin=primary] span {
            line-height: 36px !important;
            float: none;
        }

        select[multiple] + .layui-form-select .layui-form-checkbox[lay-skin=primary] i {
            position: absolute;
            left: 10px;
            top: 0;
            margin-top: 9px;
        }

        .multiSelect {
            line-height: normal;
            height: auto;
            padding: 4px 10px;
            overflow: hidden;
            min-height: 38px;
            margin-top: -38px;
            left: 0;
            z-index: 99;
            position: relative;
            background: none;
        }

        .multiSelect a {
            padding: 2px 5px;
            background: #908e8e;
            border-radius: 2px;
            color: #fff;
            display: block;
            line-height: 20px;
            height: 20px;
            margin: 2px 5px 2px 0;
            float: left;
        }

        .multiSelect a span {
            float: left;
        }

        .multiSelect a i {
            float: left;
            display: block;
            margin: 2px 0 0 2px;
            border-radius: 2px;
            width: 8px;
            height: 8px;
            padding: 4px;
            position: relative;
            -webkit-transition: all .3s;
            transition: all .3s
        }

        .multiSelect a i:before, .multiSelect a i:after {
            position: absolute;
            left: 8px;
            top: 2px;
            content: '';
            height: 12px;
            width: 1px;
            background-color: #fff
        }

        .multiSelect a i:before {
            -webkit-transform: rotate(45deg);
            transform: rotate(45deg)
        }

        .multiSelect a i:after {
            -webkit-transform: rotate(-45deg);
            transform: rotate(-45deg)
        }

        .multiSelect a i:hover {
            background-color: #545556;
        }

        .multiOption {
            display: inline-block;
            padding: 0 5px;
            cursor: pointer;
            color: #999;
        }

        .multiOption:hover {
            color: #5FB878
        }

        @font-face {
            font-family: "iconfont";
            src: url('data:application/x-font-woff;charset=utf-8;base64,d09GRgABAAAAAAaoAAsAAAAACfwAAQAAAAAAAAAAAAAAAAAAAAAAAAAAAABHU1VCAAABCAAAADMAAABCsP6z7U9TLzIAAAE8AAAARAAAAFZW7kokY21hcAAAAYAAAABwAAABsgdU06BnbHlmAAAB8AAAAqEAAAOUTgbbS2hlYWQAAASUAAAALwAAADYR+R9jaGhlYQAABMQAAAAcAAAAJAfeA4ZobXR4AAAE4AAAABMAAAAUE+kAAGxvY2EAAAT0AAAADAAAAAwB/gLGbWF4cAAABQAAAAAfAAAAIAEVAGhuYW1lAAAFIAAAAUUAAAJtPlT+fXBvc3QAAAZoAAAAPQAAAFBD0CCqeJxjYGRgYOBikGPQYWB0cfMJYeBgYGGAAJAMY05meiJQDMoDyrGAaQ4gZoOIAgCKIwNPAHicY2Bk/s04gYGVgYOpk+kMAwNDP4RmfM1gxMjBwMDEwMrMgBUEpLmmMDgwVLwwZ27438AQw9zA0AAUZgTJAQAokgyoeJzFkTEOgCAQBOdAjTH+wtbezvggKyteTPyFLpyFvsC9DNnbHIEA0AJRzKIBOzCKdqVW88hQ84ZN/UBPUKU85fVcrkvZ27tMc17FR+0NMh2/yf47+quxrtvT6cVJD7pinpzyI3l1ysy5OIQbzBsVxHicZVM9aBRBFJ43c7szyeV2s/97m9zP3ppb5ZID72+9iJfDnyIiGImCMZWFXaKdaSyuESJYCFZpRZBUCpaJcCCKaexsRVHQytrC2/Pt5ZSIy+z3vvnemwfvY4ZIhAw/s33mEoMcJyfJebJCCMgVKCk0B37YqNIKWL5kOabCwiD0eVCqsjPglGTTrrUaZUfmsgoK5KHu11phlYbQbHToaajZOYDsjLeqz83q7BFMumH+fnyRPgGrEMyqnYV4eX7JrBUNsTWl61ldfyhkSRKUplQFNh17QpqYlOOnkupZ+4UTtABT2dC7tJYpzug3txu3c3POBECvB8ZMUXm2pHkarnuebehZPp0RrpcJjpmw9TXtGlO58heCXwpnfcVes7PExknPkVWctFxSIUxANgs4Q9RaglYjjIKwCqGvANfy4NQtBL8DkYaipAVVaGqNVuTnoQBYg8NzHzNaJ7HAdpjFXfF2DSEjxF2ui7T8ifP2CsBiZTCsLCbxCv4UDvlgp+kFgQcHXgAQP64s0gdQdOOKWwSM8CGJz4V4c11gQwc70hTlH4XLv12dbwO052OotGHMYYj8VrwDJQ/eeSXA2Ib24Me42XvX993ECxm96LM+6xKdBCRCNy6TdfSDoxmJFXYBaokV5RL7K/0nOHZ9rBl+chcCP7kVMML6SGHozx8Od3ZvCEvlm5KQ0nxPTJtiLHD7ny1jsnxYsAF7imkq8QVEOBgF5Yh0yNkpPIenN2QAsSdMNX6xu85VC/tiE3Mat6P8JqWM73NLhZ9mzjBy5uAlAlJYBiMRDPQleQ+9FEFfJJImGnHQHWIEmm/5UB8h8uaIIzrc4SEPozByel3oDvFcN+4D+dU/uou/L2xv/1mUQBdTCIN+jGUEgV47UkB+Aw7YpAMAAAB4nGNgZGBgAGLbQwYd8fw2Xxm4WRhA4HrO20sI+n8DCwOzE5DLwcAEEgUAPX4LPgB4nGNgZGBgbvjfwBDDwgACQJKRARWwAgBHCwJueJxjYWBgYH7JwMDCgMAADpsA/QAAAAAAAHYA/AGIAcp4nGNgZGBgYGWIYWBjAAEmIOYCQgaG/2A+AwASVwF+AHicZY9NTsMwEIVf+gekEqqoYIfkBWIBKP0Rq25YVGr3XXTfpk6bKokjx63UA3AejsAJOALcgDvwSCebNpbH37x5Y08A3OAHHo7fLfeRPVwyO3INF7gXrlN/EG6QX4SbaONVuEX9TdjHM6bCbXRheYPXuGL2hHdhDx18CNdwjU/hOvUv4Qb5W7iJO/wKt9Dx6sI+5l5XuI1HL/bHVi+cXqnlQcWhySKTOb+CmV7vkoWt0uqca1vEJlODoF9JU51pW91T7NdD5yIVWZOqCas6SYzKrdnq0AUb5/JRrxeJHoQm5Vhj/rbGAo5xBYUlDowxQhhkiMro6DtVZvSvsUPCXntWPc3ndFsU1P9zhQEC9M9cU7qy0nk6T4E9XxtSdXQrbsuelDSRXs1JErJCXta2VELqATZlV44RelzRiT8oZ0j/AAlabsgAAAB4nGNgYoAALgbsgJWRiZGZkYWRlZGNgbGCuzw1MykzMb8kU1eXs7A0Ma8CiA05CjPz0rPz89IZGADc3QvXAAAA') format('woff')
        }

        .iconfont {
            font-family: "iconfont" !important;
            font-size: 16px;
            font-style: normal;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
        }

        .icon-fanxuan:before {
            content: "\e837";
        }

        .icon-quanxuan:before {
            content: "\e623";
        }

        .icon-qingkong:before {
            content: "\e63e";
        }

        /* 下面是页面内样式，无需引用 */
        .layui-block {
            margin-bottom: 10px;
        }

        .layui-form-label {
            width: 180px;
        }

        .code {
            color: gray;
            margin-left: 10px;
        }

        .unshow > #result {
            display: none;
        }

        pre {
            padding: 5px;
            margin: 5px;
        }

        .string {
            color: green;
        }

        .number {
            color: darkorange;
        }

        .boolean {
            color: blue;
        }

        .null {
            color: magenta;
        }

        .key {
            color: red;
        }
    </style>
    <script type="text/javascript" src="${re.contextPath}/plugin/jquery/jquery-3.2.1.min.js"></script>
<#--<script type="text/javascript" src="${re.contextPath}/plugin/layui/layui.all.js" charset="utf-8"></script>-->
    <script type="text/javascript" src="${re.contextPath}/plugin/ztree/js/jquery.ztree.core.js"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/layui_1/layui.js" charset="utf-8"></script>
    <script type="text/javascript" src="${re.contextPath}/plugin/ztree/js/jquery.ztree.excheck.js"
            charset="utf-8"></script>

</head>
<body onload="getData()">
<form class="layui-form layui-form-pane" style="margin-left: 40px;margin-right: 100px;">
    <input type="hidden" id="projectLogo" name="proLogo" value="${btcProject.proLogo}"/>
    <input type="hidden" name="id" value="${btcProject.id}"/>
    <input type="hidden" id="tep_ids" value="${tepIds}"/>
    <input type="hidden" id="ca_ids" value="${caIds}"/>
    <input type="hidden" id="platform_ids" value="${platformIds}"/>
    <div class="layui-row layui-col-space10">
        <div class="layui-col-md4">
            <div class="layui-form-item">
                <label class="layui-form-label">项目名称：</label>
                <div class="layui-input-block">
                    <input type="text" name="proName" value="${btcProject.proName}" placeholder="请输入项目名称"
                           class="layui-input"
                           lay-verify="proName"
                           autocomplete="off">
                </div>
            </div>
        </div>
        <div class="layui-col-md8">
            <div class="layui-form-item">
                <label class="layui-form-label">项目logo</label>
                <div class="layui-input-block">
                    <button type="button" class="layui-btn layui-btn-primary" id="upLoadLogo">
                        <i class="layui-icon">&#xe660;</i>
                    </button>
                    <span style="color: #aaaaaa; font-size: x-small" id="show_project_logo">
                        <#if btcProject.proLogo != null>
                            <img src="${readImage}${btcProject.proLogo}" style="width: 100px; height: 38px;">
                        </#if>
                        <#if btcProject.proLogo == null>
                            请上传图片
                        </#if>
                    </span>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-row layui-col-space10">
        <div class="layui-col-md6">
            <div class="layui-form-item">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 10px;">
                    <legend style="font-size:16px;">项目资料</legend>
                </fieldset>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">Token总量：</label>
                <div class="layui-input-block">
                    <input type="text" name="proTokenNum" value="${btcProject.proTokenNum}" placeholder="请输入数字，例如15000"
                           class="layui-input"
                           lay-verify="proTokenNum"
                           autocomplete="off">
                </div>
            </div>
            <div class="layui-block">
                <label class="layui-form-label">概念板块：</label>
                <div class="layui-input-inline">
                    <select name="proLabel" multiple lay-search id="select_get_data">
                        <option value="">请选择</option>
                    <#--<option value="0">基础链</option>-->
                    <#--<option value="1">匿名链</option>-->
                    <#--<option value="2">数据存储链</option>-->
                    <#--<option value="3">金融服务</option>-->
                    <#--<option value="4">物联网</option>-->
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">项目阶段</label>
                <div class="layui-input-block">
                    <select name="proStage" lay-filter="proStage">
                        <option value="">请选择</option>
                        <option <#if btcProject.proStage=='0'>selected</#if> value="0">宣传期</option>
                        <option <#if btcProject.proStage=='1'>selected</#if> value="1">募集期</option>
                        <option <#if btcProject.proStage=='2'>selected</#if> value="2">待上线</option>
                        <option <#if btcProject.proStage=='3'>selected</#if> value="3">已上线</option>
                        <option <#if btcProject.proStage=='4'>selected</#if> value="4">已退市</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">共识机制：</label>
                <div class="layui-input-block">
                    <input type="text" name="proConsesusMechanism" value="${btcProject.proConsesusMechanism}"
                           placeholder="共识机制" class="layui-input"
                           lay-verify="proConsesusMechanism"
                           autocomplete="off">
                </div>
            </div>
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">代币分配</label>
                <div class="layui-input-block">
                    <textarea placeholder="200字以内" name="proTokenAllot" class="layui-textarea"
                              lay-verify="proTokenAllot">${btcProject.proTokenAllot}</textarea>
                </div>
            </div>
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">项目介绍</label>
                <div class="layui-input-block">
                    <textarea placeholder="200字以内" name="proIntroduce" class="layui-textarea"
                              lay-verify="proIntroduce">${btcProject.proIntroduce}</textarea>
                </div>
            </div>
            <div class="layui-form-item">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 10px;">
                    <legend style="font-size:16px;">更多详情</legend>
                </fieldset>
                <legend style="font-size:16px;">核心成员</legend>
            </div>
            <div>
                <span class="layui-btn layui-btn-xs" id="add_core_member">新增</span>
                <div class="num" num="0">
                    <div class="layui-form-item">
                        <label class="layui-form-label">姓名：</label>
                        <div class="layui-input-block">
                            <input type="text" name="btcProjectInfos[0].inUserName"
                                   value="${btcProjectInfos[0].inUserName}"
                                   placeholder="请输入姓名"
                                   class="layui-input"
                                   autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">职位：</label>
                        <div class="layui-input-block">
                            <input type="text" name="btcProjectInfos[0].inPosition"
                                   value="${btcProjectInfos[0].inPosition}"
                                   placeholder="请输入职位"
                                   class="layui-input"
                                   autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-col-md8">
                        <div class="layui-form-item">
                            <label class="layui-form-label">头像：</label>
                            <div class="layui-input-block">
                                <button type="button"
                                        class="layui-btn layui-btn-primary UploadheadImage" id="">
                                    <i class="layui-icon">&#xe660;</i>
                                </button>
                                <span style="color: #aaaaaa; font-size: x-small"
                                      class="show_head_logo"
                                      id="">
                                    <#if btcProjectInfos[0].inHeadImage == null>请上传头像</#if>
                                    <#if btcProjectInfos[0].inHeadImage != null>
                                        <img src="${readImage}${btcProjectInfos[0].inHeadImage}" style="width: 38px; height: 38px;">
                                    </#if>
                                </span>
                                <input type="hidden" class="projectHead" id=""
                                       name="btcProjectInfos[0].inHeadImage"
                                       value="${btcProjectInfos[0].inHeadImage}"/>
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item layui-form-text">
                        <label class="layui-form-label">简介</label>
                        <div class="layui-input-block">
                                        <textarea placeholder="200字以内" name="btcProjectInfos[0].inBrief"
                                                  class="layui-textarea">${btcProjectInfos[0].inBrief}</textarea>
                        </div>
                    </div>
                </div>
                <#if btcProjectInfos?? && (btcProjectInfos?size > 1)>
                    <#list btcProjectInfos as item>
                        <#if item_index gt 0>
                            <div class="num" num="${item_index}">
                                <span class="layui-btn layui-btn-xs layui-btn-danger del_journalism">删除</span>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">姓名：</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="btcProjectInfos[${item_index}].inUserName"
                                               value="${item.inUserName}"
                                               placeholder="请输入姓名"
                                               class="layui-input"
                                               autocomplete="off">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">职位：</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="btcProjectInfos[${item_index}].inPosition"
                                               value="${item.inPosition}"
                                               placeholder="请输入职位"
                                               class="layui-input"
                                               autocomplete="off">
                                    </div>
                                </div>
                                <div class="layui-col-md8">
                                    <div class="layui-form-item">
                                        <label class="layui-form-label">头像：</label>
                                        <div class="layui-input-block">
                                            <button type="button"
                                                    class="layui-btn layui-btn-primary UploadheadImage" id="">
                                                <i class="layui-icon">&#xe660;</i>
                                            </button>
                                            <span style="color: #aaaaaa; font-size: x-small"
                                                  class="show_head_logo"
                                                  id="">
                                                <#if item.inHeadImage == null>请上传头像</#if>
                                                <#if item.inHeadImage != null>
                                                    <img src="${readImage}${item.inHeadImage}" style="width: 38px; height: 38px;">
                                                </#if>
                                            </span>
                                            <input type="hidden" class="projectHead" id=""
                                                   name="btcProjectInfos[${item_index}].inHeadImage"
                                                   value="${item.inHeadImage}"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">简介</label>
                                    <div class="layui-input-block">
                                        <textarea placeholder="200字以内" name="btcProjectInfos[${item_index}].inBrief"
                                                  class="layui-textarea">${item.inBrief}</textarea>
                                    </div>
                                </div>
                            </div>
                        </#if>
                    </#list>
                </#if>
            </div>

            <div class="layui-block">
                <label class="layui-form-label">合作伙伴：</label>
                <div class="layui-input-inline">
                    <select name="caIds" multiple lay-search id="capital_select">
                        <option value="">请选择</option>
                    </select>
                </div>
            </div>

            <div class="layui-block">
                <label class="layui-form-label">已上线交易所：</label>
                <div class="layui-input-inline">
                    <select name="platforms" multiple lay-search id="exchange_select">
                        <option value="">请选择</option>
                    <#--<option value="">1</option>-->
                    <#--<option value="">2</option>-->
                    <#--<option value="">3</option>-->
                    <#--<option value="">4</option>-->
                    </select>
                </div>
            </div>
        </div>


        <div class="layui-col-md6">
            <div class="layui-form-item">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 10px;">
                    <legend style="font-size:16px;">项目路径</legend>
                </fieldset>
            </div>
        <#--         项目路径          -->
            <div>
                <span class="layui-btn layui-btn-xs" id="add_project_path">新增</span>
                <div class="projectpath" projectpath="0">
                    <div class="layui-form-item">
                        <label class="layui-form-label">时间：</label>
                        <div class="layui-input-block">
                            <div class="layui-inline">
                                <input type="text" readonly placeholder="时间" value="${projectPaths[0].paTime}"
                                       class="layui-input paTime"
                                       name="projectPaths[0].paTime"
                                       id="paTime" lay-verify="paTime"
                                       autocomplete="off">
                            </div>
                        </div>
                    </div>
                    <div class="layui-form-item layui-form-text">
                        <label class="layui-form-label">事件</label>
                        <div class="layui-input-block">
                            <textarea placeholder="200字以内" name="projectPaths[0].paIncident"
                                      class="layui-textarea">${projectPaths[0].paIncident}</textarea>
                        </div>
                    </div>
                </div>
                <#if projectPaths?? && (projectPaths?size > 1)>
                    <#list projectPaths as item>
                        <#if item_index gt 0>
                            <div class="projectpath" projectpath="${item_index}">
                                <span class="layui-btn layui-btn-xs layui-btn-danger del_journalism">删除</span>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">时间：</label>
                                    <div class="layui-input-block">
                                        <div class="layui-inline">
                                            <input type="text" readonly placeholder="时间" value="${item.paTime}"
                                                   class="layui-input paTime"
                                                   name="projectPaths[${item_index}].paTime"
                                                   id="paTime" lay-verify="paTime"
                                                   autocomplete="off">
                                        </div>
                                    </div>
                                </div>
                                <div class="layui-form-item layui-form-text">
                                    <label class="layui-form-label">事件</label>
                                    <div class="layui-input-block">
                                                        <textarea placeholder="200字以内"
                                                                  name="projectPaths[${item_index}].paIncident"
                                                                  class="layui-textarea">${item.paIncident}</textarea>
                                    </div>
                                </div>
                            </div>
                        </#if>
                    </#list>
                </#if>
            </div>
        <#--         项目路径          -->

            <div class="layui-form-item">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 10px;">
                    <legend style="font-size:16px;">招募资料</legend>
                </fieldset>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">募集token总量：</label>
                <div class="layui-input-block">
                    <input type="text" name="raiseTokenSum" value="${btcProject.raiseTokenSum}" placeholder="募集token总量"
                           class="layui-input"
                           lay-verify="raiseTokenSum"
                           autocomplete="off">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">招募时间</label>
                <div class="layui-input-block">
                    <div class="layui-inline">
                        <input type="text" readonly placeholder="start" value="${btcProject.reTime}" class="layui-input"
                               name="reTime"
                               id="reTime" lay-verify="reTime"
                               autocomplete="off">
                    </div>&nbsp;&nbsp;-&nbsp;&nbsp;
                    <div class="layui-inline">
                        <input type="text" readonly placeholder="end" value="${btcProject.reTimeEnd}"
                               class="layui-input" name="reTimeEnd"
                               id="reTimeEnd" lay-verify="reTimeEnd"
                               autocomplete="off">
                    </div>
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">兑换比例：</label>
                <div class="layui-input-block">
                    <input type="text" name="reRatio" value="${btcProject.reRatio}" placeholder="请输入兑换比例"
                           class="layui-input"
                           lay-verify="reRatio"
                           autocomplete="off">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">硬顶：</label>
                <div class="layui-input-block">
                    <input type="text" name="reHardTop" value="${btcProject.reHardTop}" placeholder="请输入硬顶"
                           class="layui-input"
                           lay-verify="reHardTop"
                           autocomplete="off">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">锁仓</label>
                <div class="layui-input-block">
                    <select name="reLockUp" lay-filter="reLockUp">
                        <option <#if btcProject.reLockUp=='0'>selected</#if> value="0">是</option>
                        <option <#if btcProject.reLockUp=='1'>selected</#if> value="1">否</option>
                    </select>
                </div>
            </div>
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">募集资金分配</label>
                <div class="layui-input-block">
                    <textarea placeholder="200字以内" name="reCapitalAllot"
                              class="layui-textarea">${btcProject.reCapitalAllot}</textarea>
                </div>
            </div>
            <div class="layui-form-item layui-form-text">
                <label class="layui-form-label">备注</label>
                <div class="layui-input-block">
                    <textarea placeholder="200字以内" name="reRemake"
                              class="layui-textarea">${btcProject.reRemake}</textarea>
                </div>
            </div>

            <div class="layui-form-item">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 10px;">
                    <legend style="font-size:16px;">项目动态</legend>
                </fieldset>
                <legend style="font-size:16px;">相关新闻</legend>
            </div>


            <div class="journalism" journalism="0">
                <span class="layui-btn layui-btn-xs" id="add_journalism">新增</span>
                <div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">新闻标题：</label>
                        <div class="layui-input-block">
                            <input type="text" name="projectDynamics[0].dyTitle" value="${projectDynamics[0].dyTitle}"
                                   placeholder="新闻标题"
                                   class="layui-input"
                                   lay-verify="dyTitle"
                                   autocomplete="off">
                        </div>
                    </div>
                    <div class="layui-form-item">
                        <label class="layui-form-label">新闻地址：</label>
                        <div class="layui-input-block">
                            <input type="text" name="projectDynamics[0].dyUrl" value="${projectDynamics[0].dyUrl}"
                                   placeholder="新闻地址"
                                   class="layui-input"
                                   lay-verify="dyUrl"
                                   autocomplete="off">
                        </div>
                    </div>
                </div>
                <#if projectDynamics?? && (projectDynamics?size > 1)>
                    <#list projectDynamics as item>
                        <#if item_index gt 0>
                            <div>
                                <span class="layui-btn layui-btn-xs layui-btn-danger del_journalism">删除</span>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">新闻标题：</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="projectDynamics[${item_index}].dyTitle"
                                               value="${item.dyTitle}" placeholder="新闻标题"
                                               class="layui-input"
                                               lay-verify="dyTitle"
                                               autocomplete="off">
                                    </div>
                                </div>
                                <div class="layui-form-item">
                                    <label class="layui-form-label">新闻地址：</label>
                                    <div class="layui-input-block">
                                        <input type="text" name="projectDynamics[${item_index}].dyUrl"
                                               value="${item.dyUrl}" placeholder="新闻地址"
                                               class="layui-input"
                                               lay-verify="dyUrl"
                                               autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </#if>
                    </#list>
                </#if>
            </div>


            <div class="layui-form-item">
                <fieldset class="layui-elem-field layui-field-title" style="margin-top: 10px;">
                    <legend style="font-size:16px;">联系我们</legend>
                </fieldset>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">官网：</label>
                <div class="layui-input-block">
                    <input type="text" name="coOfficial" value="${btcProject.coOfficial}" placeholder="官网"
                           class="layui-input"
                           lay-verify="coOfficial"
                           autocomplete="off">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">区块链浏览器：</label>
                <div class="layui-input-block">
                    <input type="text" name="coBrowser" value="${btcProject.coBrowser}" placeholder="区块链浏览器"
                           class="layui-input"
                           lay-verify="coBrowser"
                           autocomplete="off">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">白皮书：</label>
                <div class="layui-input-block">
                    <input type="text" name="coWhite" value="${btcProject.coWhite}" placeholder="白皮书"
                           class="layui-input"
                           lay-verify="coWhite"
                           autocomplete="off">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">钱包：</label>
                <div class="layui-input-block">
                    <input type="text" name="coWallet" value="${btcProject.coWallet}" placeholder="钱包地址"
                           class="layui-input"
                           lay-verify="coWallet"
                           autocomplete="off">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">微信：</label>
                <div class="layui-input-block">
                    <input type="text" name="coWechat" value="${btcProject.coWechat}" placeholder="微信号"
                           class="layui-input"
                           lay-verify="coWechat"
                           autocomplete="off">
                </div>
            </div>

            <div class="layui-form-item">
                <label class="layui-form-label">点击量：</label>
                <div class="layui-input-block">
                    <input type="text" name="prohits" value="${btcProject.prohits}" placeholder="点击量"
                           class="layui-input"
                           lay-verify="prohits"
                           autocomplete="off">
                </div>
            </div>
            <div class="layui-form-item">
                <label class="layui-form-label">点赞量：</label>
                <div class="layui-input-block">
                    <input type="text" name="proPraise" value="${btcProject.proPraise}" placeholder="点赞量"
                           class="layui-input"
                           lay-verify="proPraise"
                           autocomplete="off">
                </div>
            </div>
        </div>

        <div class="layui-form-item">
            <div class="layui-input-block">
                <button class="layui-btn" id="saveImage" lay-filter="add" lay-submit>保存</button>
                <a href="javascript:history.back(-1)" class="layui-btn layui-btn-primary">取消</a>
            </div>
        </div>
    </div>
</form>
<script type="text/javascript">
    function getData() {
        // 概念板块
        $.post("/tep/findData", function (result) {
            var jsonRS = JSON.parse(result);
            if (jsonRS.code == 200) {
                var tepIds = $('#tep_ids').val();
                var data = jsonRS.data;
                for (var i = 0; i < data.length; i++) {
                    var id = data[i].id;
                    var proName = data[i].name;
                    if (tepIds != null && tepIds != '') {
                        if (tepIds.indexOf("|") < 0) {
                            if (id == tepIds) {
                                $('#select_get_data').append('<option value="' + id + '" selected>' + proName + '</option>');
                                break;
                            }
                        } else {
                            var tepIdArr = tepIds.split("|");
                            for (var j = 0; j < tepIdArr.length; j++) {
                                if (tepIdArr[j] == id) {
                                    $('#select_get_data').append('<option value="' + id + '" selected>' + proName + '</option>');
                                    break;
                                } else if (j == tepIdArr.length - 1) {
                                    $('#select_get_data').append('<option value="' + id + '">' + proName + '</option>');
                                }
                            }

                        }
                    } else {
                        $('#select_get_data').append('<option value="' + id + '">' + proName + '</option>');
                    }

                }
                var form = layui.form;
                form.render();
            }
            console.log(result);
        });
        // 合作伙伴
        $.post("/ca/select", function (result) {
            var jsonRS = JSON.parse(result);
            if (jsonRS.code == 200) {
                var caIds = $('#ca_ids').val();
                var data = jsonRS.data;
                for (var i = 0; i < data.length; i++) {
                    var id = data[i].id;
                    var caName = data[i].caName;
                    if (caIds != null && caIds != '') {
                        if (caIds.indexOf("|") < 0) {
                            if (caIds == id) {
                                $('#capital_select').append('<option value="' + id + '" selected>' + caName + '</option>');
                                break;
                            }
                        } else {
                            var caIdArr = caIds.split("|");
                            for (var j = 0; j < caIdArr.length; j++) {
                                if (caIdArr[i] == id) {
                                    $('#capital_select').append('<option value="' + id + '" selected>' + caName + '</option>');
                                    break;
                                } else if (j == caIdArr.length - 1) {
                                    $('#capital_select').append('<option value="' + id + '">' + caName + '</option>');
                                }
                            }
                        }
                    } else {
                        $('#capital_select').append('<option value="' + id + '">' + caName + '</option>');
                    }
                }
                var form = layui.form;
                form.render();
            }
            console.log(result);
        });
        // 已上线交易所
        $.post("/plat/select", function (result) {
            var jsonRS = JSON.parse(result);
            if (jsonRS.code == 200) {
                var data = jsonRS.data;
                var platformIds = $('#platform_ids').val();
                console.log(data);
                for (var i = 0; i < data.length; i++) {
                    var name = data[i].name;
                    if (platformIds != null && platformIds != '') {
                        if (platformIds.indexOf("|") < 0) {
                            if (platformIds == name) {
                                $('#exchange_select').append('<option value="' + name + '" selected>' + name + '</option>');
                                break;
                            }
                        } else {
                            var platformArr = platformIds.split("|");
                            for (var j = 0; j < platformArr.length; j++) {
                                if (platformArr[j] == name) {
                                    $('#exchange_select').append('<option value="' + name + '" selected>' + name + '</option>');
                                    break;
                                } else if (j == platformArr.length - 1) {
                                    $('#exchange_select').append('<option value="' + name + '">' + name + '</option>');
                                }
                            }
                        }
                    } else {
                        $('#exchange_select').append('<option value="' + name + '">' + name + '</option>');
                    }
                }
                var form = layui.form;
                form.render();
            }
        });
    }
</script>
<script>
    layui.use(['layer', 'form', 'upload', 'laydate'], function () {
        var upload = layui.upload;
        $ = layui.jquery;
        var form = layui.form
                , layer = layui.layer;
        var laydate = layui.laydate;

        // 下拉框 多选 复选框渲染(如果不加不显示)
        form.render();
        //自定义验证规则
        form.verify({
            proName: function (value) {
                if (value.trim() == "") {
                    return "项目名称不能为空";
                }
            }

        });
        $('#add_project_path').click(function () {
            var max_div = $('.projectpath')[$('.projectpath').length - 1];
            var max_str = max_div.attributes[1].nodeValue;
            var max_num = Number(max_str) + 1;
            console.log(max_num);
            $(this).parent('div').append('' +
                    '        <div class="projectpath" projectpath="' + max_num + '">' +
                    '         <span class="layui-btn layui-btn-xs layui-btn-danger del_journalism">删除</span>' +
                    '         <div class="layui-form-item">' +
                    '         <label class="layui-form-label">时间：</label>' +
                    ' <div class="layui-input-block">' +
                    '          <div class="layui-inline">' +
                    '           <input type="text" readonly placeholder="时间" value="" class="layui-input paTime" name="projectPaths[' + max_num + '].paTime"' +
                    '  id="paTime' + max_num + '" lay-verify="paTime"' +
                    '   autocomplete="off">' +
                    '          </div>' +
                    '          </div>' +
                    '          </div>' +
                    '          <div class="layui-form-item layui-form-text">' +
                    '         <label class="layui-form-label">事件</label>' +
                    '         <div class="layui-input-block">' +
                    '         <textarea placeholder="200字以内" name="projectPaths[' + max_num + '].paIncident" class="layui-textarea"></textarea>' +
                    '        </div>' +
                    '         </div>' +
                    '         </div>');
            $('.del_journalism').click(function () {
                $(this).parent('div').remove();
            });


            var laydate = layui.laydate;
            laydate.render({
                elem: '#paTime' + max_num,
                type: 'datetime'
            });
        });

        $('#add_journalism').click(function () {
            var max_div = $('.journalism')[$('.journalism').length - 1];
            var max_str = max_div.attributes[1].nodeValue;
            var max_num = Number(max_str) + 1;
            console.log(max_num);
            $(this).parent('div').append('' +
                    '<div class="num" num="' + max_num + '"> ' +
                    '        <span class="layui-btn layui-btn-xs layui-btn-danger del_journalism">删除</span>' +
                    '<div class="layui-form-item">' +
                    ' <label class="layui-form-label">新闻标题：</label>' +
                    '  <div class="layui-input-block">' +
                    '          <input type="text" name="projectDynamics[' + max_num + '].dyTitle" value="" placeholder="新闻标题" class="layui-input"' +
                    '  lay-verify="dyTitle"' +
                    '  autocomplete="off">' +
                    '          </div>' +
                    '          </div>' +
                    '          <div class="layui-form-item">' +
                    '           <label class="layui-form-label">新闻地址：</label>' +
                    '   <div class="layui-input-block">' +
                    '           <input type="text" name="projectDynamics[' + max_num + '].dyUrl" value="" placeholder="新闻地址" class="layui-input"' +
                    '   lay-verify="dyUrl"' +
                    '   autocomplete="off">' +
                    '           </div>' +
                    '          </div>' +
                    '        </div>');
            $('.del_journalism').click(function () {
                $(this).parent('div').remove();
            });
        });

        $('#add_core_member').click(function () {
            var maxdiv = $('.num')[$('.num').length - 1];
            var max_str = maxdiv.attributes[1].nodeValue;
            var max_num = Number(max_str) + 1;
            console.log(max_num);
            $(this).parent('div').after('' +
                    '     <div class="num" num="' + max_num + '"> ' +
                    '           <span class="layui-btn layui-btn-xs layui-btn-danger del_project">删除</span>' +
                    '         <div class="layui-form-item">' +
                    '           <label class="layui-form-label">姓名：</label>' +
                    '           <div class="layui-input-block">' +
                    '               <input type="text" name="btcProjectInfos[' + max_num + '].inUserName" value="" placeholder="请输入项目名称" class="layui-input"' +
                    '               autocomplete="off">' +
                    '           </div>' +
                    '         </div>' +
                    '         <div class="layui-form-item">' +
                    '           <label class="layui-form-label">职位：</label>' +
                    '           <div class="layui-input-block">' +
                    '              <input type="text" name="btcProjectInfos[' + max_num + '].inPosition" value="" placeholder="请输入项目名称" class="layui-input"' +
                    '               autocomplete="off">' +
                    '           </div>' +
                    '         </div>' +
                    '         <div class="layui-col-md8">' +
                    '           <div class="layui-form-item">' +
                    '                <label class="layui-form-label">头像：</label>' +
                    '                <div class="layui-input-block">' +
                    '                   <button type="button" class="layui-btn layui-btn-primary UploadheadImage" id="">' +
                    '                       <i class="layui-icon">&#xe660;</i>' +
                    '                    </button>' +
                    '                    <span style="color: #aaaaaa; font-size: x-small" class="show_head_logo"' +
                    '                   id="">请上传头像</span>' +
                    '                   <input type="hidden" class="projectHead" id="" name="btcProjectInfos[' + max_num + '].inHeadImage" value=""/>' +
                    '                </div>' +
                    '           </div>' +
                    '           </div>' +
                    '           <div class="layui-form-item layui-form-text">' +
                    '               <label class="layui-form-label">简介</label>' +
                    '               <div class="layui-input-block">' +
                    '                   <textarea placeholder="200字以内" name="btcProjectInfos[' + max_num + '].inBrief"' +
                    '                   class="layui-textarea"></textarea>' +
                    '               </div>' +
                    '           </div>' +
                    '        </div>');

            $('.del_project').click(function () {
                $(this).parent('div').remove();
            });
            layui.each($(".UploadheadImage"), function (index, elem) {
                layui.use('upload', function () {
                    var upload = layui.upload;
                    upload.render({
                        elem: elem
                        , url: '/upload/image/'
                        , auto: true //选择文件后不自动上传
                        , bindAction: '' //指向一个按钮触发上传
                        , choose: function (obj) {
                            //将每次选择的文件追加到文件队列
                            var doc = this.item;
                            var files = obj.pushFile();
                            //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                            obj.preview(function (index, file, result) {
                                doc.parent('div').find('.show_head_logo').html('<img style="width: 38px; height: 38px;" src="' + result + '" >');
                                // $('.show_head_logo').html('<img style="width: 100px; height: 100px;" src="' + result + '" >');
                            });
                        }
                        , done: function (res, index, upload) {
                            var doc = this.item;
                            console.log(res);
                            //假设code=0代表上传成功
                            if (res.code == 0) {
                                doc.parent('div').find('.projectHead').val(res.fileName);
                            }
                            if (res.code == -1) {
                                window.top.layer.msg("上传失败", {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                                // uploadInst.upload();  重新上传
                            }
                        }

                    });
                });
            });

        });

        // 日期
        // laydate.render({
        //     elem: '#paTime',
        //     type: 'datetime'
        // });
        $('.paTime').each(function () {
            laydate.render({
                elem: this
                , position: 'fixed'
                , format: 'yyyy-MM-dd HH:mm:ss'
            });
        })
        laydate.render({
            elem: '#reTime',
            type: 'datetime'
        });
        laydate.render({
            elem: '#reTimeEnd',
            type: 'datetime'
        });

        // LOGO
        upload.render({
            elem: '#upLoadLogo'
            , url: '/upload/image/'
            , auto: true //选择文件后不自动上传
            , bindAction: '' //指向一个按钮触发上传
            , choose: function (obj) {
                //将每次选择的文件追加到文件队列
                var files = obj.pushFile();
                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function (index, file, result) {
                    $('#show_project_logo').html('<img style="width: 100px; height: 38px;" src="' + result + '" >');
                });
            }
            , done: function (res, index, upload) {
                console.log(res);
                //假设code=0代表上传成功
                if (res.code == 0) {
                    $('#projectLogo').val(res.fileName);
                }
                if (res.code == -1) {
                    window.top.layer.msg("上传失败", {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                    // uploadInst.upload();  重新上传
                }
            }
        });

        var head = upload.render({
            elem: '.UploadheadImage'
            , url: '/upload/image/'
            , auto: true //选择文件后不自动上传
            , bindAction: '' //指向一个按钮触发上传
            , choose: function (obj) {
                //将每次选择的文件追加到文件队列
                var doc = this.item;
                var files = obj.pushFile();
                //预读本地文件，如果是多文件，则会遍历。(不支持ie8/9)
                obj.preview(function (index, file, result) {
                    doc.parent('div').find('.show_head_logo').html('<img style="width: 38px; height: 38px;" src="' + result + '" >');
                    // $('.show_head_logo').html('<img style="width: 100px; height: 100px;" src="' + result + '" >');
                });
            }
            , done: function (res, index, upload) {
                var doc = this.item;
                console.log(res);
                //假设code=0代表上传成功
                if (res.code == 0) {
                    doc.parent('div').find('.projectHead').val(res.fileName);
                }
                if (res.code == -1) {
                    window.top.layer.msg("上传失败", {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                    // uploadInst.upload();  重新上传
                }
            }
        });

        //监听提交
        form.on('submit(add)', function (data) {
            console.log(data.field);
            $.ajax({
                url: '/btc/edit',
                type: 'post',
                data: data.field,
                async: false, traditional: true,
                success: function (msg) {
                    window.top.layer.msg(msg, {icon: 6, offset: 'rb', area: ['120px', '80px'], anim: 2});
                    window.location.href = "/btc/jumpView"
                }, error: function () {
                    layer.alert("请求失败", {icon: 6}, function () {
                        var index = parent.layer.getFrameIndex(window.name);
                        parent.layer.close(index);
                    });
                }
            });
            return false;
        });
    });
</script>
</body>
</html>